/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cdr;

import java.util.concurrent.ConcurrentLinkedQueue;

public class Repository {

    public static Repository repository;

    public static Repository getInstance() {
        if (repository == null) {
            repository = new Repository();
        }
        return repository;
    }
    ConcurrentLinkedQueue<String> cdrQueue = new ConcurrentLinkedQueue<String>();

    public ConcurrentLinkedQueue<String> getCdrQueue() {
        return cdrQueue;
    }

    public void setCdrQueue(ConcurrentLinkedQueue<String> cdrQueue) {
        this.cdrQueue = cdrQueue;
    }
}
