/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.activecalls;

import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author reefat
 */
public class LiveUsers implements Serializable, Comparable<LiveUsers> {

    String call_id;
    String origin_caller;
    String origin_ip;
    String term_callee;
    String term_ip;
    String invite_time;
    String accepted_time;
    
    private static Integer field;
    private static Integer sort_order;

    public String getCall_id() {
        return call_id;
    }

    public void setCall_id(String call_id) {
        this.call_id = call_id;
    }

    public String getOrigin_caller() {
        return origin_caller;
    }

    public void setOrigin_caller(String origin_caller) {
        this.origin_caller = origin_caller;
    }

    public String getOrigin_ip() {
        return origin_ip;
    }

    public void setOrigin_ip(String origin_ip) {
        this.origin_ip = origin_ip;
    }

    public String getTerm_callee() {
        return term_callee;
    }

    public void setTerm_callee(String term_callee) {
        this.term_callee = term_callee;
    }

    public String getTerm_ip() {
        return term_ip;
    }

    public void setTerm_ip(String term_ip) {
        this.term_ip = term_ip;
    }

    public String getInvite_time() {
        return invite_time;
    }

    public void setInvite_time(String invite_time) {
        this.invite_time = invite_time;
    }

    public String getAccepted_time() {
        return accepted_time;
    }

    public void setAccepted_time(String accepted_time) {
        this.accepted_time = accepted_time;
    }

    public static Integer getField() {
        return field;
    }

    public static void setField(Integer field) {
        LiveUsers.field = field;
    }

    public static Integer getSort_order() {
        return sort_order;
    }

    public static void setSort_order(Integer sort_order) {
        LiveUsers.sort_order = sort_order;
    }

    @Override
    public int compareTo(LiveUsers t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public static Comparator<LiveUsers> liveUsersPropertiesComparator = new Comparator<LiveUsers>() {

        @Override
        public int compare(LiveUsers liveUsers1, LiveUsers liveUsers2) {

            String var_liveUsers_sorting_field_value_1 = null;// = client1.getFirstName().toUpperCase();
            String var_liveUsers_sorting_field_value_2 = null;// = liveUsers2.getFirstName().toUpperCase();

            switch (field) {

                case 0: //Origin Caller
                    var_liveUsers_sorting_field_value_1 = liveUsers1.getOrigin_caller().toUpperCase();
                    var_liveUsers_sorting_field_value_2 = liveUsers2.getOrigin_caller().toUpperCase();
                    break;
                case 1: //Oigin IP
                    var_liveUsers_sorting_field_value_1 = String.valueOf(liveUsers1.getOrigin_ip());
                    var_liveUsers_sorting_field_value_2 = String.valueOf(liveUsers2.getOrigin_ip());
                    break;
                case 2: //Term Callee
                    var_liveUsers_sorting_field_value_1 = liveUsers1.getTerm_callee().toUpperCase();
                    var_liveUsers_sorting_field_value_2 = liveUsers2.getTerm_callee().toUpperCase();
                    break;
                case 3: //Term IP
                    var_liveUsers_sorting_field_value_1 = liveUsers1.getTerm_ip().toUpperCase();
                    var_liveUsers_sorting_field_value_2 = liveUsers2.getTerm_ip().toUpperCase();
                    break;
//                case 4: //Client Type
//                    var_liveUsers_sorting_field_value_1 = String.valueOf(liveUsers1.getClient_type());
//                    var_liveUsers_sorting_field_value_2 = String.valueOf(liveUsers2.getClient_type());
//                    break;
//                case 5: //Client Status
//                    var_liveUsers_sorting_field_value_1 = String.valueOf(liveUsers1.getClient_status());
//                    var_liveUsers_sorting_field_value_2 = String.valueOf(liveUsers2.getClient_status());
//                    break;
//                case 6: //Client Credit Limit
//                    var_liveUsers_sorting_field_value_1 = String.valueOf(liveUsers1.getClient_credit_limit());
//                    var_liveUsers_sorting_field_value_2 = String.valueOf(liveUsers2.getClient_credit_limit());
//                    break;
//                case 7: //Client Balance
//                    var_liveUsers_sorting_field_value_1 = String.valueOf(liveUsers1.getClient_balance());
//                    var_liveUsers_sorting_field_value_2 = String.valueOf(liveUsers2.getClient_balance());
//                    break;
//                case 8: //Client Call Limit
//                    var_liveUsers_sorting_field_value_1 = String.valueOf(liveUsers1.getClient_call_limit());
//                    var_liveUsers_sorting_field_value_2 = String.valueOf(liveUsers2.getClient_call_limit());
//                    break;
                default:
                    break;
            }

            if (var_liveUsers_sorting_field_value_1 == null || var_liveUsers_sorting_field_value_2 == null) {
                return -1;
            }

            switch (sort_order) {
                case 0:
                    //ascending order
                    return var_liveUsers_sorting_field_value_1.compareTo(var_liveUsers_sorting_field_value_2);
                case 1:
                    //descending order
                    return var_liveUsers_sorting_field_value_2.compareTo(var_liveUsers_sorting_field_value_1);
                default:
                    return -1;
            }
        }
    };
}
