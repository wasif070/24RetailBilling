/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.activecalls;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import local.server.ServerEngine;
import org.apache.log4j.Logger;

/**
 *
 * @author reefat
 */
public class ActiveCall {

    static Logger logger = Logger.getLogger(ActiveCall.class.getName());

    public void addActiveCallEntry(ActiveCallDTO param_liveUsers) {

        PreparedStatement ps = null;
        String sql = "";
        ResultSet rs = null;
        try {

            sql = "select * from active_calls where call_id = ?";
            ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
            ps.setString(1, param_liveUsers.getCall_id());
            rs = ps.executeQuery();

            String insert_or_update_str = "INSERT INTO active_calls SET call_id = ?, ";
            String condition = "";
            while (rs.next()) {
                insert_or_update_str = "UPDATE active_calls SET ";
                condition = "WHERE call_id = ?";
            }
            sql = insert_or_update_str
                    + "origin_caller = ?, "
                    + "origin_ip = ?, "
                    + "term_callee = ?, "
                    + "term_ip = ?, "
                    + "term_gw = ?, "
                    + "invite_time = ?, "
                    + "accepted_time = ?, "
                    + "ringing_time = ?, "
                    + "progressing_time = ?, "
                    + "duration = ?, "
                    + "status = ? "
                    + condition;
            int i = 1;
            ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
            if (condition.length() == 0) {
                ps.setString(i++, param_liveUsers.getCall_id());
            }
            ps.setString(i++, param_liveUsers.getOrigin_caller());
            ps.setString(i++, param_liveUsers.getOrigin_ip());
            ps.setString(i++, param_liveUsers.getTerm_callee());
            ps.setString(i++, param_liveUsers.getTerm_ip());
            ps.setString(i++, param_liveUsers.getTerm_gw());
            ps.setLong(i++, param_liveUsers.getInvite_time());
            ps.setLong(i++, param_liveUsers.getAccepted_time());
            ps.setLong(i++, param_liveUsers.getRinging_time());
            ps.setLong(i++, param_liveUsers.getProgressing_time());
            ps.setLong(i++, param_liveUsers.getDuration());
            ps.setString(i++, param_liveUsers.getStatus());
            if (condition.length() > 0) {
                ps.setString(i++, param_liveUsers.getCall_id());
            }
            ps.executeUpdate();
        } catch (Exception e) {
            logger.debug("Exception in active call entry-->" + e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                logger.debug("Exception in active call entry finally block-->" + ex.getMessage());
            }
        }

    }

    public void removeActiveCallEntry(String param_callId) {

        PreparedStatement ps = null;
        try {
            String sql = "delete from active_calls where call_id = ?";
            ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
            ps.setString(1, param_callId);

            ps.executeUpdate();
        } catch (Exception e) {
            logger.debug("Exception in remove active call entry-->" + e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                logger.debug("Exception in remove active call entry finally block-->" + ex.getMessage());
            }
        }
    }
}
