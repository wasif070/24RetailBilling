/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dialpeer;

import databaseconnector.DBConnection;
import org.zoolu.net.SocketAddress;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import local.server.ProxyingRule;
import local.server.ServerProfile;
import org.apache.log4j.Logger;
import local.server.PrefixProxyingRule;

/**
 *
 * @author Ashraful
 */
public class Dialpeer {

    static Logger logger = Logger.getLogger(Dialpeer.class.getName());

    public Dialpeer() {
    }

    public static synchronized void getDialPeers(ServerProfile serverProfile) {
        DBConnection con = null;
        Statement stmt = null;
        Statement stmt1 = null;
        ResultSet rs = null;
        ResultSet rs_dial = null;

        try {
            Vector aux = new Vector();
            con = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();
            stmt1 = con.connection.createStatement();
            String sql = "select dp_dnis_translate,dp_dnis_pattern,dp_gateway_list,dp_priority from dialpeers where dp_enable=1";
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String prefix = rs.getString("dp_dnis_pattern");
                String nexthop = rs.getString("dp_gateway_list");

                try {
                    String dnis[] = rs.getString("dp_dnis_translate").split("->");
                    String new_prefix = null;
                    String old_prefix=dnis[0];                    
                    if (dnis.length == 2) {                        
                        new_prefix = dnis[1];
                    } else {
                        new_prefix = "";
                    }

                    sql = "select gateway_ip,gateway_port from gateway where id in(-1" + nexthop + "-1) and gateway_delete=0 ";
                    rs_dial = stmt1.executeQuery(sql);
                    while (rs_dial.next()) {
                        nexthop = rs_dial.getString("gateway_ip") + ":" + rs_dial.getString("gateway_port");
                        aux.addElement(new PrefixProxyingRule(prefix, old_prefix,new_prefix, new SocketAddress(nexthop), rs.getInt("dp_priority")));
                    }
                } catch (Exception ex) {
                    logger.debug("Exception in dialpeers dnis-->"+ex);
                }

            }

            serverProfile.phone_proxying_rules = new ProxyingRule[aux.size()];
            for (int i = 0; i < aux.size(); i++) {
                serverProfile.phone_proxying_rules[i] = (ProxyingRule) aux.elementAt(i);
            }

        } catch (Exception ex) {
            logger.debug("Exception in dialpeers-->" + ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (rs_dial != null) {
                    rs_dial.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (stmt1 != null) {
                    stmt1.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(con);
                }
            } catch (Exception ex) {
            }
        }
    }
}
