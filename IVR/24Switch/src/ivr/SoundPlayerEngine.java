/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ivr;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Ashraful
 */
public class SoundPlayerEngine {

    private String DIR = "";
    private String CODEC = "";

    public SoundPlayerEngine(Integer codec) {

        if (codec == 18) {
            this.CODEC = "g729";
        } else {
            this.CODEC = "ulaw";
        }
        this.DIR = System.getProperty("user.dir") + File.separator + CODEC + File.separator + "digits" + File.separator;
    }

    public int number_length(long num) {
        int result = 0;
        if (num > 0) {
            return String.valueOf(num).length();
        }
        return result;
    }

    public long[] sotokia(int digit_length, long number) {
        long result[] = new long[5];
        result[0] = result[1] = result[2] = 0;
        long akok = 0;
        long sotok = 0;
        long doshok = 0;

        if (digit_length == 1) {
            akok = number;
        } else if (digit_length == 2) {
            akok = number % 10;
            doshok = number - akok;
        } else if (digit_length == 3) {
            akok = number % 10;
            sotok = (int) (number / 100);
            doshok = number - ((sotok * 100) + akok);

            long t_plus_u = doshok + akok;
            if (t_plus_u > 10 && t_plus_u < 20) {
                doshok += akok;
                akok = 0;
            }
        }

        result[0] = akok;
        result[1] = sotok;
        result[2] = doshok;

        return result;
    }

    public ArrayList<String> file_selector(long number) {
        ArrayList<String> file_list = new ArrayList<String>();

        if (number == 0) {
            file_list.add(this.DIR + "0." + this.CODEC);
            return file_list;
        }

        int length = number_length(number);

        if (length == 1) {
            return akok_processor(length, number);
        } else if (length == 2) {
            return doshok_processor(length, number);
        } else if (length == 3) {
            return sotok_processor(length, number);
        } else if (length > 3 && length < 7) {
            return sohosro_processor(length, number);
        } else if (length == 7) {
            file_list.add(this.DIR + (number / 1000000) + "." + this.CODEC);
            file_list.add(this.DIR + "million." + this.CODEC);

            number = number - (number / 1000000) * 1000000;
            if (number > 0) {
                file_list.addAll(sohosro_processor(6, number));
            }
        }

        return file_list;
    }

    public ArrayList<String> akok_processor(int length, long number) {
        ArrayList<String> file_list = new ArrayList<String>();
        file_list.add(this.DIR + number + "." + this.CODEC);
        return file_list;
    }

    public ArrayList<String> doshok_processor(int length, long number) {
        ArrayList<String> file_list = new ArrayList<String>();


        if (number < 21) {
            file_list.add(this.DIR + number + "." + this.CODEC);
            return file_list;
        } else if (number >= 21) {
            long[] files = sotokia(length, number);
            if (files[2] > 0) {
                file_list.add(this.DIR + files[2] + "." + this.CODEC);
            }
            if (files[0] > 0) {
                file_list.add(this.DIR + files[0] + "." + this.CODEC);
            }
            return file_list;
        }
        return file_list;
    }

    public ArrayList<String> sotok_processor(int length, long number) {
        ArrayList<String> file_list = new ArrayList<String>();

        long[] files = sotokia(length, number);

        if (files[1] > 0) {
            file_list.add(this.DIR + files[1] + "." + this.CODEC);
            file_list.add(this.DIR + "hundred." + this.CODEC);
        }
        if (files[2] > 0) {
            file_list.add(this.DIR + files[2] + "." + this.CODEC);
        }
        if (files[0] > 0) {
            file_list.add(this.DIR + files[0] + "." + this.CODEC);
        }
        return file_list;
    }

    public ArrayList<String> sohosro_processor(int length, long number) {
        ArrayList<String> file_list = new ArrayList<String>();

        if (length == 4) {
            file_list = akok_processor(1, number / 1000);
        } else if (length == 5) {
            file_list = doshok_processor(2, number / 1000);
        } else {
            file_list = sotok_processor(3, number / 1000);
        }

        if (file_list.size() > 0) {
            file_list.add(this.DIR + "thousand." + this.CODEC);
        }
        number = number - (number / 1000) * 1000;
        if (number > 0) {
            file_list.addAll(sotok_processor(3, number));
        }

        return file_list;
    }

    /* public static void main(String[] args) {
     //for (int i = 999999; i < 1000032; i++) {
     String[] sr = {"192.168.1.95", "3000", "" + 9999999};
     AudioSender.sendAudio(sr);
     //}
     }*/
}
