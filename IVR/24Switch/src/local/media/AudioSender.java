package local.media;

import ivr.SoundPlayerEngine;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 * AudioSender is a pure-java audio stream sender. It uses the javax.sound
 * library (package).
 */
public class AudioSender {

    private Logger logger = Logger.getLogger(AudioSender.class.getName());
    private Integer codec_port = 0;
    private String CODEC = "";
    private String DIR = "";

    public AudioSender(Integer codec) {
        if (codec == 18) {
            this.codec_port = 18;
            this.CODEC = "g729";
        } else {
            this.codec_port = 0;
            this.CODEC = "ulaw";
        }
        this.DIR = System.getProperty("user.dir") + File.separator + CODEC + File.separator;
    }

    public void sendAudio(String[] args) {

        String daddr = args[0];
        int dport = Integer.parseInt(args[1]);
        //int socket = Integer.parseInt(args[2]);
        int number = Integer.parseInt(args[2]);

        int payload_type = 18;
        int sample_rate = 8000;
        int sample_size = 1;
        int frame_size = 20;
        int frame_rate = 0;
        int hour = number / 60;
        int min = number - (hour * 60);
        SoundPlayerEngine engine = new SoundPlayerEngine(this.codec_port);
        ArrayList<String> filenames = new ArrayList<String>();

        filenames.add(this.DIR + "vm-youhave." + this.CODEC);
        filenames.addAll(engine.file_selector(hour));
        filenames.add(this.DIR + "hours."+ this.CODEC);
        filenames.addAll(engine.file_selector(min));        
        filenames.add(this.DIR + "minutes."+ this.CODEC);

        frame_rate = sample_rate / (frame_size / sample_size);

        try {
            for (String filename : filenames) {
                File file = new File(filename);
                //AudioFileFormat format = AudioSystem.getAudioFileFormat(file);
                //AudioFormat ULAW_FORMAT = new AudioFormat(AudioFormat.Encoding.ULAW, 8000, 8, 1, 1, 8000, false);

                //logger.debug("File audio format: " + format);
                InputStream ins = new FileInputStream(file);
                //AudioInputStream input_stream = AudioSystem.getAudioInputStream(ins);
                RtpStreamSender sender = new RtpStreamSender(ins, true, payload_type, frame_rate, frame_size, daddr, dport);
                sender.run();
                sender.halt();
                //Thread.sleep(1000);
                //ins.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}