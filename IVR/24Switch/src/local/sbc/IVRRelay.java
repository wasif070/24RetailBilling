/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This source code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package local.sbc;

import callcontroll.CallRepository;
import local.media.AudioSender;
import local.media.RtpStreamSender;
import org.apache.log4j.Logger;
import org.zoolu.net.*;
import org.zoolu.tools.Timer;
import org.zoolu.tools.TimerListener;
// logs
import org.zoolu.tools.Log;
import org.zoolu.tools.ExceptionPrinter;

/**
 * SymmetricUdpRelay implements a symmetric bidirectional UDP relay system.
 */
public class IVRRelay implements UdpProviderListener, TimerListener {

    /**
     * Log.
     */
    protected Log log = null;
    /**
     * SymmetricUdpRelay listener
     */
    protected IVRRelayListener listener;
    /**
     * Left side udp interface.
     */
    public static UdpProvider udp = null;
    /**
     * Left side udp interface.
     */
    protected UdpProvider left_udp = null;
    /**
     * Left port.
     */
    //protected int left_port;  
    /**
     * Right port.
     */
    //protected int right_port;
    /**
     * Left peer address.
     */
    protected SocketAddress left_soaddr;
    /**
     * Maximum time that the SymmetricUdpRelay remains active without receiving
     * UDP datagrams (in milliseconds)
     */
    protected long relay_time = 0;
    /**
     * Absolute time when the SymmetricUdpRelay should expire (if no packet is
     * received meanwhile).
     */
    protected long expire_time = 0;
    /**
     * Timer that fires whether the SymmetricUdpRelay must expire.
     */
    protected Timer timer = null;
    /**
     * Whether the SymmetricUdpRelay is running.
     */
    //protected boolean is_running=true;
    /**
     * Last change time of left soaddr (in milliseconds)
     */
    protected long last_left_change;
    /**
     * Last change time of right soaddr (in milliseconds)
     */
    protected long last_right_change;
    private static Logger logger = Logger.getLogger(RtpStreamSender.class.getName());
    private String client_id;
    private Integer codec = 0;

    /**
     * Costructs a new SymmetricUdpRelay.
     */
    protected IVRRelay() {
    }

    /**
     * Costructs a new SymmetricUdpRelay.
     */
    public IVRRelay(SocketAddress left_soaddr, int left_port, long relay_time, Log log, IVRRelayListener listener, String client_id, Integer codec) {
        init(left_soaddr, left_port, relay_time, log, listener, client_id, codec);
    }

    /**
     * Initializes the SymmetricUdpRelay.
     */
    private void init(SocketAddress left_soaddr, int left_port, long relay_time, Log log, IVRRelayListener listener, String client_id, Integer codec) {
        this.log = log;
        this.left_soaddr = left_soaddr;
        this.relay_time = relay_time;
        this.listener = listener;
        this.client_id = client_id;
        this.codec = codec;

        try {
            left_udp = new UdpProvider(new UdpSocket(left_port), 0, this);
            logger.debug("udp interfce: " + left_udp.toString() + " started");

        } catch (Exception e) {
            printException(e, Log.LEVEL_HIGH);
        }

        if (relay_time > 0) {
            long timer_time = relay_time / 2;
            expire_time = System.currentTimeMillis() + relay_time;
            timer = new Timer(timer_time, null, this);
            timer.start();
        }
        last_left_change = last_right_change = System.currentTimeMillis();
    }

    /**
     * Whether the UDP receivers are running
     */
    public boolean isRunning() {  //return is_running;
        if (left_udp != null && left_udp.isRunning()) {
            return true;
        }
        return false;
    }

    /**
     * Stops the SymmetricUdpRelay
     */
    public void halt() {
        if (left_udp != null) {
            left_udp.halt();
        }
    }

    /**
     * Gets the left peer SocketAddress.
     */
    public SocketAddress getLeftSoAddress() {
        return left_soaddr;
    }

    /**
     * Sets a new left peer SocketAddress.
     */
    public void setLeftSoAddress(SocketAddress left_soaddr) {
        printLog("left soaddr " + this.left_soaddr + " becomes " + left_soaddr, Log.LEVEL_HIGH);
        this.left_soaddr = left_soaddr;
        last_left_change = System.currentTimeMillis();
    }

    /**
     * Gets the time of the last change of left soaddr.
     */
    public long getLastLeftChangeTime() {
        return last_left_change;
    }

    /**
     * Gets the time of the last change of left soaddr.
     */
    public long getLastRightChangeTime() {
        return last_right_change;
    }

    /**
     * When receiving a new packet.
     */
    @Override
    public void onReceivedPacket(UdpProvider udp_service, UdpPacket packet) {
        //if (packet.getLength()<=2) return; // discard the packet        

        // postpone the expire time 
        if (relay_time > 0) {
            expire_time = System.currentTimeMillis() + relay_time;
        }

        // set addresses for outgoing packet, and check whether remote addresses are changed for incoming packet
        SocketAddress src_soaddr = new SocketAddress(packet.getIpAddress(), packet.getPort());
        //UdpProvider udp = null;
        if (udp_service == left_udp) {  // set the actual dest address and src socket for outgoing packet            
            // check whether the source address and port are changed for incoming packet
            if (!left_soaddr.equals(src_soaddr)) {
                logger.debug("left peer addr " + left_soaddr + " changed to " + src_soaddr);

                if (listener != null) {
                    listener.onIVRSymmetricUdpRelayLeftPeerChanged(this, src_soaddr);
                }
            }
            //logger.debug("checking left udp-->"+dest_soaddr.toString());
        }

        udp = left_udp;

        //logger.debug("this.client_id-->" + this.client_id + " udp-->" + udp + " dest sock addr-->" + left_soaddr);
        if (udp != null) {
            long minute = 0;
            //logger.debug("this.client_id-->" + this.client_id);
            if (this.client_id != null && this.client_id.length() > 0 && CallRepository.getInstance().containsMinute(this.client_id)) {
                minute = CallRepository.getInstance().getMinute(this.client_id);
                CallRepository.getInstance().removeMinute(this.client_id);

                //logger.debug("IVR Minute--->" + minute);
                String[] params = {left_soaddr.getAddress().toString(), "" + left_soaddr.getPort(), "" + minute};
                //logger.debug("param-->" + left_soaddr.getAddress().toString() + ":" + left_soaddr.getPort());
                AudioSender as = new AudioSender(this.codec);
                as.sendAudio(params);
                System.out.println("IVR HAS BEEN SENT");
                udp.halt();
            }
        }
    }

    /**
     * When UdpProvider stops receiving UDP datagrams.
     */
    public void onServiceTerminated(UdpProvider udp_service, Exception error) {
        printLog("udp " + udp_service.toString() + " terminated", Log.LEVEL_HIGH);
        if (error != null) {
            printLog("DEBUG: udp " + udp_service.toString() + " exception:\n" + error.toString(), Log.LEVEL_HIGH);
        }
        udp_service.getUdpSocket().close();
        if (!isRunning() && listener != null) {
            listener.onIVRSymmetricUdpRelayTerminated(this);
        }
    }

    /**
     * When the Timer exceeds
     */
    public void onTimeout(Timer t) {
        long now = System.currentTimeMillis();
        if (now < expire_time) {
            long timer_time = relay_time / 2;
            timer = new Timer(timer_time, null, this);
            timer.start();
        } else {
            timer = null;
            printLog("relay inactive for more than " + relay_time + "ms", Log.LEVEL_HIGH);
            halt();
        }
    }

    /**
     * Gets a String representation of the Object
     */
    public String toString() {
        return "";
        //return left_soaddr + "<-->" + left_udp.getUdpSocket().getLocalPort() + "[--]" + right_udp.getUdpSocket().getLocalPort() + "<-->" + right_soaddr;
    }

    // ****************************** Logs *****************************
    /**
     * Adds a new string to the default Log
     */
    private void printLog(String str, int level) {
        if (log != null) {
            log.println("SymmetricUdpRelay: " + str, SessionBorderController.LOG_OFFSET + level);
        }
    }

    /**
     * Adds the Exception message to the default Log
     */
    protected void printException(Exception e, int level) {
        printLog("Exception: " + ExceptionPrinter.getStackTraceOf(e), level);
    }
}
