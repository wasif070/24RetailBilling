/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package callcontroll;

/**
 *
 * @author Ashraful
 */
public class ByeInfo {

    protected String remoteAddr;
    protected int remotePort;
    protected String contactInfo;

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }
}
