/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package callcontroll;

import org.apache.log4j.Logger;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.zoolu.net.UdpPacket;
import org.zoolu.sip.message.BaseMessageFactory;
import org.zoolu.sip.message.Message;
import org.zoolu.sip.provider.SipProvider;
import org.zoolu.sip.utils.Utilities;

/**
 *
 * @author Ashraful
 */
public class CallController implements TimerListener {

    private static Logger logger = Logger.getLogger(CallController.class.getName());

    public CallController(int timeout, boolean first_time, InfoDTO info_dto) {
        DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        ResultSet rs_rate = null;
        double rate_per_min = 0.00;
        int next_timeout = 0;
        String client_id = "";
        RateDTO rate_dto = null;

        try {
            double balance = 0;
            con = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();
            rs = stmt.executeQuery("select client_id,client_balance,rateplan_id from clients where id=" + info_dto.getClientId());

            String dialed_no = info_dto.getDialedNo();

            if (rs.next()) {
                balance = rs.getDouble("client_balance");
                client_id = rs.getString("client_id");

                rate_dto = ClientRepository.getInstance().getRateDTO(client_id, dialed_no);

                if (balance > 0 && rate_dto != null) {
                    int n_pulse = rate_dto.getRate_next_pulse() == 0 ? 1 : rate_dto.getRate_next_pulse();

                    rate_per_min = rate_dto.getRate_per_min();
                    double rate_per_second = rate_per_min / 60;
                    rate_per_second = Utilities.FormatBalance(rate_per_second);

                    if (balance >= rate_per_min) {
                        timeout = 60;
                    } else if (balance > 0) {
                        timeout = ((int) (balance / rate_per_min * 60 / n_pulse)) * n_pulse;
                    } else {
                        timeout = 0;
                    }

                    /*----------------calculate balance for other calls durations by same user---------------*/
                    int sum_timeout = TimeoutLoader.getInstance().putAndGetTimeout(info_dto.getStrClientId(), info_dto.getCallId(), timeout);
                    balance -= rate_per_second * (sum_timeout - timeout);
                    balance = Utilities.FormatBalance(balance);
                    if (balance >= rate_per_min) {
                        timeout = 60;
                    } else if (balance > 0) {
                        timeout = ((int) (balance / rate_per_min * 60 / n_pulse)) * n_pulse;
                    } else {
                        timeout = 0;
                    }
                    logger.debug("temporary balance-->" + info_dto.getStrClientId()+ "-->" + balance + "-->sum time out-->" + sum_timeout);
                    /* --------------------------end of other calls balance calculation--------------------*/                    

                    if (balance >= rate_per_min) {
                        next_timeout = 60;
                    } else if (balance > 0) {
                        next_timeout = ((int) (balance / rate_per_min * 60 / n_pulse)) * n_pulse;
                    } else {
                        next_timeout = 0;
                    }

                } else {
                    timeout = 0;
                    next_timeout = 0;
                }
            }

            info_dto.setRate_dto(rate_dto);
            info_dto.setTimeout(timeout);
            info_dto.setNextTimeout(next_timeout);
            CallRepository.getInstance().storeFirstPulseProcessTag(info_dto.getCallId(), first_time);
            Reminder reminder = new Reminder(this, info_dto);

        } catch (Exception ex) {
            logger.debug("Exception-->" + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                }
            }
            if (rs_rate != null) {
                try {
                    rs_rate.close();
                } catch (Exception ex) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                }
            }
            if (con != null) {
                try {
                    databaseconnector.DBConnector.getInstance().freeConnection(con);
                } catch (Exception ex) {
                }
            }

        }
    }

    @Override
    public void onTimeout(InfoDTO info) {
        if (info.getTimeout() == 0) {
            System.err.println("Time's up!");
            try {
                ArrayList<UdpPacket> packets = BaseMessageFactory.createByeRequest(info.getCallId());
                for (UdpPacket packet : packets) {
                    String dest_addr = packet.getIpAddress().toString();
                    int dest_port = packet.getPort();
                    info.getSipProvider().sendMessage(new Message(packet), SipProvider.PROTO_UDP, dest_addr, dest_port, 0);
                }
                for (UdpPacket packet : packets) {
                    info.getCall_logger().update(new Message(packet), info.getLocation_service());
                    break;
                }

            } catch (Exception ex) {
                logger.debug("on timeout ex-->" + ex);
            }
        } else {
            if (CallRepository.getInstance().containsByeInfo(info.getCallId())) {
                BalanceFunctions.getInstance().updateBalance(info);
                CallController cc = new CallController(info.getNextTimeout(), false, info);
            }
        }
    }
}
