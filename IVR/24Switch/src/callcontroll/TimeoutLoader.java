package callcontroll;

import org.apache.log4j.Logger;
import java.util.HashMap;
import java.util.Set;

public class TimeoutLoader {

    static Logger logger = Logger.getLogger(TimeoutLoader.class.getName());
    static TimeoutLoader loader = null;
    private HashMap<String, HashMap<String, Integer>> callerTimeoutMap;

    public TimeoutLoader() {
        callerTimeoutMap = new HashMap<String, HashMap<String, Integer>>();
    }

    public static TimeoutLoader getInstance() {
        if (loader == null) {
            createLoader();
        }
        return loader;
    }

    private synchronized static void createLoader() {
        if (loader == null) {
            loader = new TimeoutLoader();
        }
    }

    public synchronized int getTimeout(String caller) {
        int timeout = 0;
        if (callerTimeoutMap.containsKey(caller)) {
            HashMap<String, Integer> callidTimeoutMap = callerTimeoutMap.get(caller);
            Set<String> keys = callidTimeoutMap.keySet();
            for (String key : keys) {
                timeout += callidTimeoutMap.get(key);
            }
        }
        logger.debug("get timeout for caller-->" + caller + " timeout-->" + timeout);
        return timeout;
    }

    public synchronized int putAndGetTimeout(String caller, String call_id, int timeout) {
        if (callerTimeoutMap.containsKey(caller)) {
            HashMap<String, Integer> callidTimeoutMap = callerTimeoutMap.get(caller);
            Set<String> keys = callidTimeoutMap.keySet();
            for (String key : keys) {
                timeout += callidTimeoutMap.get(key);
            }
            callidTimeoutMap.put(call_id, timeout);
            callerTimeoutMap.put(caller, callidTimeoutMap);
            logger.debug("appending timeout for call_id-->" + caller + "-->" + call_id + "-->" + timeout);
        } else {
            HashMap<String, Integer> callidTimeoutMap = new HashMap<String, Integer>();
            callidTimeoutMap.put(call_id, timeout);
            callerTimeoutMap.put(caller, callidTimeoutMap);
            logger.debug("adding timeout for call_id-->" + caller + "-->" + call_id + timeout);
        }

        return timeout;
    }

    public synchronized void removeTimeout(String caller, String call_id) {
        if (callerTimeoutMap.containsKey(caller)) {
            if (callerTimeoutMap.get(caller).containsKey(call_id)) {
                callerTimeoutMap.get(caller).remove(call_id);
                logger.debug("removing timeout for -->" + caller + "-->" + call_id);
            }
        }
    }
}
