package org.zoolu.sound;

/** Base interface for all codecs. */
public interface Codec {

    public byte[] encode(short src16[], int off, int len);

    public short[] decode(byte src8[], int off, int len);

    public int encode(short audio_stream[], int offset, byte buffer[], int frame_size);

    public int decode(byte encoded_data[], short audio_stream[], int frame_size);
}
