/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package org.zoolu.sip.provider;

import callcontroll.BalanceFunctions;
import callcontroll.CallRepository;
import callcontroll.CallController;
import callcontroll.ClientRepository;
import callcontroll.InfoDTO;
import callcontroll.RateDTO;
import callcontroll.TimeoutLoader;
import org.zoolu.net.*;
import org.zoolu.sip.message.Message;
import java.io.IOException;
import org.apache.log4j.Logger;

/**
 * UdpTransport provides an UDP transport service for SIP.
 */
public class UdpTransport implements Transport, UdpProviderListener {

    private static Logger logger = Logger.getLogger(UdpTransport.class.getName());
    protected boolean stopCounting = false;
    /**
     * UDP protocol type
     */
    public static final String PROTO_UDP = "udp";
    /**
     * UDP provider
     */
    UdpProvider udp_provider;
    /**
     * Transport listener
     */
    TransportListener listener = null;

    /**
     * Creates a new UdpTransport
     */
    public UdpTransport(UdpSocket socket) {
        udp_provider = new UdpProvider(socket, this);
    }

    /**
     * Creates a new UdpTransport
     */
    public UdpTransport(int local_port) throws IOException {
        initUdp(local_port, null);
    }

    /**
     * Creates a new UdpTransport
     */
    public UdpTransport(int local_port, IpAddress host_ipaddr) throws IOException {
        initUdp(local_port, host_ipaddr);
    }

    /**
     * Inits the UdpTransport
     */
    protected void initUdp(int local_port, IpAddress host_ipaddr) throws IOException {
        if (udp_provider != null) {
            udp_provider.halt();
        }
        // start udp
        UdpSocket socket = (host_ipaddr == null) ? new UdpSocket(local_port) : new UdpSocket(local_port, host_ipaddr);
        //UdpSocket socket=(host_ipaddr==null)? new org.zoolu.net.JumboUdpSocket(local_port,500) : new org.zoolu.net.JumboUdpSocket(local_port,host_ipaddr,500);
        udp_provider = new UdpProvider(socket, this);

    }

    /**
     * Gets protocol type
     */
    public String getProtocol() {
        return PROTO_UDP;
    }

    /**
     * Gets port
     */
    public int getLocalPort() {
        try {
            return udp_provider.getUdpSocket().getLocalPort();
        } catch (Exception e) {
            return 0;
        }

    }

    /**
     * Sets transport listener
     */
    public void setListener(TransportListener listener) {
        this.listener = listener;
    }

    /**
     * Sends a Message to a destination address and port
     */
    public TransportConn sendMessage(Message msg, IpAddress dest_ipaddr, int dest_port, int ttl) throws IOException {
        if (udp_provider != null) {
            //System.err.println("sent :: \n" + msg.toString());
            byte[] data = msg.toString().getBytes();

            /*---------------start encription-------------

             for (int i = 0; i < data.length; i++) {
             data[i] = (byte) (~data[i]);
             }
             ---------------end encription-------------*/

            UdpPacket packet = new UdpPacket(data, data.length);
            // if (ttl>0 && multicast_address) do something?
            packet.setIpAddress(dest_ipaddr);
            packet.setPort(dest_port);
            udp_provider.send(packet);


            if (msg.isAck()) {
                try {
                    //logger.debug("ACK sending--->>>>");
                    String call_id = msg.getCallIdHeader().getCallId();
                    if (CallRepository.getInstance().getCallType(call_id) == CallRepository.TERMINATION_CALL) {
                        if (CallRepository.getInstance().getByeInfo(call_id) != null) {
                            CallRepository.getInstance().storeACK(call_id, packet);
                            CallRepository.getInstance().storeACKTime(call_id, System.currentTimeMillis());
                            CallController cc = new CallController(0, true, CallRepository.getInstance().getInfoDTO(call_id));
                        }
                    }
                } catch (Exception ex) {
                    logger.debug("Exception in call controlling-->" + ex);
                }
            } else if (msg.isBye()) {
                String call_id = msg.getCallIdHeader().getCallId();
                if (CallRepository.getInstance().getCallType(call_id) == CallRepository.TERMINATION_CALL) {
                    InfoDTO info_dto = CallRepository.getInstance().getInfoDTO(call_id);
                    if (CallRepository.getInstance().containsByeTime(call_id)) {
                        long bill_time = CallRepository.getInstance().getByeTime(call_id) - CallRepository.getInstance().getACKTime(call_id);
                        bill_time = (bill_time + 999) / 1000;

                        logger.debug("Do billing for manual bye-->" + bill_time);

                        if (info_dto != null) {
                            info_dto.setTimeout((int) bill_time);
                            RateDTO r_dto = ClientRepository.getInstance().getRateDTO(info_dto.getStrClientId(), info_dto.getDialedNo());
                            info_dto.setRate_dto(r_dto);
                            BalanceFunctions.getInstance().updateBalance(info_dto);
                        }
                        CallRepository.getInstance().removeByeTime(call_id);
                    }

                    CallRepository.getInstance().removeByeInfo(call_id);
                    CallRepository.getInstance().removeInfoDTO(call_id);
                    CallRepository.getInstance().removeACK(call_id);
                    CallRepository.getInstance().removeACKTime(call_id);
                    if (info_dto != null) {
                        TimeoutLoader.getInstance().removeTimeout(info_dto.getStrClientId(), call_id);
                    }
                    logger.debug("Total bill-->" + CallRepository.getInstance().getBill(call_id));
                }
                CallRepository.getInstance().removeCallType(call_id);
            }
        }
        return null;
    }

    /**
     * Stops running
     */
    public void halt() {
        if (udp_provider != null) {
            udp_provider.halt();
        }
    }

    /**
     * Gets a String representation of the Object
     */
    public String toString() {
        if (udp_provider != null) {
            return udp_provider.toString();
        } else {
            return null;
        }
    }

    //************************* Callback methods *************************
    /**
     * When a new UDP datagram is received.
     */
    @Override
    public void onReceivedPacket(UdpProvider udp, UdpPacket packet) {
        Message msg = new Message(packet);
//        if (msg.toString().contains("Ringing")) {
//            logger.debug("Packet Received----> \n" + msg.toString());
//        }
        //System.out.println("Packet Received----> \n" + msg.toString());        
        msg.setRemoteAddress(packet.getIpAddress().toString());
        msg.setRemotePort(packet.getPort());
        msg.setTransport(PROTO_UDP);
        if (listener != null) {
            listener.onReceivedMessage(this, msg);
        }
    }

    /**
     * When DatagramService stops receiving UDP datagrams.
     */
    public void onServiceTerminated(UdpProvider udp, Exception error) {
        if (listener != null) {
            listener.onTransportTerminated(this, error);
        }
        UdpSocket socket = udp.getUdpSocket();
        if (socket != null) {
            try {
                socket.close();
            } catch (Exception e) {
            }
        }
        this.udp_provider = null;
        this.listener = null;
    }
}
