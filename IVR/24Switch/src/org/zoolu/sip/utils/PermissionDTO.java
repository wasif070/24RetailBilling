/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zoolu.sip.utils;

/**
 *
 * @author Ashraful
 */
public class PermissionDTO {

    private int errorCode;
    private String prefix;
    private double balance;
    private double rate_per_second;

    public PermissionDTO() {
        this.errorCode = 403;
        this.prefix = "";
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getRate_per_second() {
        return rate_per_second;
    }

    public void setRate_per_second(double rate_per_second) {
        this.rate_per_second = rate_per_second;
    }
}
