<%-- 
    Document   : gateway
    Created on : Nov 12, 2013, 2:42:21 PM
    Author     : reefat
--%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Gateway</title>
    </head>
    <body>


        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content ">
                        <s:url var="gatewayEditUrl" action="delete" namespace="/gateway"/>
                        <s:url var="gatewayRemoteUrl" action="jsonListURL" namespace="/gateway" />
                        <s:url var="formatURL" action="gateway/retrieve.action" />

                        <s:url var="clientNameSelectUrl" action="selectClientNameAction"  namespace="/content"/>
                        <s:url var="StatusSelectUrl" action="selectStatusAction"  namespace="/content"/>   
                        <s:url var="gatewayTypeSelectUrl" action="selectGatewayTypeAction"  namespace="/content"/>
                        <div class="content_table">

                            <s:actionerror/>
                            <div class="action_message"><s:actionmessage cssStyle="color:blue" /></div>

                            <s:if test="hasActionErrors()"><p></p></s:if>
                            <s:if test="hasActionMessages()"><p></p></s:if>
                            <sjg:grid 
                                id="gridtable"
                                caption="Gateway"
                                dataType="json"
                                draggable="true"
                                droppable="true"
                                href="%{gatewayRemoteUrl}"
                                pager="true"                        
                                navigator="true"
                                navigatorSearch="true"
                                navigatorSearchOptions="{multipleSearch:true}"     
                                navigatorAdd="false"                             
                                navigatorAddOptions="{
                                reloadAfterSubmit:true,
                                afterSubmit:function(response, postdata) {
                                return utilities.funcResultado(response,postdata);                        
                                }
                                }"
                                navigatorEdit="false"                      
                                navigatorEditOptions="{  
                                reloadAfterSubmit:true,
                                afterSubmit:function(response, postdata) {
                                return utilities.funcResultado(response,postdata);
                                }
                                }"
                                navigatorView="true"
                                navigatorDelete="true"
                                navigatorDeleteOptions="{
                                reloadAfterSubmit:true,
                                afterSubmit:function(response, postdata) {
                                window.location = 'gateway/list.action';
                                return [true,'Successfully Deleted'];
                                }
                                }"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="15"
                                rownumbers="true"
                                editurl="%{gatewayEditUrl}"
                                editinline="false"
                                multiselect="true"
                                viewrecords="true"
                                autowidth="true"
                                navigatorExtraButtons="{
                                alert : { 
                                title : 'Add', 
                                icon: 'ui-icon-carat-1-n',
                                caption : 'Add', 
                                onclick: function(){ 
                                window.location = 'gateway/show.action';                        
                                }
                                }
                                }"                      
                                >
                                <sjg:gridColumn 
                                    name="gateway_db_id" 
                                    index="gateway_db_id" 
                                    title=""
                                    formatter="integer" 
                                    hidden="true"
                                    />
                                <sjg:gridColumn 
                                    name="gateway_name" 
                                    index="gateway_name" 
                                    title="GW name" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="text"
                                    editrules="{
                                    required : true
                                    }"
                                    formatter="utilities.formatLink"
                                    formatoptions="{url:'gateway/retrieve.action',id_name:'id',col_name:'id'}"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                                    align="center"
                                    />                        
                                <sjg:gridColumn 
                                    name="gateway_ip" 
                                    index="gateway_ip" 
                                    title="GW IP" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="text"
                                    editrules="{
                                    required : true
                                    }"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                                    align="center"
                                    />
                                <sjg:gridColumn 
                                    name="gateway_port" 
                                    index="gateway_port" 
                                    title="GW Port" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="text"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge']}"
                                    align="center"
                                    />                        
                                <sjg:gridColumn 
                                    name="gateway_status" 
                                    index="gateway_status" 
                                    title="GW status" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="select"
                                    surl="%{StatusSelectUrl}"
                                    editoptions="{ dataUrl : '%{StatusSelectUrl}' }" 
                                    search="true" 
                                    searchtype="select"
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc'], dataUrl : '%{StatusSelectUrl}'}"   
                                    align="center"
                                    width="100"
                                    />            
                                <sjg:gridColumn 
                                    name="gateway_type" 
                                    index="gateway_type" 
                                    title="GW type" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="select"                            
                                    search="true" 
                                    searchtype="select"
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{gatewayTypeSelectUrl}'}"
                                    editoptions="{ dataUrl : '%{gatewayTypeSelectUrl}',dataEvents: [{type: 'change',fn:function(e){utilities.getClientList(this);}}] }"   
                                    align="center"
                                    width="100"
                                    />
                                <sjg:gridColumn 
                                    name="client_id" 
                                    index="client_id" 
                                    title="Client" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="select"
                                    surl="%{clientNameSelectUrl}"
                                    editoptions="{ dataUrl : '%{clientNameSelectUrl}' }" 
                                    search="true" 
                                    searchtype="select"
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc'], dataUrl : '%{clientNameSelectUrl}'}"   
                                    align="center"
                                    width="100"                            
                                    />
                            </sjg:grid>

                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>         
    </body>
</html>
