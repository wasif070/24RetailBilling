<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Billing</title>

    </head>
    <body> 

        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content">
                        <div class="min-heigth" style="min-height: 400px">
                            <s:set var="gate_type" value="gateway_type"/>                   

                            <s:url var="clientNameSelectUrl" action="selectClientNameAction?gateway_type=%{#gate_type}"  namespace="/content"/>
                            <s:url var="statusSelectUrl" action="selectStatusAction"  namespace="/content"/>   
                            <s:url var="gatewayTypeSelectUrl" action="selectGatewayTypeAction"  namespace="/content"/>
                            <div class="form_content">
                                <div class="ui-jqdialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding:10px;"><span class="ui-jqdialog-title">Gateway Information</span></div>
                                <p>
                                    <s:set name="currentAction" value="action"/>    
                                    <s:actionerror />
                                    <s:actionmessage cssStyle="color:blue" />

                                    <s:set name="currentAction" value="action"/>

                                    <s:form action="%{#currentAction}" namespace="/gateway" method="post">
                                        <s:hidden id="id" name="id" />                                 

                                        <s:if test="%{#currentAction=='add'}" >
                                            <s:textfield id="gateway_name" name="gateway_name" key="GW Name" required="true"/>
                                        </s:if>
                                        <s:else>
                                            <s:textfield id="gateway_name" name="gateway_name" key="GW Name" readonly="true"  />
                                        </s:else>                            

                                        <s:textfield id="gateway_ip" key="GW IP" name="gateway_ip" required="true"/>
                                        <s:textfield id="gateway_port" key="GW Port" name="gateway_port" />
                                        <sj:select id="gateway_status" key="Status" name="gateway_status" list="statusList" href="%{statusSelectUrl}" />
                                        <sj:select id="gateway_type" key="GW type" name="gateway_type" list="gatewayTypeList" href="%{gatewayTypeSelectUrl}" /> 
                                        <sj:select id="client_id" key="client" name="client_id" list="clientNameList"  href="%{clientNameSelectUrl}" />

                                        <s:submit key="Save" align="center" cssClass="button"/>
                                    </s:form>
                                </p>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>          
    </body>
</html>


