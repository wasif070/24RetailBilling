<%-- 
    Document   : left_menu
    Created on : Sep 26, 2013, 11:06:31 AM
    Author     : reefat
--%>
<%@page import="com.ipvision.user.Users"%>
<%
    Users users = (Users) request.getSession().getAttribute("userName");
%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style type="text/css">
    #browser {
        font-family: Verdana, helvetica, arial, sans-serif;
        font-size: 140%;
    }
</style>

<ul id="browser">
    <li><span><s:a action="home" namespace="/" >Home</s:a></span></li>    
        <%
            if (users != null && users.getRole_id() < 0) {
        %>  
    <li><s:a action="list" namespace="client">Client</s:a></li>    
    <li><s:a action="list" namespace="payment">Payment History</s:a></li> 
    <li><s:a action="list" namespace="dialplan">Dial Plan</s:a></li>
    <li><s:a action="list" namespace="gateway">Gateway</s:a></li>
    <li><s:a action="list" namespace="rateplan">Rate Plan</s:a> </li>
    <li><s:a action="list" namespace="announcement">Announcement</s:a> </li>
        <%} else {%>
    <li><s:a action="list" namespace="payment">Payment History</s:a></li>   
    <li><s:a action="rateList" namespace="rateplan">Rate Plan</s:a> </li>
    <li><s:a action="retrieve" namespace="client">Edit profile</s:a></li>    
        <%}%>
    <li><s:a action="list" namespace="activeCalls">Active Calls</s:a></li>
    <li><s:a action="list" namespace="report">Reports</s:a></li>    
    <li class="last"><s:a action="logout" namespace="">Logout</s:a> </li>
</ul>

<script>
    $(document).ready(function() {
        utilities.selectedItem();
    });
</script>