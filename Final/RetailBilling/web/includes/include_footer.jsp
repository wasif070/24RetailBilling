<%-- 
    Document   : include_footer
    Created on : Sep 26, 2013, 10:06:46 AM
    Author     : reefat
--%>

<%@page import="util.Utils"%>
<div class="footer">
    <div class="blue_row">
        Billing Server Time <span class="bold current_time"><%=Utils.getDateDDMMYYhhmmss(System.currentTimeMillis())%></span>
    </div>
    <a href="http://www.ipvision-inc.com/" target="blank" title="IP Vision Ltd.">Powered by IPVision Inc.</a>
</div>
