<%-- 
    Document   : header
    Created on : Sep 26, 2013, 9:52:34 AM
    Author     : reefat
--%>
<%
    String getProtocol = request.getScheme();
    String getDomain = request.getServerName();
    String getPort = Integer.toString(request.getServerPort());
    String hostName = getProtocol + "://" + getDomain + ":" + getPort + "/";


    String baseURL = "";
    if (request.getServerPort() > 0) {
        baseURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + application.getContextPath() + "/";
    } else {
        baseURL = request.getScheme() + "://" + request.getServerName() + application.getContextPath() + "/";
    }
    request.getSession(true).setAttribute("BASE_URL", baseURL);

%>

<title>Billing</title>
<base href="<%=baseURL%>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->

<link href="css/default-styles.css" rel="stylesheet" media="screen">
<script src="http://code.jquery.com/jquery.js"></script>

<script type="text/javascript">
    var $j = jQuery.noConflict();
    var base_url = "<%=baseURL%>";
</script>

<script src="js/utils.js"></script>
