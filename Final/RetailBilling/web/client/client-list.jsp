<%-- 
    Document   : json
    Created on : Sep 18, 2013, 10:14:44 AM
    Author     : wasif
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>

<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Client</title>
        <style type="text/css">
            .welcome {
                /*                background-color:#DDFFDD;
                                border:1px solid #009900;
                                width:200px;*/
            }
            .welcome li{
                list-style: none;
            }
        </style>
    </head>
    <body>
        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content ">
                        <s:url var="clientEditUrl" action="delete" namespace="/client"/>
                        <s:url var="clientRemoteUrl" action="jsonListURL" namespace="/client" />
                        <s:url var="formatURL" action="client/retrieve.action" />

                        <s:url var="rateplanSelectUrl" action="selectRatePlanAction"  namespace="/content" />
                        <s:url var="clientTypeSelectUrl" action="selectClientTypeAction"  namespace="/content" />
                        <s:url var="StatusSelectUrl" action="selectStatusAction"  namespace="/content" /> 
                        <div class="content_table">

                            <s:actionerror/>
                            <div class="action_message"><s:actionmessage cssStyle="color:blue" /></div>

                            <s:if test="hasActionErrors()"><p></p></s:if>
                            <s:if test="hasActionMessages()"><p></p></s:if>

                            <sjg:grid 
                                id="gridtable"
                                caption="Client"
                                dataType="json"
                                draggable="true"
                                droppable="true"
                                href="%{clientRemoteUrl}"
                                pager="true"                        
                                navigator="true"
                                navigatorSearch="true"
                                navigatorSearchOptions="{multipleSearch:true}"     
                                navigatorAdd="false"                             
                                navigatorAddOptions="{
                                reloadAfterSubmit:true,
                                afterSubmit:function(response, postdata) {
                                return utilities.funcResultado(response,postdata);                        
                                }
                                }"
                                navigatorEdit="false"            
                                navigatorEditOptions="{  
                                reloadAfterSubmit:true,
                                afterSubmit:function(response, postdata) {
                                return utilities.funcResultado(response,postdata);
                                }
                                }"
                                navigatorView="false"
                                navigatorDelete="true"
                                navigatorDeleteOptions="{
                                reloadAfterSubmit:true,
                                afterSubmit:function(response, postdata) {
                                window.location = 'client/list.action';
                                return [true,'Successfully Deleted'];
                                }
                                }"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="15"
                                rownumbers="true"
                                editurl="%{clientEditUrl}"                        
                                editinline="false"
                                multiselect="true"
                                viewrecords="true"
                                autowidth="false" 
                                navigatorExtraButtons="{
                                alert : { 
                                title : 'Add', 
                                caption : 'Add', 
                                icon: 'ui-icon-carat-1-n',
                                onclick: function(){ 
                                window.location = 'client/show.action';                        
                                }
                                }
                                }"
                                >
                                <sjg:gridColumn 
                                    name="client_db_id" 
                                    index="client_db_id" 
                                    title=""
                                    formatter="integer" 
                                    hidden="true"
                                    />
                                <sjg:gridColumn 
                                    name="client_id" 
                                    index="client_id" 
                                    title="ID" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="text"                            
                                    editrules="{
                                    required : true
                                    }"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                                    align="center"                             
                                    formatter="utilities.formatLink"
                                    formatoptions="{url:'client/retrieve.action',id_name:'id',col_name:'id'}"
                                    />                        
                                <sjg:gridColumn 
                                    name="rateplan_id"                      
                                    index="rateplan_id" 
                                    title="RatePlan" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="select"
                                    search="true" 
                                    searchtype="select"
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge'], dataUrl : '%{rateplanSelectUrl}'}"
                                    editoptions="{ dataUrl : '%{rateplanSelectUrl}' }"
                                    align="center"
                                    width="120"
                                    />
                                <sjg:gridColumn 
                                    name="client_password"    
                                    hidden="true"
                                    index="client_password" 
                                    title="Password" 
                                    sortable="false"                            
                                    editable="true" 
                                    edittype="password"
                                    editrules="{
                                    required : true,
                                    edithidden : true
                                    }"
                                    search="false"   
                                    align="center"
                                    />   
                                <sjg:gridColumn 
                                    name="client_name" 
                                    index="client_name" 
                                    title="Name" 
                                    sortable="true" 
                                    editable="true"
                                    edittype="text"              
                                    search="true"                                               
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}" 
                                    align="center"
                                    />                        
                                <sjg:gridColumn 
                                    name="client_email" 
                                    index="client_email" 
                                    title="Email" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="text" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"   
                                    align="center"
                                    />                        
                                <sjg:gridColumn 
                                    name="client_type" 
                                    index="client_type" 
                                    title="Type" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="select"
                                    search="true" 
                                    searchtype="select"
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{clientTypeSelectUrl}'}"
                                    editoptions="{ dataUrl : '%{clientTypeSelectUrl}' }"   
                                    align="center"
                                    width="100"
                                    />     
                                <sjg:gridColumn 
                                    name="client_credit_limit"                             
                                    index="client_credit_limit" 
                                    formatter="currency"
                                    title="Credit Limit" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="text" 
                                    editrules="{
                                    number : true
                                    }"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge']}"   
                                    align="center"                            
                                    />
                                <!--formatoptions="{showAction:'employees-detail.action'}"-->
                                <sjg:gridColumn 
                                    name="client_balance" 
                                    index="client_balance"                             
                                    title="Balance" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="text" 
                                    editrules="{
                                    number : true
                                    }"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge']}"   
                                    align="center"                            
                                    />
                                <sjg:gridColumn 
                                    name="prefix" 
                                    index="prefix" 
                                    title="Prefix" 
                                    sortable="false" 
                                    editable="true" 
                                    edittype="text" 
                                    search="true" 
                                    hidden=""
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"   
                                    align="center"
                                    />            
                                <sjg:gridColumn 
                                    name="client_call_limit"                             
                                    index="client_call_limit" 
                                    title="Call Limit" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="text" 
                                    editrules="{
                                    number : true
                                    }"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge']}"   
                                    align="center"
                                    />
                                <sjg:gridColumn 
                                    name="client_status" 
                                    index="client_status" 
                                    title="Status" 
                                    sortable="true" 
                                    editable="true" 
                                    edittype="select"
                                    editoptions="{ dataUrl : '%{StatusSelectUrl}' }" 
                                    search="true" 
                                    searchtype="select"
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc'], dataUrl : '%{StatusSelectUrl}'}"   
                                    align="center"
                                    width="100"
                                    />
                                <sjg:gridColumn 
                                    name="payment" 
                                    index="payment" 
                                    align="center" 
                                    title="Payment" 
                                    sortable="false"  
                                    editable="false" 
                                    edittype="text" 
                                    hidden="false" 
                                    formatter="utilities.formatLink"
                                    formatoptions="{url:'client/clientPayment.action',id_name:'id',col_name:'id'}"
                                    />
                            </sjg:grid>

                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>        
    </body>

</html>
