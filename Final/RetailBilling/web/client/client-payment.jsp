<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Client Payment</title>
    </head>
    <body>



        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content">
                        <div class="min-heigth" style="min-height: 400px">
                            <s:url var="paymentTypeSelectUrl" action="selectPaymentTypeAction"  namespace="/content" />
                            <div class="form_content">
                                <div class="ui-jqdialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding:10px;"><span class="ui-jqdialog-title">Payment Information</span></div>
                                <p>
                                    <%--<s:set name="currentAction" value="action"/>--%>    
                                    <s:actionerror />
                                    <s:actionmessage cssStyle="color:blue" />

                                    <s:form action="clientPaymentSubmit" namespace="/client" method="post">
                                        <%--<s:hidden id="client_db_id" name="client_db_id" />--%>
                                        <s:hidden id="id" name="id" />

                                        <s:textfield id="client_id" name="client_id" key="ID" readonly="true"/>
                                        <sj:select id="payment_type" key="Payment type" name="payment_type" list="paymentTypeList" href="%{paymentTypeSelectUrl}" />
                                        <s:textfield id="payment_amount" key="Payment amount" name="payment_amount" value="0.0"/>

                                        <s:submit key="Save" align="center" cssClass="button"/>
                                    </s:form>
                                </p>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>          
    </body>
</html>


