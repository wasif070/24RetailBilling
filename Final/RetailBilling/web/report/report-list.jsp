<%-- 
    Document   : report-list
    Created on : Nov 24, 2013, 2:40:52 PM
    Author     : Wasif Islam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>

<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Reports</title>
    </head>
    <body>
        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content ">
                        <s:url var="reportRemoteUrl" action="jsonListURL" namespace="/report" />
                        <div class="content_table">

                            <s:actionerror/>
                            <div class="action_message"><s:actionmessage cssStyle="color:blue" /></div>

                            <s:if test="hasActionErrors()"><p></p></s:if>
                            <s:if test="hasActionMessages()"><p></p></s:if>

                            <sjg:grid 
                                id="gridtable"
                                caption="Call Detail Report"
                                dataType="json"
                                draggable="true"
                                droppable="true"
                                href="%{reportRemoteUrl}"
                                pager="true"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="15"
                                rownumbers="true"
                                viewrecords="true"
                                editurl="false"
                                navigator="true"
                                navigatorEdit="false"
                                navigatorAdd="false"
                                navigatorDelete="false"
                                footerrow="true"
                                groupSummary="true"
                                userDataOnFooter="true"
                                navigatorSearch="true"
                                navigatorSearchOptions="{multipleSearch:true}"
                                autowidth="false"
                                >
                                <sjg:gridColumn 
                                    name="origin_caller" 
                                    index="origin_caller" 
                                    title="Orgination Caller" 
                                    sortable="true"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="false" />
                                <sjg:gridColumn 
                                    name="origin_ip" 
                                    index="origin_ip" 
                                    title="Orgination IP" 
                                    sortable="false"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="false" />
                                <sjg:gridColumn 
                                    name="origin_rate" 
                                    index="origin_rate" 
                                    title="Orgination Rate" 
                                    sortable="true"
                                    align="center"                            
                                    search="true" 
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="origin_bill" 
                                    index="origin_bill" 
                                    title="Orgination Bill" 
                                    sortable="true"
                                    align="center"                            
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge']}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="term_callee" 
                                    index="term_callee" 
                                    title="Termination Callee" 
                                    sortable="true"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="term_ip" 
                                    index="term_ip" 
                                    title="Termination IP" 
                                    sortable="false"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="term_rate" 
                                    index="term_rate" 
                                    title="Termination Rate" 
                                    sortable="true"
                                    align="center"                            
                                    search="true" 
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="term_bill" 
                                    index="term_bill" 
                                    title="Termination Bill" 
                                    sortable="true"
                                    align="center"                            
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge']}" 
                                    editable="false" />
                                <sjg:gridColumn 
                                    name="invite_time" 
                                    index="invite_time" 
                                    title="Invite Time" 
                                    sortable="true" 
                                    align="center"
                                    hidden="true"
                                    search="false"
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge']}" 
                                    editable="false" />
                                <sjg:gridColumn 
                                    name="ringing_time" 
                                    index="ringing_time" 
                                    title="Ringing Time" 
                                    sortable="true" 
                                    align="center"
                                    hidden="true"
                                    search="false"
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge']}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="accepted_time" 
                                    index="accepted_time" 
                                    title="Start Time" 
                                    sortable="true" 
                                    align="center"
                                    search="true"
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge'], dataInit:datePick,attr:{title:'Your Search Date'}}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="refused_time" 
                                    index="refused_time" 
                                    title="Refused Time" 
                                    sortable="true" 
                                    align="center"
                                    search="true"
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge'],  dataInit:datePick,attr:{title:'Your Search Date'}}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="bye_time" 
                                    index="bye_time" 
                                    title="End Time" 
                                    sortable="true" 
                                    align="center"
                                    search="true"
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge'],  dataInit:datePick,attr:{title:'Your Search Date'}}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="ringing_duration" 
                                    index="ringing_duration" 
                                    title="Ringing Duration" 
                                    sortable="true" 
                                    align="center"
                                    search="true"
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge'],  dataInit:timePick,attr:{title:'Your Search Time'}}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="call_duration" 
                                    index="call_duration" 
                                    title="Call Duration" 
                                    sortable="true" 
                                    align="center"
                                    search="true"
                                    searchtype="text"
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge'],  dataInit:timePick,attr:{title:'Your Search Time'}}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="disconnect_cause" 
                                    index="disconnect_cause" 
                                    title="Disconnect Cause" 
                                    sortable="false" 
                                    align="center"
                                    search="true"
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="false"/>

                            </sjg:grid>  
                            <div hidden id="hidden_div">                               
                                <sj:datepicker id="datetime" label="Select a DateTime" value="%{new java.util.Date()}" timepicker="true" timepickerShowSecond="true" timepickerFormat="hh:mm:ss"/>
                            </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div> 
        <script type="text/javascript">
            $(document).ready(function() {
                $("#hidden_div").hide();
            }
            );
            datePick = function(elem) {
                $(elem).datetimepicker({
                   dateFormat: 'dd-mm-yy',
                   timeFormat: 'HH:mm:ss'
                });
            };
            timePick = function(elem) {
                $(elem).timepicker({
                    timeFormat: 'HH:mm:ss'
                });
            };
        </script>

    </body>
</html>