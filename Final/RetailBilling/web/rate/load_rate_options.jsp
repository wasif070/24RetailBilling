<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils,java.text.DecimalFormat,java.text.NumberFormat" %>
<%
    ArrayList<Integer> hours = Utils.getTimeValue(24);
    ArrayList<Integer> minsec = Utils.getTimeValue(60);
    NumberFormat formatter = new DecimalFormat("00");
    String output = "";
    output = "<tr><td><select name='rate_day[]' class='width_100'>";
    for (int i = 0; i < Constants.DAY_VALUE.length; i++) {
        output += "<option value=" + Constants.DAY_VALUE[i] + ">" + Constants.DAY_STRING[i] + "</option>";
    }
    output += "</select></td>";
    output += "<td><select name='rate_fromhour[]' class='width_50'>";
    for (int i = 0; i < hours.size(); i++) {
        String increment = String.valueOf(i);
        String temp = formatter.format((i));
        output += "<option value=" + increment + ">" + temp + "</option>";
    }
    output += "</select> ";
    output += "<select name='rate_frommin[]' class='width_50'>";
    for (int i = 0; i < minsec.size(); i++) {
        String increment = String.valueOf(i);
        String temp = formatter.format((i));
        output += "<option value=" + increment + ">" + temp + "</option>";
    }
    output += "</select></td>";
    output += "<td><select name='rate_tohour[]' class='width_50'>";
    for (int i = 0; i < hours.size(); i++) {
        String increment = String.valueOf(i);
        String temp = formatter.format((i));
        output += "<option value=" + increment + ">" + temp + "</option>";
    }
    output += "</select>";
    output += " <select name='rate_tomin[]' class='width_50'>";
    for (int i = 0; i < minsec.size(); i++) {
        String increment = String.valueOf(i);
        String temp = formatter.format((i));
        output += "<option value=" + increment + ">" + temp + "</option>";
    }
    output += "</select></td>";
    output += "<td><input type=\"text\" name=\"ratePerMin[]\" value=\"0.00\" style=\"width:50px\" /></td>";
    output += "<td><input type=\"text\" name=\"firstPulse[]\" value=\"1\" style=\"width:50px\" /></td>";
    output += "<td><input type=\"text\" name=\"nextPulse[]\" value=\"1\" style=\"width:50px\" /></td>";
    output += "<td><input type=\"text\" name=\"gracePeriod[]\" value=\"0\" style=\"width:50px\" /></td>";
    output += "<td><input type=\"text\" name=\"failedPeriod[]\" value=\"0\" style=\"width:50px\" /></td>";
    output += "</tr><tr class='more_option'><td>"
            + "<input type=\"button\" class=\"jadd_more_rate pointer\" value=\"Add More\" />"
            + "</td></tr>";

%>
<%=output%>