<%-- 
    Document   : dialplan
    Created on : Nov 14, 2013, 9:32:02 AM
    Author     : reefat
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>


<!DOCTYPE html>
<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Dialplan</title>
        <style type="text/css">
            .welcome {
            }
            .welcome li{
                list-style: none;
            }
        </style>
    </head>
    <body>


        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content ">

                        <s:url var="dialplanEditUrl" action="delete" namespace="/dialplan"/>
                        <s:url var="dialplanRemoteUrl" action="jsonListURL" namespace="/dialplan" />
                        <s:url var="formatURL" action="dialplan/retrieve.action" />

                        <s:url var="termGatewaySelectUrl" action="selectTermGatewayAction" namespace="/content"/>
                        <div class="content_table">

                            <s:actionerror/>
                            <div class="action_message"><s:actionmessage cssStyle="color:blue" /></div>

                            <s:if test="hasActionErrors()"><p></p></s:if>
                            <s:if test="hasActionMessages()"><p></p></s:if>

                            
                    <sjg:grid 
                        id="gridtable"
                        caption="Dialplan"
                        dataType="json"
                        draggable="true"
                        droppable="true"
                        href="%{dialplanRemoteUrl}"
                        pager="true"                        
                        navigator="true"
                        navigatorSearch="true"
                        navigatorSearchOptions="{multipleSearch:true}"     
                        navigatorAdd="false"                              
                        navigatorAddOptions="{
                        reloadAfterSubmit:true,
                        afterSubmit:function(response, postdata) {
                        return utilities.funcResultado(response,postdata);                        
                        }
                        }"
                        navigatorEdit="false"               
                        navigatorEditOptions="{  
                        reloadAfterSubmit:true,
                        afterSubmit:function(response, postdata) {
                        return utilities.funcResultado(response,postdata);
                        }
                        }"
                        navigatorView="false"
                        navigatorDelete="true"
                        navigatorDeleteOptions="{
                        reloadAfterSubmit:true,
                        afterSubmit:function(response, postdata) {
                        window.location = 'dialplan/list.action';
                        return [true,'Successfully Deleted'];
                        }
                        }"

                        gridModel="gridModel"
                        rowList="10,15,20"
                        rowNum="15"
                        rownumbers="true"
                        editurl="%{dialplanEditUrl}"
                        editinline="false"
                        multiselect="true"
                        viewrecords="true"
                        autowidth="false"     
                        navigatorExtraButtons="{
                        alert : { 
                        title : 'Add', 
                        icon: 'ui-icon-carat-1-n',
                        caption : 'Add', 
                        onclick: function(){ 
                        window.location = 'dialplan/show.action';                        
                        }
                        }
                        }"                 
                        >
                        <sjg:gridColumn 
                            name="dialplan_db_id" 
                            index="dialplan_db_id" 
                            title=""
                            formatter="integer" 
                            hidden="true"
                            />
                        <sjg:gridColumn 
                            name="dp_name" 
                            index="dp_name" 
                            title="Name" 
                            sortable="true" 
                            editable="true" 
                            edittype="text"
                            editrules="{
                            required : true
                            }"
                            search="true" 
                            searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                            align="center"                    
                            formatter="utilities.formatLink"
                            formatoptions="{url:'dialplan/retrieve.action',id_name:'id',col_name:'id'}"
                            />  
                        <sjg:gridColumn 
                            name="dp_description" 
                            index="dp_description" 
                            title="Description" 
                            sortable="false" 
                            editable="true" 
                            edittype="text"
                            search="false"
                            align="center"
                            />   
                        <sjg:gridColumn 
                            name="dp_dnis_pattern" 
                            index="dp_dnis_pattern" 
                            title="DNIS pattern" 
                            sortable="true" 
                            editable="true" 
                            editrules="{
                            required : true
                            }"
                            edittype="text"
                            search="true" 
                            searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                            align="center"
                            />  
                        <sjg:gridColumn 
                            name="dp_capacity"                      
                            index="dp_capacity" 
                            title="Capacity" 
                            formatter="integer" 
                            sortable="true" 
                            editable="true" 
                            edittype="text"
                            editrules="{
                            minValue : 0,
                            integer : true
                            }"
                            align="center"
                            width="120"
                            />
                        <sjg:gridColumn 
                            name="dp_ani_translate" 
                            index="dp_ani_translate" 
                            title="ANI Translate" 
                            sortable="true" 
                            editable="true" 
                            edittype="text"
                            search="true" 
                            searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                            align="center"
                            />  
                        <sjg:gridColumn 
                            name="dp_dnis_translate" 
                            index="dp_dnis_translate" 
                            title="DNIS Translate" 
                            sortable="true" 
                            editable="true" 
                            edittype="text"
                            search="true" 
                            searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                            align="center"
                            />  
                        <sjg:gridColumn 
                            name="dp_priority"                      
                            index="dp_priority" 
                            title="Priority" 
                            formatter="integer" 
                            sortable="true" 
                            editable="true" 
                            edittype="text"
                            editrules="{
                            integer:true
                            }"
                            search="true" 
                            searchtype="text"
                            searchoptions="{sopt:['eq','ne','lt','le','gt','ge']}"
                            align="center"
                            width="120"
                            />
                        <sjg:gridColumn 
                            name="dp_gateway_list" 
                            index="dp_gateway_list" 
                            title="GW List" 
                            sortable="true" 
                            editable="true" 
                            edittype="select"
                            editoptions="{multiple:true, dataUrl : '%{termGatewaySelectUrl}' }" 
                            search="false"
                            align="center"
                            width="100"
                            />  
                    </sjg:grid>

                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>         


    </body>
</html>
