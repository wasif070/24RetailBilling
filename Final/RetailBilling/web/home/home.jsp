<%@page import="com.ipvision.home.HomeLoader"%>
<%@page import="com.ipvision.home.Home"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>

<html>

    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Home</title>
        <style type="text/css">
            #browser {
                font-family: Verdana, helvetica, arial, sans-serif;
                font-size: 140%;
            }
        </style>
    </head>

    <body>       
        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>           
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content">                            
                       
                        <div class="min-heigth" style="min-height: 400px">
                            <div class="form_content" id="browser">
                                <%
                                    Users usr = (Users) request.getSession().getAttribute("userName");
                                    if (usr != null) {
                                        ArrayList<Home> homeList = HomeLoader.getInstance().getHomeDTOList(usr);
                                        if (homeList != null && homeList.size() > 0) {
                                            for (int i = 0; i < homeList.size(); i++) {
                                                Home dto = homeList.get(i);
                                %>   
                                <div class="ui-jqdialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding:10px;"><span class="ui-jqdialog-title">Welcome <%=dto.getClient_name()%></span></div>
                                <table border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                        <th>User:</th> 
                                        <td>                                       
                                            <%=dto.getClient_name()%>
                                        </td>
                                    </tr>
                                    <% if (usr.getRole_id() > 0) {%> 
                                    <tr>
                                        <th>Current Balance: </th>
                                        <td><%=dto.getClient_balance()%></td>   
                                    </tr>

                                    <tr>
                                        <th>Total Successful Calls:</th>
                                        <td><%=dto.getCalls()%></td>   
                                    </tr>
                                    <tr>
                                        <th>Total Call Duration:</th>
                                        <td><%=dto.getDuration()%></td> 
                                    </tr>   
                                    <%}
                                                }
                                            }
                                        }%>
                                </table>
                            </div>                        
                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>
    </body>
</html>


