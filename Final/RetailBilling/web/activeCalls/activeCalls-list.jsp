<%-- 
    Document   : active_calls
    Created on : Sep 26, 2013, 11:15:33 AM
    Author     : reefat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>


<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Active Calls</title>
    </head>

    <!--<body onload="JavaScript:timedRefresh(60);">-->    
    <body>


        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content ">
                        <s:url var="remoteurl" action="jsonListURL" namespace="/activeCalls" />
                        <div class="content_table">

                            <s:actionerror/>
                            <div class="action_message"><s:actionmessage cssStyle="color:blue" /></div>

                            <s:if test="hasActionErrors()"><p></p></s:if>
                            <s:if test="hasActionMessages()"><p></p></s:if>

                            <sjg:grid 
                                id="gridtable"                             
                                caption="Active Calls"
                                dataType="json"
                                draggable="true"
                                droppable="true"
                                href="%{remoteurl}"       
                                pager="true"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="15"
                                rownumbers="true"
                                navigator="true"
                                navigatorAdd="false"
                                navigatorEdit="false"
                                navigatorDelete="false"
                                navigatorRefresh="true"
                                viewrecords="true"
                                autowidth="false"
                                navigatorSearch="true"
                                navigatorSearchOptions="{multipleSearch:true}"
                                >
                                <sjg:gridColumn
                                    name="call_id"
                                    index="call_id"
                                    title="Call ID"
                                    align="center"
                                    search="true"
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                                    />
                                <sjg:gridColumn
                                    name="origin_caller"
                                    index="origin_caller"
                                    title="Origin Caller"
                                    align="center"
                                    search="true"
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                                    />
                                <sjg:gridColumn 
                                    name="origin_ip" 
                                    index="origin_ip" 
                                    title="Origin IP" 
                                    align="center" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                                    />
                                <sjg:gridColumn 
                                    name="term_callee" 
                                    index="term_callee" 
                                    title="Term Callee" 
                                    align="center" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                                    />
                                <sjg:gridColumn 
                                    name="term_ip" 
                                    index="term_ip" 
                                    title="Term IP" 
                                    align="center" 
                                    search="true"
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                                    />
                                <sjg:gridColumn 
                                    name="term_gw" 
                                    index="term_gw" 
                                    title="Term GW" 
                                    align="center" 
                                    search="true"
                                    searchoptions="{sopt:['eq','ne','bw','bn','ew','en','cn','nc']}"
                                    />
                                <sjg:gridColumn 
                                    name="invite_time" 
                                    index="invite_time" 
                                    title="Invite Time" 
                                    align="center" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','ge','lt','le'], dataInit:datePick, attr:{title:'Your Search Date'}}" 
                                    />
                                <sjg:gridColumn 
                                    name="accepted_time" 
                                    index="accepted_time" 
                                    title="Accepted Time" 
                                    align="center" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','ge','lt','le'], dataInit:datePick, attr:{title:'Your Search Date'}}" 
                                    />
                                <sjg:gridColumn 
                                    name="duration" 
                                    index="duration" 
                                    title="Call Duration" 
                                    align="center"
                                    searchoptions="{sopt:['eq','ne','lt','le','gt','ge'], dataInit:timePick, attr:{title:'Your Search Time'}}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="status" 
                                    index="status" 
                                    title="Status" 
                                    align="center" 
                                    search="true"
                                    />
                            </sjg:grid>
                            <div hidden id="hidden_div">
                                <sj:datepicker id="datetime" label="Select a DateTime" value="%{new java.util.Date()}" timepicker="true" timepickerShowSecond="true" timepickerFormat="hh:mm:ss"/>
                            </div>

                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div> 
        <script type="text/javascript">
            $(document).ready(function() {
                $("#hidden_div").hide();
            });

            setInterval(function() {
                utilities.autoRefreshGrid()
            }, 10000);

            datePick = function(elem) {
                $(elem).datetimepicker({
                    dateFormat: 'dd-mm-yy',
                    timeFormat: 'HH:mm:ss'
                });
            };

            timePick = function(elem) {
                $(elem).timepicker({
                    timeFormat: 'HH:mm:ss'
                });
            };
        </script>

    </body>
</html>
