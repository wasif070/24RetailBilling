<%-- 
    Document   : rateplan-opt
    Created on : Nov 20, 2013, 3:58:21 PM
    Author     : Wasif Islam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>

<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Rateplan</title>

    </head>
    <body>


        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content">
                        <div class="min-heigth" style="min-height: 400px">
                            <s:url var="StatusSelectUrl" action="selectStatusAction" namespace="/content"/>
                            <div class="form_content">
                                <div class="ui-jqdialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding:10px;"><span class="ui-jqdialog-title">Rate Plan Information</span></div>
                                <p>
                                    <s:set name="currentAction" value="action"/>    
                                    <s:actionerror />
                                    <s:actionmessage cssStyle="color:blue" />

                                    <s:form action="%{#currentAction}" namespace="/rateplan" method="post">
                                        <s:hidden id="rateplan_id" name="rateplan_id" />
                                        <s:textfield id="rateplan_name" name="rateplan_name" key="Name" />                           
                                        <s:textfield id="rateplan_des" name="rateplan_des" key="Description" />
                                        <sj:select id="rateplan_status" name="rateplan_status" key="Status" list="statusList" href="%{StatusSelectUrl}" />

                                        <s:submit key="Save" align="center" cssClass="button"/>
                                    </s:form>
                                </p>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>            

    </body>
</html>


