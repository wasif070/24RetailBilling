<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>

<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Dialplan Edit</title>
    </head>

    <body>


        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content">
                        <div class="min-heigth" style="min-height: 400px">
                            <s:url var="termGatewaySelectUrl" action="selectTermGatewayAction" namespace="/content"/>
                            <div class="form_content">
                                <div class="ui-jqdialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding:10px;"><span class="ui-jqdialog-title">Dialplan Information</span></div>

                                <p>
                                    <s:set name="currentAction" value="action"/>

                                    <s:form action="%{#currentAction}" namespace="/dialplan" method="post">

                                        <s:hidden id="id" name="id" />

                                        <s:if test="%{#currentAction=='add'}" >
                                            <s:textfield id="dp_name" name="dp_name" key="Name" required="true"/>
                                        </s:if>
                                        <s:else>
                                            <s:textfield id="dp_name" name="dp_name" key="Name" readonly="true" />
                                        </s:else>                            

                                        <s:textfield id="dp_description" key="Description" name="dp_description"/>
                                        <s:textfield id="dp_dnis_pattern" key="DNIS Pattern" name="dp_dnis_pattern" required="true"/>                            
                                        <s:textfield id="dp_capacity" key="Capacity" name="dp_capacity" />
                                        <s:textfield id="dp_ani_translate" key="ANI Translate" name="dp_ani_translate" />
                                        <s:textfield id="dp_dnis_translate" key="DNIS Translate" name="dp_dnis_translate" />
                                        <s:textfield id="dp_priority" key="Priority" name="dp_priority" />
                                        <sj:select onCompleteTopics="complete" id="dp_gateway_list" custom_prop="%{temp_dp_gateway_list}" key="Gateway List" multiple="true" name="dp_gateway_list" list="termGatewayList" href="%{termGatewaySelectUrl}"/>

                                        <s:submit key="Save" align="center" cssClass="button"/>
                                    </s:form>
                                </p>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>        
        <script>
            $(document).ready(function() {
                $.subscribe('complete', function(event, data) {
                    selectedMe();
                });
            });
            function selectedMe() {
                var sel = $("#dp_gateway_list").attr('custom_prop');

                if (sel != null && sel.length > 0) {
                    var v = sel.split(',');
                    $("#dp_gateway_list option").each(function()
                    {
                        for (var i = 0; i < v.length; i++) {
//                        alert(v[i] + "  <==>  " + $("#dp_gateway_list").val());
                            if (($(this).val()) == v[i]) {
                                $(this).attr('selected', true);
                            }
                        }
                    });
                }
            }
        </script>
    </body>
</html>


