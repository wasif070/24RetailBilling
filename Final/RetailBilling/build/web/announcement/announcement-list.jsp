<%-- 
    Document   : rateplan-list
    Created on : Nov 20, 2013, 3:58:11 PM
    Author     : Wasif Islam
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>

<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Announcement</title>

    </head>
    <body>


        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content ">
                        <s:url var="announcementEditUrl" action="delete" namespace="/announcement"/>
                        <s:url var="announcementRemoteUrl" action="jsonListURL" namespace="/announcement" />
                        <s:url var="StatusSelectUrl" action="selectStatusAction" namespace="/content"/>   
                        <s:url var="prioritySelectUrl" action="selectPriorityAction" namespace="/content"/>   
                        <s:url var="formatURL" action="announcement/retrieve.action" />
                        <div class="content_table">

                            <s:actionerror/>
                            <div class="action_message"><s:actionmessage cssStyle="color:blue" /></div>

                            <s:if test="hasActionErrors()"><p></p></s:if>
                            <s:if test="hasActionMessages()"><p></p></s:if>

                            <sjg:grid 
                                id="gridtable"
                                caption="Rateplan"
                                dataType="json"
                                draggable="true"
                                droppable="true"
                                href="%{announcementRemoteUrl}"
                                pager="true"                        
                                navigator="true"
                                navigatorSearch="true"
                                navigatorSearchOptions="{multipleSearch:true}"     
                                navigatorAdd="false"                             
                                navigatorEdit="false"            
                                navigatorView="false"
                                navigatorDelete="true"
                                navigatorDeleteOptions="{
                                reloadAfterSubmit:true,
                                afterSubmit:function(response, postdata) {
                                window.location = 'announcement/list.action';
                                return [true,'Successfully Deleted'];
                                }
                                }"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="15"
                                rownumbers="true"
                                editurl="%{announcementEditUrl}"  
                                editinline="false"
                                multiselect="true"
                                viewrecords="true"
                                autowidth="true" 
                                navigatorExtraButtons="{
                                alert : { 
                                title : 'Add', 
                                icon: 'ui-icon-carat-1-n',
                                caption : 'Add', 
                                onclick: function(){ 
                                window.location = 'announcement/show.action';                        
                                }
                                }
                                }"
                                >
                                <sjg:gridColumn 
                                    name="announcement_id"
                                    index="announcement_id" 
                                    align="center" 
                                    title="ID" 
                                    sortable="false"  
                                    editable="true" 
                                    edittype="text"                           
                                    hidden="true" 
                                    />  
                                <sjg:gridColumn 
                                    name="announcement_name" 
                                    index="announcement_name" 
                                    align="center" 
                                    title="Announcement Name" 
                                    sortable="true" 
                                    search="true" 
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="true" 
                                    editrules="{required:true}"
                                    edittype="text" 
                                    formatter="utilities.formatLink"
                                    formatoptions="{url:'announcement/retrieve.action',id_name:'id',col_name:'announcement_id'}"
                                    />
                                <sjg:gridColumn 
                                    name="announcement_priority" 
                                    index="announcement_priority" 
                                    align="center" 
                                    title="Priority" 
                                    sortable="true" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{prioritySelectUrl}'}" 
                                    editable="true" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{prioritySelectUrl}' }" 
                                    />
                                <sjg:gridColumn 
                                    name="announcement_status_name" 
                                    index="announcement_status_name" 
                                    align="center" 
                                    title="Status" 
                                    sortable="true" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{StatusSelectUrl}'}" 
                                    editable="true" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{StatusSelectUrl}' }" 
                                    />    
                            </sjg:grid>

                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>         
    </body>

</html>