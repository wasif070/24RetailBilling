<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Client Edit</title>
    </head>
    <body>
        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content">
                        <div class="min-heigth" style="min-height: 400px">
                            <s:url var="rateplanSelectUrl" action="selectRatePlanAction"  namespace="/content" />
                            <s:url var="clientTypeSelectUrl" action="selectClientTypeAction"  namespace="/content" />
                            <s:url var="statusSelectUrl" action="selectStatusAction"  namespace="/content" />
                            <div class="form_content">
                                <div class="ui-jqdialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding:10px;"><span class="ui-jqdialog-title">Client Information</span></div>
                                <p>
                                    <s:set name="currentAction" value="action"/>    
                                    <s:actionerror />
                                    <s:actionmessage cssStyle="color:blue" />

                                    <s:form action="%{#currentAction}" namespace="/client" method="post">
                                        <%--<s:hidden id="client_db_id" name="client_db_id" />--%>
                                        <s:hidden id="id" name="id" />

                                        <s:if test="%{#currentAction=='add'}" >
                                            <s:textfield id="client_id" name="client_id" key="ID" required="true"/>
                                        </s:if>
                                        <s:else>
                                            <s:textfield id="client_id" name="client_id" key="ID" readonly="true"/>
                                        </s:else>                            
                                        <s:password id="client_password" key="Password" name="client_password" required="true" showPassword="true" />
                                        <s:textfield id="client_name" key="Name" name="client_name" />                            
                                        <s:textfield id="client_email" key="Email" name="client_email" />
                                        <%if (users.getRole_id() < 0) {%>
                                        <sj:select id="rateplan_id" key="rateplan" name="rateplan_id" list="ratePlanList" href="%{rateplanSelectUrl}" />
                                        <sj:select id="client_type" key="client type" name="client_type" list="clientTypeList" href="%{clientTypeSelectUrl}" /> 
                                        <sj:select id="client_status" key="client status" name="client_status" list="statusList" href="%{statusSelectUrl}" />                            
                                        <s:textfield id="client_credit_limit" key="Credit Limit" name="client_credit_limit" />
                                        <s:if test="%{#currentAction=='add'}" >
                                            <s:textfield id="client_balance" key="Balance" name="client_balance"/>
                                        </s:if>
                                        <s:else>
                                            <s:textfield id="client_balance" key="Balance" name="client_balance" readonly="true"/>
                                        </s:else>
                                        <s:textfield id="prefix" key="Client Prefix" name="prefix" />
                                        <s:textfield id="client_call_limit" key="Call Limit" name="client_call_limit" />
                                        <%}%>
                                        <s:submit value="Save" align="center" cssClass="button"/>
                                    </s:form>
                                </p>
                            </div>                           
                        </div> 
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>        
    </body>
</html>


