<%-- 
    Document   : report-list
    Created on : Nov 24, 2013, 2:40:52 PM
    Author     : Wasif Islam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>

<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Client</title>

    </head>
    <body>



        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content ">
                        <s:url var="paymentRemoteUrl" action="jsonListURL" namespace="/payment" />
                        <div class="content_table">

                            <s:actionerror/>
                            <div class="action_message"><s:actionmessage cssStyle="color:blue" /></div>

                            <s:if test="hasActionErrors()"><p></p></s:if>
                            <s:if test="hasActionMessages()"><p></p></s:if>


                            <sjg:grid 
                                id="gridtable"
                                caption="Payments"
                                dataType="json"
                                draggable="true"
                                droppable="true"
                                href="%{paymentRemoteUrl}"
                                pager="true"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="15"
                                rownumbers="true"
                                viewrecords="true"
                                editurl="false"
                                navigator="true"
                                navigatorEdit="false"
                                navigatorAdd="false"
                                navigatorDelete="false"
                                navigatorSearch="true"
                                navigatorSearchOptions="{multipleSearch:true}"
                                autowidth="true"
                                >
                                <sjg:gridColumn 
                                    name="payment_id" 
                                    index="payment_id" 
                                    title="payment_id" 
                                    sortable="false"
                                    align="center"
                                    search="false" 
                                    editable="false" 
                                    hidden="true"/>
                                <sjg:gridColumn 
                                    name="client_id" 
                                    index="client_id" 
                                    title="Client" 
                                    sortable="false"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="false" />
                                <sjg:gridColumn 
                                    name="cur_balance" 
                                    index="cur_balance" 
                                    title="Current Balance" 
                                    sortable="true"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','ge','lt','le']}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="payment_type_name" 
                                    index="payment_type_name" 
                                    title="Payment Type" 
                                    sortable="true"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne']}" 
                                    editable="false" />
                                <sjg:gridColumn 
                                    name="payment_recharge" 
                                    index="payment_recharge" 
                                    title="Recharge" 
                                    sortable="true"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','ge','lt','le']}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="payment_return" 
                                    index="payment_return" 
                                    title="Return" 
                                    sortable="true"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','ge','lt','le']}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="payment_receive" 
                                    index="payment_receive" 
                                    title="Receive" 
                                    sortable="true"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','ge','lt','le']}" 
                                    editable="false"/>
                                <sjg:gridColumn 
                                    name="payment_date" 
                                    index="payment_date" 
                                    title="Transaction Date" 
                                    sortable="true"
                                    align="center"
                                    search="true" 
                                    searchoptions="{sopt:['gt','ge','lt','le'], dataInit:datePick, attr:{title:'Your Search Date'}}" 
                                    editable="false"/>
                            </sjg:grid>
                            <div hidden  id="hidden_div">
                                <sj:datepicker id="date" label="Select a Date" />
                            </div>

                        </div>
                    </td>
                </tr>
            </table>
            <script type="text/javascript">
                $(document).ready(function() {
                    $("#hidden_div").hide();
                });

                datePick = function(elem) {
                    $(elem).datepicker();
                };
            </script>
            <%@include file="../includes/include_footer.jsp" %>
        </div>             

    </body>
</html>