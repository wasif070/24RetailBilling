
jQuery(document).ready(function() {
    utilities.init();
});

var customers = {
    row_count: 0,
    init: function() {
        $j('.receiver').click(function() {
            customers.customerList(this);
            return true;
        });
        $j('.customer').on('click', function() {
            customers.customerInfo(this);
            return true;
        });
    }
};

var utilities = {
    formatURL: '',
    init: function() {
        $j('#gateway_type').on('change', function() {
            utilities.getClientList(this);
        });
//        $('.left_content ul li a').hover(function() {
//            $(this).addClass("sel");
//        }, function() {
////            alert('remove');
//            $(this).removeClass("sel");
//        });
    },
    getClientList: function(obj) {
        $j.ajax({
            type: 'POST',
            async: false,
            url: base_url + 'content/selectClientNameAction.action',
            data: {
                gateway_type: $j(obj).val(),
                time: new Date().getMilliseconds()
            },
            dataType: 'json',
            success: function(json) {
                var html = '';
                $j.each(json.clientNameList, function(key, val) {
                    
                    html += '<option value=' + key + '>' + val + '</option>'
                });
                $j('#client_id').html(html);
            },
            error: function(e, m, s) {
                alert(e.responseText);
            }
        });
    }, formatLink: function(cellvalue, options, rowObject) {
        //options=[rowId:id,colModel:cm]
        var formatoptions = options.colModel['formatoptions'];
        return "<a href='" + formatoptions.url + "?" + formatoptions.id_name + "=" + rowObject[formatoptions.col_name] + "'>" + cellvalue + "</a>";
    }, autoRefreshGrid: function() {
        $("#gridtable").trigger("reloadGrid");
    }, selectedItem: function() {
        var url = window.location.href;
//        alert(url);
        $("#browser a").each(function() {            
            if (url == (this.href)) {
//                alert('1');
//                $(this).closest("li").addClass("active");
                $(this).addClass("sel");
            }
        });
//        alert('selected...');
    }

};

