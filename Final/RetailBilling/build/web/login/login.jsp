<%-- 
    Document   : login
    Created on : Sep 26, 2013, 9:54:36 AM
    Author     : reefat
--%>

<%@page import="com.ipvision.login.LoginLoader"%>
<%@page import="java.util.ArrayList"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <title>Billing</title>
        <%@include file="../includes/header.jsp" %>
        <link id="jquery_theme_link" rel="stylesheet" href="/RetailBilling/struts/themes/redmond/jquery-ui.css?s2j=3.6.1" type="text/css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>  
        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <div class='login_bg'>
                <div class="ui-jqdialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding:10px;"><span class="ui-jqdialog-title">Login</span></div>
                <div class='login_box'>
                    <div class="table">
                        <s:actionerror />
                        <s:form action="login" method="post" namespace="/">
                            <table>
                                <tr><th>User Name: </th><td><s:textfield name="username" key="label.username" size="20" theme="simple" /></td></tr>
                                <tr><th>Password: </th><td><s:password name="password" key="label.password" size="20" theme="simple" /></td></tr>
                                <tr><td colspan="2" align="center">
                                        <s:submit method="execute" key="label.login" align="center" cssClass="button" theme="simple"/>
                                        <s:reset key="label.reset" align="center" cssClass="button" theme="simple"/>
                                        <div class="clear"></div>
                                    </td>
                                </tr>
                            </table>
                        </s:form>
                    </div>
                </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0">
                <%
                    ArrayList<String> announcementList = LoginLoader.getInstance().getLoginDTOList();
                    if (announcementList != null && announcementList.size() > 0) {
                        for (int i = 0; i < announcementList.size(); i++) {
                            String announce = announcementList.get(i);
                %>   
                <tr><marquee><%=announce%></marquee></tr>
                    <%}
                        }%>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>
    </body>
</html>
