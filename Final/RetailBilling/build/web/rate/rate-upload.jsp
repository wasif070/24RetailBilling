<%-- 
    Document   : rates
    Created on : Nov 3, 2013, 1:19:49 PM
    Author     : Wasif Islam
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Rate Upload</title>

    </head>
    <body>


        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content">
                        <div class="min-heigth" style="min-height: 400px">
                            <s:url var="rateplanSelectUrl" action="selectRatePlanAction"  namespace="/content" />
                            <s:url var="clientTypeSelectUrl" action="selectClientTypeAction"  namespace="/content" />
                            <s:url var="statusSelectUrl" action="selectStatusAction"  namespace="/content" />
                            <div class="form_content">
                                <div class="ui-jqdialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding:10px;"><span class="ui-jqdialog-title">Upload Rate CSV File</span></div> 
                                    <s:property value="currentAction" />
                                    <s:set name="currentAction" value="action"/>
                                    <s:actionerror />
                                    <s:actionmessage cssStyle="color:blue" />

                                    <s:form action="uploadRates" namespace="/rate" enctype="multipart/form-data">                       
                                    <table class="input_table" style="width:100%;" cellspacing="0" cellpadding="0" >
                                        <tbody>

                                            <tr>
                                                <td colspan="2" align="center"  valign="bottom">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center"  valign="bottom">

                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">Note: Data Format[Prefix, Destination Name,Rate(Per Min), First Pulse, Next Pulse, Grace Period, Failed Period,Day,From Hour,From Min,To Hour, To Min] <br />Example: 880,Bangladesh,1.2,30,10,10,10,-1,0,0,23,59</td>
                                            </tr>
                                            <tr>
                                                <th valign="top" >Upload File</th>
                                                <td valign="top">                                      
                                                    <s:file name="upload" label="" accept=".csv,text/csv" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <td>	
                                                    <s:submit value="Upload" align="center" cssClass="button"/>
                                                </td>
                                            </tr>
                                            <tr><td colspan="2" align="center">Day [-1=>All, 0=>Sunday, 1=>Monday, 2=>Tuesday, 3=>Wednesday, 4=>Thursday, 5=>Friday, 6=>Saturday]</td></tr>
                                        </tbody>
                                    </table>
                                    <div class="blank-height"></div>
                                </s:form>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>        
    </body>
</html>
