<%-- 
    Document   : rate-list
    Created on : Nov 21, 2013, 12:42:24 PM
    Author     : Wasif Islam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>

<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Rate</title>

    </head>
    <body>



        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content ">

                        <s:url var="rateEditUrl" action="delete" namespace="/rate"/>
                        <s:url var="rateRemoteUrl" action="jsonListURL" namespace="/rate" />
                        <s:url var="StatusSelectUrl" action="selectStatusAction" namespace="/content"/>   
                        <s:url var="formatURL" action="rate/retrieve.action" />
                        <s:url var="selectMinUrl" action="selectMinAction" namespace="/content"/>
                        <s:url var="selectHourUrl" action="selectHourAction" namespace="/content"/>
                        <s:url var="selectDayUrl" action="selectDayAction" namespace="/content"/>
                        <div class="content_table">

                            <s:actionerror/>
                            <div class="action_message"><s:actionmessage cssStyle="color:blue" /></div>

                            <s:if test="hasActionErrors()"><p></p></s:if>
                            <s:if test="hasActionMessages()"><p></p></s:if>

                            <%
                                Users usr = (Users) request.getSession().getAttribute("userName");
                                if (usr != null && usr.getRole_id() < 0) {
                            %>
                            <sjg:grid 
                                id="gridtable"
                                caption="Rate"
                                dataType="json"
                                draggable="true"
                                droppable="true"
                                href="%{rateRemoteUrl}"
                                pager="true"                        
                                navigator="true"
                                navigatorSearch="true"
                                navigatorSearchOptions="{multipleSearch:true}"     
                                navigatorAdd="false"                             
                                navigatorEdit="false"            
                                navigatorView="false"
                                navigatorDelete = "true"
                                navigatorDeleteOptions="{
                                reloadAfterSubmit:true,
                                afterSubmit:function(response, postdata) {
                                window.location = 'rate/list.action';
                                return [true,'Successfully Deleted'];
                                }
                                }"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="15"
                                rownumbers="true"
                                editurl="%{rateEditUrl}"  
                                editinline="false"
                                multiselect="true"
                                viewrecords="true"
                                autowidth="true" 
                                navigatorExtraButtons="{
                                add : { 
                                title : 'Add', 
                                icon: 'ui-icon-carat-1-n',
                                caption : 'Add', 
                                onclick: function(){ 
                                window.location = 'rate/show.action?action=add';                        
                                }
                                },
                                upload:  { 
                                title : 'Upload', 
                                icon: 'ui-icon-arrowthickstop-1-n', 
                                caption : 'Upload', 
                                onclick: function(){ 
                                window.location = 'rate/rate-upload.jsp';                        
                                }
                                },
                                download:  { 
                                title : 'Download', 
                                icon: 'ui-icon-arrowthickstop-1-s', 
                                caption : 'Download', 
                                onclick: function(){ 
                                window.location = 'rate/downloadRates.action';                        
                                }
                                }
                                }"

                                >
                                <sjg:gridColumn 
                                    name="rate_id" 
                                    index="rate_id" 
                                    align="center"
                                    title="Rate ID" 
                                    sortable="false" 
                                    hidden="true" 
                                    search="false" 
                                    editable="true"
                                    edittype="text"/>  
                                <sjg:gridColumn 
                                    name="rate_destination_code" 
                                    index="rate_destination_code" 
                                    align="center"
                                    title="Prefix" 
                                    sortable="true" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','bw','ew','cn']}" 
                                    editable="true" 
                                    editrules="{number:true, required:true}"
                                    edittype="text" 
                                    formatter="utilities.formatLink"
                                    formatoptions="{url:'rate/retrieve.action',id_name:'id',col_name:'rate_id'}"/>  
                                <sjg:gridColumn 
                                    name="rate_destination_name" 
                                    index="rate_destination_name" 
                                    align="center"
                                    title="Name" 
                                    sortable="true" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','bw','ew','cn']}" 
                                    editable="true" 
                                    edittype="text" />
                                <sjg:gridColumn 
                                    name="rate_per_min" 
                                    index="rate_per_min" 
                                    align="center"
                                    title="Rate per min." 
                                    sortable="true" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','lt']}" 
                                    editable="true" 
                                    editrules="{number:true, required:true}"
                                    edittype="text"/>
                                <sjg:gridColumn 
                                    name="rate_first_pulse" 
                                    index="rate_first_pulse"
                                    align="center"
                                    title="First pulse" 
                                    sortable="false" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','lt']}" 
                                    editable="true" 
                                    editrules="{number:true}"
                                    edittype="text" />    
                                <sjg:gridColumn 
                                    name="rate_next_pulse" 
                                    index="rate_next_pulse" 
                                    align="center"
                                    title="Next pulse" 
                                    sortable="false" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','lt']}" 
                                    editable="true" 
                                    editrules="{number:true}"
                                    edittype="text" />
                                <sjg:gridColumn 
                                    name="rate_grace_period" 
                                    index="rate_grace_period" 
                                    align="center"
                                    title="Grace Period" 
                                    sortable="false" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','lt']}" 
                                    editable="true" 
                                    editrules="{number:true}"
                                    edittype="text" />    
                                <sjg:gridColumn 
                                    name="rate_failed_period" 
                                    index="rate_failed_period"
                                    align="center"
                                    title="Failed Period" 
                                    sortable="false" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','lt']}" 
                                    editable="true" 
                                    editrules="{number:true}"
                                    edittype="text" 
                                    />    
                                <sjg:gridColumn 
                                    name="rate_day_name" 
                                    index="rate_day_name" 
                                    align="center"
                                    title="Day" 
                                    sortable="false" 
                                    surl="%{selectDayUrl}" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'] , dataUrl : '%{selectDayUrl}'}" 
                                    editable="true" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{selectDayUrl}' }" />
                                <sjg:gridColumn 
                                    name="rate_from_hour" 
                                    index="rate_from_hour" 
                                    align="center"
                                    title="From Hour" 
                                    sortable="false" 
                                    surl="%{selectHourUrl}" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{selectHourUrl}'}" 
                                    editable="true"                             
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{selectHourUrl}' }"/>      
                                <sjg:gridColumn 
                                    name="rate_from_min" 
                                    index="rate_from_min" 
                                    align="center"
                                    title="From Min." 
                                    sortable="false" 
                                    surl="%{selectMinUrl}" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{selectMinUrl}'}" 
                                    editable="true" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{selectMinUrl}' }" />
                                <sjg:gridColumn 
                                    name="rate_to_hour" 
                                    index="rate_to_hour" 
                                    align="center"
                                    title="To Hour" 
                                    sortable="false" 
                                    surl="%{selectHourUrl}" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{selectHourUrl}'}" 
                                    editable="true" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{selectHourUrl}' }" />   
                                <sjg:gridColumn 
                                    name="rate_to_min" 
                                    index="rate_to_min" 
                                    align="center"
                                    title="To Min." 
                                    sortable="false" 
                                    surl="%{selectMinUrl}" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{selectMinUrl}'}" 
                                    editable="true" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{selectMinUrl}' }" />
                                <sjg:gridColumn 
                                    name="rate_status_name" 
                                    index="rate_status_name"
                                    align="center"
                                    title="Status" 
                                    sortable="false" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne']}" 
                                    editable="true" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{StatusSelectUrl}' }"    
                                    />
                            </sjg:grid>       
                            <%} else {%>
                            <sjg:grid 
                                id="gridtable"
                                caption="Rate"
                                dataType="json"
                                draggable="true"
                                droppable="true"
                                href="%{rateRemoteUrl}"
                                pager="true"                        
                                navigator="true"
                                navigatorSearch="true"
                                navigatorSearchOptions="{multipleSearch:true}"     
                                navigatorAdd="false"                             
                                navigatorEdit="false"            
                                navigatorView="false"
                                navigatorDelete = "false"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="15"
                                rownumbers="true"
                                editurl="%{rateEditUrl}"  
                                editinline="false"
                                multiselect="true"
                                viewrecords="true"
                                autowidth="true" 
                                >
                                <sjg:gridColumn 
                                    name="rate_id" 
                                    index="rate_id" 
                                    align="center"
                                    title="Rate ID" 
                                    sortable="false" 
                                    hidden="true" 
                                    search="false" 
                                    editable="true"
                                    edittype="text"/>  
                                <sjg:gridColumn 
                                    name="rate_destination_code" 
                                    index="rate_destination_code" 
                                    align="center"
                                    title="Prefix" 
                                    sortable="true" 
                                    search="true" 
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="false" 
                                    edittype="text" />  
                                <sjg:gridColumn 
                                    name="rate_destination_name" 
                                    index="rate_destination_name" 
                                    align="center"
                                    title="Name" 
                                    sortable="true" 
                                    search="true" 
                                    searchoptions="{sopt:['bw','ew','cn']}" 
                                    editable="true" 
                                    edittype="text" />
                                <sjg:gridColumn 
                                    name="rate_per_min" 
                                    index="rate_per_min" 
                                    align="center"
                                    title="Rate per min." 
                                    sortable="true" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','lt']}" 
                                    editable="false" 
                                    editrules="{number:true, required:true}"
                                    edittype="text"/>
                                <sjg:gridColumn 
                                    name="rate_first_pulse" 
                                    index="rate_first_pulse"
                                    align="center"
                                    title="First pulse" 
                                    sortable="false" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','lt']}" 
                                    editable="false" 
                                    editrules="{number:true}"
                                    edittype="text" />    
                                <sjg:gridColumn 
                                    name="rate_next_pulse" 
                                    index="rate_next_pulse" 
                                    align="center"
                                    title="Next pulse" 
                                    sortable="false" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','lt']}" 
                                    editable="false" 
                                    editrules="{number:true}"
                                    edittype="text" />
                                <sjg:gridColumn 
                                    name="rate_grace_period" 
                                    index="rate_grace_period" 
                                    align="center"
                                    title="Grace Period" 
                                    sortable="false" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','lt']}" 
                                    editable="false" 
                                    editrules="{number:true}"
                                    edittype="text" />    
                                <sjg:gridColumn 
                                    name="rate_failed_period" 
                                    index="rate_failed_period"
                                    align="center"
                                    title="Failed Period" 
                                    sortable="false" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne','gt','lt']}" 
                                    editable="false" 
                                    editrules="{number:true}"
                                    edittype="text" 
                                    />    
                                <sjg:gridColumn 
                                    name="rate_day_name" 
                                    index="rate_day_name" 
                                    align="center"
                                    title="Day" 
                                    sortable="false" 
                                    surl="%{selectDayUrl}" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'] , dataUrl : '%{selectDayUrl}'}" 
                                    editable="false" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{selectDayUrl}' }" />
                                <sjg:gridColumn 
                                    name="rate_from_hour" 
                                    index="rate_from_hour" 
                                    align="center"
                                    title="From Hour" 
                                    sortable="false" 
                                    surl="%{selectHourUrl}" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{selectHourUrl}'}" 
                                    editable="false"                             
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{selectHourUrl}' }"/>      
                                <sjg:gridColumn 
                                    name="rate_from_min" 
                                    index="rate_from_min" 
                                    align="center"
                                    title="From Min." 
                                    sortable="false" 
                                    surl="%{selectMinUrl}" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{selectMinUrl}'}" 
                                    editable="false" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{selectMinUrl}' }" />
                                <sjg:gridColumn 
                                    name="rate_to_hour" 
                                    index="rate_to_hour" 
                                    align="center"
                                    title="To Hour" 
                                    sortable="false" 
                                    surl="%{selectHourUrl}" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{selectHourUrl}'}" 
                                    editable="true" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{selectHourUrl}' }" />   
                                <sjg:gridColumn 
                                    name="rate_to_min" 
                                    index="rate_to_min" 
                                    align="center"
                                    title="To Min." 
                                    sortable="false" 
                                    surl="%{selectMinUrl}" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne'], dataUrl : '%{selectMinUrl}'}" 
                                    editable="false" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{selectMinUrl}' }" />
                                <sjg:gridColumn 
                                    name="rate_status_name" 
                                    index="rate_status_name"
                                    align="center"
                                    title="Status" 
                                    sortable="false" 
                                    search="true" 
                                    searchoptions="{sopt:['eq','ne']}" 
                                    editable="false" 
                                    edittype="select" 
                                    editoptions="{ dataUrl : '%{StatusSelectUrl}' }"    
                                    />
                            </sjg:grid>   
                            <%}%>

                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>         
    </body>
</html>