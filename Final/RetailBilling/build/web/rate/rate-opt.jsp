<%-- 
    Document   : rateplan-opt
    Created on : Nov 20, 2013, 3:58:21 PM
    Author     : Wasif Islam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>

<html>
    <head>
        <%@include file="../includes/header.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <sj:head jqueryui="true" jquerytheme="redmond" />
        <title>Rate</title>

    </head>
    <body>



        <div id="body_container">
            <%@include file="../includes/include_header.jsp" %>
            <table>
                <tr>
                    <td class="left_content">
                        <%@include file="../includes/left_menu.jsp" %>
                    </td>
                    <td class="right_content">
                        <div class="min-heigth" style="min-height: 400px">
                            <s:url var="StatusSelectUrl" action="selectStatusAction" namespace="/content"/>
                            <s:url var="selectDayUrl" action="selectDayAction" namespace="/content"/>
                            <s:url var="selectMinUrl" action="selectMinAction" namespace="/content"/>
                            <s:url var="selectHourUrl" action="selectHourAction" namespace="/content"/>
                            <div class="form_content">
                                <div class="ui-jqdialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="padding:10px;"><span class="ui-jqdialog-title">Rate Information</span></div>
                                <p>
                                    <s:set name="currentAction" value="action"/>    
                                    <s:actionerror />
                                    <s:actionmessage cssStyle="color:blue" />

                                    <s:form action="%{#currentAction}" namespace="/rate" method="post">
                                        <s:hidden id="rate_id" name="rate_id" />
                                        <s:hidden id="rateplan_id" name="rateplan_id" />
                                        <s:textfield id="rate_destination_code" name="rate_destination_code" key="Prefix" />
                                        <s:textfield id="rate_destination_name" name="rate_destination_name" key="Name" />
                                        <s:textfield id="rate_per_min" name="rate_per_min" key="Rate per minute" />
                                        <s:textfield id="rate_first_pulse" name="rate_first_pulse" key="First pulse" />
                                        <s:textfield id="rate_next_pulse" name="rate_next_pulse" key="Next pulse" />
                                        <s:textfield id="rate_grace_period" name="rate_grace_period" key="Grace period" />
                                        <s:textfield id="rate_failed_period" name="rate_failed_period" key="Failed period" />
                                        <sj:select id="rate_day" name="rate_day" key="Day" list="dayList" href="%{selectDayUrl}"/>
                                        <sj:select id="rate_from_hour" name="rate_from_hour" key="From hour" list="hourList" href="%{selectHourUrl}"/>
                                        <sj:select id="rate_from_min" name="rate_from_min" key="From minute" list="minList" href="%{selectMinUrl}"/>
                                        <sj:select id="rate_to_hour" name="rate_to_hour" key="To hour" list="hourList" href="%{selectHourUrl}"/>
                                        <sj:select id="rate_to_min" name="rate_to_min" key="To minute" list="minList" href="%{selectMinUrl}"/>
                                        <sj:select id="rate_status" name="rate_status" key="Status" list="statusList" href="%{StatusSelectUrl}" />
                                        <s:submit key="Save" align="center"  cssClass="button"/>
                                    </s:form>
                                </p>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <%@include file="../includes/include_footer.jsp" %>
        </div>        
    </body>
</html>


