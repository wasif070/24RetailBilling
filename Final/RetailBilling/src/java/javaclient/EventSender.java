/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaclient;

import com.ipvision.log.DBLoggerConstant;
import com.ipvision.log.IPVLogger;
import databaseconnector.DBConnection;
import java.io.*;
import java.net.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import util.Constants;

public class EventSender {

    static IPVLogger logger = new IPVLogger();
    private PreparedStatement ps = null;
    private Statement stmt = null;

    public static synchronized void sendEventMsg(String msg) {
        try {
            logger.addLogEntry("EventSender", DBLoggerConstant.INFO, DBLoggerConstant.OPERATION, "Sending client info to switch (Inside sendEventMsg)", msg, EventSender.class.getName());
            if (Constants.getAuth_domains() == null || Constants.getAuth_domains().length() < 1 || Constants.getAuth_port() == null) {
                try {
                    logger.addLogEntry("EventSender", DBLoggerConstant.INFO, DBLoggerConstant.OPERATION, "Read ip + port from db", Constants.getAuth_domains(), EventSender.class.getName());
                    DBConnection local_dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                    String sql = "";
                    PreparedStatement prepared_statement = null;
                    ResultSet rs = null;
                    sql = "select config_name,config_value from configurations where config_enable = 1 AND (config_name = 'host_ip' OR config_name = 'auth_port')";
                    prepared_statement = local_dbConnection.connection.prepareStatement(sql);
                    rs = prepared_statement.executeQuery();
                    while (rs.next()) {
                        String field = rs.getString("config_name");
                        if (field.equalsIgnoreCase("auth_port")) {
                            Constants.setAuth_port(rs.getInt("config_value"));
                        } else if (field.equalsIgnoreCase("host_ip")) {
                            Constants.setAuth_domains(rs.getString("config_value"));
                        }
                    }
                } catch (Exception e) {
                    System.out.println(""+e);
                }

            }
            logger.addLogEntry("EventSender", DBLoggerConstant.INFO, DBLoggerConstant.OPERATION, "domain : port --> ", Constants.getAuth_domains() + " -- " + Constants.getAuth_port(), EventSender.class.getName());

            Socket socket = new Socket(Constants.getAuth_domains(), Constants.getAuth_port());
            DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
            outToServer.writeBytes(msg);
            socket.close();
            
        } catch (IOException ex) {
            logger.addLogEntry("EventSender", DBLoggerConstant.ERROR, DBLoggerConstant.OPERATION, "Error in sending msg: ", ex.getMessage(), EventSender.class.getName());
//            System.out.println("Error in sending msg: " + ex.getMessage());
        }
    }

//    public static void main(String[] args) {
//        sendEventMsg("sdsd");
//    }
}
