/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.user;

import com.ipvision.clients.*;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import util.Constants;
import util.MyAppError;
import util.Utils;

/**
 *
 * @author reefat
 */
public class UserDAO {

    private PreparedStatement ps = null;
    protected DBConnection dbConnection = null;
    private static long last_update_time = 0;

    static {
        user_list = buildList();
    }
    static Map<String, Users> user_list;
    static Logger logger = Logger.getLogger(UserDAO.class.getName());

    public static Map<String, Users> buildList() {
        Map<String, Users> v_temp_user_list = new HashMap<String, Users>();
        DBConnection local_dbConnection = null;
        String sql = "";
        PreparedStatement prepared_statement = null;
        ResultSet rs = null;
        try {
            local_dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            sql = "select * from users where user_delete = 0 order by id asc";
            prepared_statement = local_dbConnection.connection.prepareStatement(sql);
            rs = prepared_statement.executeQuery();
            while (rs.next()) {
                Users users = new Users();
                users.setDb_id(rs.getInt("id"));
                users.setUser_id(rs.getString("user_id"));
                users.setRole_id(rs.getInt("role_id"));
                users.setUser_password(rs.getString("user_password"));
                users.setUser_status(rs.getInt("user_status"));
                users.setUser_full_name(rs.getString("user_full_name"));
                users.setUser_email("");
                v_temp_user_list.put(users.getUser_id(), users);
            }

        } catch (Exception ex) {
            logger.debug("Exception in UserDAO , buildList() ---> " + ex.getMessage());
        } finally {
            try {
                if (prepared_statement != null) {
                    prepared_statement.close();
                }
            } catch (Exception e) {
            }
            try {

                if (local_dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(local_dbConnection);
                }
            } catch (Exception e) {
            }
        }

        List<Client> client_list_local = ClientDAO.getClient_list();
        for (Client client : client_list_local) {
            Users users = new Users();
            users.setUser_id(client.getClient_id());
            users.setDb_id(client.getClient_db_id());
            users.setUser_password(client.getClient_password());
            users.setRole_id(1);
            users.setUser_full_name(client.getClient_name());
            users.setUser_status(Utils.findIndex(Constants.getSTATUS_STRING(), client.getClient_status()));
            users.setUser_email(client.getClient_email());

            v_temp_user_list.put(users.getUser_id(), users);
        }
        return v_temp_user_list;
    }

    public void addSingleClient(Client client) {
        Users users = new Users();
        users.setUser_id(client.getClient_id());
        users.setDb_id(client.getClient_db_id());
        users.setUser_password(client.getClient_password());
        users.setUser_full_name(client.getClient_name());
        users.setRole_id(1);
        users.setUser_status(Utils.findIndex(Constants.getSTATUS_STRING(), client.getClient_status()));
        users.setUser_email(client.getClient_email());

        user_list.put(users.getUser_id(), users);
    }

    public void editSingleClient(Client client) {
        if (user_list != null) {
            Users users = user_list.get(client.getClient_id());
            users.setUser_password(client.getClient_password());
            users.setUser_full_name(client.getClient_name());
            users.setUser_email(client.getClient_email());
            if (users.getRole_id() < 0) {
                users.setUser_status(Utils.findIndex(Constants.getSTATUS_STRING(), client.getClient_status()));
            }
            user_list.put(users.getUser_id(), users);
        }
    }

    public void removeClient(ArrayList<Client> param_client_list) {
        String key;
        if (user_list != null) {
            for (Client client : param_client_list) {
                key = client.getClient_id();
                if (user_list.containsKey(key)) {
                    user_list.remove(key);
                }
            }
        }
    }

    MyAppError save(Users users) {
        System.out.println("--------------------------------------- Not implemented yet --------------------------");
        throw new UnsupportedOperationException("Not supported yet.");
    }

    MyAppError update(Client client) {
        System.out.println("--------------------------------------- Not implemented yet --------------------------");
        throw new UnsupportedOperationException("Not supported yet.");
    }

    MyAppError delete(ArrayList<Client> param_client_list) {
        System.out.println("--------------------------------------- Not implemented yet --------------------------");
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Users findByUserId(String param_user_id) {
        if (user_list != null && user_list.containsKey(param_user_id)) {
            return user_list.get(param_user_id);
        } else {
            return null;
        }
    }

    Integer count() {
        return user_list.size();
    }

    private void checkForReload() {
        long current_time = System.currentTimeMillis();
        if (current_time - last_update_time > Constants.getUpdate_time_difference()) {
            forceReload();
        }
    }

    private void forceReload() {
        user_list = buildList();
        last_update_time = System.currentTimeMillis();
    }

    public MyAppError checkUserValidity(String username, String password) {
        MyAppError myAppError = new MyAppError();

        Users users = findByUserId(username);
        if (users == null) {
            myAppError.setErrorType(MyAppError.ValidationError);
            myAppError.setErrorMessage("User not found");
        } else {
            if (!users.getUser_password().equals(password)) {
                myAppError.setErrorType(MyAppError.ValidationError);
                myAppError.setErrorMessage("error.login");
            }
        }
        return myAppError;
    }
}
