/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.payment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ipvision.common.Filters;
import com.ipvision.common.FiltersSubFields;
import com.ipvision.user.Users;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import util.Constants;

/**
 *
 * @author Wasif Islam
 */
public class PaymentDAO {

    static Logger logger = Logger.getLogger(PaymentDAO.class.getName());
    private static ArrayList<Payment> paymentList = null;
    private static HashMap<Long, Payment> paymentMap = new HashMap<Long, Payment>();
    static PaymentDAO paymentLoader = null;

    public PaymentDAO() {
    }

    public static PaymentDAO getInstance() {
        if (paymentLoader == null) {
            createPaymentDAO();
        }
        return paymentLoader;
    }

    private synchronized static void createPaymentDAO() {
        if (paymentLoader == null) {
            paymentLoader = new PaymentDAO();
        }
    }

    private String condition(String filters, Users usr) {
        String condition = "where ( ";

        Filters filters_obj;
        Gson json = new GsonBuilder().serializeNulls().create();
        filters_obj = json.fromJson(filters, Filters.class);
        FiltersSubFields filtersSubFields = null;
        for (int i = 0; i < filters_obj.getRules().size(); i++) {
            String op = "";

            filtersSubFields = json.fromJson(filters_obj.getRules().get(i).toString(), FiltersSubFields.class);
            if("payment_date".equals(filtersSubFields.getField())){
                String[] data = filtersSubFields.getData().split("/");        
                filtersSubFields.setData(data[2]+"/"+data[0]+"/"+data[1]);
            }
            
            if ("eq".equals(filtersSubFields.getOp())) {
                op = " = '" + filtersSubFields.getData() + "'";
            } else if ("ne".equals(filtersSubFields.getOp())) {
                op = " != '" + filtersSubFields.getData() + "'";
            } else if ("bw".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'" + filtersSubFields.getData() + "%'";
            } else if ("ew".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "'";
            } else if ("cn".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "%'";
            } else if ("gt".equals(filtersSubFields.getOp())) {
                op = " > " + "'" + filtersSubFields.getData() + "'";
            } else if ("lt".equals(filtersSubFields.getOp())) {
                op = " < " + "'" + filtersSubFields.getData() + "'";
            } else if ("ge".equals(filtersSubFields.getOp())) {
                op = " >= " + "'" + filtersSubFields.getData() + "'";
            } else if ("le".equals(filtersSubFields.getOp())) {
                op = " <= " + "'" + filtersSubFields.getData() + "'";
            }

            if (i > 0) {
                condition += " " + filters_obj.getGroupOp() + " ";
            }
            condition += filtersSubFields.getField() + op;
        }
        if (usr.getRole_id() > 0) {
            condition += " ) AND (client_id='" + usr.getUser_id() + "')";
        } else {
            condition += " )";
        }
        return condition;
    }

    public int count(String filters, Users usr) {
        DBConnection dbConnection = null;
        Statement statement = null;
        int count = 0;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "";
            if (filters != null && filters.length() > 0) {
                sql = "select count(*) from payments " + condition(filters, usr);
            } else {
                sql = "select count(*) from payments " + (usr.getRole_id() > 0 ? "where client_id='" + usr.getUser_id() + "'" : "");
            }
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                count = resultSet.getInt("count(*)");
            }
        } catch (Exception e) {
            logger.fatal("Exception in payment getCount:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return count;
    }

    public List<Payment> search(String filters, String sort, String sort_field, int from, int to, Users usr) {
        DBConnection dbConnection = null;
        Statement statement = null;
        paymentList = new ArrayList<Payment>();
        paymentMap = new HashMap<Long, Payment>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();

            String limit_sql = "";
            String sql = "";
            if (!(to < 0)) {
                limit_sql = "limit " + from + "," + (to - from);
            }

            String order_sql = "";
            if (sort != null && sort.length() > 0 && sort_field != null && sort_field.length() > 0) {

                order_sql = "order by " + sort_field + " " + sort;
            } else {
                order_sql = "order by payment_id desc";
            }

            if (filters != null && filters.length() > 0) {
                sql = "select * from payments " + condition(filters, usr) + " " + order_sql + " " + limit_sql;
            } else {
                sql = "select * from payments " + (usr.getRole_id() > 0 ? "where client_id='" + usr.getUser_id() + "' " : "") + order_sql + " " + limit_sql;
            }
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Payment dto = new Payment();
                try {
                    dto.setPayment_id(resultSet.getLong("payment_id"));
                    dto.setClient_id(resultSet.getString("client_id"));
                    dto.setPayment_type(resultSet.getInt("payment_type"));
                    dto.setPayment_type_name(Constants.PAYMENT_STRING[dto.getPayment_type()]);
                    dto.setCur_balance(resultSet.getDouble("cur_balance"));
                    dto.setPayment_recharge(resultSet.getDouble("payment_recharge"));
                    dto.setPayment_return(resultSet.getDouble("payment_return"));
                    dto.setPayment_receive(resultSet.getDouble("payment_receive"));
                    dto.setPayment_date(resultSet.getString("payment_date"));

                } catch (Exception e) {
                }
                paymentMap.put(dto.getPayment_id(), dto);
                paymentList.add(dto);
            }

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in searchPaymentDTOsList:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return paymentList;
    }
}
