/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.payment;

import com.ipvision.user.Users;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author Wasif Islam
 */
public class PaymentAction extends ActionSupport {

    private List<Payment> gridModel;
    private Integer rows = 10;
    private Integer page = 1;
    private String sord;
    private String sidx;
    private String filters;
    private Integer total = 0;
    private Integer records = 0;

    public String execute() throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        if (request.getSession().getAttribute("userName") != null) {
            Users usr = (Users) request.getSession().getAttribute("userName");
            
            
            PaymentDAO pDao = new PaymentDAO();
            int to = (rows * page);
            int from = to - rows;
            records = pDao.count(getFilters(), usr);
            setGridModel(pDao.search(getFilters(), getSord(), getSidx(), from, to, usr));
            total = (int) Math.ceil((double) records / (double) rows);
        }
        return SUCCESS;
    }

    public List<Payment> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<Payment> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getRecords() {
        return records;
    }

    public void setRecords(Integer records) {
        this.records = records;
    }
}
