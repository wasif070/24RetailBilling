/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.payment;

/**
 *
 * @author Wasif Islam
 */
public class Payment {
    private Long payment_id;
    private String client_id;
    private Double cur_balance;
    private Integer payment_type;
    private String payment_type_name;
    private Double payment_recharge;
    private Double payment_return;
    private Double payment_receive;
    private String payment_date;

    public Long getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(Long payment_id) {
        this.payment_id = payment_id;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }
    public Double getCur_balance() {
        return cur_balance;
    }

    public void setCur_balance(Double cur_balance) {
        this.cur_balance = cur_balance;
    }

    public Integer getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(Integer payment_type) {
        this.payment_type = payment_type;
    }

    public String getPayment_type_name() {
        return payment_type_name;
    }

    public void setPayment_type_name(String payment_type_name) {
        this.payment_type_name = payment_type_name;
    }

    public Double getPayment_recharge() {
        return payment_recharge;
    }

    public void setPayment_recharge(Double payment_recharge) {
        this.payment_recharge = payment_recharge;
    }

    public Double getPayment_return() {
        return payment_return;
    }

    public void setPayment_return(Double payment_return) {
        this.payment_return = payment_return;
    }

    public Double getPayment_receive() {
        return payment_receive;
    }

    public void setPayment_receive(Double payment_receive) {
        this.payment_receive = payment_receive;
    }

    public String getPayment_date() {
        return payment_date;
    }

    public void setPayment_date(String payment_date) {
        this.payment_date = payment_date;
    }
    
    
    }
