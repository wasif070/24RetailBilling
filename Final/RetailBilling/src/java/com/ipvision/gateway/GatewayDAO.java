/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.gateway;

//import static com.opensymphony.xwork2.Action.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ipvision.clients.ClientDAO;
import com.ipvision.common.ConditionChecking;
import com.ipvision.common.DataStorage;
import com.ipvision.common.*;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import util.Constants;
import util.MyAppError;
import util.Utils;

/**
 *
 * @author reefat
 */
public class GatewayDAO implements Comparable<Integer> {

    static {
        gateway_list = buildList();
    }
    static Logger logger = Logger.getLogger(GatewayDAO.class.getName());
    private PreparedStatement ps = null;
    private Statement stmt;
    protected DBConnection dbConnection = null;
    private static long last_update_time = 0;
    static List<Gateway> gateway_list;
    static List<Gateway> gateway_filtered_list;
    static List<Gateway> gateway_visible_list;
    private static HashMap<String, Integer> id_to_db_id_mapping;// = new HashMap<String, Integer>();
    private static HashMap<String, String> ip_to_name_mapping;
    private static HashMap<String, HashMap<String, String>> client_to_ip_mapping;

    public static List<Gateway> buildList() {
        ArrayList<Gateway> v_temp_gateway_list = new ArrayList<Gateway>();
        if (id_to_db_id_mapping == null) {
            id_to_db_id_mapping = new HashMap<String, Integer>();
        }
        if (ip_to_name_mapping == null) {
            ip_to_name_mapping = new HashMap<String, String>();
        }
        client_to_ip_mapping = new HashMap<String, HashMap<String, String>>();

        DBConnection local_dbConnection = null;
        String sql = "";
        PreparedStatement prepared_statement = null;
        ResultSet rs;// = null;
        ClientDAO clientDAO = new ClientDAO();
        try {
            local_dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            sql = "select * from gateway where gateway_delete = 0 order by id desc";
            prepared_statement = local_dbConnection.connection.prepareStatement(sql);
            rs = prepared_statement.executeQuery();
            while (rs.next()) {
                Gateway gateway = new Gateway();
                gateway.setGateway_db_id(rs.getInt("id"));
                gateway.setGateway_name(rs.getString("gateway_name"));
                gateway.setGateway_ip(rs.getString("gateway_ip"));
                gateway.setGateway_port(rs.getInt("gateway_port"));
                gateway.setGateway_status(Constants.getSTATUS_STRING()[rs.getInt("gateway_status")]);
//                gateway.setGateway_type_int(rs.getInt("gateway_type"));
                gateway.setGateway_type(Constants.getTYPE_STRING()[rs.getInt("gateway_type")]);
//                gateway.setGateway_type(Constants.getTYPE_STRING()[gateway.getGateway_type_int()]);
                gateway.setClient_db_id(rs.getInt("client_id"));
                gateway.setClient_id(clientDAO.findClientIdByDbId(gateway.getClient_db_id()).getClient_id());
                gateway.setGateway_delete(rs.getInt("gateway_delete"));
                v_temp_gateway_list.add(gateway);

                id_to_db_id_mapping.put(gateway.getGateway_name(), gateway.getGateway_db_id());
                ip_to_name_mapping.put(gateway.getGateway_ip(), gateway.getGateway_name());

                if (client_to_ip_mapping.containsKey(gateway.getClient_id())) {
                    client_to_ip_mapping.get(gateway.getClient_id()).put(gateway.getGateway_ip(), gateway.getGateway_name());
                } else {
                    HashMap ip = new HashMap();
                    ip.put(gateway.getGateway_ip(), gateway.getGateway_name());
                    client_to_ip_mapping.put(gateway.getClient_id(), ip);
                }
            }

        } catch (Exception ex) {
            logger.debug("Exception in GatewayDAO , buildList() ---> " + ex.getMessage());
        } finally {
            try {
                if (prepared_statement != null) {
                    prepared_statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (local_dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(local_dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return v_temp_gateway_list;
    }

    MyAppError save(Gateway gateway) {
        MyAppError myAppError = new MyAppError();
        ClientDAO clientDAO = new ClientDAO();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            ps = null;
            ResultSet rs = null;

            sql = "select * from gateway where gateway_delete = 0 AND (gateway_name = ? OR gateway_ip = ?)";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, gateway.getGateway_name());
            ps.setString(2, gateway.getGateway_ip());
            rs = ps.executeQuery();

            if (rs.next()) {
                myAppError.setErrorType(MyAppError.DBError);
                myAppError.setErrorMessage("Duplicate ID OR IP");
                return myAppError;
            }

            if (ps != null) {
                ps.close();
            }

            sql = "insert into gateway set "
                    + "gateway_name = ?, "
                    + "gateway_ip = ?, "
                    + "gateway_port = ?, "
                    + "gateway_status = ?, "
                    + "gateway_type = ?, "
                    + "client_id = ?, "
                    + "owner_id = ? ";
            ps = dbConnection.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            ps.setString(i++, gateway.getGateway_name());
            ps.setString(i++, gateway.getGateway_ip());
            ps.setInt(i++, gateway.getGateway_port());
            ps.setInt(i++, Integer.parseInt(gateway.getGateway_status()));
            ps.setInt(i++, Integer.parseInt(gateway.getGateway_type()));
            ps.setInt(i++, Integer.parseInt(gateway.getClient_id()));
            ps.setInt(i++, -1);
            logger.debug("insert gateway sql --> " + Utils.getSQLString(ps));
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                gateway.setGateway_db_id(rs.getInt(1));
            }

            gateway.setGateway_type(Constants.getTYPE_STRING()[Integer.parseInt(gateway.getGateway_type())]);
            gateway.setGateway_status(Constants.getSTATUS_STRING()[Integer.parseInt(gateway.getGateway_status())]);
            gateway.setClient_id(clientDAO.findClientIdByDbId(Integer.parseInt(gateway.getClient_id())).getClient_id());


            gateway_list.add(gateway);
            id_to_db_id_mapping.put(gateway.getGateway_name(), gateway.getGateway_db_id());
            ip_to_name_mapping.put(gateway.getGateway_ip(), gateway.getGateway_name());

            if (client_to_ip_mapping.containsKey(gateway.getClient_id())) {
                client_to_ip_mapping.get(gateway.getClient_id()).put(gateway.getGateway_ip(), gateway.getGateway_name());
            } else {
                HashMap ip = new HashMap();
                ip.put(gateway.getGateway_ip(), gateway.getGateway_name());
                client_to_ip_mapping.put(gateway.getClient_id(), ip);
            }
            myAppError.setErrorMessage("Successfully added");

        } catch (Exception ex) {
            logger.debug("Exception in GatewayDAO , save() ---> " + ex.getMessage());
            myAppError.setErrorType(MyAppError.DBError);
            myAppError.setErrorMessage("DB error : " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return myAppError;
    }

    MyAppError update(Gateway gateway) {
        MyAppError myAppError = new MyAppError();
        ClientDAO clientDAO = new ClientDAO();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            ps = null;
            ResultSet rs = null;

            sql = "select id from gateway where gateway_delete = 0 AND gateway_name != ? AND gateway_ip = ?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, gateway.getGateway_name());
            ps.setString(2, gateway.getGateway_ip());
            rs = ps.executeQuery();

            if (rs.next()) {
                myAppError.setErrorType(MyAppError.DBError);
                myAppError.setErrorMessage("Duplicate IP");
                return myAppError;
            }

            if (ps != null) {
                ps.close();
            }

//----------------------------------- added later (start) ----------------------------------------
            if (Integer.parseInt(gateway.getGateway_type()) == 0) {
                String var_error_message = "";

                sql = "select dp_name from dialpeers where dp_gateway_list like '%," + gateway.getGateway_db_id() + ",%'";
                stmt = dbConnection.connection.createStatement();
                ResultSet resultSet = stmt.executeQuery(sql);

                String dial_peers;

                if (resultSet.next()) {
                    dial_peers = resultSet.getString("dp_name");
                    while (resultSet.next()) {
                        dial_peers += "," + resultSet.getString("dp_name");
                    }
                    var_error_message += "Please remove these Dialplans : " + dial_peers + " first \n";
                }
                if (var_error_message.length() > 0) {
                    myAppError.setErrorType(MyAppError.NotUpdated);
                    myAppError.setErrorMessage(var_error_message);
                    return myAppError;
                }
            }
//----------------------------------- added later (end) ----------------------------------------
            sql = "select id from gateway where gateway_delete = 0 AND id = ?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setInt(1, gateway.getGateway_db_id());
            rs = ps.executeQuery();
            if (rs.next()) {
                sql = "Update gateway set "
                        + "gateway_ip = ?, "
                        + "gateway_port = ?, "
                        + "gateway_status = ?, "
                        + "gateway_type = ?, "
                        + "client_id = ? "
                        + "where "
                        + "id = ? ";
                if (ps != null) {
                    ps.close();
                }
                ps = dbConnection.connection.prepareStatement(sql);
                int i = 1;
                ps.setString(i++, gateway.getGateway_ip());
                ps.setInt(i++, gateway.getGateway_port());
                ps.setInt(i++, Integer.parseInt(gateway.getGateway_status()));
                ps.setInt(i++, Integer.parseInt(gateway.getGateway_type()));
                ps.setInt(i++, Integer.parseInt(gateway.getClient_id()));
                ps.setInt(i++, gateway.getGateway_db_id());

                ps.executeUpdate();

                for (Gateway temp_gateway : gateway_visible_list) {
                    if (gateway.getGateway_db_id() == temp_gateway.getGateway_db_id()) {
                        temp_gateway.setGateway_ip(gateway.getGateway_ip());
                        temp_gateway.setGateway_port(gateway.getGateway_port());
                        temp_gateway.setGateway_status(Constants.getSTATUS_STRING()[Integer.parseInt(gateway.getGateway_status())]);
                        temp_gateway.setGateway_type(Constants.getTYPE_STRING()[Integer.parseInt(gateway.getGateway_type())]);
                        temp_gateway.setClient_id(clientDAO.findClientIdByDbId(Integer.parseInt(gateway.getClient_id())).getClient_id());

                        break;
                    }
                }
                myAppError.setErrorMessage("Successfully edited");
            } else {
                myAppError.setErrorType(MyAppError.NotUpdated);
                myAppError.setErrorMessage("Gateway Not found ");
            }

        } catch (Exception ex) {
            logger.debug("Exception in GatewayDAO , update() ---> " + ex.getMessage());
            myAppError.setErrorType(MyAppError.DBError);
            myAppError.setErrorMessage("DB error : " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return myAppError;
    }

    MyAppError delete(ArrayList<Gateway> param_gateway_list) {
        MyAppError myAppError = new MyAppError();
        String db_id = "";
        String var_error_message = "";

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            ps = null;
            stmt = null;

            for (Gateway gateway_temp_for_db_id : param_gateway_list) {
                Integer var_gw_db_id = gateway_temp_for_db_id.getGateway_db_id();
                sql = "select dp_name from dialpeers where dp_gateway_list like '%," + var_gw_db_id + ",%'";
                stmt = dbConnection.connection.createStatement();
                ResultSet resultSet = stmt.executeQuery(sql);

                String dial_peers;

                if (resultSet.next()) {
                    dial_peers = resultSet.getString("dp_name");
                    while (resultSet.next()) {
                        dial_peers += "," + resultSet.getString("dp_name");
                    }
                    var_error_message += "Dialplans : " + dial_peers + " is/are associated with gateway : " + gateway_temp_for_db_id.getGateway_name() + "\n";
                } else {
                    db_id += var_gw_db_id + " , ";
                }
            }
            if (var_error_message.length() > 0) {
                myAppError.setErrorMessage(var_error_message);
            }

            if (db_id.length() == 0) {
                myAppError.setErrorType(MyAppError.DBError);
                return myAppError;
            } else {
                db_id = db_id.substring(0, db_id.length() - 3);


                sql = "Update gateway set gateway_delete = 1 where id in ( " + db_id + " )";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.executeUpdate();

                int size = param_gateway_list.size();
                int[] individual_id_int = new int[size];
                List<Integer> to_be_deleted = new ArrayList<Integer>();
                int counter = 0;
                for (int i = 0; i < size; i++) {
                    individual_id_int[i] = param_gateway_list.get(i).getGateway_db_id();
                }
                int index_of_objects = 0;

                for (Gateway temp_gateway : gateway_visible_list) {
                    if (contains(temp_gateway.getGateway_db_id(), individual_id_int)) {
                        counter++;
                        to_be_deleted.add(index_of_objects);

                        id_to_db_id_mapping.remove(temp_gateway.getGateway_name());
                        if (counter == size) {
                            break;
                        }
                    }
                    index_of_objects++;
                }
                Collections.sort(to_be_deleted, intComparator);
                for (int i = 0; i < to_be_deleted.size(); i++) {
                    gateway_visible_list.remove(to_be_deleted.get(i).intValue());
                }

                myAppError.setErrorMessage(myAppError.getErrorMessage().length() > 0 ? myAppError.getErrorMessage().concat("#response.deletedsuccessfully") : "response.deletedsuccessfully");
            }
        } catch (Exception ex) {
            logger.debug("Exception in GatewayDAO , delete() ---> " + ex.getMessage());
            myAppError.setErrorType(MyAppError.DBError);
            myAppError.setErrorMessage("Error : " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
        }
        return myAppError;
    }

    Gateway findById(int parseInt) {
        for (Gateway gateway : gateway_visible_list) {
            if (gateway.getId() == parseInt) {
                return gateway;
            }
        }
        return null;
    }

    ArrayList<Gateway> findMultipleById(int[] parseInt) {
        ArrayList<Gateway> multiples_clients_to_be_deleted = new ArrayList<Gateway>();
        int counter = 0;
        for (Gateway client : gateway_visible_list) {
            if (contains(client.getId(), parseInt)) {
                multiples_clients_to_be_deleted.add(client);
                counter++;
                if (counter == parseInt.length) {
                    break;
                }
            }
        }
        return multiples_clients_to_be_deleted;
    }

    boolean contains(int param_one, int[] target_value_list) {
        boolean does_contain = false;
        for (int i = 0; i < target_value_list.length; i++) {
            if (param_one == target_value_list[i]) {
                does_contain = true;
                break;
            }
        }
        return does_contain;
    }

    Integer count() {
        return (gateway_list != null ? gateway_list.size() : 0);
    }

    Integer countFilteredData() {
        return (gateway_filtered_list != null ? gateway_filtered_list.size() : 0);
    }

    public List<Gateway> findByType(int[] param_type) {
        List<Gateway> v_temp_dialplan_list;// = new ArrayList<Client>();
        checkForReload();
        String gateway_type;//
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < param_type.length; i++) {
            if (i != 0) {
                sb.append(",");
            }
            gateway_type = Constants.getTYPE_STRING()[param_type[i]];
            sb.append("{\"field\":\"gateway_type\",\"op\":\"eq\",\"data\":\"".concat(gateway_type)).append("\"}");
        }

        String json_string = "{\"groupOp\":\"OR\",\"rules\":[" + sb.toString() + "]}";
        v_temp_dialplan_list = findFilteredList(json_string, 0, -1);
        return v_temp_dialplan_list;
    }

    public Gateway findByIdFromMainList(int parseInt) {
        for (Gateway gateway : gateway_list) {
            if (gateway.getGateway_db_id() == parseInt) {
                return gateway;
            }
        }
        return null;
    }

    List<Gateway> find(int from, int to) {
        checkForReload();
        if (Gateway.getSort_order() != null) {
            Collections.sort(gateway_list, Gateway.GatewayPropertiesComparator);
        }
        gateway_visible_list = gateway_list.subList(from, to);
        for (int i = 0; i < to - from; i++) {
            gateway_visible_list.get(i).setId(i + 1);
        }
        return gateway_visible_list;
    }

    List<Gateway> findFilteredList(String json_data, int from, int to) {
        checkForReload();
        ConditionChecking conditionChecking = new ConditionChecking();
        gateway_filtered_list = new ArrayList<Gateway>();
        FiltersSubFields filtersSubFields;
        Filters filters_obj;
        Gson json = new GsonBuilder().serializeNulls().create();
        filters_obj = json.fromJson(json_data, Filters.class);

        String comparison_type;
        boolean condition_matched = true;
        boolean return_val = false;

        for (Gateway filtered_gateway : gateway_list) {
            condition_matched = true;
            for (int i = 0; i < filters_obj.getRules().size(); i++) {
                filtersSubFields = json.fromJson(filters_obj.getRules().get(i).toString(), FiltersSubFields.class);
                comparison_type = filtersSubFields.getOp();
                switch (DataStorage.getInstance().getFieldMappingGateway(filtersSubFields.getField())) {
                    case 0:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_gateway.getGateway_name(), filtersSubFields.getData());
                        break;
                    case 1:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_gateway.getGateway_ip(), filtersSubFields.getData());
                        break;
                    case 2:
                        return_val = conditionChecking.checkConditionNumeric(comparison_type, filtered_gateway.getGateway_port(), Integer.parseInt(filtersSubFields.getData()));
                        break;
                    case 3:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_gateway.getGateway_status(), filtersSubFields.getData());
                        break;
                    case 4:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_gateway.getGateway_type(), filtersSubFields.getData());
                        break;
                    case 5:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_gateway.getClient_id(), filtersSubFields.getData());
                        break;
                    default:
                        break;
                }

                if (filters_obj.getGroupOp().equalsIgnoreCase("And")) {
                    if (return_val == false) {
                        condition_matched = false;
                        break;
                    }
                } else {
                    if (return_val == true) {
                        condition_matched = true;
                        break;
                    } else if (i == filters_obj.getRules().size() - 1) {
                        condition_matched = false;
                    }
                }
            }
            if (condition_matched) {
                gateway_filtered_list.add(filtered_gateway);
            }
        }

        if (Gateway.getSort_order() != null) {
            Collections.sort(gateway_filtered_list, Gateway.GatewayPropertiesComparator);
        }
        if (to > gateway_filtered_list.size() || to == -1) {
            to = gateway_filtered_list.size() - from;
        }

        gateway_visible_list = gateway_filtered_list.subList(from, to);

        for (int i = 0; i < to - from; i++) {
            gateway_visible_list.get(i).setId(i + 1);
        }

        return gateway_visible_list;
    }

    private void checkForReload() {
        long current_time = System.currentTimeMillis();
        if (current_time - last_update_time > Constants.getUpdate_time_difference()) {
            forceReload();
        }
    }

    private void forceReload() {
        gateway_list = buildList();
        last_update_time = System.currentTimeMillis();
    }

    public Integer[] getGatewayDBIdList(String paramGatewayNameList) {
        String[] v_array_of_string = paramGatewayNameList.split(",");
        Integer[] return_array = new Integer[v_array_of_string.length];
        int i = 0;
        for (String s : v_array_of_string) {
            return_array[i++] = id_to_db_id_mapping.get(s);
        }
        return return_array;
    }

    public String getGatewayName(String ip) {
        return (ip_to_name_mapping != null ? ip_to_name_mapping.get(ip) : "");
    }

    public String getGatewayIPbyClient(String client) {
        String gateways = "";
        if(client_to_ip_mapping.containsKey(client)){
        for (String s : client_to_ip_mapping.get(client).keySet()) {
            gateways += "'" + s + "',";
        }
        //return (client_to_ip_mapping != null ? client_to_ip_mapping.get(client) : "");
        return gateways.substring(0, gateways.length() - 1);
        }else{
            return "-1";
        }
    }

    @Override
    public int compareTo(Integer t) {
        return this.compareTo(t);
    }
    public static Comparator<Integer> intComparator = new Comparator<Integer>() {
        @Override
        public int compare(Integer t, Integer t1) {
            return t1.compareTo(t);
        }
    };

    public boolean isClientChangeAllowed(String param_client_id, int param_client_type) {
        boolean v_return_val = true;
        if (param_client_type == 2) {
            return v_return_val;
        }
        ArrayList<Gateway> temp_gw_list = new ArrayList<Gateway>();
        for (Gateway local_gw_obj : gateway_list) {
            if (local_gw_obj.getClient_id().equals(param_client_id)) {
                temp_gw_list.add(local_gw_obj);
            }
        }
        if (temp_gw_list.size() > 0) {
            System.out.println("temp_gw_list.size() --> " + temp_gw_list.size());
            int counter = 1;
            for (Gateway local_gw_obj : temp_gw_list) {
                System.out.println("counter --> " + counter++);
                switch (param_client_type) {
                    case 0:
                        if (local_gw_obj.getGateway_type().equals(Constants.getTYPE_STRING()[1])) {
                            v_return_val = false;
                        }
                        break;
                    case 1:
                        if (local_gw_obj.getGateway_type().equals(Constants.getTYPE_STRING()[0])) {
                            v_return_val = false;
                        }
                        break;
                }
                if (!v_return_val) {
                    break;
                }
            }
        }
        return v_return_val;
    }
}
