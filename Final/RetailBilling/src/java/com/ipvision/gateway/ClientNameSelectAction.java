/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.gateway;

import com.ipvision.clients.Client;
import com.ipvision.clients.ClientDAO;
import com.opensymphony.xwork2.ActionSupport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author reefat
 */
public class ClientNameSelectAction extends ActionSupport {

    public int gateway_type;

    public int getGateway_type() {
        return gateway_type;
    }

    public void setGateway_type(int gateway_type) {
        this.gateway_type = gateway_type;
    }
    public Map<Integer, String> clientNameList = new HashMap<Integer, String>();

    public Map<Integer, String> getClientNameList() {
        return clientNameList;
    }

    public void setClientNameList(Map<Integer, String> clientNameList) {
        this.clientNameList = clientNameList;
    }

    public ClientNameSelectAction() {
    }

    @Override
    public String execute() throws Exception {
        ClientDAO clientDAO = new ClientDAO();
        List<Client> client_list = clientDAO.findByType(getGateway_type());

        for (Client temp_client : client_list) {
            if (temp_client.getClient_status().equalsIgnoreCase("Active")) {
                clientNameList.put(temp_client.getClient_db_id(), temp_client.getClient_id());
            }
        }
        return SUCCESS;
    }
}