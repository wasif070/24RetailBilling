/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.gateway;

import com.ipvision.common.Admin;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import util.Constants;
import util.MyAppError;
import util.Utils;

/**
 *
 * @author reefat
 */
public class GatewayOperationAction extends ActionSupport implements Admin {

    private String action;
    private String id;
    private Integer gateway_db_id;
    private String gateway_name;
    private String gateway_ip;
    private Integer gateway_port;
    private int gateway_status;
    private int gateway_type;
    private String client_id;

    public GatewayOperationAction() {
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String add() throws Exception {
        String return_val = SUCCESS;

        Gateway gateway = new Gateway();
        gateway.setGateway_name(gateway_name);
        gateway.setGateway_ip(gateway_ip);
        gateway.setGateway_port((gateway_port != null ? gateway_port : 0));
        gateway.setGateway_status(String.valueOf(gateway_status));
        gateway.setGateway_type(String.valueOf(gateway_type));
        gateway.setClient_id(client_id);

        GatewayDAO gatewayDAO = new GatewayDAO();
        MyAppError myAppError = gatewayDAO.save(gateway);

        if (myAppError.getErrorType() > 0) {
            addActionError(myAppError.getErrorMessage());
            return_val = INPUT;
        } else {
            addActionMessage(myAppError.getErrorMessage());
        }

        return return_val;
    }

    public String update() throws Exception {

        String return_val = SUCCESS;

        GatewayDAO gatewayDAO = new GatewayDAO();
        Gateway gateway = gatewayDAO.findById(Integer.parseInt(id));

        if (gateway != null) {
            gateway.setGateway_ip(gateway_ip);
            gateway.setGateway_port(gateway_port);
            gateway.setGateway_status(String.valueOf(gateway_status));
            gateway.setGateway_type(String.valueOf(gateway_type));
            gateway.setClient_id(client_id);

            MyAppError myAppError = gatewayDAO.update(gateway);

            if (myAppError.getErrorType() > 0) {
                addActionError(myAppError.getErrorMessage());
                return_val = INPUT;
            } else {
                addActionMessage(myAppError.getErrorMessage());
            }
        }else{
            addActionError("Gateway not found");
        }

        return return_val;

    }

    public String delete() throws Exception {

        String return_val = SUCCESS;

        ArrayList<Gateway> gateway_list;
        String[] individual_id_str = id.split(",");
        int[] individual_id_int = new int[individual_id_str.length];
        for (int i = 0; i < individual_id_str.length; i++) {
            individual_id_int[i] = Integer.valueOf(individual_id_str[i]);
        }
        GatewayDAO gatewayDAO = new GatewayDAO();
        gateway_list = gatewayDAO.findMultipleById(individual_id_int);
        MyAppError myAppError = gatewayDAO.delete(gateway_list);

        if (myAppError.getErrorType() > 0) {
            addActionError(myAppError.getErrorMessage());
            return_val = INPUT;
        } else {
            String[] var_msg = myAppError.getErrorMessage().split("#");
            if (var_msg.length > 1) {
                addActionError(var_msg[0]);
                addActionMessage(getText(var_msg[1]));
            } else {
                addActionMessage(getText(myAppError.getErrorMessage()));
            }
        }
        return return_val;
    }

    public String retrieve() throws Exception {
        GatewayDAO gatewayDAO = new GatewayDAO();

        HttpServletRequest request = ServletActionContext.getRequest();
        if (request.getParameter("id") != null) {
            int row_id = Integer.valueOf(request.getParameter("id"));
            Gateway gateway = gatewayDAO.findById(row_id);
            if (gateway != null) {
                this.setId(String.valueOf(row_id));
                this.setGateway_db_id(gateway.getGateway_db_id());
                this.setGateway_name(gateway.getGateway_name());
                this.setGateway_ip(gateway.getGateway_ip());
                this.setGateway_port(gateway.getGateway_port());
                this.setGateway_status(Utils.findIndex(Constants.STATUS_STRING, gateway.getGateway_status()));
                this.setGateway_type(Utils.findIndex(Constants.TYPE_STRING, gateway.getGateway_type()));
                this.setClient_id(String.valueOf(gateway.getClient_db_id()));
            }

        }
        this.setAction("update");
        return INPUT;
    }

    public String display() throws Exception {
        this.setAction("add");
        return INPUT;
    }

    @Override
    public void validate() {

        this.setAction(ActionContext.getContext().getName());
        String actn = this.getAction();
        if (actn.equalsIgnoreCase("add") || actn.equalsIgnoreCase("update")) {
            if (actn.equalsIgnoreCase("add")) {
                if (this.gateway_name == null || this.gateway_name.length() == 0) {
                    addFieldError("gateway_name", getText("required.common"));
                }

                if (gateway_name.contains(" ")) {
                    addActionError(getText("notAllowed.space") + " Gateway Name");
                }
            }

            if (this.gateway_ip == null || this.gateway_ip.length() == 0) {
                addFieldError("gateway_ip", getText("required.common"));
            } else {
                if (!Utils.checkValidIP(this.gateway_ip)) {
                    addFieldError("gateway_ip", getText("invalid.ip"));
                }
            }

            if (!(this.gateway_type == 0)) {
                if (this.gateway_port == null) {
                    addFieldError("gateway_port", getText("required.port"));
                } else if (this.gateway_port == 0) {
                    addFieldError("gateway_port", getText("invalid.port"));
                }
            }

            if (this.client_id == null || this.client_id.length() == 0) {
                addFieldError("client_id", getText("required.common"));
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getGateway_db_id() {
        return gateway_db_id;
    }

    public void setGateway_db_id(Integer gateway_db_id) {
        this.gateway_db_id = gateway_db_id;
    }

    public String getGateway_name() {
        return gateway_name;
    }

    public void setGateway_name(String gateway_name) {
        this.gateway_name = gateway_name;
    }

    public String getGateway_ip() {
        return gateway_ip;
    }

    public void setGateway_ip(String gateway_ip) {
        this.gateway_ip = gateway_ip;
    }

    public Integer getGateway_port() {
        return gateway_port;
    }

    public void setGateway_port(Integer gateway_port) {
        this.gateway_port = gateway_port;
    }

    public int getGateway_status() {
        return gateway_status;
    }

    public void setGateway_status(int gateway_status) {
        this.gateway_status = gateway_status;
    }

    public int getGateway_type() {
        return gateway_type;
    }

    public void setGateway_type(int gateway_type) {
        this.gateway_type = gateway_type;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
