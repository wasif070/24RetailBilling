/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.gateway;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author reefat
 */
public class GatewayTypeSelectAction extends ActionSupport implements Preparable {

    public Map<Integer, String> gatewayTypeList;

    public Map<Integer, String> getGatewayTypeList() {
        return gatewayTypeList;
    }

    public void setGatewayTypeList(Map<Integer, String> gatewayTypeList) {
        this.gatewayTypeList = gatewayTypeList;
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    @Override
    public void prepare() throws Exception {
        gatewayTypeList = new HashMap<Integer, String>();
        gatewayTypeList.put(0, "Origination");
        gatewayTypeList.put(1, "Termination");
        gatewayTypeList.put(2, "Both");
    }
}
