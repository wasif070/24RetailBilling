/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.gateway;

import java.util.Comparator;

/**
 *
 * @author reefat
 */
public class Gateway implements Comparable<Gateway> {
    
    private Integer id;
    private Integer gateway_db_id;
    private String gateway_name;
    private String gateway_ip;
    private int gateway_port;
    private String gateway_status;
    private String gateway_type;    
    private String client_id;
    private Integer client_db_id;
    private Integer gateway_delete;
    private String inserted_time;    
    private String owner_id;
    
    private static Integer field;
    private static Integer sort_order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGateway_db_id() {
        return gateway_db_id;
    }

    public void setGateway_db_id(Integer gateway_db_id) {
        this.gateway_db_id = gateway_db_id;
    }

    public String getGateway_name() {
        return gateway_name;
    }

    public void setGateway_name(String gateway_name) {
        this.gateway_name = gateway_name;
    }

    public String getGateway_ip() {
        return gateway_ip;
    }

    public void setGateway_ip(String gateway_ip) {
        this.gateway_ip = gateway_ip;
    }

    public int getGateway_port() {
        return gateway_port;
    }

    public void setGateway_port(int gateway_port) {
        this.gateway_port = gateway_port;
    }

    public String getGateway_status() {
        return gateway_status;
    }

    public void setGateway_status(String gateway_status) {
        this.gateway_status = gateway_status;
    }

    public String getGateway_type() {
        return gateway_type;
    }

    public void setGateway_type(String gateway_type) {
        this.gateway_type = gateway_type;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public Integer getClient_db_id() {
        return client_db_id;
    }

    public void setClient_db_id(Integer client_db_id) {
        this.client_db_id = client_db_id;
    }

    public Integer getGateway_delete() {
        return gateway_delete;
    }

    public void setGateway_delete(Integer gateway_delete) {
        this.gateway_delete = gateway_delete;
    }

    public String getInserted_time() {
        return inserted_time;
    }

    public void setInserted_time(String inserted_time) {
        this.inserted_time = inserted_time;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public static Integer getField() {
        return field;
    }

    public static void setField(Integer field) {
        Gateway.field = field;
    }

    public static Integer getSort_order() {
        return sort_order;
    }

    public static void setSort_order(Integer sort_order) {
        Gateway.sort_order = sort_order;
    }



    @Override
    public int compareTo(Gateway o) {
        Integer var1 = 0;// = this.client_call_limit;
        Integer var2 = 0;// = o.getClient_call_limit();
        switch (field) {
            case 2:
                var1 = this.gateway_port;
                var2 = o.getGateway_port();
                break;
            default:
                break;
        }
        switch (sort_order) {
            case 0:
                //ascending order
                return var1.compareTo(var2);
            case 1:
                //descending order
                return var2.compareTo(var1);
            default:
                return -1;
        }
    }
    
    public static Comparator<Gateway> GatewayPropertiesComparator = new Comparator<Gateway>() {
        public int compare(Gateway gateway1, Gateway gateway2) {

            String var_gateway_sorting_field_value_1 = null;// = gateway1.getFirstName().toUpperCase();
            String var_gateway_sorting_field_value_2 = null;// = gateway2.getFirstName().toUpperCase();

            switch (field) {

                case 0: //Gateway Name
                    var_gateway_sorting_field_value_1 = gateway1.getGateway_name().toUpperCase();
                    var_gateway_sorting_field_value_2 = gateway2.getGateway_name().toUpperCase();
                    break;
                case 1: //Gateway IP
                    var_gateway_sorting_field_value_1 = String.valueOf(gateway1.getGateway_ip());
                    var_gateway_sorting_field_value_2 = String.valueOf(gateway2.getGateway_ip());
                    break;
                case 2: //Gateway Port
                    return gateway1.compareTo(gateway2);
                case 3: //Gateway Status
                    var_gateway_sorting_field_value_1 = gateway1.getGateway_status().toUpperCase();
                    var_gateway_sorting_field_value_2 = gateway2.getGateway_status().toUpperCase();
                    break;
                case 4: //Gateway Type
                    var_gateway_sorting_field_value_1 = gateway1.getGateway_type().toUpperCase();
                    var_gateway_sorting_field_value_2 = gateway2.getGateway_type().toUpperCase();
                    break;
                case 5: //Client ID
                    var_gateway_sorting_field_value_1 = String.valueOf(gateway1.getClient_id());
                    var_gateway_sorting_field_value_2 = String.valueOf(gateway2.getClient_id());
                    break;
                default:
                    break;
            }

            if (var_gateway_sorting_field_value_1 == null || var_gateway_sorting_field_value_2 == null) {
                return -1;
            }

            switch (sort_order) {
                case 0:
                    //ascending order
                    return var_gateway_sorting_field_value_1.compareTo(var_gateway_sorting_field_value_2);
                case 1:
                    //descending order
                    return var_gateway_sorting_field_value_2.compareTo(var_gateway_sorting_field_value_1);
                default:
                    return -1;
            }
        }
    };
}
