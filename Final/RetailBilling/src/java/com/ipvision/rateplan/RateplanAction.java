/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.rateplan;

import com.ipvision.common.Admin;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.util.List;
import util.Constants;

/**
 *
 * @author Wasif Islam
 */
public class RateplanAction implements Admin {
    //private String oper;

    private String filters;
    //Your result List
    private List<Rateplan> gridModel;
    //get how many rows we want to have into the grid - rowNum attribute in the grid
    private Integer rows = 10;
    //Get the requested page. By default grid sets this to 1.
    private Integer page = 1;
    // sorting order - asc or desc
    private String sord;
    // get index row - i.e. user click to sort.
    private String sidx;
    // Your Total Pages
    private Integer total = 0;
    // All Record
    private Integer records = 0;

    public String execute() throws Exception {

        RateplanDAO rDao = new RateplanDAO();
        int to = (rows * page);
        int from = to - rows;
        records = rDao.count(getFilters());
        setGridModel(rDao.search(getFilters(), Constants.ALL_DB_FIELDS ,getSord(), getSidx(), from, to));
        total = (int) Math.ceil((double) records / (double) rows);
        return SUCCESS;

    }


    public List<Rateplan> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<Rateplan> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getRecords() {
        return records;
    }

    public void setRecords(Integer records) {
        this.records = records;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }
}
