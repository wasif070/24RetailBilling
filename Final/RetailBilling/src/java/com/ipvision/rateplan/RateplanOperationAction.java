/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.rateplan;

import com.ipvision.common.Admin;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import util.MyAppError;

/**
 *
 * @author Wasif Islam
 */
public class RateplanOperationAction extends ActionSupport implements Admin {

    private String action;
    private String id;
    private Long rateplan_id;
    private String rateplan_name;
    private String rateplan_des;
    private Integer rateplan_status;
    private String rate;

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String add() throws Exception {
        System.out.println("ADD");
        Rateplan rateplan = new Rateplan();
        rateplan.setRateplan_name(rateplan_name);
        rateplan.setRateplan_des(rateplan_des);
        rateplan.setRateplan_status(rateplan_status);

        RateplanDAO rateplanDAO = new RateplanDAO();
        MyAppError error = rateplanDAO.save(rateplan);
        if (error.getErrorType() > 0) {
            addActionError(error.getErrorMessage());
            return INPUT;
        } else {
            addActionMessage("Successfully added");
            return SUCCESS;
        }
    }

    public String update() throws Exception {
        System.out.println("EDIT");

        Rateplan rateplan = new Rateplan();
        rateplan.setRateplan_id(rateplan_id);
        rateplan.setRateplan_name(rateplan_name);
        rateplan.setRateplan_des(rateplan_des);
        rateplan.setRateplan_status(rateplan_status);

        RateplanDAO rateplanDAO = new RateplanDAO();
        MyAppError error = rateplanDAO.update(rateplan);
        if (error.getErrorType() > 0) {
            addActionError(error.getErrorMessage());
            return INPUT;
        } else {
            addActionMessage("Successfully edited");
            return SUCCESS;
        }
    }

    public String delete() throws Exception {
        System.out.println("DEL");
        RateplanDAO rateplanDAO = new RateplanDAO();
        String rateplanIDs = rateplanDAO.getDBIds(getId());

        MyAppError error = rateplanDAO.delete(rateplanIDs);
        if (error.getErrorType() > 0) {
            addActionError(error.getErrorMessage());
            return INPUT;
        } else {
            //addActionMessage(getText("successful.delete"));
            addActionMessage(error.getErrorMessage());
            return SUCCESS;
        }
    }

    public String retrieve() throws Exception {
        System.out.println("RETRIEVE");
        HttpServletRequest request = ServletActionContext.getRequest();
        if (request.getParameter("id") != null) {
            long db_id = Long.valueOf(request.getParameter("id"));
            Rateplan rateplan = RateplanDAO.getRateplanDTO(db_id);

            this.setRateplan_id(db_id);
            this.setRateplan_name(rateplan.getRateplan_name());
            this.setRateplan_des(rateplan.getRateplan_des());
            this.setRateplan_status(rateplan.getRateplan_status());
        }
        this.setAction("update");

        return INPUT;
    }

    public String display() throws Exception {
        this.setAction("add");
        return INPUT;
    }

    @Override
    public void validate() {
        this.setAction(ActionContext.getContext().getName());
        String action = getAction();
        if("add".equals(action) || "update".equals(action)){
        if (this.rateplan_name == null || this.rateplan_name.length() == 0) {
            addFieldError("rateplan_name", "*Rateplan Name Required");
        }
        }
    }

    public Long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(Long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public String getRateplan_name() {
        return rateplan_name;
    }

    public void setRateplan_name(String rateplan_name) {
        this.rateplan_name = rateplan_name;
    }

    public String getRateplan_des() {
        return rateplan_des;
    }

    public void setRateplan_des(String rateplan_des) {
        this.rateplan_des = rateplan_des;
    }

    public Integer getRateplan_status() {
        return rateplan_status;
    }

    public void setRateplan_status(Integer rateplan_status) {
        this.rateplan_status = rateplan_status;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
