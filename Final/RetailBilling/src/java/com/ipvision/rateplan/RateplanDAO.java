/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.rateplan;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ipvision.common.Filters;
import com.ipvision.common.FiltersSubFields;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import util.Constants;
import util.MyAppError;

/**
 *
 * @author Wasif Islam
 */
public class RateplanDAO {

    static Logger logger = Logger.getLogger(RateplanDAO.class.getName());
    private static ArrayList<Rateplan> rateplanList = null;
    private static HashMap<Long, Rateplan> rateplanMap = new HashMap<Long, Rateplan>();
    static RateplanDAO rateplanLoader = null;

    public RateplanDAO() {
    }

    public static RateplanDAO getInstance() {
        if (rateplanLoader == null) {
            createRateplanDAO();
        }
        return rateplanLoader;
    }

    private synchronized static void createRateplanDAO() {
        if (rateplanLoader == null) {
            rateplanLoader = new RateplanDAO();
        }
    }

    private String condition(String filters) {
        String condition = "where rateplan_delete = 0 AND ";

        Filters filters_obj;
        Gson json = new GsonBuilder().serializeNulls().create();
        filters_obj = json.fromJson(filters, Filters.class);
        FiltersSubFields filtersSubFields = null;
        for (int i = 0; i < filters_obj.getRules().size(); i++) {
            String op = "";
            filtersSubFields = json.fromJson(filters_obj.getRules().get(i).toString(), FiltersSubFields.class);

            if ("eq".equals(filtersSubFields.getOp())) {
                if ("rateplan_status".equals(filtersSubFields.getField())) {
                    if (filtersSubFields.getData().startsWith("a")) {
                        op = " = 0";
                    } else {
                        op = " = 1";
                    }
                } else {
                    op = " = '" + filtersSubFields.getData() + "'";
                }
            } else if ("ne".equals(filtersSubFields.getOp())) {
                if ("rateplan_status".equals(filtersSubFields.getField())) {
                    if (filtersSubFields.getData().startsWith("a")) {
                        op = " != 0";
                    } else {
                        op = " != 1";
                    }
                } else {
                    op = " != '" + filtersSubFields.getData() + "'";
                }
            } else if ("bw".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'" + filtersSubFields.getData() + "%'";
            } else if ("ew".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "'";
            } else if ("cn".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "%'";
            }

            if (i > 0) {
                condition += " " + filters_obj.getGroupOp() + " ";
            }

            condition += filtersSubFields.getField() + op;
        }
        return condition;
    }

    public int count(String filters) {
        DBConnection dbConnection = null;
        Statement statement = null;
        int count = 0;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "";
            if (filters != null && filters.length() > 0) {
                sql = "select count(*) from rateplans " + condition(filters);
            } else {
                sql = "select count(*) from rateplans where rateplan_delete = 0";
            }
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                count = resultSet.getInt("count(*)");
            }
        } catch (Exception e) {
            logger.fatal("Exception in getCount:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return count;
    }

    public List<Rateplan> search(String filters, String fields, String sort, String sort_field, int from, int to) {
        DBConnection dbConnection = null;
        Statement statement = null;
        rateplanList = new ArrayList<Rateplan>();
        rateplanMap = new HashMap<Long, Rateplan>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();

            String limit_sql = "";
            String sql = "";
            if (!(to < 0)) {
                limit_sql = "limit " + from + "," + (to - from);
            }


            String order_sql = "";
            if (sort != null && sort.length() > 0 && sort_field != null && sort_field.length() > 0) {

                order_sql = "order by " + sort_field + " " + sort;
            } else {
                order_sql = "order by rateplan_id desc";
            }

            if (filters != null && filters.length() > 0) {
                sql = "select " + fields + " from rateplans " + condition(filters) + " " + order_sql + " " + limit_sql;
            } else {
                sql = "select " + fields + " from rateplans where rateplan_delete = 0 " + order_sql + " " + limit_sql;
            }
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Rateplan dto = new Rateplan();
                try {
                    dto.setRateplan_id(resultSet.getLong("rateplan_id"));
                    dto.setRateplan_name(resultSet.getString("rateplan_name"));
                    dto.setRateplan_status(resultSet.getInt("rateplan_status"));
                    dto.setRateplan_des(resultSet.getString("rateplan_des"));                    
                    dto.setRateplan_status_name(Constants.STATUS_STRING[dto.getRateplan_status()]);
                    dto.setRate("Rate List");
                } catch (Exception e) {
                }
                rateplanMap.put(dto.getRateplan_id(), dto);
                rateplanList.add(dto);
            }

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in searchRateplansDTOList:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return rateplanList;
    }

    public MyAppError save(Rateplan rateplan) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select rateplan_name from rateplans where rateplan_name=? AND rateplan_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, rateplan.getRateplan_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Rateplan name conflicts with other rateplans");
                resultSet.close();
                return error;
            }
            sql = "insert into rateplans(rateplan_name, rateplan_des, rateplan_status) "
                    + "values(?,?,?)";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, rateplan.getRateplan_name());
            ps.setString(2, rateplan.getRateplan_des());
            ps.setInt(3, rateplan.getRateplan_status());
            ps.executeUpdate();

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Error while adding Rateplan:" + ex);
            logger.fatal("Error while adding Rateplan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return error;
    }

    public MyAppError update(Rateplan rateplan) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select rateplan_name from rateplans where rateplan_name=? AND rateplan_delete=0 AND rateplan_id!=" + rateplan.getRateplan_id();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, rateplan.getRateplan_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Rateplan name conflicts with other rateplans");
                resultSet.close();
                return error;
            }

            sql = "update rateplans set rateplan_name=?,rateplan_des=?,rateplan_status=? where rateplan_id=" + rateplan.getRateplan_id();

            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, rateplan.getRateplan_name());
            ps.setString(2, rateplan.getRateplan_des());
            ps.setInt(3, rateplan.getRateplan_status());
            ps.executeUpdate();

            sql = "update rates set rate_status=? where rateplan_id=" + rateplan.getRateplan_id();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setInt(1, rateplan.getRateplan_status());
            ps.executeUpdate();

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing rateplan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public String getDBIds(String id) {
        String rateplanIds = "";
        String[] ids = id.split(",");
        for (int i = 0; i < ids.length; i++) {
            if (i > 0) {
                rateplanIds += ",";
            }
            Rateplan rdto = new Rateplan();
            rdto = rateplanList.get(Integer.parseInt(ids[i]) - 1);
            rateplanIds += rdto.getRateplan_id();
        }
        return rateplanIds;
    }

    public MyAppError delete(String rateplan_id) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        String invalid_names = "";
        String valid_ids = "-1";

        ArrayList<Long> allIds = new ArrayList<Long>();
        String[] id = rateplan_id.split(",");
        for (int i = 0; i < id.length; i++) {
            allIds.add(Long.parseLong(id[i]));
        }

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select distinct(rateplans.rateplan_name),clients.rateplan_id from clients,rateplans where clients.rateplan_id=rateplans.rateplan_id and client_delete = 0 and clients.rateplan_id in(" + rateplan_id + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery(sql);
            while (resultSet.next()) {
                invalid_names += (invalid_names.length() > 0 ? ", " : "") + resultSet.getString("rateplan_name");
                allIds.remove(resultSet.getLong("rateplan_id"));
            }
            resultSet.close();

            if (!allIds.isEmpty()) {

                for (long validId : allIds) {
                    valid_ids += "," + validId;
                }

                sql = "update rateplans set rateplan_delete = 1 where rateplan_id in (" + valid_ids + ")";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.executeUpdate();

                sql = "update rates set rate_delete = 1 where rateplan_id in (" + valid_ids + ")";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.executeUpdate();
            }
            if (invalid_names.length() > 0) {
                error.setErrorType(MyAppError.NotUpdated);
                error.setErrorMessage("Rate Plan is assigned to a Client, can not be deleted : " + invalid_names);
            }else{
                error.setErrorMessage("Deleted Successfully");
            }

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting rateplan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public static Rateplan getRateplanDTO(long id) {
        Rateplan rateplan = null;
        if (rateplanMap.containsKey(id)) {
            rateplan = rateplanMap.get(id);
        } else {
            DBConnection dbConnection = null;
            PreparedStatement ps = null;
            Statement statement = null;
            try {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

                String sql = "select rateplan_name from rateplans where rateplan_delete = 0 AND rateplan_id=" + id;
                ps = dbConnection.connection.prepareStatement(sql);
//                ps.setLong(1, id);

                ResultSet resultSet = ps.executeQuery(sql);
                if (resultSet.next()) {
                    rateplan = new Rateplan();
                    rateplan.setRateplan_name(resultSet.getString("rateplan_name"));
                }

                resultSet.close();
            } catch (Exception e) {
//                logger.fatal("Exception in searchRateplansDTOList:", e);
            } finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }
            }
        }
        return rateplan;
    }
}
