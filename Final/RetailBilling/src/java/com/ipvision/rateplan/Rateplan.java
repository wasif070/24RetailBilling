/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.rateplan;

/**
 *
 * @author Wasif Islam
 */
public class Rateplan {
    private Long rateplan_id;
    private String rateplan_name;
    private String rateplan_des;
    private Integer rateplan_status;
    private String rateplan_status_name;
    private String rate;

    public Long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(Long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public String getRateplan_name() {
        return rateplan_name;
    }

    public void setRateplan_name(String rateplan_name) {
        this.rateplan_name = rateplan_name;
    }

    public String getRateplan_des() {
        return rateplan_des;
    }

    public void setRateplan_des(String rateplan_des) {
        this.rateplan_des = rateplan_des;
    }

    public Integer getRateplan_status() {
        return rateplan_status;
    }

    public void setRateplan_status(Integer rateplan_status) {
        this.rateplan_status = rateplan_status;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRateplan_status_name() {
        return rateplan_status_name;
    }

    public void setRateplan_status_name(String rateplan_status_name) {
        this.rateplan_status_name = rateplan_status_name;
    }
    
}
