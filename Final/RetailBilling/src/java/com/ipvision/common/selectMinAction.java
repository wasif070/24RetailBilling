/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.common;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.ArrayList;

/**
 *
 * @author Wasif Islam
 */
public class selectMinAction extends ActionSupport implements Preparable {

    public ArrayList<Integer> minList;

    public ArrayList<Integer> getMinList() {
        return minList;
    }

    public void setMinList(ArrayList<Integer> minList) {
        this.minList = minList;
    }
    

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    @Override
    public void prepare() throws Exception {
        minList = new ArrayList<Integer>();
        for (int i = 0; i < 60; i++) {
            minList.add(i);
        }
    }
}
