/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.common;

/**
 *
 * @author reefat
 */
public class FiltersSubFields {
    private String field;
    private String op;
    private String data;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{\"field\":\"" + field + "\",\"op\":\"" + op + "\",\"data\":\"" + data + "\"}";
    }


}
