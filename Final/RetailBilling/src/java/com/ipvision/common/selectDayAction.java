/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.common;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Wasif Islam
 */
public class selectDayAction extends ActionSupport implements Preparable {

    public Map<Integer, String> dayList;

    public Map<Integer, String> getDayList() {
        return dayList;
    }

    public void setDayList(Map<Integer, String> dayList) {
        this.dayList = dayList;
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    @Override
    public void prepare() throws Exception {
        dayList = new HashMap<Integer, String>();
        dayList.put(-1, "All");
        dayList.put(0, "Sunday");
        dayList.put(1, "Monday");
        dayList.put(2, "Tuesday");
        dayList.put(3, "Wednesday");
        dayList.put(4, "Thursday");
        dayList.put(5, "Friday");
        dayList.put(6, "Saturday");


    }
}
