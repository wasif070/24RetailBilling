/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.common;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author reefat
 */
public class StatusSelectAction extends ActionSupport implements Preparable {

    public Map<Integer, String> statusList;

    public Map<Integer, String> getStatusList() {
        return statusList;
    }

    public void setStatusList(Map<Integer, String> statusList) {
        this.statusList = statusList;
    }
    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    @Override
    public void prepare() throws Exception {
        statusList = new HashMap<Integer, String>();
        statusList.put(0, "Active");
        statusList.put(1, "Block");                
    }
}
