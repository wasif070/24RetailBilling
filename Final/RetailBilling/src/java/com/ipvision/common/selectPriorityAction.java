/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.common;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.ArrayList;

/**
 *
 * @author Wasif Islam
 */
public class selectPriorityAction extends ActionSupport implements Preparable {

    public ArrayList<Integer> priorityList;

    public ArrayList<Integer> getPriorityList() {
        return priorityList;
    }

    public void setPriorityList(ArrayList<Integer> priorityList) {
        this.priorityList = priorityList;
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    @Override
    public void prepare() throws Exception {
        priorityList = new ArrayList<Integer>();
        for (int i = 1; i < 11; i++) {
            priorityList.add(i);
        }
    }
}
