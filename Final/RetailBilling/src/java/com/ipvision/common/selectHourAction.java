/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.common;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.ArrayList;

/**
 *
 * @author Wasif Islam
 */
public class selectHourAction extends ActionSupport implements Preparable {

    public ArrayList<Integer> hourList;

    public ArrayList<Integer> getHourList() {
        return hourList;
    }

    public void setHourList(ArrayList<Integer> hourList) {
        this.hourList = hourList;
    }
    

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    @Override
    public void prepare() throws Exception {
        hourList = new ArrayList<Integer>();
        for (int i = 0; i < 24; i++) {
            hourList.add(i);
        }
    }
}
