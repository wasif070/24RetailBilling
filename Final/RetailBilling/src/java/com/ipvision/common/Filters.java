/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.common;

import java.util.ArrayList;

/**
 *
 * @author reefat
 */
public class Filters {
    private String groupOp;
    private ArrayList<FiltersSubFields> rules;

    public String getGroupOp() {
        return groupOp;
    }

    public void setGroupOp(String groupOp) {
        this.groupOp = groupOp;
    }

    public ArrayList getRules() {
        return rules;
    }

    public void setRules(ArrayList rules) {
        this.rules = rules;
    }    
}
