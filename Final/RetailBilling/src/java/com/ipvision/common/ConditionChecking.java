/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.log4j.Logger;

/**
 *
 * @author reefat
 */
public class ConditionChecking {

    Logger logger = Logger.getLogger(ConditionChecking.class.getName());
    public boolean checkConditionNumeric(String param_comparison_type, double id, double parseInt) {
        boolean return_val = false;
        switch (DataStorage.getInstance().getComparisonTypeMapping(param_comparison_type)) {
            case 0:
                return_val = (id == parseInt) ? true : false;
                break;
            case 1:
                return_val = (id != parseInt) ? true : false;
                break;
            case 2:
                return_val = (id < parseInt) ? true : false;
                break;
            case 3:
                return_val = (id <= parseInt) ? true : false;
                break;
            case 4:
                return_val = (id > parseInt) ? true : false;
                break;
            case 5:
                return_val = (id >= parseInt) ? true : false;
                break;
            case 6:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 7:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 8:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 9:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 10:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 11:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 12:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 13:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            default:
                break;
        }
        return return_val;
    }

    public boolean checkConditionString(String param_comparison_type, String name, String data) {
        boolean return_val = false;
        switch (DataStorage.getInstance().getComparisonTypeMapping(param_comparison_type)) {
            case 0:
                return_val = (name.equals(data)) ? true : false;
                break;
            case 1:
                return_val = (!name.equals(data)) ? true : false;
                break;
            case 2:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 3:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 4:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 5:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 6:
                return_val = (name.startsWith(data)) ? true : false;
                break;
            case 7:
                return_val = (!name.startsWith(data)) ? true : false;
                break;
            case 8:
                return_val = (name.contains(data) && (name.indexOf(data) + data.length() == name.length())) ? true : false;
                break;
            case 9:
                return_val = (!(name.contains(data) && (name.indexOf(data) + data.length() == name.length()))) ? true : false;
                break;
            case 10:
                return_val = (name.contains(data)) ? true : false;
                break;
            case 11:
                return_val = (!name.contains(data)) ? true : false;
                break;
            case 12:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 13:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            default:
                break;
        }
        return return_val;
    }

    public boolean checkConditionTime(String param_comparison_type, String param_01, String param_02) {
//        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
        Calendar cal_data = Calendar.getInstance();
        Calendar cal_search_param = Calendar.getInstance();
        Calendar cal_search_param_temp = Calendar.getInstance();
        int v_comparison_type = DataStorage.getInstance().getComparisonTypeMapping(param_comparison_type);

        try {
            cal_data.setTime(ft.parse(param_01));
            cal_search_param.setTime(formatter.parse(param_02 + " 00:00:00.000"));
            cal_search_param_temp.setTime(formatter.parse(param_02 + " 23:59:59.999"));
        } catch (Exception e) {
            logger.debug("Exception in date convertion in CondtionChecking class :: " + e);
        }

        boolean return_val = false;
        switch (v_comparison_type) {
            case 0:
                cal_search_param_temp.add(Calendar.DATE, -1);
                cal_search_param.add(Calendar.DATE, 1);
                return_val = (cal_data.getTime().before(cal_search_param.getTime()) && cal_data.getTime().after(cal_search_param_temp.getTime())) ? true : false;
                break;
            case 1:
                cal_search_param_temp.add(Calendar.DATE, -1);
                cal_search_param.add(Calendar.DATE, 1);
                return_val = (!(cal_data.getTime().before(cal_search_param.getTime()) && cal_data.getTime().after(cal_search_param_temp.getTime()))) ? true : false;
                break;
            case 2:
                return_val = (cal_data.getTime().before(cal_search_param.getTime())) ? true : false;
                break;
            case 3:
                return_val = (cal_data.getTime().before(cal_search_param_temp.getTime())) ? true : false;
                break;
            case 4:
                return_val = (cal_data.getTime().after(cal_search_param_temp.getTime())) ? true : false;
                break;
            case 5:
                return_val = (cal_data.getTime().after(cal_search_param.getTime())) ? true : false;
                break;
            case 6:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 7:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 8:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 9:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 10:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 11:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 12:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            case 13:
                System.err.println("*** NOT IMPLEMENTED ***");
                break;
            default:
                break;
        }
        return return_val;
    }
}
