/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.common;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author reefat
 */
public class DataStorage {

    private static Map<String, Integer> field_mapping;// = new HashMap<String, Integer>();
    private static Map<String, Integer> field_mapping_live_users;// = new HashMap<String, Integer>();
    private static Map<String, Integer> field_mapping_gateway;// = new HashMap<String, Integer>();
    private static Map<String, Integer> field_mapping_dialplan;
    private static Map<String, Integer> comparison_type_mapping;
    private static DataStorage dataStorage;

    DataStorage() {
        if (field_mapping == null) {
            field_mapping = new HashMap<String, Integer>();

            field_mapping.put("client_id", 0);
            field_mapping.put("rateplan_id", 1);
            field_mapping.put("client_name", 2);
            field_mapping.put("client_email", 3);
            field_mapping.put("client_type", 4);
            field_mapping.put("client_status", 5);
            field_mapping.put("client_credit_limit", 6);
            field_mapping.put("client_balance", 7);
            field_mapping.put("client_call_limit", 8);
        }



        if (comparison_type_mapping == null) {
            comparison_type_mapping = new HashMap<String, Integer>();

            comparison_type_mapping.put("eq", 0);
            comparison_type_mapping.put("ne", 1);
            comparison_type_mapping.put("lt", 2);
            comparison_type_mapping.put("le", 3);
            comparison_type_mapping.put("gt", 4);
            comparison_type_mapping.put("ge", 5);

            comparison_type_mapping.put("bw", 6);
            comparison_type_mapping.put("bn", 7);
            comparison_type_mapping.put("ew", 8);
            comparison_type_mapping.put("en", 9);
            comparison_type_mapping.put("cn", 10);
            comparison_type_mapping.put("nc", 11);
            comparison_type_mapping.put("in", 12);
            comparison_type_mapping.put("ni", 13);
        }


        if (field_mapping_live_users == null) {
            field_mapping_live_users = new HashMap<String, Integer>();

            field_mapping_live_users.put("call_id", 0);
            field_mapping_live_users.put("origin_caller", 1);
            field_mapping_live_users.put("origin_ip", 2);
            field_mapping_live_users.put("term_callee", 3);
            field_mapping_live_users.put("term_ip", 4);
            field_mapping_live_users.put("term_gw", 5);
            field_mapping_live_users.put("invite_time", 6);
            field_mapping_live_users.put("accepted_time", 7);
            field_mapping_live_users.put("duration", 8);
            field_mapping_live_users.put("status", 9);
        }
        if (field_mapping_gateway == null) {
            field_mapping_gateway = new HashMap<String, Integer>();

            field_mapping_gateway.put("gateway_name", 0);
            field_mapping_gateway.put("gateway_ip", 1);
            field_mapping_gateway.put("gateway_port", 2);
            field_mapping_gateway.put("gateway_status", 3);
            field_mapping_gateway.put("gateway_type", 4);
            field_mapping_gateway.put("client_id", 5);
        }
        
        if (field_mapping_dialplan == null) {
            field_mapping_dialplan = new HashMap<String, Integer>();

            field_mapping_dialplan.put("dp_name", 0);
            field_mapping_dialplan.put("dp_dnis_pattern", 1);
            field_mapping_dialplan.put("dp_capacity", 2);
            field_mapping_dialplan.put("dp_ani_translate", 3);
            field_mapping_dialplan.put("dp_dnis_translate", 4);
            field_mapping_dialplan.put("dp_priority", 5);
        }

    }

    public static DataStorage getInstance() {
        if (dataStorage == null) {
            dataStorage = new DataStorage();
        }

        return dataStorage;
    }

    public Integer getFieldMapping(String param) {
        return field_mapping.get(param);
    }

    public Integer getFieldMappingLiveUsers(String param) {
        return field_mapping_live_users.get(param);
    }

    public Integer getFieldMappingGateway(String param) {
        return field_mapping_gateway.get(param);
    }
    
    public Integer getFieldMappingDialplan(String param) {
        return field_mapping_dialplan.get(param);
    }    

    public Integer getComparisonTypeMapping(String param) {
        return comparison_type_mapping.get(param);
    }
}
