/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.login;

import com.ipvision.home.HomeLoader;
import com.ipvision.log.DBLogger;
import com.ipvision.user.UserDAO;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;
import util.MyAppError;

/**
 *
 * @author reefat
 */
public class LoginAction extends ActionSupport implements SessionAware {

    private String username;
    private String password;
    private Map<String, Object> userSession;

    @Override
    public String execute() {
        UserDAO userDAO = new UserDAO();
        MyAppError myAppError = userDAO.checkUserValidity(getUsername(), getPassword());
        if (myAppError.getErrorType() > 0) {
            addActionError(getText(myAppError.getErrorMessage()));
            return ERROR;
        } else {
            getUserSession().put("userName", userDAO.findByUserId(getUsername()));
            DBLogger.getInstance().startThread();
            return SUCCESS;
        }
    }

    public String checkSessionTimeout() {
        HttpServletRequest request = ServletActionContext.getRequest();
        if (request.getSession().getAttribute("userName") == null) {
            return LOGIN;
        }
        return SUCCESS;
    }

    public String logout() throws Exception {
        System.out.println("LogOut");
        getUserSession().put("userName", null);
        return SUCCESS;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        userSession = session;
    }

    private Map<String, Object> getUserSession() {
        return userSession;
    }
}
