/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.login;

import com.ipvision.user.Users;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author Wasif Islam
 */
public class LoginLoader {

    static Logger logger = Logger.getLogger(LoginLoader.class.getName());
    public static ArrayList<String> loginList = null;
    static LoginLoader loginLoader = null;

    public LoginLoader() {
    }

    public static LoginLoader getInstance() {
        if (loginLoader == null) {
            createHomeLoader();
        }
        return loginLoader;
    }

    private synchronized static void createHomeLoader() {
        if (loginLoader == null) {
            loginLoader = new LoginLoader();
        }
    }

    public synchronized ArrayList<String> getLoginDTOList() {
        loginList = new ArrayList<String>();

        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select announcement_str from announcements where announcement_status=0 order by announcement_priority asc";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                loginList.add(resultSet.getString("announcement_str"));
            }

        } catch (Exception e) {
            logger.fatal("Exception in LoginLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return loginList;
    }
}
