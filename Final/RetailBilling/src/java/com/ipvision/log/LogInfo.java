/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.log;

/**
 *
 * @author reefat
 */
public class LogInfo {
    private int id;
    private String tag;
    private String type;
    private String operation_name;
    private String description;
    private String error_description;
    private String error_location;
    private String insert_time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperation_name() {
        return operation_name;
    }

    public void setOperation_name(String operation_name) {
        this.operation_name = operation_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        if(error_description != null && error_description.length() > 1000){
            error_description = error_description.substring(0, 996) + "...";
        }
        this.error_description = error_description;
    }

    public String getError_location() {
        return error_location;
    }

    public void setError_location(String error_location) {
        this.error_location = error_location;
    }

    public String getInsert_time() {
        return insert_time;
    }

    public void setInsert_time(String insert_time) {
        this.insert_time = insert_time;
    }
}
