/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.log;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author reefat
 */
public class IPVLogger {

    private static Queue<LogInfo> v_queue_for_log;// = new SynchronousQueue<LogInfo>();

    public static Queue<LogInfo> getV_queue_for_log() {
        return v_queue_for_log;
    }

    public void addLogEntry(String i_tag, String i_type, String i_operation_name, String i_description, String i_error_description, String i_error_location) {
        LogInfo logInfo = new LogInfo();
        logInfo.setTag(i_tag);
        logInfo.setType(i_type);
        logInfo.setOperation_name(i_operation_name);
        logInfo.setDescription(i_description);
        logInfo.setError_description(i_error_description);
        logInfo.setError_location(i_error_location);
        if (v_queue_for_log == null) {
            v_queue_for_log = new LinkedList<LogInfo>();
        }
        v_queue_for_log.add(logInfo);
    }
}
