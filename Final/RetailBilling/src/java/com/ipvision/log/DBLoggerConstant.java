/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.log;

/**
 *
 * @author reefat
 */
public class DBLoggerConstant {
    public static final String SUCCESSFUL = "successful";
    public static final String WARNING = "warning";
    public static final String ERROR = "error";
    public static final String INFO = "info";
    
    public static final String SQL = "sql";
    public static final String DB = "db";
    public static final String INSERT = "insert";
    public static final String UPDATE = "update";
    public static final String DELETE = "delete";
    public static final String OPENING_DB_CONNECTION = "opendb";
    public static final String CLOSING_DB_CONNECTION = "closedb";
    public static final String MEMCACHE = "memcache";
    public static final String OPERATION = "operation";
    public static final String FETCH = "fetch";
}
