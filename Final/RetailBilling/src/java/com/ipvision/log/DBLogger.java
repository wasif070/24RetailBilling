/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.log;

import databaseconnector.DBConnection;
import databaseconnector.DBConnector;
import static java.lang.Thread.sleep;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 *
 * @author reefat
 */
public class DBLogger extends Thread {

    static Logger logger = Logger.getLogger(DBLogger.class.getName());
    static boolean stop = false;
    private static DBLogger dBLogger = null;
    static boolean thread_started = false;

    public static DBLogger getInstance() {
        if (dBLogger == null) {
            dBLogger = new DBLogger();
        }
        return dBLogger;
    }

    public void stopThread() {
        stop = true;
    }

    public void startThread() {
        if (!thread_started) {
            this.start();
            thread_started = true;
        }
    }
    DBConnection dBConnection = null;

    @Override
    public void run() {
        logger.debug("---------------------------------- DBLogger thread started ----------------------------------");
        while (!stop) {
            if (IPVLogger.getV_queue_for_log() == null || (IPVLogger.getV_queue_for_log() != null && IPVLogger.getV_queue_for_log().isEmpty())) {
                try {
                    sleep(1000);
                } catch (InterruptedException ex) {
                    logger.debug("Error while sending 'DBLogger' thread to sleep --> " + ex);
                }
            } else {
                PreparedStatement ps = null;
                LogInfo logInfo = IPVLogger.getV_queue_for_log().poll();
                if (dBConnection == null) {
                    try {
                        dBConnection = DBConnector.getInstance().makeConnection();
                    } catch (Exception ex) {
                        logger.debug("Error while opening db connection --> " + ex);
                    }
                }
                try {
                    String sql = "";
                    sql = "insert into ipv_log set "
                            + "tag = ?, "
                            + "type = ?, "
                            + "operation_name = ?, "
                            + "description = ?, "
                            + ((logInfo.getError_description() != null) ? "error_description = ?, " : "")
                            + "error_location = ? ";
                    ps = dBConnection.connection.prepareStatement(sql);
                    int i = 1;
                    ps.setString(i++, logInfo.getTag());
                    ps.setString(i++, logInfo.getType());
                    ps.setString(i++, logInfo.getOperation_name());
                    ps.setString(i++, logInfo.getDescription());
                    if (logInfo.getError_description() != null) {
                        ps.setString(i++, logInfo.getError_description());
                    }
                    ps.setString(i++, logInfo.getError_location());

                    ps.executeUpdate();
                } catch (Exception ex) {
                    logger.debug("Error while inserting log to DB --> " + ex);
                } finally {
                    if (dBConnection != null) {
                        try {
                            databaseconnector.DBConnector.getInstance().freeConnection(dBConnection);
                        } catch (Exception ex) {
                            logger.debug("Error while making dbConnection free --> " + ex);
                        }
                    }

                    if (ps != null) {
                        try {
                            ps.close();
                        } catch (SQLException ex) {
                            logger.debug("Error while making ps free --> " + ex);
                        }
                    }
                }
            }
        }

        logger.debug("------------------------ DbLogger thread stopped ------------------------");
    }
}