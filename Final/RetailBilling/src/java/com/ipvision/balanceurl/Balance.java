/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.balanceurl;

import com.opensymphony.xwork2.ActionSupport;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author reefat
 */
public class Balance extends ActionSupport {
    static Logger logger = Logger.getLogger(Balance.class.getName());
//    http://38.108.92.154/24RetailBilling/balance/showBalance.action?pin=123456
//    http://localhost:8080/RetailBilling/balance/showBalance.action?pin=123456

    private String balance;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public Balance() {
    }

    @Override
    public String execute() throws Exception {
        String sql = "";
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        HttpServletRequest request = ServletActionContext.getRequest();
        String client_id = request.getParameter("pin");
//        select client_balance from clients where client_delete = 0 and  client_id = '123456';
        sql = "SELECT client_balance FROM clients WHERE client_delete = 0 and client_id = ?";

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, client_id);
            rs = ps.executeQuery();
            if (rs != null && rs.next()) {
                this.setBalance(String.valueOf(rs.getDouble("client_balance")));                
            } else {
                this.setBalance("n/a");
            }
        } catch (Exception e) {
            logger.debug("Exception in Balance read --> " + e);
            this.setBalance("n/a");
        } finally {
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
        }

        return SUCCESS;
    }
}