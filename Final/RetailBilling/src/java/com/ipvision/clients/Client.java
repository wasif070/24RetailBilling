/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.clients;

import java.util.Comparator;

/**
 *
 * @author reefat
 */
public class Client implements Comparable<Client> {
    private Integer id;
    private Integer client_db_id;
    private String client_id;
    private String rateplan_id;
    private Long rateplan_id_long;
    private String client_password;
    private String client_name;
    private String client_email;
    private String client_type;
    private String client_status;
    private Double client_credit_limit;
    private Double client_balance;
    private Integer client_delete;
    private String prefix;
    private Long client_call_limit;
    private String client_creation_time;
    private static Integer field;
    private static Integer sort_order;
    private String payment;

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClient_db_id() {
        return client_db_id;
    }

    public void setClient_db_id(Integer client_db_id) {
        this.client_db_id = client_db_id;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(String rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public Long getRateplan_id_long() {
        return rateplan_id_long;
    }

    public void setRateplan_id_long(Long rateplan_id_long) {
        this.rateplan_id_long = rateplan_id_long;
    }

    public String getClient_password() {
        return client_password;
    }

    public void setClient_password(String client_password) {
        this.client_password = client_password;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getClient_email() {
        return client_email;
    }

    public void setClient_email(String client_email) {
        this.client_email = client_email;
    }

    public String getClient_type() {
        return client_type;
    }

    public void setClient_type(String client_type) {
        this.client_type = client_type;
    }

    public String getClient_status() {
        return client_status;
    }

    public void setClient_status(String client_status) {
        this.client_status = client_status;
    }

    public Double getClient_credit_limit() {
        return client_credit_limit;
    }

    public void setClient_credit_limit(Double client_credit_limit) {
        this.client_credit_limit = client_credit_limit;
    }

    public Double getClient_balance() {
        return client_balance;
    }

    public void setClient_balance(Double client_balance) {
        this.client_balance = client_balance;
    }

    public Integer getClient_delete() {
        return client_delete;
    }

    public void setClient_delete(Integer client_delete) {
        this.client_delete = client_delete;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Long getClient_call_limit() {
        return client_call_limit;
    }

    public void setClient_call_limit(Long client_call_limit) {
        this.client_call_limit = client_call_limit;
    }

    public String getClient_creation_time() {
        return client_creation_time;
    }

    public void setClient_creation_time(String client_creation_time) {
        this.client_creation_time = client_creation_time;
    }

    public static Integer getField() {
        return field;
    }

    public static void setField(Integer field) {
        Client.field = field;
    }

    public static Integer getSort_order() {
        return sort_order;
    }

    public static void setSort_order(Integer sort_order) {
        Client.sort_order = sort_order;
    }

    @Override
    public int compareTo(Client o) {
        Double var1 = 0.0;// = this.client_call_limit;
        Double var2 = 0.0;// = o.getClient_call_limit();
        switch (field) {
            case 6:
                var1 = this.client_credit_limit;
                var2 = o.getClient_credit_limit();
                break;
            case 7:
                var1 = this.client_balance;
                var2 = o.getClient_balance();
                break;
            case 8:
                var1 = this.client_call_limit * 1.0;
                var2 = o.getClient_call_limit() * 1.0;
                break;
            default:
                break;
        }
        switch (sort_order) {
            case 0:
                //ascending order
                return var1.compareTo(var2);
            case 1:
                //descending order
                return var2.compareTo(var1);
            default:
                return -1;
        }
    }
    public static Comparator<Client> ClientPropertiesComparator = new Comparator<Client>() {
        public int compare(Client client1, Client client2) {

            String var_client_sorting_field_value_1 = null;// = client1.getFirstName().toUpperCase();
            String var_client_sorting_field_value_2 = null;// = client2.getFirstName().toUpperCase();

            switch (field) {

                case 0: //Client ID
                    var_client_sorting_field_value_1 = client1.getClient_id().toUpperCase();
                    var_client_sorting_field_value_2 = client2.getClient_id().toUpperCase();
                    break;
                case 1: //ratePlan ID
                    var_client_sorting_field_value_1 = String.valueOf(client1.getRateplan_id());
                    var_client_sorting_field_value_2 = String.valueOf(client2.getRateplan_id());
                    break;
                case 2: //Client Name
                    var_client_sorting_field_value_1 = client1.getClient_name().toUpperCase();
                    var_client_sorting_field_value_2 = client2.getClient_name().toUpperCase();
                    break;
                case 3: //Client Email
                    var_client_sorting_field_value_1 = client1.getClient_email().toUpperCase();
                    var_client_sorting_field_value_2 = client2.getClient_email().toUpperCase();
                    break;
                case 4: //Client Type
                    var_client_sorting_field_value_1 = String.valueOf(client1.getClient_type());
                    var_client_sorting_field_value_2 = String.valueOf(client2.getClient_type());
                    break;
                case 5: //Client Status
                    var_client_sorting_field_value_1 = String.valueOf(client1.getClient_status());
                    var_client_sorting_field_value_2 = String.valueOf(client2.getClient_status());
                    break;
                case 6: //Client Credit Limit
                    return client1.compareTo(client2);
                case 7: //Client Balance
                    return client1.compareTo(client2);
                case 8: //Client Call Limit
                    return client1.compareTo(client2);
                default:
                    break;
            }

            if (var_client_sorting_field_value_1 == null || var_client_sorting_field_value_2 == null) {
                return -1;
            }

            switch (sort_order) {
                case 0:
                    //ascending order
                    return var_client_sorting_field_value_1.compareTo(var_client_sorting_field_value_2);
                case 1:
                    //descending order
                    return var_client_sorting_field_value_2.compareTo(var_client_sorting_field_value_1);
                default:
                    return -1;
            }
        }
    };
}
