/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.clients;

/**
 *
 * @author reefat
 */
public class Payment {
    
    private Integer id;
    private Integer client_db_id;
    private String client_id;
    private Integer paymentType;
    private Double payment_amount;    
    private Double client_balance;    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClient_db_id() {
        return client_db_id;
    }

    public void setClient_db_id(Integer client_db_id) {
        this.client_db_id = client_db_id;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Double getClient_balance() {
        return client_balance;
    }

    public void setClient_balance(Double client_balance) {
        this.client_balance = client_balance;
    }

    public Double getPayment_amount() {
        return payment_amount;
    }

    public void setPayment_amount(Double payment_amount) {
        this.payment_amount = payment_amount;
    }
}
