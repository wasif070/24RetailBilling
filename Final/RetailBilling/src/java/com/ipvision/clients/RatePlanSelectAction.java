/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.clients;

import com.ipvision.rateplan.Rateplan;
import com.ipvision.rateplan.RateplanDAO;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 *
 * @author reefat
 */
public class RatePlanSelectAction extends ActionSupport implements Preparable {

    public Map<Long, String> ratePlanList;

    public Map<Long, String> getRatePlanList() {
        return ratePlanList;
    }

    public void setRatePlanList(Map<Long, String> ratePlanList) {
        this.ratePlanList = ratePlanList;
    }


    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    @Override
    public void prepare() throws Exception {
        ratePlanList = new HashMap<Long, String>();
        List<Rateplan> list_rateplan = RateplanDAO.getInstance().search(null, "rateplan_id, rateplan_name,rateplan_status", null, null, 0, -1);
        if (list_rateplan != null) {
            for (Rateplan objRateplan : list_rateplan) {
                if (objRateplan.getRateplan_status() == 0) {
                    ratePlanList.put(objRateplan.getRateplan_id(), objRateplan.getRateplan_name());
                }
                
            }
        }
    }
}
