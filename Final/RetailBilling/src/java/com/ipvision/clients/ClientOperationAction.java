/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.clients;

import com.ipvision.common.Admin;
import com.ipvision.gateway.GatewayDAO;
import com.ipvision.user.Users;
import static com.opensymphony.xwork2.Action.*;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import util.Constants;
import util.MyAppError;
import util.Utils;

/**
 *
 * @author reefat
 */
public class ClientOperationAction extends ActionSupport implements Admin {

    private String action;
    private String id;
    private int client_db_id;
    private String client_id;
    private String client_name;
    private String client_password;
    private String client_email;
    private long rateplan_id;
    private int client_type;
    private int client_status;
    private Double client_credit_limit;
    private Double client_balance;
    private String prefix;
    private Long client_call_limit;
    private Double payment_amount;
    private Integer payment_type;

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String add() throws Exception {
        String return_val = SUCCESS;

        Client client = new Client();
        client.setClient_id(client_id);
        client.setRateplan_id(String.valueOf(rateplan_id));
        client.setClient_name(client_name);
        client.setClient_password(client_password);
        client.setClient_email(client_email);
        client.setClient_type(String.valueOf(client_type));
        client.setClient_status(String.valueOf(client_status));
        client.setClient_credit_limit((client_credit_limit != null ? client_credit_limit : 0.00));
        client.setClient_balance((client_balance != null ? client_balance : 0.00));
        client.setPrefix(prefix);
        client.setClient_call_limit((client_call_limit != null ? client_call_limit : 0));

        ClientDAO clientDAO = new ClientDAO();
        MyAppError myAppError = clientDAO.save(client);

        if (myAppError.getErrorType() > 0) {
            addActionError(myAppError.getErrorMessage());
            return_val = INPUT;
        } else {
            addActionMessage(myAppError.getErrorMessage());
        }

        return return_val;
    }

    public String update() throws Exception {
        String return_val = SUCCESS;

        Client client = null;
        HttpServletRequest request = ServletActionContext.getRequest();
        Users usr = (Users) request.getSession().getAttribute("userName");
        ClientDAO clientDAO = new ClientDAO();

        if (id != null && id.length() > 0 && usr.getRole_id() < 0) {
            client = clientDAO.findById(Integer.parseInt(id));
        } else {
            if (usr != null && usr.getRole_id() > 0 && usr.getUser_id() != null && usr.getUser_id().length() > 0) {
                client = clientDAO.findClientIdByDbId(usr.getDb_id());
            }
        }

        if (usr != null && (usr.getRole_id() < 0 || (client != null ? usr.getUser_id().equals(client.getClient_id()) : false))) {
            if (client != null) {
                MyAppError myAppError;// = new MyAppError();
                GatewayDAO gatewayDAO = new GatewayDAO();
                if (gatewayDAO.isClientChangeAllowed(client.getClient_id(), client_type)) {
                    client.setRateplan_id(String.valueOf(rateplan_id));
                    client.setClient_name(client_name);
                    client.setClient_password(client_password);
                    client.setClient_email(client_email);
                    client.setClient_type(String.valueOf(client_type));
                    client.setClient_status(String.valueOf(client_status));
                    client.setClient_credit_limit((client_credit_limit != null ? client_credit_limit : 0.00));
                    client.setClient_balance((client_balance != null ? client_balance : 0.00));
                    client.setPrefix(prefix);
                    client.setClient_call_limit((client_call_limit != null ? client_call_limit : 0));

                    myAppError = clientDAO.update(client, usr.getRole_id());
                }else{
                    myAppError = new MyAppError();
                    myAppError.setErrorType(MyAppError.ValidationError);
                    myAppError.setErrorMessage("Please remove the gateways, assigned to this client first");
                }



                if (myAppError.getErrorType() > 0) {
                    addActionError(myAppError.getErrorMessage());
                    return_val = INPUT;
                } else {
                    addActionMessage(myAppError.getErrorMessage());
                }
            } else {
                addActionError(getText("notFound.client"));
                return_val = INPUT;
            }
            if (usr.getRole_id() > 0) {
                return_val = NONE;
            }
        } else {
            return_val = LOGIN;
        }

        return return_val;
    }

    public String delete() throws Exception {
        String return_val = SUCCESS;
        ArrayList<Client> client_list = null;
        String[] individual_id_str = id.split(",");
        int[] individual_id_int = new int[individual_id_str.length];
        for (int i = 0; i < individual_id_str.length; i++) {
            individual_id_int[i] = Integer.valueOf(individual_id_str[i]);
        }
        ClientDAO clientDAO = new ClientDAO();
        client_list = clientDAO.findMultipleById(individual_id_int);

        MyAppError myAppError = clientDAO.delete(client_list);

        if (myAppError.getErrorType() > 0) {
            addActionError(myAppError.getErrorMessage());
            return_val = INPUT;
        } else {
            String[] var_msg = myAppError.getErrorMessage().split("#");
            if (var_msg.length > 1) {
                addActionError(var_msg[0]);
                addActionMessage(getText(var_msg[1]));
            } else {
                addActionMessage(getText(myAppError.getErrorMessage()));
            }
        }
        return return_val;
    }

    public String payment() throws Exception {
        String return_val = SUCCESS;

        Payment payment = new Payment();
        Client client;
        ClientDAO clientDAO = new ClientDAO();
        client = clientDAO.findById(Integer.parseInt(id));

        if (client != null) {
            if (payment_amount == null || payment_amount < 0) {
                addFieldError("payment_amount", "*invalid payment amount");
                return INPUT;
            } else if (payment_type == 1 && client.getClient_balance() < payment_amount) {
                addActionError("Payment amount exceeds balance");
                return INPUT;
            }
            payment.setClient_db_id(client.getClient_db_id());
            payment.setClient_id(client.getClient_id());
            payment.setPaymentType(payment_type);
            payment.setPayment_amount(payment_amount);
            payment.setClient_balance(client.getClient_balance());

            MyAppError myAppError = clientDAO.updatePaymentInfo(payment);

            if (myAppError.getErrorType() > 0) {
                addActionError(myAppError.getErrorMessage());
                return_val = INPUT;
            } else {
                Double cur_balance = payment.getClient_balance() + (payment.getPaymentType() == 0 ? payment.getPayment_amount() : (payment.getPayment_amount() * -1));
                if (payment.getPaymentType() == 0) {
                    cur_balance = payment.getClient_balance() + payment.getPayment_amount();
                } else if (payment.getPaymentType() == 1) {
                    cur_balance = payment.getClient_balance() - payment.getPayment_amount();
                }
                client.setClient_balance(cur_balance);
                addActionMessage(myAppError.getErrorMessage());
            }
        } else {
            addActionError(getText("notFound.client"));
            return_val = INPUT;
        }

        return return_val;
    }

    public String retrieve() throws Exception {
        ClientDAO clientDAO = new ClientDAO();
        HttpServletRequest request = ServletActionContext.getRequest();
        Users usr = (Users) request.getSession().getAttribute("userName");
        Client clnt = null;

        if (request.getParameter("id") != null) {
            int row_id = Integer.valueOf(request.getParameter("id"));
            clnt = clientDAO.findById(row_id);
        } else if (true) {
            if (usr != null && usr.getUser_id().length() > 0) {
                this.setClient_id(usr.getUser_id());
                this.setClient_name(usr.getUser_full_name());
                this.setClient_password(usr.getUser_password());
                this.setClient_email(usr.getUser_email());
                this.setClient_db_id(usr.getDb_id());
            }
        }

        if (clnt != null) {
//            this.setId(String.valueOf(row_id));
            this.setClient_db_id(clnt.getClient_db_id());
            this.setClient_id(clnt.getClient_id());
            this.setRateplan_id(clnt.getRateplan_id_long());
            this.setClient_name(clnt.getClient_name());
            this.setClient_password(clnt.getClient_password());
            this.setClient_email(clnt.getClient_email());
            this.setClient_type(Utils.findIndex(Constants.TYPE_STRING, clnt.getClient_type()));
            this.setClient_status(Utils.findIndex(Constants.STATUS_STRING, clnt.getClient_status()));
            this.setClient_credit_limit(clnt.getClient_credit_limit());
            this.setClient_balance(clnt.getClient_balance());
            this.setPrefix(clnt.getPrefix());
            this.setClient_call_limit(clnt.getClient_call_limit());
        }

        this.setAction("update");

        return INPUT;
    }

    public String display() throws Exception {
        this.setAction("add");
        return INPUT;
    }

    @Override
    public void validate() {
        this.setAction(ActionContext.getContext().getName());
        String actn = this.getAction();
        HttpServletRequest request = ServletActionContext.getRequest();
        Users usr = (Users) request.getSession().getAttribute("userName");
//        if (usr != null && usr.getRole_id() > 0 && !(actn.equals("retrieve") || actn.equals("update"))) {
//            addActionError("");
//        }else 

        if (actn.equalsIgnoreCase("add") || actn.equalsIgnoreCase("update")) {
            if (actn.equalsIgnoreCase("add")) {
                if (this.client_id == null || this.client_id.length() == 0) {
                    addFieldError("client_id", getText("required.common"));
                }

                if (client_id.contains(" ")) {
                    addActionError(getText("notAllowed.space") + " Client ID");
                }
            }
            if (this.client_password == null || this.client_password.length() == 0) {
                addFieldError("client_password", getText("required.common"));
            } else {
                if (this.client_password.contains(",")) {
                    addFieldError("client_password", getText("notAllowed.comma") + " password");
                }
            }
            if (usr.getRole_id() != null && usr.getRole_id() < 0 && this.rateplan_id == 0) {
                addFieldError("rateplan_id", getText("required.common"));
            }

            if (this.client_email != null && this.client_email.length() > 0 && !Utils.checkEmailId(this.client_email)) {
                addFieldError("client_email", getText("invalid.email"));
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getClient_db_id() {
        return client_db_id;
    }

    public void setClient_db_id(int client_db_id) {
        this.client_db_id = client_db_id;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getClient_password() {
        return client_password;
    }

    public void setClient_password(String client_password) {
        this.client_password = client_password;
    }

    public String getClient_email() {
        return client_email;
    }

    public void setClient_email(String client_email) {
        this.client_email = client_email;
    }

    public long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public int getClient_type() {
        return client_type;
    }

    public void setClient_type(int client_type) {
        this.client_type = client_type;
    }

    public int getClient_status() {
        return client_status;
    }

    public void setClient_status(int client_status) {
        this.client_status = client_status;
    }

    public Double getClient_credit_limit() {
        return client_credit_limit;
    }

    public void setClient_credit_limit(Double client_credit_limit) {
        this.client_credit_limit = client_credit_limit;
    }

    public Double getClient_balance() {
        return client_balance;
    }

    public void setClient_balance(Double client_balance) {
        this.client_balance = client_balance;
    }

    public Double getPayment_amount() {
        return payment_amount;
    }

    public void setPayment_amount(Double payment_amount) {
        this.payment_amount = payment_amount;
    }

    public int getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(Integer payment_type) {
        this.payment_type = payment_type;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Long getClient_call_limit() {
        return client_call_limit;
    }

    public void setClient_call_limit(Long client_call_limit) {
        this.client_call_limit = client_call_limit;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
