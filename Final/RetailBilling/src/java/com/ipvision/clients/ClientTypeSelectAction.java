/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.clients;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author reefat
 */
public class ClientTypeSelectAction extends ActionSupport implements Preparable {

    public Map<Integer, String> clientTypeList;

    public Map<Integer, String> getClientTypeList() {
        return clientTypeList;
    }

    public void setClientTypeList(Map<Integer, String> clientTypeList) {
        this.clientTypeList = clientTypeList;
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    @Override
    public void prepare() throws Exception {
        clientTypeList = new HashMap<Integer, String>();
        clientTypeList.put(0, "Origination");
        clientTypeList.put(1, "Termination");
        clientTypeList.put(2, "Both");
    }
}
