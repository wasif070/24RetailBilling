/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.clients;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author reefat
 */
public class PaymentTypeSelectAction extends ActionSupport implements Preparable {

    public Map<Integer, String> paymentTypeList;

    public Map<Integer, String> getPaymentTypeList() {
        return paymentTypeList;
    }

    public void setPaymentTypeList(Map<Integer, String> paymentTypeList) {
        this.paymentTypeList = paymentTypeList;
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    @Override
    public void prepare() throws Exception {
        paymentTypeList = new HashMap<Integer, String>();
        paymentTypeList.put(0, "Recharge");
        paymentTypeList.put(1, "Return");
        paymentTypeList.put(2, "Receive");
    }
}
