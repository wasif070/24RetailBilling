/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.clients;

import com.ipvision.common.DataStorage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ipvision.common.ConditionChecking;
import com.ipvision.common.Filters;
import com.ipvision.common.FiltersSubFields;
import com.ipvision.log.DBLoggerConstant;
import com.ipvision.log.IPVLogger;
import com.ipvision.rateplan.RateplanDAO;
import com.ipvision.user.UserDAO;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import javaclient.EventSender;
import util.Constants;
import util.MyAppError;

/**
 *
 * @author reefat
 */
public class ClientDAO implements Comparable<Integer> {

    static IPVLogger logger = new IPVLogger();
//    private static Logger logger = Logger.getLogger(ClientDAO.class.getName());
    private PreparedStatement ps = null;
    protected DBConnection dbConnection = null;
    private Statement stmt = null;
    private static long last_update_time = 0;
//    public static HashMap<String, Double> clientBalance = null;

    static {
        client_list = buildList();
    }
    static List<Client> client_list;
    static List<Client> client_filtered_list;
    static List<Client> client_visible_list;

    public static List<Client> getClient_list() {
        return client_list;
    }

    public static List<Client> buildList() {
        ArrayList<Client> v_temp_client_list = new ArrayList<Client>();
//        clientBalance = new HashMap<String, Double>();
        DBConnection local_dbConnection = null;
        String sql;
        PreparedStatement prepared_statement = null;
        ResultSet rs;
        try {
            local_dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "select * from clients where client_delete = 0 order by id asc";
            prepared_statement = local_dbConnection.connection.prepareStatement(sql);
            rs = prepared_statement.executeQuery();
            while (rs.next()) {
                Client client = new Client();
                client.setClient_db_id(rs.getInt("id"));
                client.setClient_id(rs.getString("client_id"));
                client.setRateplan_id(RateplanDAO.getRateplanDTO(rs.getLong("rateplan_id")).getRateplan_name());
                client.setRateplan_id_long(rs.getLong("rateplan_id"));
                client.setClient_name(rs.getString("client_name"));
                client.setClient_password(rs.getString("client_password"));
                client.setClient_email(rs.getString("client_email"));
                client.setClient_type(Constants.getTYPE_STRING()[rs.getInt("client_type")]);
                client.setClient_status(Constants.getSTATUS_STRING()[rs.getInt("client_status")]);
                client.setClient_credit_limit(rs.getDouble("client_credit_limit"));
                client.setClient_balance(rs.getDouble("client_balance"));
                client.setClient_delete(rs.getInt("client_delete"));
                client.setPrefix(rs.getString("prefix"));
                client.setClient_call_limit(rs.getLong("client_call_limit"));
                client.setPayment("Payment");
//                clientBalance.put(client.getClient_id(), client.getClient_balance());

                v_temp_client_list.add(client);
            }

        } catch (Exception ex) {
            logger.addLogEntry("clients", DBLoggerConstant.ERROR, DBLoggerConstant.DB, "Error while fetching data from clients table ", ex.getMessage(), ClientDAO.class.getName());
//            logger.debug("Exception in ClientDAO , buildList() ---> " + ex.getMessage());
//            System.out.println("" + ex);
        } finally {
            try {
                if (prepared_statement != null) {
                    prepared_statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (local_dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(local_dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return v_temp_client_list;
    }

    MyAppError save(Client client) {
        MyAppError myAppError = new MyAppError();
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            ps = null;


            sql = "select * from clients where client_delete = 0 AND client_id = ?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, client.getClient_id());
            rs = ps.executeQuery();

            if (rs.next()) {
                myAppError.setErrorType(MyAppError.DBError);
                myAppError.setErrorMessage("Duplicate ID");
            } else {
                try {
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                }
                sql = "insert into clients set "
                        + "client_id = ?, "
                        + "rateplan_id = ?, "
                        + "client_password = ?, "
                        + "client_name = ?, "
                        + "client_email = ?, "
                        + "client_type = ?, "
                        + "client_status = ?, "
                        + "client_credit_limit = ?, "
                        + "prefix = ?, "
                        + "client_call_limit = ? ";
                ps = dbConnection.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                int i = 1;
                ps.setString(i++, client.getClient_id());
                ps.setLong(i++, Long.parseLong(client.getRateplan_id()));
                ps.setString(i++, client.getClient_password());
                ps.setString(i++, client.getClient_name());
                ps.setString(i++, client.getClient_email());
                ps.setInt(i++, Integer.parseInt(client.getClient_type()));
                ps.setInt(i++, Integer.parseInt(client.getClient_status()));
                ps.setDouble(i++, client.getClient_credit_limit());
                ps.setString(i++, client.getPrefix());
                ps.setLong(i++, client.getClient_call_limit());

                ps.executeUpdate();
                rs = ps.getGeneratedKeys();
                if (rs != null && rs.next()) {
                    client.setClient_db_id(rs.getInt(1));
                }

                Payment payment = new Payment();
                payment.setPaymentType(0);//recharge
                payment.setClient_id(client.getClient_id());
                payment.setPayment_amount(client.getClient_balance());
                payment.setClient_db_id(client.getClient_db_id());
                updatePaymentInfo(payment);

                client.setClient_type(Constants.getTYPE_STRING()[Integer.parseInt(client.getClient_type())]);
                client.setClient_status(Constants.getSTATUS_STRING()[Integer.parseInt(client.getClient_status())]);

                client_list.add(client);
                UserDAO userDAO = new UserDAO();
                userDAO.addSingleClient(client);

                this.sendClientInfoToAuthServer(client, Constants.ADD);
                myAppError.setErrorMessage("Successfully added");
            }
        } catch (Exception ex) {
            logger.addLogEntry("clients", DBLoggerConstant.ERROR, DBLoggerConstant.INSERT, "Error while save data to clients table ", ex.getMessage(), ClientDAO.class.getName());
//            logger.debug("Exception in ClientDAO , save() ---> " + ex.getMessage());
            myAppError.setErrorType(MyAppError.DBError);
            myAppError.setErrorMessage("DB error : " + ex.getMessage());
        } finally {
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
        }

        return myAppError;
    }

    MyAppError update(Client client, Integer role) {
        MyAppError myAppError = new MyAppError();
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            ps = null;

            sql = "select id from clients where client_delete = 0 AND id = ?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setInt(1, client.getClient_db_id());
            rs = ps.executeQuery();
            if (rs.next()) {
                sql = "Update clients set "
                        + "client_password = ? "
                        + ",client_name = ? "
                        + ",client_email = ? "
                        + (role < 0
                        ? ",rateplan_id = ? "
                        + ",client_type = ? "
                        + ",client_status = ? "
                        + ",client_credit_limit = ? "
                        + ",prefix = ? "
                        + ",client_call_limit = ? " : "")
                        + "where "
                        + "id = ? ";
                if (ps != null) {
                    ps.close();
                }
                ps = dbConnection.connection.prepareStatement(sql);
                int i = 1;
                ps.setString(i++, client.getClient_password());
                ps.setString(i++, client.getClient_name());
                ps.setString(i++, client.getClient_email());
                if (role < 0) {
                    ps.setLong(i++, Long.parseLong(client.getRateplan_id()));
                    ps.setInt(i++, Integer.parseInt(client.getClient_type()));
                    ps.setInt(i++, Integer.parseInt(client.getClient_status()));
                    ps.setDouble(i++, client.getClient_credit_limit());
                    //ps.setDouble(i++, client.getClient_balance());
                    ps.setString(i++, client.getPrefix());
                    ps.setLong(i++, client.getClient_call_limit());
//                ps.setTimestamp(i++, Timestamp.valueOf(DateFormat.getTimeStamp(System.currentTimeMillis())));
                }
                ps.setInt(i++, client.getClient_db_id());

                ps.executeUpdate();

                for (Client temp_client : (client_visible_list != null ? client_visible_list : client_list)) {
                    if (client.getClient_db_id() == temp_client.getClient_db_id()) {
                        temp_client.setClient_password(client.getClient_password());
                        temp_client.setClient_name(client.getClient_name());
                        temp_client.setClient_email(client.getClient_email());
                        if (role < 0) {
                            temp_client.setRateplan_id(client.getRateplan_id());
                            temp_client.setClient_type(Constants.getTYPE_STRING()[Integer.parseInt(client.getClient_type())]);
                            temp_client.setClient_status(Constants.getSTATUS_STRING()[Integer.parseInt(client.getClient_status())]);
                            temp_client.setClient_credit_limit(client.getClient_credit_limit());
                            temp_client.setClient_balance(client.getClient_balance());
                            temp_client.setPrefix(client.getPrefix());
                            temp_client.setClient_call_limit(client.getClient_call_limit());
                        }
                        break;
                    }
                }

                UserDAO userDAO = new UserDAO();
                userDAO.editSingleClient(client);

                if (client.getClient_status().equalsIgnoreCase("Active")) {
                    this.sendClientInfoToAuthServer(client, Constants.ADD);
                } else if (client.getClient_status().equalsIgnoreCase("Block")) {
                    this.sendClientInfoToAuthServer(client, Constants.DELETE);
                }
                myAppError.setErrorMessage("Successfully edited");
            } else {
                myAppError.setErrorType(MyAppError.NotUpdated);
                myAppError.setErrorMessage("Client Not found ");
            }

        } catch (Exception ex) {
            logger.addLogEntry("clients", DBLoggerConstant.ERROR, DBLoggerConstant.UPDATE, "Error while update data in clients table ", ex.getMessage(), ClientDAO.class.getName());
//            logger.debug("Exception in ClientDAO , update() ---> " + ex.getMessage());
            myAppError.setErrorType(MyAppError.DBError);
            myAppError.setErrorMessage("DB error : " + ex.getMessage());
        } finally {
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
        }
        return myAppError;
    }

    MyAppError delete(ArrayList<Client> param_client_list) {
        MyAppError myAppError = new MyAppError();
        ArrayList<Client> var_client_list_for_removing_from_users = new ArrayList<Client>();
        ResultSet resultSet = null;
        String db_id = "";
        String var_error_message = "";
        String var_seperator = " , ";

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            ps = null;
            stmt = null;

            for (Client client_temp_for_db_id : param_client_list) {
                Integer var_client_db_id = client_temp_for_db_id.getClient_db_id();
                sql = "select gateway_name from gateway where gateway_delete = 0 AND client_id = " + var_client_db_id + "";
                stmt = dbConnection.connection.createStatement();
                resultSet = stmt.executeQuery(sql);

                String gateways_;// = "";

                if (resultSet.next()) {
                    gateways_ = resultSet.getString("gateway_name");
                    while (resultSet.next()) {
                        gateways_ += "," + resultSet.getString("gateway_name");
                    }
                    var_error_message += (var_error_message.length() > 0 ? "\n" : "") + "Gateways : " + gateways_ + " is/are associated with client : " + client_temp_for_db_id.getClient_id();
                } else {
                    var_client_list_for_removing_from_users.add(client_temp_for_db_id);
                    db_id += var_client_db_id + var_seperator;
                }
            }
            if (var_error_message.length() > 0) {
                myAppError.setErrorMessage(var_error_message);
            }

            if (db_id.length() == 0) {
                myAppError.setErrorType(MyAppError.DBError);
                return myAppError;
            } else {
                db_id = db_id.substring(0, db_id.length() - var_seperator.length());

                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

                ps = null;

                sql = "Update clients set client_delete = 1 where id in ( " + db_id + " )";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.executeUpdate();

                int size = param_client_list.size();
                int[] individual_id_int = new int[size];
                List<Integer> to_be_deleted = new ArrayList<Integer>();
                int counter = 0;
                for (int i = 0; i < size; i++) {
                    individual_id_int[i] = param_client_list.get(i).getClient_db_id();
                }
                int index_of_objects = 0;

                for (Client temp_client : client_visible_list) {
                    if (contains(temp_client.getClient_db_id(), individual_id_int)) {
                        counter++;
                        to_be_deleted.add(index_of_objects);
                        if (counter == size) {
                            break;
                        }
                    }
                    index_of_objects++;
                }
                Collections.sort(to_be_deleted, intComparator);
                for (int i = 0; i < to_be_deleted.size(); i++) {
                    client_visible_list.remove(to_be_deleted.get(i).intValue());
                }

                UserDAO userDAO = new UserDAO();
                userDAO.removeClient(var_client_list_for_removing_from_users);

                for (Client client_ : var_client_list_for_removing_from_users) {
                    this.sendClientInfoToAuthServer(client_, Constants.DELETE);
                }

                myAppError.setErrorMessage(myAppError.getErrorMessage().length() > 0 ? myAppError.getErrorMessage().concat("#response.deletedsuccessfully") : "response.deletedsuccessfully");
            }
        } catch (Exception ex) {
            logger.addLogEntry("clients", DBLoggerConstant.ERROR, DBLoggerConstant.DELETE, "Error while delete data from clients table ", ex.getMessage(), ClientDAO.class.getName());
//            logger.debug("Exception in ClientDAO , delete() ---> " + ex.getMessage());
            myAppError.setErrorType(MyAppError.DBError);
            myAppError.setErrorMessage("Error : " + ex.getMessage());
        } finally {
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
        }
        return myAppError;
    }

    Client findById(int parseInt) {
        for (Client client : client_visible_list) {
            if (client.getId() == parseInt) {
                return client;
            }
        }
        return null;
    }

    public Client findClientIdByDbId(int parseInt) {
        for (Client client : client_list) {
            if (client.getClient_db_id() == parseInt) {
                return client;
            }
        }
        return null;
    }

    ArrayList<Client> findMultipleById(int[] parseInt) {
        ArrayList<Client> multiples_clients_to_be_deleted = new ArrayList<Client>();
        int counter = 0;
        for (Client client : client_visible_list) {
            if (contains(client.getId(), parseInt)) {
                multiples_clients_to_be_deleted.add(client);
                counter++;
                if (counter == parseInt.length) {
                    break;
                }
            }
        }
        return multiples_clients_to_be_deleted;
    }

    boolean contains(int param_one, int[] target_value_list) {
        boolean does_contain = false;
        for (int i = 0; i < target_value_list.length; i++) {
            if (param_one == target_value_list[i]) {
                does_contain = true;
                break;
            }
        }
        return does_contain;
    }

    Integer count() {
        return (client_list != null ? client_list.size() : 0);
    }

    Integer countFilteredData() {
        return (client_filtered_list != null ? client_filtered_list.size() : 0);
    }

    List<Client> find(int from, int to) {
        checkForReload();
        if (Client.getSort_order() != null) {
            Collections.sort(client_list, Client.ClientPropertiesComparator);
        }
        client_visible_list = client_list.subList(from, to);
        for (int i = 0; i < to - from; i++) {
            client_visible_list.get(i).setId(i + 1);
        }
        return client_visible_list;
    }

    List<Client> findFilteredList(String json_data, int from, int to) {
        checkForReload();
        ConditionChecking conditionChecking = new ConditionChecking();
        client_filtered_list = new ArrayList<Client>();
        FiltersSubFields filtersSubFields;
        Filters filters_obj;
        Gson json = new GsonBuilder().serializeNulls().create();
        filters_obj = json.fromJson(json_data, Filters.class);

        String comparison_type;
        boolean condition_matched = true;
        boolean return_val = false;

        for (Client filtered_client : client_list) {
            condition_matched = true;
            for (int i = 0; i < filters_obj.getRules().size(); i++) {
                filtersSubFields = json.fromJson(filters_obj.getRules().get(i).toString(), FiltersSubFields.class);
                comparison_type = filtersSubFields.getOp();
                switch (DataStorage.getInstance().getFieldMapping(filtersSubFields.getField())) {
                    case 0:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_client.getClient_id(), filtersSubFields.getData());
                        break;
                    case 1:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_client.getRateplan_id(), filtersSubFields.getData());
                        break;
                    case 2:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_client.getClient_name(), filtersSubFields.getData());
                        break;
                    case 3:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_client.getClient_email(), filtersSubFields.getData());
                        break;
                    case 4:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_client.getClient_type(), filtersSubFields.getData());
                        break;
                    case 5:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_client.getClient_status(), filtersSubFields.getData());
                        break;
                    case 6:
                        return_val = conditionChecking.checkConditionNumeric(comparison_type, filtered_client.getClient_credit_limit(), Double.parseDouble(filtersSubFields.getData()));
                        break;
                    case 7:
                        return_val = conditionChecking.checkConditionNumeric(comparison_type, filtered_client.getClient_balance(), Double.parseDouble(filtersSubFields.getData()));
                        break;
                    case 8:
                        return_val = conditionChecking.checkConditionNumeric(comparison_type, filtered_client.getClient_call_limit(), Integer.parseInt(filtersSubFields.getData()));
                        break;
                    default:
                        break;
                }

                if (filters_obj.getGroupOp().equalsIgnoreCase("And")) {
                    if (return_val == false) {
                        condition_matched = false;
                        break;
                    }
                } else {
                    if (return_val == true) {
                        condition_matched = true;
                        break;
                    } else if (i == filters_obj.getRules().size() - 1) {
                        condition_matched = false;
                    }
                }
            }
            if (condition_matched) {
                client_filtered_list.add(filtered_client);
            }
        }

        if (Client.getSort_order() != null) {
            Collections.sort(client_filtered_list, Client.ClientPropertiesComparator);
        }
        if (to > client_filtered_list.size() || to == -1) {
            to = client_filtered_list.size();// - from;
        }

        client_visible_list = client_filtered_list.subList(from, to);

        for (int i = 0; i < to - from; i++) {
            client_visible_list.get(i).setId(i + 1);
        }
        return client_visible_list;
    }

    private void checkForReload() {
        long current_time = System.currentTimeMillis();
        if (current_time - last_update_time > Constants.getUpdate_time_difference()) {
            forceReload();
        }
    }

    public static void forceReload() {
        client_list = buildList();
        last_update_time = System.currentTimeMillis();
    }

    public HashMap<String, Object> getClientInfoForHome(String client_id) {
        /*checkForReload();     
         Double balance = 0.0;
         balance = clientBalance.get(client_id);
         return balance;*/
        HashMap<String, Object> map = new HashMap<String, Object>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select client_balance, rateplan_id from clients where client_id='" + client_id + "'";
            ps = dbConnection.connection.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                map.put("balance", resultSet.getDouble("client_balance"));
                map.put("rateplanID", resultSet.getLong("rateplan_id"));
            }
            resultSet.close();

        } catch (Exception ex) {
            logger.addLogEntry("clients", DBLoggerConstant.ERROR, DBLoggerConstant.OPERATION, "getClientInfoForHome ", ex.getMessage(), ClientDAO.class.getName());
//            logger.fatal("Error while fetching client balance: " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return map;
    }

    private void sendClientInfoToAuthServer(Client client, int param_type) {
        logger.addLogEntry("clients", DBLoggerConstant.INFO, DBLoggerConstant.OPERATION, "Inside sendClientInfoToAuthServer -->  ", (client.getClient_id() + " -- " + param_type), ClientDAO.class.getName());
//        logger.debug("Inside sendClientInfoToAuthServer --> " + client.getClient_id() + " -- " + param_type);
        String msg = "";
        switch (param_type) {
            case 0:
                msg = msg.concat("add").concat(",").concat(client.getClient_id()).concat(",").concat(client.getClient_password());
                break;
            case 1:
                msg = msg.concat("edit").concat(",").concat(client.getClient_id()).concat(",").concat(client.getClient_password()).concat(",").concat(client.getClient_id());
                break;
            case 2:
                msg = msg.concat("delete").concat(",").concat(client.getClient_id());
                break;
        }
        EventSender.sendEventMsg(msg);
    }

    @Override
    public int compareTo(Integer t) {
        return this.compareTo(t);
    }

    public List<Client> findByType(int param_type) {
        if(param_type == 2){
            return client_list;
        }
        List<Client> v_temp_client_list;
        checkForReload();
        String client_type = Constants.getTYPE_STRING()[param_type];
//        String h = "{\"groupOp\":\"AND\",\"rules\":[{\"field\":\"client_type\",\"op\":\"eq\",\"data\":\"Origination\"},{\"field\":\"client_type\",\"op\":\"eq\",\"data\":\"Both\"}]}";
        String json_string = " {\"groupOp\":\"OR\",\"rules\":[{\"field\":\"client_type\",\"op\":\"eq\",\"data\":\"" + client_type + "\"},{\"field\":\"client_type\",\"op\":\"eq\",\"data\":\"Both\"}]}";
        v_temp_client_list = findFilteredList(json_string, 0, -1);
        return v_temp_client_list;
    }
    public static Comparator<Integer> intComparator = new Comparator<Integer>() {
        @Override
        public int compare(Integer t, Integer t1) {
            return t1.compareTo(t);
        }
    };

    MyAppError updatePaymentInfo(Payment payment) {
        MyAppError myAppError = new MyAppError();
        Statement stmt = null;

        Double payment_amount = payment.getPaymentType() == 0 ? payment.getPayment_amount() : (payment.getPayment_amount() * -1);
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            ps = null;
            stmt = dbConnection.connection.createStatement();
            ResultSet rs = null;

            sql = "select cur_balance from payments where payment_id=(select max(payment_id) from payments where client_id='" + payment.getClient_id() + "')";
            rs = stmt.executeQuery(sql);
            double cur_balance = 0;
            if (rs.next()) {
                cur_balance = rs.getDouble("cur_balance");
            }
            rs.close();

            sql = "insert into payments set "
                    + "client_id = ?, "
                    + "payment_type = ?, "
                    + (payment.getPaymentType() == 0 ? "payment_recharge = ?, " : "")
                    + (payment.getPaymentType() == 1 ? "payment_return = ?, " : "")
                    + (payment.getPaymentType() == 2 ? "payment_receive = ?, " : "")
                    + "cur_balance =? ";

            ps = dbConnection.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            ps.setString(i++, payment.getClient_id());
            ps.setInt(i++, payment.getPaymentType());
            ps.setDouble(i++, payment.getPayment_amount());
            ps.setDouble(i++, (cur_balance + payment_amount));

            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                myAppError.setErrorMessage("Payment Successful");

                if (payment.getPaymentType() == 0 || payment.getPaymentType() == 1) {
                    sql = "Update clients set "
                            + "client_balance=client_balance+" + payment_amount
                            + " where id =" + payment.getClient_db_id();
                    if (ps != null) {
                        ps.close();
                    }
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.executeUpdate();

                }
            }

        } catch (Exception ex) {
            logger.addLogEntry("clients", DBLoggerConstant.ERROR, DBLoggerConstant.UPDATE, "Exception in updatePaymentInfo() ---> ", ex.getMessage(), ClientDAO.class.getName());
//            logger.debug("Exception in ClientDAO , updatePaymentInfo() ---> " + ex.getMessage());
            myAppError.setErrorType(MyAppError.DBError);
            myAppError.setErrorMessage("DB error : " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return myAppError;
    }
}
