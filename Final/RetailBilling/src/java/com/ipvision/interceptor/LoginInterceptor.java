/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.interceptor;

import com.ipvision.clients.ClientOperationAction;
import com.ipvision.common.Admin;
import com.ipvision.login.LoginAction;
import com.ipvision.user.Users;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import java.util.Map;

/**
 *
 * @author reefat
 */
public class LoginInterceptor implements Interceptor {

    public LoginInterceptor() {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init() {
    }

    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception {
        Map<String, Object> session = ActionContext.getContext().getSession();
        Users users = (Users) session.get("userName");

        Object action = actionInvocation.getAction();
        String invocationContextName = actionInvocation.getInvocationContext().getName();
//        System.err.println("action.toString ==> " + actionInvocation.getInvocationContext().getName());
        if (users != null) {
            if (users.getRole_id() > 0) {
                if ((action instanceof Admin)) {
                    if (action instanceof ClientOperationAction) {
                        if (!(invocationContextName.equals("retrieve") || invocationContextName.equals("update"))) {
                            return Action.LOGIN;
                        }
                    } else {
                        return Action.LOGIN;
                    }
                }
            }
            String s = actionInvocation.invoke();
//            System.out.println("s ===>> " + s + " +++ UserId ==> " + users.getUser_id());
            return s;
        }
        if (!(action instanceof LoginAction)) {
            return Action.LOGIN;
        }
//        return Action.LOGIN;
        return actionInvocation.invoke();
    }
}