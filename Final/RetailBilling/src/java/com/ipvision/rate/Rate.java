/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.rate;

/**
 *
 * @author Wasif Islam
 */
public class Rate {
    private Long rate_id;
    private long rateplan_id;
    private String rate_destination_code;
    private String rate_destination_name;
    private Double rate_per_min;
    private Integer rate_first_pulse;
    private Integer rate_next_pulse;
    private Integer rate_grace_period;
    private Integer rate_failed_period;
    private Integer rate_day;
    private String rate_day_name;
    private Integer rate_from_hour;
    private Integer rate_from_min;
    private Integer rate_to_hour;
    private Integer rate_to_min;
    //private Long rate_created_date;
    private Integer rate_status;
    private String rate_status_name;

    public Long getRate_id() {
        return rate_id;
    }

    public void setRate_id(Long rate_id) {
        this.rate_id = rate_id;
    }

    public long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public String getRate_destination_code() {
        return rate_destination_code;
    }

    public void setRate_destination_code(String rate_destination_code) {
        this.rate_destination_code = rate_destination_code;
    }

    public String getRate_destination_name() {
        return rate_destination_name;
    }

    public void setRate_destination_name(String rate_destination_name) {
        this.rate_destination_name = rate_destination_name;
    }

    public Double getRate_per_min() {
        return rate_per_min;
    }

    public void setRate_per_min(Double rate_per_min) {
        this.rate_per_min = rate_per_min;
    }

    public Integer getRate_first_pulse() {
        return rate_first_pulse;
    }

    public void setRate_first_pulse(Integer rate_first_pulse) {
        this.rate_first_pulse = rate_first_pulse;
    }

    public Integer getRate_next_pulse() {
        return rate_next_pulse;
    }

    public void setRate_next_pulse(Integer rate_next_pulse) {
        this.rate_next_pulse = rate_next_pulse;
    }

    public Integer getRate_grace_period() {
        return rate_grace_period;
    }

    public void setRate_grace_period(Integer rate_grace_period) {
        this.rate_grace_period = rate_grace_period;
    }

    public Integer getRate_failed_period() {
        return rate_failed_period;
    }

    public void setRate_failed_period(Integer rate_failed_period) {
        this.rate_failed_period = rate_failed_period;
    }

    public Integer getRate_day() {
        return rate_day;
    }

    public void setRate_day(Integer rate_day) {
        this.rate_day = rate_day;
    }

    public String getRate_day_name() {
        return rate_day_name;
    }

    public void setRate_day_name(String rate_day_name) {
        this.rate_day_name = rate_day_name;
    }

    public Integer getRate_from_hour() {
        return rate_from_hour;
    }

    public void setRate_from_hour(Integer rate_from_hour) {
        this.rate_from_hour = rate_from_hour;
    }

    public Integer getRate_from_min() {
        return rate_from_min;
    }

    public void setRate_from_min(Integer rate_from_min) {
        this.rate_from_min = rate_from_min;
    }

    public Integer getRate_to_hour() {
        return rate_to_hour;
    }

    public void setRate_to_hour(Integer rate_to_hour) {
        this.rate_to_hour = rate_to_hour;
    }

    public Integer getRate_to_min() {
        return rate_to_min;
    }

    public void setRate_to_min(Integer rate_to_min) {
        this.rate_to_min = rate_to_min;
    }

    /*public Long getRate_created_date() {
        return rate_created_date;
    }

    public void setRate_created_date(Long rate_created_date) {
        this.rate_created_date = rate_created_date;
    }*/

    public Integer getRate_status() {
        return rate_status;
    }

    public void setRate_status(Integer rate_status) {
        this.rate_status = rate_status;
    }

    public String getRate_status_name() {
        return rate_status_name;
    }

    public void setRate_status_name(String rate_status_name) {
        this.rate_status_name = rate_status_name;
    }
    
}
