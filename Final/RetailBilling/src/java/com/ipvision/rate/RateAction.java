/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.rate;

import com.ipvision.home.Home;
import com.ipvision.home.HomeLoader;
import com.ipvision.user.Users;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
//import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
//import org.apache.struts2.dispatcher.SessionMap;
//import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Wasif Islam
 */
public class RateAction extends ActionSupport implements SessionAware {

    //public static Integer rateplanID;
    private SessionMap<String, Object> sessionMap;
    private int id;
    private String filters;
    //Your result List
    private List<Rate> gridModel;
    //get how many rows we want to have into the grid - rowNum attribute in the grid
    private Integer rows = 10;
    //Get the requested page. By default grid sets this to 1.
    private Integer page = 1;
    // sorting order - asc or desc
    private String sord;
    // get index row - i.e. user click to sort.
    private String sidx;
    // Search Field
    private Integer total = 0;
    // Your Total Pages
    private Integer records = 0;
    // All Record
    private Integer rateplanID = 0;

    @Override
    public void setSession(Map<String, Object> map) {
        sessionMap = (SessionMap) map;
    }

    @Override
    public String execute() throws Exception {

        RateDAO rDao = new RateDAO();
        int to = (rows * page);
        int from = to - rows;

        HttpServletRequest request = ServletActionContext.getRequest();
        if (request.getParameter("id") != null) {
            rateplanID = Integer.valueOf(request.getParameter("id"));
            sessionMap.put("rateplanID", getId());
        }

        Users usr = (Users) request.getSession().getAttribute("userName");
        if (usr != null && usr.getRole_id() > 0) {
            if (HomeLoader.homeList != null && HomeLoader.homeList.size() > 0) {
                for (int i = 0; i < HomeLoader.homeList.size(); i++) {
                    Home dto = HomeLoader.homeList.get(i);
                    rateplanID = Integer.valueOf(dto.getRateplan_id().toString());
                }
                sessionMap.put("rateplanID", rateplanID);
            }
        }
        //sessionMap.put("rateplanID", rateplanID);
        records = rDao.count(getFilters(), (Integer) sessionMap.get("rateplanID"));
        setGridModel(rDao.search(getFilters(), getSord(), getSidx(), from, to, (Integer) sessionMap.get("rateplanID")));
        total = (int) Math.ceil((double) records / (double) rows);


        return SUCCESS;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public List<Rate> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<Rate> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getRecords() {
        return records;
    }

    public void setRecords(Integer records) {
        this.records = records;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
