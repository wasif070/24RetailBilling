/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.rate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ipvision.common.Filters;
import com.ipvision.common.FiltersSubFields;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import util.Constants;
import util.MyAppError;

/**
 *
 * @author Wasif Islam
 */
public class RateDAO {

    static Logger logger = Logger.getLogger(RateDAO.class.getName());
    private static ArrayList<Rate> rateList = null;
    private static HashMap<Long, Rate> rateMap = new HashMap<Long, Rate>();
    static RateDAO rateLoader = null;

    public RateDAO() {
    }

    public static RateDAO getInstance() {
        if (rateLoader == null) {
            createRateDAO();
        }
        return rateLoader;
    }

    private synchronized static void createRateDAO() {
        if (rateLoader == null) {
            rateLoader = new RateDAO();
        }
    }

    private String condition(String filters) {
        String condition = " AND rate_delete = 0 AND ";
        Filters filters_obj;
        Gson json = new GsonBuilder().serializeNulls().create();
        filters_obj = json.fromJson(filters, Filters.class);
        FiltersSubFields filtersSubFields = null;
        for (int i = 0; i < filters_obj.getRules().size(); i++) {
            String op = "";
            filtersSubFields = json.fromJson(filters_obj.getRules().get(i).toString(), FiltersSubFields.class);


            if ("eq".equals(filtersSubFields.getOp())) {
                if ("rate_status".equals(filtersSubFields.getField())) {
                    if (filtersSubFields.getData().startsWith("a")) {
                        op = " = 0";
                    } else {
                        op = " = 1";
                    }
                } else {
                    op = " = " + filtersSubFields.getData();
                }
            } else if ("ne".equals(filtersSubFields.getOp())) {
                if ("rateplan_status".equals(filtersSubFields.getField())) {
                    if (filtersSubFields.getData().startsWith("a")) {
                        op = " != 0";
                    } else {
                        op = " != 1";
                    }
                } else {
                    op = " != " + filtersSubFields.getData();
                }
            } else if ("bw".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'" + filtersSubFields.getData() + "%'";
            } else if ("ew".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "'";
            } else if ("cn".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "%'";
            } else if ("gt".equals(filtersSubFields.getOp())) {
                op = " > " + filtersSubFields.getData();
            } else if ("lt".equals(filtersSubFields.getOp())) {
                op = " < " + filtersSubFields.getData();
            }

            if (i > 0) {
                condition += " " + filters_obj.getGroupOp() + " ";
            }

            condition += filtersSubFields.getField() + op;
        }
        return condition;
    }

    public int count(String filters, int id) {
        DBConnection dbConnection = null;
        Statement statement = null;
        int count = 0;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "";
            if (filters != null && filters.length() > 0) {
                sql = "select count(*) from rates where rateplan_id = " + id + condition(filters);
            } else {
                sql = "select count(*) from rates where rateplan_id = " + id + " AND rate_delete = 0";
            }
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                count = resultSet.getInt("count(*)");
            }
        } catch (Exception e) {
            logger.fatal("Exception in getCount:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return count;
    }

    public List<Rate> search(String filters, String sort, String sort_field, int from, int to, int id) {
        DBConnection dbConnection = null;
        Statement statement = null;
        rateList = new ArrayList<Rate>();
        rateMap = new HashMap<Long, Rate>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();

            String sql = "";
            String limit_sql = "";
            if (from >= 0 && to > 0) {
                limit_sql = "limit " + from + "," + (to - from);
            }
            String order_sql = "";
            if (sort != null && sort.length() > 0 && sort_field != null && sort_field.length() > 0) {
                order_sql = "order by " + sort_field + " " + sort;
            } else {
                order_sql = "order by rate_id desc";
            }

            if (filters != null && filters.length() > 0) {
                sql = "select * from rates where rateplan_id=" + id + condition(filters) + " " + order_sql + " " + limit_sql;
            } else {
                sql = "select * from rates where rateplan_id=" + id + " AND rate_delete = 0 " + order_sql + " " + limit_sql;
            }
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Rate dto = new Rate();
                dto.setRate_id(resultSet.getLong("rate_id"));
                dto.setRateplan_id(resultSet.getLong("rateplan_id"));
                dto.setRate_destination_code(resultSet.getString("rate_destination_code"));
                dto.setRate_destination_name(resultSet.getString("rate_destination_name"));
                dto.setRate_per_min(resultSet.getDouble("rate_per_min"));
                dto.setRate_first_pulse(resultSet.getInt("rate_first_pulse"));
                dto.setRate_next_pulse(resultSet.getInt("rate_next_pulse"));
                dto.setRate_grace_period(resultSet.getInt("rate_grace_period"));
                dto.setRate_failed_period(resultSet.getInt("rate_failed_period"));
                dto.setRate_day(resultSet.getInt("rate_day"));
                dto.setRate_day_name(Constants.DAY_STRING[dto.getRate_day() + 1]);
                dto.setRate_from_hour(resultSet.getInt("rate_from_hour"));
                dto.setRate_from_min(resultSet.getInt("rate_from_min"));
                dto.setRate_to_hour(resultSet.getInt("rate_to_hour"));
                dto.setRate_to_min(resultSet.getInt("rate_to_min"));
                //dto.setRate_created_date(resultSet.getLong("rate_created_date"));
                dto.setRate_status(resultSet.getInt("rate_status"));
                dto.setRate_status_name(Constants.STATUS_STRING[dto.getRate_status()]);
                rateMap.put(dto.getRate_id(), dto);
                rateList.add(dto);
            }

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in searchRatesDTOList:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return rateList;
    }

    public MyAppError save(Rate dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            //String sql = "select rate_destination_code from rates where rate_destination_code=? AND rateplan_id=? AND rate_delete = 0";

            String sql = "select rate_destination_code from rates where rate_destination_code=? AND rateplan_id=? AND rate_delete=0 ";
            if (dto.getRate_day() > -1 ) {
                sql += "AND (rate_day BETWEEN 0 AND 6) ";
            } else {
                sql += "AND (rate_day=-1) ";
            }
            sql += "AND ("
                    + "(((rate_from_hour*60+rate_from_min) <= ?) AND ((rate_to_hour*60+rate_to_min) > ?)) "
                    + "OR "
                    + "(((rate_from_hour*60+rate_from_min) <= ?) AND ((rate_to_hour*60+rate_to_min) > ?))"
                    + "OR "
                    + "(((rate_from_hour*60+rate_from_min) >= ?) AND ((rate_to_hour*60+rate_to_min) < ?))"
                    + ")";

            ps = dbConnection.connection.prepareStatement(sql);

            int i = 1;

            ps.setString(i++, dto.getRate_destination_code());
            ps.setLong(i++, dto.getRateplan_id());
            if (dto.getRate_day() > -1) {
                ps.setInt(i++, dto.getRate_day());
            }
            ps.setInt(i++, dto.getRate_to_hour() * 60 + dto.getRate_to_min());
            ps.setInt(i++, dto.getRate_to_hour() * 60 + dto.getRate_to_min());

            ps.setInt(i++, dto.getRate_from_hour() * 60 + dto.getRate_from_min());
            ps.setInt(i++, dto.getRate_from_hour() * 60 + dto.getRate_from_min());

            ps.setInt(i++, dto.getRate_from_hour() * 60 + dto.getRate_from_min());
            ps.setInt(i++, dto.getRate_to_hour() * 60 + dto.getRate_to_min());

            ResultSet resultSet = ps.executeQuery();
            //System.out.println("sql --> \n" + util.Utils.getSQLString(ps));
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Rate conflicts with other rates");
                resultSet.close();
                return error;
            }

            sql = "insert into rates"
                    + "(rateplan_id, "
                    + "rate_destination_code, "
                    + "rate_destination_name, "
                    + "rate_per_min, "
                    + "rate_first_pulse, "
                    + "rate_next_pulse, "
                    + "rate_grace_period, "
                    + "rate_failed_period, "
                    + "rate_day, "
                    + "rate_from_hour, "
                    + "rate_from_min, "
                    + "rate_to_hour, "
                    + "rate_to_min, "
                    + "rate_status, "
                    + "rate_delete) "
                    + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            ps = dbConnection.connection.prepareStatement(sql);
            ps.setLong(1, dto.getRateplan_id());
            ps.setString(2, dto.getRate_destination_code());
            ps.setString(3, dto.getRate_destination_name());
            ps.setDouble(4, dto.getRate_per_min());
            ps.setInt(5, dto.getRate_first_pulse());
            ps.setInt(6, dto.getRate_next_pulse());
            ps.setInt(7, dto.getRate_grace_period());
            ps.setInt(8, dto.getRate_failed_period());
            ps.setInt(9, dto.getRate_day());
            ps.setInt(10, dto.getRate_from_hour());
            ps.setInt(11, dto.getRate_from_min());
            ps.setInt(12, dto.getRate_to_hour());
            ps.setInt(13, dto.getRate_to_min());
            ps.setInt(14, dto.getRate_status());
            ps.setInt(15, 0);

            ps.executeUpdate();

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error while adding Rate");
            logger.fatal("Error while adding Rate: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return error;
    }

    public MyAppError update(Rate dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select rate_destination_code from rates where rate_destination_code=? AND rateplan_id=? AND rate_delete=0 AND rate_id!=" + dto.getRate_id();
            if (dto.getRate_day() > -1) {
                sql += " AND (rate_day BETWEEN 0 AND 6) ";
            } else {
                sql += " AND (rate_day=-1) ";
            }
            sql += "AND ("
                    + "(((rate_from_hour*60+rate_from_min) <= ?) AND ((rate_to_hour*60+rate_to_min) > ?)) "
                    + "OR "
                    + "(((rate_from_hour*60+rate_from_min) <= ?) AND ((rate_to_hour*60+rate_to_min) > ?))"
                    + "OR "
                    + "(((rate_from_hour*60+rate_from_min) >= ?) AND ((rate_to_hour*60+rate_to_min) < ?))"
                    + ")";

            ps = dbConnection.connection.prepareStatement(sql);

            int i = 1;

            ps.setString(i++, dto.getRate_destination_code());
            ps.setLong(i++, dto.getRateplan_id());
            if (dto.getRate_day() > -1) {
                ps.setInt(i++, dto.getRate_day());
            }
            ps.setInt(i++, dto.getRate_to_hour() * 60 + dto.getRate_to_min());
            ps.setInt(i++, dto.getRate_to_hour() * 60 + dto.getRate_to_min());

            ps.setInt(i++, dto.getRate_from_hour() * 60 + dto.getRate_from_min());
            ps.setInt(i++, dto.getRate_from_hour() * 60 + dto.getRate_from_min());

            ps.setInt(i++, dto.getRate_from_hour() * 60 + dto.getRate_from_min());
            ps.setInt(i++, dto.getRate_to_hour() * 60 + dto.getRate_to_min());

            ResultSet resultSet = ps.executeQuery();
            //System.out.println("sql --> \n" + util.Utils.getSQLString(ps));
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Rate conflicts with other rates");
                resultSet.close();
                return error;
            }


            sql = "update rates set "
                    + "rate_destination_code=?, "
                    + "rate_destination_name=?, "
                    + "rate_per_min=?, "
                    + "rate_first_pulse=?, "
                    + "rate_next_pulse=?, "
                    + "rate_grace_period=?, "
                    + "rate_failed_period=?, "
                    + "rate_day=?, "
                    + "rate_from_hour=?, "
                    + "rate_from_min=?, "
                    + "rate_to_hour=?, "
                    + "rate_to_min=?, "
                    + "rate_status=? "
                    + "where rate_id=" + dto.getRate_id();

            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, dto.getRate_destination_code());
            ps.setString(2, dto.getRate_destination_name());
            ps.setDouble(3, dto.getRate_per_min());
            ps.setInt(4, dto.getRate_first_pulse());
            ps.setInt(5, dto.getRate_next_pulse());
            ps.setInt(6, dto.getRate_grace_period());
            ps.setInt(7, dto.getRate_failed_period());
            ps.setInt(8, dto.getRate_day());
            ps.setInt(9, dto.getRate_from_hour());
            ps.setInt(10, dto.getRate_from_min());
            ps.setInt(11, dto.getRate_to_hour());
            ps.setInt(12, dto.getRate_to_min());
            ps.setInt(13, dto.getRate_status());
            ps.executeUpdate();

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error while editing rate");
            logger.fatal("Error while editing rate: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public String getDBIds(String id) {
        String rateIds = "";
        String[] ids = id.split(",");
        for (int i = 0; i < ids.length; i++) {
            if (i > 0) {
                rateIds += ",";
            }
            Rate rdto = new Rate();
            rdto = rateList.get(Integer.parseInt(ids[i]) - 1);
            rateIds += rdto.getRate_id();
        }
        return rateIds;
    }

    public MyAppError delete(String rate_id) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "update rates set rate_delete = 1 where rate_id in (" + rate_id + ")";
            ps = dbConnection.connection.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error while deleting rate");
            logger.fatal("Error while deleting rate: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError uploadFlieData(ArrayList<Rate> dto_array) {
        MyAppError error = new MyAppError();
        int count_suc = 0, count_fail = 0;
        String msg_suc = " rates uploaded successfully";
        String msg_fail = " rates uploading failed";
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;
        String sql = "";
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            if (dto_array.size() > 0) {
                for (int inc = 0; inc < dto_array.size(); inc++) {
                    Rate p_dto = dto_array.get(inc);

                    if (p_dto.getRate_from_hour() <= 23 && p_dto.getRate_from_min() <= 59 && p_dto.getRate_to_hour() <= 23 && p_dto.getRate_to_min() <= 59 && ((p_dto.getRate_from_hour() * 60) + p_dto.getRate_from_min()) < ((p_dto.getRate_to_hour() * 60) + p_dto.getRate_to_min())) {

                        sql = "select rate_destination_code from rates where rate_destination_code=? AND rateplan_id=? AND rate_delete=0 ";
                        if (p_dto.getRate_day() > -1) {
                            sql += "AND (rate_day BETWEEN 0 AND 6) ";
                        } else {
                            sql += "AND (rate_day=-1) ";
                        }
                        sql += "AND ("
                                + "(((rate_from_hour*60+rate_from_min) <= ?) AND ((rate_to_hour*60+rate_to_min) > ?)) "
                                + "OR "
                                + "(((rate_from_hour*60+rate_from_min) <= ?) AND ((rate_to_hour*60+rate_to_min) > ?))"
                                + "OR "
                                + "(((rate_from_hour*60+rate_from_min) >= ?) AND ((rate_to_hour*60+rate_to_min) < ?))"
                                + ")";


                        ps = dbConnection.connection.prepareStatement(sql);

                        int i = 1;

                        ps.setString(i++, p_dto.getRate_destination_code());
                        ps.setLong(i++, p_dto.getRateplan_id());
                        if (p_dto.getRate_day() > -1) {
                            ps.setInt(i++, p_dto.getRate_day());
                        }
                        ps.setInt(i++, p_dto.getRate_to_hour() * 60 + p_dto.getRate_to_min());
                        ps.setInt(i++, p_dto.getRate_to_hour() * 60 + p_dto.getRate_to_min());

                        ps.setInt(i++, p_dto.getRate_from_hour() * 60 + p_dto.getRate_from_min());
                        ps.setInt(i++, p_dto.getRate_from_hour() * 60 + p_dto.getRate_from_min());

                        ps.setInt(i++, p_dto.getRate_from_hour() * 60 + p_dto.getRate_from_min());
                        ps.setInt(i++, p_dto.getRate_to_hour() * 60 + p_dto.getRate_to_min());
                        //System.out.println("sql --> \n" + util.Utils.getSQLString(ps));
                        ResultSet resultSet = ps.executeQuery();

                        if (resultSet.next()) {
                            error.setErrorType(MyAppError.PartialUpdated);
                            resultSet.close();
                            count_fail++;
                        } else {

                            sql = "insert into rates(rateplan_id,rate_destination_code,rate_destination_name,rate_per_min,rate_first_pulse,rate_next_pulse,rate_grace_period,rate_failed_period,rate_day,rate_from_hour,rate_from_min,rate_to_hour,rate_to_min,rate_status) "
                                    + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                            ps = dbConnection.connection.prepareStatement(sql);
                            ps.setLong(1, p_dto.getRateplan_id());
                            ps.setString(2, p_dto.getRate_destination_code().trim());
                            ps.setString(3, p_dto.getRate_destination_name().trim());
                            ps.setDouble(4, p_dto.getRate_per_min());
                            ps.setInt(5, p_dto.getRate_first_pulse());
                            ps.setInt(6, p_dto.getRate_next_pulse());
                            ps.setInt(7, p_dto.getRate_grace_period());
                            ps.setInt(8, p_dto.getRate_failed_period());
                            ps.setInt(9, p_dto.getRate_day());
                            ps.setInt(10, p_dto.getRate_from_hour());
                            ps.setInt(11, p_dto.getRate_from_min());
                            ps.setInt(12, p_dto.getRate_to_hour());
                            ps.setInt(13, p_dto.getRate_to_min());
                            ps.setInt(14, p_dto.getRate_status());
                            ps.executeUpdate();
                            count_suc++;
                        }
                    } else {
                        error.setErrorType(MyAppError.PartialUpdated);
                        count_fail++;
                    }
                }
                msg_suc = count_suc + msg_suc;
                msg_fail = count_fail + msg_fail;
                error.setErrorMessage(msg_suc + "\n" + msg_fail);
            }
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while uploading rates: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public StringBuffer getRateCSVString(int id) {
        StringBuffer csvString = new StringBuffer();
        List<Rate> rateFullList = search(null, null, null, 0, 0, id);
        if (rateFullList.size() > 0) {
            for (int inc = 0; inc < rateFullList.size(); inc++) {
                Rate dto = rateFullList.get(inc);
                csvString.append(dto.getRate_destination_code().trim());
                csvString.append(',');
                csvString.append(dto.getRate_destination_name());
                csvString.append(',');
                csvString.append(dto.getRate_per_min());
                csvString.append(',');
                csvString.append(dto.getRate_first_pulse());
                csvString.append(',');
                csvString.append(dto.getRate_next_pulse());
                csvString.append(',');
                csvString.append(dto.getRate_grace_period());
                csvString.append(',');
                csvString.append(dto.getRate_failed_period());
                csvString.append(',');
                csvString.append(dto.getRate_day());
                csvString.append(',');
                csvString.append(dto.getRate_from_hour());
                csvString.append(',');
                csvString.append(dto.getRate_from_min());
                csvString.append(',');
                csvString.append(dto.getRate_to_hour());
                csvString.append(',');
                csvString.append(dto.getRate_to_min());
                csvString.append('\n');
            }
        }
        return csvString;
    }

    public static Rate getRateDTO(long id) {
        return rateMap.get(id);
    }
}
