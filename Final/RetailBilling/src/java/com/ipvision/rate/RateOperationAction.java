/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.rate;

import com.ipvision.common.Admin;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletResponseAware;
import util.MyAppError;
import util.Utils;

/**
 *
 * @author Wasif Islam
 */
public class RateOperationAction extends ActionSupport implements ServletResponseAware, Admin {

    static Logger logger = Logger.getLogger(RateOperationAction.class.getName());
    private HttpServletResponse servletResponse;
    private String action;
    private String id;
    private Long rate_id;
    private long rateplan_id;
    private String rate_destination_code;
    private String rate_destination_name;
    private Double rate_per_min = 0.0;
    private Integer rate_first_pulse = 0;
    private Integer rate_next_pulse = 0;
    private Integer rate_grace_period = 0;
    private Integer rate_failed_period = 0;
    private Integer rate_day;
    private Integer rate_from_hour;
    private Integer rate_from_min;
    private Integer rate_to_hour = 23;
    private Integer rate_to_min = 59;
    private Integer rate_status;
    private File upload;
    private String uploadFileContentType;
    private String uploadFileName;

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String add() throws Exception {
        HttpSession session = ServletActionContext.getRequest().getSession(false);
        int ratePlanId = Integer.parseInt(session.getAttribute("rateplanID").toString());
        System.out.println("ADD");
        Rate rDto = new Rate();
        rDto.setRateplan_id(ratePlanId);
        rDto.setRate_destination_code(getRate_destination_code());
        rDto.setRate_destination_name(getRate_destination_name());
        rDto.setRate_per_min(getRate_per_min());
        rDto.setRate_first_pulse(getRate_first_pulse());
        rDto.setRate_next_pulse(getRate_next_pulse());
        rDto.setRate_grace_period(getRate_grace_period());
        rDto.setRate_failed_period(getRate_failed_period());
        rDto.setRate_day(getRate_day());
        rDto.setRate_from_hour(getRate_from_hour());
        rDto.setRate_from_min(getRate_from_min());
        rDto.setRate_to_hour(getRate_to_hour());
        rDto.setRate_to_min(getRate_to_min());
        rDto.setRate_status(getRate_status());
        RateDAO rDao = new RateDAO();

        MyAppError error = rDao.save(rDto);
        if (error.getErrorType() > 0) {
            addActionError(error.getErrorMessage());
            return INPUT;
        } else {
            addActionMessage("Successfully added");
            return SUCCESS;
        }

    }

    public String update() throws Exception {
        HttpSession session = ServletActionContext.getRequest().getSession(false);
        int ratePlanId = Integer.parseInt(session.getAttribute("rateplanID").toString());
        System.out.println("EDIT");
        Rate rDto = new Rate();
        rDto.setRate_id(getRate_id());
        rDto.setRateplan_id(ratePlanId);
        rDto.setRate_destination_code(getRate_destination_code());
        rDto.setRate_destination_name(getRate_destination_name());
        rDto.setRate_per_min(getRate_per_min());
        rDto.setRate_first_pulse(getRate_first_pulse());
        rDto.setRate_next_pulse(getRate_next_pulse());
        rDto.setRate_grace_period(getRate_grace_period());
        rDto.setRate_failed_period(getRate_failed_period());
        rDto.setRate_day(getRate_day());
        rDto.setRate_from_hour(getRate_from_hour());
        rDto.setRate_from_min(getRate_from_min());
        rDto.setRate_to_hour(getRate_to_hour());
        rDto.setRate_to_min(getRate_to_min());
        rDto.setRate_status(getRate_status());
        RateDAO rDao = new RateDAO();

        MyAppError error = rDao.update(rDto);
        if (error.getErrorType() > 0) {
            addActionError(error.getErrorMessage());
            return INPUT;
        } else {
            addActionMessage("Successfully edited");
            return SUCCESS;
        }

    }

    public String delete() throws Exception {
        System.out.println("DEL");
        RateDAO rDAO = new RateDAO();
        String rateplanIDs = rDAO.getDBIds(getId());

        MyAppError error = rDAO.delete(rateplanIDs);
        if (error.getErrorType() > 0) {
            addActionError(error.getErrorMessage());
            return INPUT;
        } else {
            addActionMessage("Successfully deleted");
            return SUCCESS;
        }
    }

    public String retrieve() throws Exception {
        System.out.println("RETRIEVE");
        HttpServletRequest request = ServletActionContext.getRequest();
        if (request.getParameter("id") != null) {
            long db_id = Long.valueOf(request.getParameter("id"));
            Rate rate = RateDAO.getRateDTO(db_id);
            this.setRate_id(db_id);
            this.setRateplan_id(rate.getRateplan_id());
            this.setRate_destination_code(rate.getRate_destination_code());
            this.setRate_destination_name(rate.getRate_destination_name());
            this.setRate_per_min(rate.getRate_per_min());
            this.setRate_first_pulse(rate.getRate_first_pulse());
            this.setRate_next_pulse(rate.getRate_next_pulse());
            this.setRate_grace_period(rate.getRate_grace_period());
            this.setRate_failed_period(rate.getRate_failed_period());
            this.setRate_day(rate.getRate_day());
            this.setRate_from_hour(rate.getRate_from_hour());
            this.setRate_from_min(rate.getRate_from_min());
            this.setRate_to_hour(rate.getRate_to_hour());
            this.setRate_to_min(rate.getRate_to_min());
            this.setRate_status(rate.getRate_status());

        }
        this.setAction("update");

        return INPUT;
    }

    public String upload() throws Exception {
        HttpSession session = ServletActionContext.getRequest().getSession(false);
        int ratePlanId = Integer.parseInt(session.getAttribute("rateplanID").toString());
        System.out.println("UPLOAD");

        //this.uploadFileContentType = ServletActionContext.getServletContext().getMimeType(this.uploadFileName);
        RateDAO rDao = new RateDAO();
        //if ("text/csv".equals(this.uploadFileContentType)) {
        if(this.uploadFileName.contains(".csv")){
            String thisLine;
            //DataInputStream myInput = new DataInputStream(myFile.getInputStream());
            BufferedReader d = new BufferedReader(new FileReader(this.upload));

            ArrayList<Rate> dtoList = new ArrayList<Rate>();
            try {
                while ((thisLine = d.readLine()) != null) {
                    if (thisLine.trim().length() > 0) {
                        String strar[] = thisLine.split(",");
                        if (strar.length >= 6) {
                            Rate dto = new Rate();
                            dto.setRateplan_id(ratePlanId);
                            dto.setRate_destination_code(strar[0].trim());
                            dto.setRate_destination_name(strar[1].trim());
                            dto.setRate_per_min(Double.parseDouble(strar[2].trim()));
                            dto.setRate_first_pulse(Integer.parseInt(strar[3].trim()));
                            dto.setRate_next_pulse(Integer.parseInt(strar[4].trim()));
                            dto.setRate_grace_period(Integer.parseInt(strar[5].trim()));
                            dto.setRate_failed_period(Integer.parseInt(strar[6].trim()));
                            dto.setRate_day(Integer.parseInt(strar[7].trim()));
                            dto.setRate_from_hour(Integer.parseInt(strar[8].trim()));
                            dto.setRate_from_min(Integer.parseInt(strar[9].trim()));
                            dto.setRate_to_hour(Integer.parseInt(strar[10].trim()));
                            dto.setRate_to_min(Integer.parseInt(strar[11].trim()));
                            dto.setRate_status(0);
                            dtoList.add(dto);

                        }
                    }
                }
                MyAppError error = rDao.uploadFlieData(dtoList);
                if (error.getErrorType() > 0) {
                    addActionError(error.getErrorMessage());
                    return INPUT;
                } else {
                    addActionMessage(error.getErrorMessage());
                    return SUCCESS;
                }
            } catch (Exception e) {
                addActionError("Rate Uploaded Not Successful! : " + e.getMessage());
                return INPUT;
            }
        } else {
            addActionError(uploadFileContentType + " not supported!");
            return INPUT;
        }

    }

    public String download() throws Exception {
        HttpSession session = ServletActionContext.getRequest().getSession(false);
        int ratePlanId = Integer.parseInt(session.getAttribute("rateplanID").toString());

        servletResponse.setContentType("application/octet-stream");
        servletResponse.setHeader("Content-Disposition", "attachment;filename=rates.csv");

        RateDAO rDao = new RateDAO();
        try {
            ServletOutputStream out = servletResponse.getOutputStream();

            StringBuffer sb = rDao.getRateCSVString(ratePlanId);

            InputStream in = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
            int inSize = in.available();
            byte[] outputByte = new byte[inSize];
            while (in.read(outputByte, 0, inSize) != -1) {
                out.write(outputByte, 0, inSize);
            }
            in.close();
            out.flush();
            out.close();

        } catch (Exception e) {
            addActionError("Rate Downloaded Not Successful! : " + e.getMessage());
            return INPUT;
        }
        return SUCCESS;
    }

    public String display() throws Exception {
        this.setAction("add");
        return INPUT;
    }

    @Override
    public void validate() {
        this.setAction(ActionContext.getContext().getName());
        String action = getAction();
        if ("add".equals(action) || "update".equals(action)) {
            if (this.rate_destination_code == null || this.rate_destination_code.length() == 0) {
                addFieldError("rate_destination_code", "*Prefix Required");
            }
            if (!Utils.isDouble(String.valueOf(this.rate_per_min)) || Double.parseDouble(String.valueOf(this.rate_per_min)) <= 0) {
                addFieldError("rate_per_min", "*Rate per minute Required");
            }
            if (!Utils.isInteger(String.valueOf(this.rate_first_pulse)) || Integer.parseInt(String.valueOf(this.rate_first_pulse)) < 0) {
                addFieldError("rate_first_pulse", "*Rate first pulse Required");
            }
            if (!Utils.isInteger(String.valueOf(this.rate_next_pulse)) || Integer.parseInt(String.valueOf(this.rate_next_pulse)) < 0) {
                addFieldError("rate_next_pulse", "*Rate next pulse Required");
            }
            if (!Utils.isInteger(String.valueOf(this.rate_grace_period)) || Integer.parseInt(String.valueOf(this.rate_grace_period)) < 0) {
                addFieldError("rate_grace_period", "*Rate grace period Required");
            }
            if (!Utils.isInteger(String.valueOf(this.rate_failed_period)) || Integer.parseInt(String.valueOf(this.rate_failed_period)) < 0) {
                addFieldError("rate_failed_period", "*Rate failed period Required");
            }
            if (((this.getRate_from_hour() * 60) + this.getRate_from_min()) >= ((this.getRate_to_hour() * 60) + this.getRate_to_min())) {
                addFieldError("rate_from_hour", "*'To time' must be greater than 'From time'");
            }
        }
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getRate_id() {
        return rate_id;
    }

    public void setRate_id(Long rate_id) {
        this.rate_id = rate_id;
    }

    public long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public String getRate_destination_code() {
        return rate_destination_code;
    }

    public void setRate_destination_code(String rate_destination_code) {
        this.rate_destination_code = rate_destination_code;
    }

    public String getRate_destination_name() {
        return rate_destination_name;
    }

    public void setRate_destination_name(String rate_destination_name) {
        this.rate_destination_name = rate_destination_name;
    }

    public Double getRate_per_min() {
        return rate_per_min;
    }

    public void setRate_per_min(Double rate_per_min) {
        this.rate_per_min = rate_per_min;
    }

    public Integer getRate_first_pulse() {
        return rate_first_pulse;
    }

    public void setRate_first_pulse(Integer rate_first_pulse) {
        this.rate_first_pulse = rate_first_pulse;
    }

    public Integer getRate_next_pulse() {
        return rate_next_pulse;
    }

    public void setRate_next_pulse(Integer rate_next_pulse) {
        this.rate_next_pulse = rate_next_pulse;
    }

    public Integer getRate_grace_period() {
        return rate_grace_period;
    }

    public void setRate_grace_period(Integer rate_grace_period) {
        this.rate_grace_period = rate_grace_period;
    }

    public Integer getRate_failed_period() {
        return rate_failed_period;
    }

    public void setRate_failed_period(Integer rate_failed_period) {
        this.rate_failed_period = rate_failed_period;
    }

    public Integer getRate_day() {
        return rate_day;
    }

    public void setRate_day(Integer rate_day) {
        this.rate_day = rate_day;
    }

    public Integer getRate_from_hour() {
        return rate_from_hour;
    }

    public void setRate_from_hour(Integer rate_from_hour) {
        this.rate_from_hour = rate_from_hour;
    }

    public Integer getRate_from_min() {
        return rate_from_min;
    }

    public void setRate_from_min(Integer rate_from_min) {
        this.rate_from_min = rate_from_min;
    }

    public Integer getRate_to_hour() {
        return rate_to_hour;
    }

    public void setRate_to_hour(Integer rate_to_hour) {
        this.rate_to_hour = rate_to_hour;
    }

    public Integer getRate_to_min() {
        return rate_to_min;
    }

    public void setRate_to_min(Integer rate_to_min) {
        this.rate_to_min = rate_to_min;
    }

    public Integer getRate_status() {
        return rate_status;
    }

    public void setRate_status(Integer rate_status) {
        this.rate_status = rate_status;
    }

    public File getUpload() {
        return upload;
    }

    public void setUpload(File upload) {
        this.upload = upload;
    }

    public String getUploadFileContentType() {
        return uploadFileContentType;
    }

    public void setUploadFileContentType(String uploadFileContentType) {
        this.uploadFileContentType = uploadFileContentType;
    }

    public String getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    @Override
    public void setServletResponse(HttpServletResponse servletResponse) {
        this.servletResponse = servletResponse;
    }

}
