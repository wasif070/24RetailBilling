/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.home;

/**
 *
 * @author Wasif Islam
 */
public class Home {
    private Integer client_id;
    private String client_name;
    private Long calls;
    private String duration;
    private Double client_balance;
    private Long rateplan_id;

    public Integer getClient_id() {
        return client_id;
    }

    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public Long getCalls() {
        return calls;
    }

    public void setCalls(Long calls) {
        this.calls = calls;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Double getClient_balance() {
        return client_balance;
    }

    public void setClient_balance(Double client_balance) {
        this.client_balance = client_balance;
    }

    public Long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(Long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }
 
}
