/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.home;

import com.ipvision.clients.ClientDAO;
import com.ipvision.gateway.GatewayDAO;
import com.ipvision.user.Users;
//import com.mysql.jdbc.PreparedStatement;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import util.Utils;

/**
 *
 * @author Wasif Islam
 */
public class HomeLoader {

    static Logger logger = Logger.getLogger(HomeLoader.class.getName());
    public static ArrayList<Home> homeList = null;
    static HomeLoader homeLoader = null;
    private HashMap<String, Object> clientInfo = new HashMap<String, Object>();
    GatewayDAO gatewayDAO = new GatewayDAO();

    public HomeLoader() {
    }

    public static HomeLoader getInstance() {
        if (homeLoader == null) {
            createHomeLoader();
        }
        return homeLoader;
    }

    private synchronized static void createHomeLoader() {
        if (homeLoader == null) {
            homeLoader = new HomeLoader();
        }
    }

    public synchronized ArrayList<Home> getHomeDTOList(Users usr_dto) {
        clientInfo = new HashMap<String, Object>();
        homeList = new ArrayList<Home>();
        Home hm = new Home();

        if (usr_dto.getRole_id() > 0) {
            DBConnection dbConnection = null;
            Statement statement = null;
            try {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                statement = dbConnection.connection.createStatement();
                String sql = "select count(*), SUM(call_duration) from reports where disconnect_cause='200 OK' AND (origin_caller='" + usr_dto.getUser_id() + "' OR term_ip in (" + gatewayDAO.getGatewayIPbyClient(usr_dto.getUser_id()) + "))";
                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    hm.setCalls(resultSet.getLong("count(*)"));
                    hm.setDuration(Utils.getTimeHHMMSS(resultSet.getLong("SUM(call_duration)")));
                }

            } catch (Exception e) {
                logger.fatal("Exception in HomeLoader:" + e);
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }
            }
            ClientDAO dao = new ClientDAO();
            clientInfo = dao.getClientInfoForHome(usr_dto.getUser_id());

            hm.setClient_balance(Utils.FormatBalance(Double.parseDouble(clientInfo.get("balance").toString())));
            hm.setRateplan_id(Long.parseLong(clientInfo.get("rateplanID").toString()));
        }
        hm.setClient_name(usr_dto.getUser_id());

        homeList.add(hm);
        return homeList;
    }
}
