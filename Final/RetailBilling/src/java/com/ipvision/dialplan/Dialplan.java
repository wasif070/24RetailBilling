/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.dialplan;

import java.util.Comparator;

/**
 *
 * @author reefat
 */
public class Dialplan implements Comparable<Dialplan>{

    private Integer id;
    private Integer dialplan_db_id;    
    private String dp_name;
    private String dp_description;
    private String dp_dnis_pattern;
    private Integer dp_capacity;
    private String dp_ani_translate;
    private String dp_dnis_translate;
    private Integer dp_priority;
    private String dp_gateway_list;
    private Integer dp_enable;
    private String dp_update_time;
    
    private static Integer field;
    private static Integer sort_order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDialplan_db_id() {
        return dialplan_db_id;
    }

    public void setDialplan_db_id(Integer dialplan_db_id) {
        this.dialplan_db_id = dialplan_db_id;
    }

    public static Integer getField() {
        return field;
    }

    public static void setField(Integer field) {
        Dialplan.field = field;
    }

    public static Integer getSort_order() {
        return sort_order;
    }

    public static void setSort_order(Integer sort_order) {
        Dialplan.sort_order = sort_order;
    }

    public String getDp_name() {
        return dp_name;
    }

    public void setDp_name(String dp_name) {
        this.dp_name = dp_name;
    }

    public String getDp_description() {
        return dp_description;
    }

    public void setDp_description(String dp_description) {
        this.dp_description = dp_description;
    }

    public String getDp_dnis_pattern() {
        return dp_dnis_pattern;
    }

    public void setDp_dnis_pattern(String dp_dnis_pattern) {
        this.dp_dnis_pattern = dp_dnis_pattern;
    }

    public Integer getDp_capacity() {
        return dp_capacity;
    }

    public void setDp_capacity(Integer dp_capacity) {
        this.dp_capacity = dp_capacity;
    }

    public String getDp_ani_translate() {
        return dp_ani_translate;
    }

    public void setDp_ani_translate(String dp_ani_translate) {
        this.dp_ani_translate = dp_ani_translate;
    }

    public String getDp_dnis_translate() {
        return dp_dnis_translate;
    }

    public void setDp_dnis_translate(String dp_dnis_translate) {
        this.dp_dnis_translate = dp_dnis_translate;
    }

    public Integer getDp_priority() {
        return dp_priority;
    }

    public void setDp_priority(Integer dp_priority) {
        this.dp_priority = dp_priority;
    }

    public String getDp_gateway_list() {
        return dp_gateway_list;
    }

    public void setDp_gateway_list(String dp_gateway_list) {
        this.dp_gateway_list = dp_gateway_list;
    }

    public Integer getDp_enable() {
        return dp_enable;
    }

    public void setDp_enable(Integer dp_enable) {
        this.dp_enable = dp_enable;
    }

    public String getDp_update_time() {
        return dp_update_time;
    }

    /**
     *
     * @param dp_update_time : the standard  time difference for fetching data from database
     */
    public void setDp_update_time(String dp_update_time) {
        this.dp_update_time = dp_update_time;
    }
    

    @Override
    public int compareTo(Dialplan o) {   
        Integer var1 = 0;
        Integer var2 = 0;
        switch (field) {
            case 2:
                var1 = this.dp_capacity;
                var2 = o.getDp_capacity();
                break;
            default:
                break;
        }
        switch (sort_order) {
            case 0:
                //ascending order
                return var1.compareTo(var2);
            case 1:
                //descending order
                return var2.compareTo(var1);
            default:
                return -1;
        }
    }    
    
    /**
     * Comparator Used for sorting
     */
    public static Comparator<Dialplan> DialplanPropertiesComparator = new Comparator<Dialplan>() {
        @Override
        public int compare(Dialplan dialplan1, Dialplan dialplan2) {

            String var_dialplan_sorting_field_value_1 = null;// = dialplan1.getFirstName().toUpperCase();
            String var_dialplan_sorting_field_value_2 = null;// = dialplan2.getFirstName().toUpperCase();

            switch (field) {
                case 0: //Dialplan Name
                    var_dialplan_sorting_field_value_1 = dialplan1.getDp_name().toUpperCase();
                    var_dialplan_sorting_field_value_2 = dialplan2.getDp_name().toUpperCase();
                    break;
                case 1: //Dialplan dnis_pattern
                    var_dialplan_sorting_field_value_1 = dialplan1.getDp_dnis_pattern().toUpperCase();
                    var_dialplan_sorting_field_value_2 = dialplan2.getDp_dnis_pattern().toUpperCase();
                    break;
                case 2: //Dialplan Capacity
                    return dialplan1.compareTo(dialplan2);
                case 3: //Dialplan ani_translate
                    var_dialplan_sorting_field_value_1 = dialplan1.getDp_ani_translate().toUpperCase();
                    var_dialplan_sorting_field_value_2 = dialplan2.getDp_ani_translate().toUpperCase();
                    break;
                case 4: //Dialplan dnis_translate
                    var_dialplan_sorting_field_value_1 = dialplan1.getDp_dnis_translate().toUpperCase();
                    var_dialplan_sorting_field_value_2 = dialplan1.getDp_dnis_translate().toUpperCase();
                    break;
                default:
                    break;
            }

            if (var_dialplan_sorting_field_value_1 == null || var_dialplan_sorting_field_value_2 == null) {
                return -1;
            }

            switch (sort_order) {
                case 0:
                    //ascending order
                    return var_dialplan_sorting_field_value_1.compareTo(var_dialplan_sorting_field_value_2);
                case 1:
                    //descending order
                    return var_dialplan_sorting_field_value_2.compareTo(var_dialplan_sorting_field_value_1);
                default:
                    return -1;
            }
        }
    };    
}
