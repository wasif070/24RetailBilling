/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.dialplan;

import com.ipvision.gateway.Gateway;
import com.ipvision.gateway.GatewayDAO;
import com.opensymphony.xwork2.ActionSupport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author reefat
 */
public class TermGatewaySelectAction extends ActionSupport {

    public Map<Integer, String> termGatewayList = new HashMap<Integer, String>();

    public Map<Integer, String> getTermGatewayList() {
        return termGatewayList;
    }

    public void setTermGatewayList(Map<Integer, String> termGatewayList) {
        this.termGatewayList = termGatewayList;
    }

    public TermGatewaySelectAction() {
    }

    @Override
    public String execute() throws Exception {
        GatewayDAO gatewayDAO = new GatewayDAO();
        int[] gw_type = {1,2};
        List<Gateway> gateway_list = gatewayDAO.findByType(gw_type);

        for (Gateway temp_gateway : gateway_list) {
            if (temp_gateway.getGateway_status().equalsIgnoreCase("Active")) {
                System.out.println("temp_gateway.getGateway_name() --> "+temp_gateway.getGateway_name());
                termGatewayList.put(temp_gateway.getGateway_db_id(), temp_gateway.getGateway_name());
            }            
        }

        return SUCCESS;
    }
}