/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.dialplan;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ipvision.common.ConditionChecking;
import com.ipvision.common.DataStorage;
import com.ipvision.common.*;
import com.ipvision.gateway.GatewayDAO;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.log4j.Logger;
import util.Constants;
import util.MyAppError;
import util.Utils;

/**
 *
 * @author reefat
 */
public class DialplanDAO implements Comparable<Integer> {

    static Logger logger = Logger.getLogger(DialplanDAO.class.getName());
    private PreparedStatement ps = null;
    protected DBConnection dbConnection = null;
    private static long last_update_time = 0;

    static {
        dialplan_list = buildList();
    }
    static List<Dialplan> dialplan_list;
    static List<Dialplan> dialplan_filtered_list;
    static List<Dialplan> dialplan_visible_list;

    public static List<Dialplan> buildList() {
        ArrayList<Dialplan> v_temp_dialplan_list = new ArrayList<Dialplan>();

        DBConnection local_dbConnection = null;
        PreparedStatement prepared_statement = null;
        ResultSet rs;// = null;
        try {
            local_dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql;// = "";

            sql = "select * from dialpeers order by dp_id asc";
            prepared_statement = local_dbConnection.connection.prepareStatement(sql);
            rs = prepared_statement.executeQuery();
            while (rs.next()) {
                Dialplan dialplan = new Dialplan();
                dialplan.setDialplan_db_id(rs.getInt("dp_id"));
                dialplan.setDp_name(rs.getString("dp_name"));
                dialplan.setDp_description(rs.getString("dp_description"));
                dialplan.setDp_dnis_pattern(rs.getString("dp_dnis_pattern"));
                dialplan.setDp_capacity(rs.getInt("dp_capacity"));
                dialplan.setDp_ani_translate(rs.getString("dp_ani_translate"));
                dialplan.setDp_dnis_translate(rs.getString("dp_dnis_translate"));
                dialplan.setDp_priority(rs.getInt("dp_priority"));
                dialplan.setDp_gateway_list(gatewayNamesList(rs.getString("dp_gateway_list")));
                dialplan.setDp_enable(rs.getInt("dp_enable"));

                v_temp_dialplan_list.add(dialplan);
            }

        } catch (Exception ex) {
            logger.debug("Exception in DialplanDAO , buildList() ---> " + ex.getMessage());
        } finally {
            try {
                if (prepared_statement != null) {
                    prepared_statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (local_dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(local_dbConnection);
                }                
            } catch (Exception e) {
            }
        }

        return v_temp_dialplan_list;
    }

    MyAppError save(Dialplan dialplan) {
        MyAppError myAppError = new MyAppError();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            ps = null;
            ResultSet rs = null;

            sql = "select * from dialpeers where dp_name = ?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, dialplan.getDp_name());
            rs = ps.executeQuery();

            if (rs.next()) {
                myAppError.setErrorType(MyAppError.DBError);
                myAppError.setErrorMessage("Duplicate ID");
            } else {
                if (ps != null) {
                    ps.close();
                }
                sql = "insert into dialpeers set "
                        + "dp_name = ?, "
                        + "dp_description = ?, "
                        + "dp_dnis_pattern = ?, "
                        + (dialplan.getDp_capacity() != null ? "dp_capacity = ?, " : "")
                        + "dp_ani_translate = ?, "
                        + "dp_dnis_translate = ?, "
                        + (dialplan.getDp_priority() != null ? "dp_priority = ?, " : "")
                        + "dp_gateway_list = ? ";
                ps = dbConnection.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                int index = 1;

                ps.setString(index++, dialplan.getDp_name());
                ps.setString(index++, dialplan.getDp_description());
                ps.setString(index++, dialplan.getDp_dnis_pattern());
                if (dialplan.getDp_capacity() != null) {
                    ps.setInt(index++, dialplan.getDp_capacity());
                }
                ps.setString(index++, dialplan.getDp_ani_translate());
                ps.setString(index++, dialplan.getDp_dnis_translate());
                if (dialplan.getDp_priority() != null) {
                    ps.setInt(index++, dialplan.getDp_priority());
                }
                ps.setString(index++, dialplan.getDp_gateway_list());

                logger.debug("insert dialplan sql --> " + Utils.getSQLString(ps));
                ps.executeUpdate();
                rs = ps.getGeneratedKeys();
                if (rs != null && rs.next()) {
                    dialplan.setDialplan_db_id(rs.getInt(1));
                }
                dialplan.setDp_gateway_list(gatewayNamesList(dialplan.getDp_gateway_list()));
                dialplan_list.add(dialplan);
                myAppError.setErrorMessage("Successfully added");
            }
        } catch (Exception ex) {
            logger.debug("Exception in DialplanDAO , save() ---> " + ex.getMessage());
            myAppError.setErrorType(MyAppError.DBError);
            myAppError.setErrorMessage("DB error : " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return myAppError;
    }

    MyAppError update(Dialplan dialplan) {
        MyAppError myAppError = new MyAppError();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            ps = null;
            ResultSet rs = null;
            sql = "select dp_id from dialpeers where dp_id = ?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setInt(1, dialplan.getDialplan_db_id());
            rs = ps.executeQuery();
            if (rs.next()) {
                sql = "Update dialpeers set "
                        + "dp_name = ?, "
                        + "dp_description = ?, "
                        + "dp_dnis_pattern = ?, "
                        + "dp_capacity = ?, "
                        + "dp_ani_translate = ?, "
                        + "dp_dnis_translate = ?, "
                        + "dp_priority = ?, "
                        + "dp_gateway_list = ? "
                        + "where "
                        + "dp_id = ? ";
                if (ps != null) {
                    ps.close();
                }
                ps = dbConnection.connection.prepareStatement(sql);

                ps.setString(1, dialplan.getDp_name());
                ps.setString(2, dialplan.getDp_description());
                ps.setString(3, dialplan.getDp_dnis_pattern());
                ps.setInt(4, dialplan.getDp_capacity());
                ps.setString(5, dialplan.getDp_ani_translate());
                ps.setString(6, dialplan.getDp_dnis_translate());
                ps.setInt(7, dialplan.getDp_priority());
                ps.setString(8, dialplan.getDp_gateway_list());
                ps.setInt(9, dialplan.getDialplan_db_id());

                ps.executeUpdate();

                for (Dialplan temp_dialplan : dialplan_visible_list) {
                    if (dialplan.getDialplan_db_id() == temp_dialplan.getDialplan_db_id()) {

                        temp_dialplan.setDp_name(dialplan.getDp_name());
                        temp_dialplan.setDp_description(dialplan.getDp_description());
                        temp_dialplan.setDp_dnis_pattern(dialplan.getDp_dnis_pattern());
                        temp_dialplan.setDp_capacity(dialplan.getDp_capacity());
                        temp_dialplan.setDp_ani_translate(dialplan.getDp_ani_translate());
                        temp_dialplan.setDp_dnis_translate(dialplan.getDp_dnis_translate());
                        temp_dialplan.setDp_priority(dialplan.getDp_priority());
                        temp_dialplan.setDp_gateway_list(gatewayNamesList(dialplan.getDp_gateway_list()));
                        break;
                    }
                }
                myAppError.setErrorMessage("Successfully edited");
            } else {
                myAppError.setErrorType(MyAppError.NotUpdated);
                myAppError.setErrorMessage("Dialplan Not found ");
            }

        } catch (Exception ex) {
            logger.debug("Exception in DialplanDAO , update() ---> " + ex.getMessage());
            myAppError.setErrorType(MyAppError.DBError);
            myAppError.setErrorMessage("DB error : " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {

                if (dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return myAppError;
    }

    MyAppError delete(ArrayList<Dialplan> param_dialplan_list) {
        MyAppError myAppError = new MyAppError();
        String db_id = "";
        String var_seperator = " , ";
        for (Dialplan dialplan_temp_for_db_id : param_dialplan_list) {
            db_id += dialplan_temp_for_db_id.getDialplan_db_id() + var_seperator;
        }

        db_id = db_id.substring(0, db_id.length() - var_seperator.length());
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            ps = null;
            sql = "Delete from dialpeers where dp_id IN ( " + db_id + " )";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.executeUpdate();

            int size = param_dialplan_list.size();
            int[] individual_id_int = new int[size];
            List<Integer> to_be_deleted = new ArrayList<Integer>();
            int counter = 0;
            for (int i = 0; i < size; i++) {
                individual_id_int[i] = param_dialplan_list.get(i).getDialplan_db_id();
            }
            int index_of_objects = 0;

            for (Dialplan temp_dialplan : dialplan_visible_list) {
                if (contains(temp_dialplan.getDialplan_db_id(), individual_id_int)) {
                    counter++;
                    to_be_deleted.add(index_of_objects);
                    if (counter == size) {
                        break;
                    }
                }
                index_of_objects++;
            }
            Collections.sort(to_be_deleted, intComparator);
            for (int i = 0; i < to_be_deleted.size(); i++) {
                dialplan_visible_list.remove(to_be_deleted.get(i).intValue());
            }
            myAppError.setErrorMessage("response.deletedsuccessfully");
        } catch (Exception ex) {
            logger.debug("Exception in DialplanDAO , delete() ---> " + ex.getMessage());
            myAppError.setErrorType(MyAppError.DBError);
            myAppError.setErrorMessage("Error : " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return myAppError;
    }

    Dialplan findById(int parseInt) {
        for (Dialplan dialplan : dialplan_visible_list) {
            if (dialplan.getId() == parseInt) {
                return dialplan;
            }
        }
        return null;
    }

    ArrayList<Dialplan> findMultipleById(int[] parseInt) {
        ArrayList<Dialplan> multiples_dialplan_to_be_deleted = new ArrayList<Dialplan>();
        int counter = 0;
        for (Dialplan dialplan : dialplan_visible_list) {
            if (contains(dialplan.getId(), parseInt)) {
                multiples_dialplan_to_be_deleted.add(dialplan);
                counter++;
                if (counter == parseInt.length) {
                    break;
                }
            }
        }
        return multiples_dialplan_to_be_deleted;
    }

    boolean contains(int param_one, int[] target_value_list) {
        boolean does_contain = false;
        for (int i = 0; i < target_value_list.length; i++) {
            if (param_one == target_value_list[i]) {
                does_contain = true;
                break;
            }
        }
        return does_contain;
    }

    Integer count() {
        return (dialplan_list != null ? dialplan_list.size() : 0);
    }

    Integer countFilteredData() {
        return (dialplan_filtered_list != null ? dialplan_filtered_list.size() : 0);
    }

    List<Dialplan> find(int from, int to) {
        checkForReload();
        if (Dialplan.getSort_order() != null) {
            Collections.sort(dialplan_list, Dialplan.DialplanPropertiesComparator);
        }
        dialplan_visible_list = dialplan_list.subList(from, to);
        for (int i = 0; i < to - from; i++) {
            dialplan_visible_list.get(i).setId(i + 1);
        }
        return dialplan_visible_list;
    }

    List<Dialplan> findFilteredList(String json_data, int from, int to) {
        checkForReload();
        ConditionChecking conditionChecking = new ConditionChecking();
        dialplan_filtered_list = new ArrayList<Dialplan>();
        FiltersSubFields filtersSubFields;
        Filters filters_obj;
        Gson json = new GsonBuilder().serializeNulls().create();
        filters_obj = json.fromJson(json_data, Filters.class);

        String comparison_type;
        boolean condition_matched = true;
        boolean return_val = false;

        for (Dialplan filtered_dialplan : dialplan_list) {
            condition_matched = true;
            for (int i = 0; i < filters_obj.getRules().size(); i++) {
                filtersSubFields = json.fromJson(filters_obj.getRules().get(i).toString(), FiltersSubFields.class);
                comparison_type = filtersSubFields.getOp();
                switch (DataStorage.getInstance().getFieldMappingDialplan(filtersSubFields.getField())) {
                    case 0:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_dialplan.getDp_name(), filtersSubFields.getData());
                        break;
                    case 1:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_dialplan.getDp_dnis_pattern(), filtersSubFields.getData());
                        break;
                    case 2:
                        return_val = conditionChecking.checkConditionNumeric(comparison_type, filtered_dialplan.getDp_capacity(), Integer.parseInt(filtersSubFields.getData()));
                        break;
                    case 3:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_dialplan.getDp_ani_translate(), filtersSubFields.getData());
                        break;
                    case 4:
                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_dialplan.getDp_dnis_translate(), filtersSubFields.getData());
                        break;
                    case 5:
                        return_val = conditionChecking.checkConditionNumeric(comparison_type, filtered_dialplan.getDp_priority(), Integer.parseInt(filtersSubFields.getData()));
                        break;
                    default:
                        break;
                }

                if (filters_obj.getGroupOp().equalsIgnoreCase("And")) {
                    if (return_val == false) {
                        condition_matched = false;
                        break;
                    }
                } else {
                    if (return_val == true) {
                        condition_matched = true;
                        break;
                    } else if (i == filters_obj.getRules().size() - 1) {
                        condition_matched = false;
                    }
                }
            }
            if (condition_matched) {
                dialplan_filtered_list.add(filtered_dialplan);
            }
        }

        if (Dialplan.getSort_order() != null) {
            Collections.sort(dialplan_filtered_list, Dialplan.DialplanPropertiesComparator);
        }
        if (to > dialplan_filtered_list.size() || to == -1) {
            to = dialplan_filtered_list.size() - from;
        }

        dialplan_visible_list = dialplan_filtered_list.subList(from, to);

        for (int i = 0; i < to - from; i++) {
            dialplan_visible_list.get(i).setId(i + 1);
        }

        return dialplan_visible_list;
    }

    private void checkForReload() {
        long current_time = System.currentTimeMillis();
        if (current_time - last_update_time > Constants.getUpdate_time_difference()) {
            forceReload();
        }
    }

    private void forceReload() {
        dialplan_list = buildList();
        last_update_time = System.currentTimeMillis();
    }

    private static String gatewayNamesList(String param_string) {
        if (param_string == null || param_string.length() <= 0) {
            return "";
        }
        GatewayDAO gatewayDAO = new GatewayDAO();
        StringBuilder sb = new StringBuilder();
        String[] temp_str = (param_string.substring(1, param_string.length() - 1)).split(",");
        for (String s : temp_str) {
            sb.append(gatewayDAO.findByIdFromMainList(Integer.parseInt(s)).getGateway_name()).append(",");
        }
        return sb.toString().substring(0, sb.length() - 1);
    }

    @Override
    public int compareTo(Integer t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public static Comparator<Integer> intComparator = new Comparator<Integer>() {
        @Override
        public int compare(Integer t, Integer t1) {
            return t1.compareTo(t);
        }
    };
}
