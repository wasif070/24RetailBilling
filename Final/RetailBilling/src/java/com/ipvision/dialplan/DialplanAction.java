/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.dialplan;

import com.ipvision.common.Admin;
import com.ipvision.common.DataStorage;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;

/**
 *
 * @author reefat
 */
public class DialplanAction extends ActionSupport implements Admin {
    

    //Your result List
    private List<Dialplan> gridModel;
    //get how many rows we want to have into the grid - rowNum attribute in the grid
    private Integer rows = 10;
    //Get the requested page. By default grid sets this to 1.
    private Integer page = 1;
    // sorting order - asc or desc
    private String sord;
    // get index row - i.e. user click to sort.
    private String sidx;
    // Your Total Pages
    private Integer total = 0;
    // All Record
    private Integer records = 0;
    
    private String filters;

    public List<Dialplan> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<Dialplan> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getRecords() {
        return records;
    }

    public void setRecords(Integer records) {
        this.records = records;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }
    
    public DialplanAction() {
    }
    
    @Override
    public String execute() throws Exception {
        int to = (rows * page);
        int from = to - rows;

        //Count Rows (select count(*) from custumer)
        DialplanDAO dialplanDAO = new DialplanDAO();
        records = dialplanDAO.count();

        if (to > records) {
            to = records;
        }

//---------------------------------------- sorting portion (start) ---------------------------------        
        if (getSidx() != null && getSidx().length() > 0) {
            Dialplan.setField(DataStorage.getInstance().getFieldMappingDialplan(getSidx()));

            if (getSord() != null) {
                if (getSord().equals("asc")) {
                    Dialplan.setSort_order(0);
                } else if (getSord().equals("desc")) {
                    Dialplan.setSort_order(1);
                }
            }
        }
//---------------------------------------- sorting portion (end) --------------------------------- 
        
        if (getFilters() != null && getFilters().length() > 0) {
            gridModel = dialplanDAO.findFilteredList(getFilters(), from, to);
            records = dialplanDAO.countFilteredData();
        } else {
            gridModel = dialplanDAO.find(from, to);
        }

        //calculate the total pages for the query
        total = (int) Math.ceil((double) records / (double) rows);

        return SUCCESS;
    }
}