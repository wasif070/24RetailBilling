/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.dialplan;

import com.ipvision.common.Admin;
import com.ipvision.gateway.GatewayDAO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import util.MyAppError;

/**
 *
 * @author reefat
 */
public class DialplanOperationAction extends ActionSupport implements Admin {

    private String action;
    private String id;
    private Integer dialplan_db_id;
    private String dp_name;
    private String dp_description;
    private String dp_dnis_pattern;
    private Integer dp_capacity;
    private String dp_ani_translate;
    private String dp_dnis_translate;
    private Integer dp_priority;
//    private String dp_gateway_list;
    private Integer[] dp_gateway_list;
    private String temp_dp_gateway_list;

    public DialplanOperationAction() {
    }

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String add() throws Exception {
        String return_val = SUCCESS;

        Dialplan dialplan = new Dialplan();
        dialplan.setDp_name(dp_name);
        dialplan.setDp_description(dp_description);
        dialplan.setDp_dnis_pattern(dp_dnis_pattern);
        dialplan.setDp_capacity(dp_capacity != null ? dp_capacity : 0);
        dialplan.setDp_ani_translate(dp_ani_translate);
        dialplan.setDp_dnis_translate(dp_dnis_translate);
        dialplan.setDp_priority(dp_priority != null ? dp_priority : 0);
        StringBuilder sb = new StringBuilder(",");
        for (Integer s : dp_gateway_list) {
            sb.append(s).append(",");
        }
        dialplan.setDp_gateway_list(sb.toString());
//        dialplan.setDp_gateway_list("," + dp_gateway_list.replace(" ", "") + ",");

        DialplanDAO dialplanDAO = new DialplanDAO();
        MyAppError myAppError = dialplanDAO.save(dialplan);

        if (myAppError.getErrorType() > 0) {
            addActionError(myAppError.getErrorMessage());
            return_val = INPUT;
        } else {
            addActionMessage(myAppError.getErrorMessage());
        }

        return return_val;
    }

    public String update() throws Exception {
        String return_val = SUCCESS;

        Dialplan dialplan;// = new Dialplan();
        DialplanDAO dialplanDAO = new DialplanDAO();
        dialplan = dialplanDAO.findById(Integer.parseInt(id));
        if (dialplan != null) {
            dialplan.setDp_description(dp_description);
            dialplan.setDp_dnis_pattern(dp_dnis_pattern);
            dialplan.setDp_capacity(dp_capacity != null ? dp_capacity : 0);
            dialplan.setDp_ani_translate(dp_ani_translate);
            dialplan.setDp_dnis_translate(dp_dnis_translate);
            dialplan.setDp_priority(dp_priority != null ? dp_priority : 0);
//            dialplan.setDp_gateway_list("," + dp_gateway_list.replace(" ", "") + ",");
            StringBuilder sb = new StringBuilder(",");
            for (Integer s : dp_gateway_list) {
                sb.append(s).append(",");
            }
            dialplan.setDp_gateway_list(sb.toString());

            MyAppError myAppError = dialplanDAO.update(dialplan);

            if (myAppError.getErrorType() > 0) {
                addActionError(myAppError.getErrorMessage());
                return_val = INPUT;
            } else {
                addActionMessage(myAppError.getErrorMessage());
            }
        } else {
            addActionError(getText("notFound.dialplan"));
            return_val = INPUT;
        }

        return return_val;

    }

    public String delete() throws Exception {

        String return_val = SUCCESS;

        ArrayList<Dialplan> dialplan_list = null;
        String[] individual_id_str = id.split(",");
        int[] individual_id_int = new int[individual_id_str.length];
        for (int i = 0; i < individual_id_str.length; i++) {
            individual_id_int[i] = Integer.valueOf(individual_id_str[i]);
        }
        DialplanDAO dialplanDAO = new DialplanDAO();
        dialplan_list = dialplanDAO.findMultipleById(individual_id_int);
        MyAppError myAppError = dialplanDAO.delete(dialplan_list);

        if (myAppError.getErrorType() > 0) {
            addActionError(myAppError.getErrorMessage());
            return_val = INPUT;
        } else {
            addActionMessage(getText(myAppError.getErrorMessage()));
        }
        return return_val;
    }

    public String retrieve() throws Exception {
        DialplanDAO dialplanDAO = new DialplanDAO();
        GatewayDAO gatewayDAO = new GatewayDAO();
        HttpServletRequest request = ServletActionContext.getRequest();
        if (request.getParameter("id") != null) {
            int row_id = Integer.valueOf(request.getParameter("id"));
            Dialplan dialplan = dialplanDAO.findById(row_id);
            if (dialplan != null) {
                this.setId(String.valueOf(row_id));
                this.setDialplan_db_id(dialplan.getDialplan_db_id());
                this.setDp_name(dialplan.getDp_name());
                this.setDp_description(dialplan.getDp_description());
                this.setDp_dnis_pattern(dialplan.getDp_dnis_pattern());
                this.setDp_capacity(dialplan.getDp_capacity());
                this.setDp_ani_translate(dialplan.getDp_ani_translate());
                this.setDp_dnis_translate(dialplan.getDp_dnis_translate());
                this.setDp_priority(dialplan.getDp_priority());
                Integer[] ss = gatewayDAO.getGatewayDBIdList(dialplan.getDp_gateway_list());

                StringBuilder str_temp = new StringBuilder();
                for (int i : ss) {
                    str_temp.append(i).append(",");
                }
                setTemp_dp_gateway_list(str_temp.substring(0, str_temp.length() - 1));
                System.out.println("Loggggggggg ==> " + getTemp_dp_gateway_list());
                this.setDp_gateway_list(ss);
//                this.setDp_gateway_list(gatewayDAO.getGatewayDBIdList(dialplan.getDp_gateway_list()));
            }

        }
        this.setAction("update");

        return INPUT;
    }

    public String display() throws Exception {
        this.setAction("add");
        return INPUT;
    }

    @Override
    public void validate() {
        this.setAction(ActionContext.getContext().getName());
        String actn = this.getAction();
        if (actn.equalsIgnoreCase("add") || actn.equalsIgnoreCase("update")) {
            if (actn.equalsIgnoreCase("add")) {
                if (this.dp_name == null || this.dp_name.length() == 0) {
                    addFieldError("dp_name", getText("required.common"));
                }

                if (dp_name.contains(" ")) {
                    addActionError(getText("notAllowed.space") + " Dialplan Name");
                }
            }

            if (this.dp_dnis_pattern == null || this.dp_dnis_pattern.length() == 0) {
                addFieldError("dp_dnis_pattern", getText("required.common"));
            }
            if (this.dp_gateway_list == null) {
                addFieldError("dp_gateway_list", getText("required.select"));
            }
//            if (this.dp_gateway_list == null || this.dp_gateway_list.length() == 0) {
//                addFieldError("dp_gateway_list", getText("required.select"));
//            }
        }
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getDialplan_db_id() {
        return dialplan_db_id;
    }

    public void setDialplan_db_id(Integer dialplan_db_id) {
        this.dialplan_db_id = dialplan_db_id;
    }

    public String getDp_name() {
        return dp_name;
    }

    public void setDp_name(String dp_name) {
        this.dp_name = dp_name;
    }

    public String getDp_description() {
        return dp_description;
    }

    public void setDp_description(String dp_description) {
        this.dp_description = dp_description;
    }

    public String getDp_dnis_pattern() {
        return dp_dnis_pattern;
    }

    public void setDp_dnis_pattern(String dp_dnis_pattern) {
        this.dp_dnis_pattern = dp_dnis_pattern;
    }

    public Integer getDp_capacity() {
        return dp_capacity;
    }

    public void setDp_capacity(Integer dp_capacity) {
        this.dp_capacity = dp_capacity;
    }

    public String getDp_ani_translate() {
        return dp_ani_translate;
    }

    public void setDp_ani_translate(String dp_ani_translate) {
        this.dp_ani_translate = dp_ani_translate;
    }

    public String getDp_dnis_translate() {
        return dp_dnis_translate;
    }

    public void setDp_dnis_translate(String dp_dnis_translate) {
        this.dp_dnis_translate = dp_dnis_translate;
    }

    public Integer getDp_priority() {
        return dp_priority;
    }

    public void setDp_priority(Integer dp_priority) {
        this.dp_priority = dp_priority;
    }

    public Integer[] getDp_gateway_list() {
        return dp_gateway_list;
    }

    public void setDp_gateway_list(Integer[] dp_gateway_list) {
        this.dp_gateway_list = dp_gateway_list;
    }

    public String getTemp_dp_gateway_list() {
        return temp_dp_gateway_list;
    }

    public void setTemp_dp_gateway_list(String temp_dp_gateway_list) {
        this.temp_dp_gateway_list = temp_dp_gateway_list;
    }
}