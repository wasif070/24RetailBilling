/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.announcement;

/**
 *
 * @author Wasif Islam
 */
public class Announcement{
    private Long announcement_id;
    private String announcement_name;
    private String announcement_str;
    private Integer announcement_status;
    private String announcement_status_name;
    private Integer announcement_priority;


    public Long getAnnouncement_id() {
        return announcement_id;
    }

    public void setAnnouncement_id(Long announcement_id) {
        this.announcement_id = announcement_id;
    }

    public String getAnnouncement_str() {
        return announcement_str;
    }

    public void setAnnouncement_str(String announcement_str) {
        this.announcement_str = announcement_str;
    }

    public Integer getAnnouncement_status() {
        return announcement_status;
    }

    public void setAnnouncement_status(Integer announcement_status) {
        this.announcement_status = announcement_status;
    }

    public String getAnnouncement_status_name() {
        return announcement_status_name;
    }

    public void setAnnouncement_status_name(String announcement_status_name) {
        this.announcement_status_name = announcement_status_name;
    }

    public Integer getAnnouncement_priority() {
        return announcement_priority;
    }

    public void setAnnouncement_priority(Integer announcement_priority) {
        this.announcement_priority = announcement_priority;
    }

    public String getAnnouncement_name() {
        return announcement_name;
    }

    public void setAnnouncement_name(String announcement_name) {
        this.announcement_name = announcement_name;
    }
    
}
