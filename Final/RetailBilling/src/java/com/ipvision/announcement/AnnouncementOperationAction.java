/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.announcement;

import com.ipvision.common.Admin;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import util.MyAppError;
import util.Utils;

/**
 *
 * @author Wasif Islam
 */
public class AnnouncementOperationAction extends ActionSupport implements Admin {

    private String action;
    private String id;
    private Long announcement_id;
    private String announcement_name;
    private String announcement_str;
    private Integer announcement_status;
    private Integer announcement_priority;

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    public String add() throws Exception {
        System.out.println("ADD");
        Announcement announce = new Announcement();
        announce.setAnnouncement_name(announcement_name);
        announce.setAnnouncement_str(announcement_str);
        announce.setAnnouncement_status(announcement_status);
        announce.setAnnouncement_priority(announcement_priority);

        AnnouncementDAO announcementDAO = new AnnouncementDAO();
        MyAppError error = announcementDAO.save(announce);
        if (error.getErrorType() > 0) {
            addActionError(error.getErrorMessage());
            return INPUT;
        } else {
            addActionMessage("Successfully added");
            return SUCCESS;
        }
    }

    public String update() throws Exception {
        System.out.println("EDIT");

        Announcement announce = new Announcement();
        announce.setAnnouncement_id(announcement_id);
        announce.setAnnouncement_name(announcement_name);
        announce.setAnnouncement_str(announcement_str);
        announce.setAnnouncement_status(announcement_status);
        announce.setAnnouncement_priority(announcement_priority);

        AnnouncementDAO announcementDAO = new AnnouncementDAO();
        MyAppError error = announcementDAO.update(announce);
        if (error.getErrorType() > 0) {
            addActionError(error.getErrorMessage());
            return INPUT;
        } else {
            addActionMessage("Successfully edited");
            return SUCCESS;
        }
    }

    public String delete() throws Exception {
        System.out.println("DEL");
        AnnouncementDAO announcementDAO = new AnnouncementDAO();
        String announcementIDs = announcementDAO.getDBIds(getId());

        MyAppError error = announcementDAO.delete(announcementIDs);
        if (error.getErrorType() > 0) {
            addActionError(error.getErrorMessage());
            return INPUT;
        } else {
            addActionMessage("Successfully deleted");
            return SUCCESS;
        }
    }

    public String retrieve() throws Exception {
        System.out.println("RETRIEVE");
        HttpServletRequest request = ServletActionContext.getRequest();
        if (request.getParameter("id") != null) {
            long db_id = Long.valueOf(request.getParameter("id"));
            Announcement announce = AnnouncementDAO.getAnnouncementDTO(db_id);

            this.setAnnouncement_id(db_id);
            this.setAnnouncement_name(announce.getAnnouncement_name());
            this.setAnnouncement_str(announce.getAnnouncement_str());
            this.setAnnouncement_status(announce.getAnnouncement_status());
            this.setAnnouncement_priority(announce.getAnnouncement_priority());
        }
        this.setAction("update");

        return INPUT;
    }

    public String display() throws Exception {
        int priority = AnnouncementDAO.getPriority();
        this.setAnnouncement_priority(priority);

        if (priority >= 0 ) {
            this.setAction("add");
            return INPUT;
        } else {
           addActionError("Announcements can not be add more than 10");
           return ERROR;
        }

    }

    @Override
    public void validate() {
        this.setAction(ActionContext.getContext().getName());
        String action = getAction();
        if ("add".equals(action) || "update".equals(action)) {
            if (this.announcement_str == null || this.announcement_str.length() == 0) {
                addFieldError("announcement_str", "*Announcement Required");
            } else {
                if (this.announcement_str.contains("\n")) {
                    addFieldError("announcement_str", "*Announcement can not contain new lines");
                }
            }
            if (!Utils.isInteger(String.valueOf(this.announcement_priority)) || Integer.parseInt(String.valueOf(this.announcement_priority)) < 0) {
                addFieldError("announcement_priority", "*Priority Required");
            }
            if (this.announcement_name == null || this.announcement_name.length() == 0) {
                addFieldError("announcement_name", "*Announcement Name Required");
            }
        }
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getAnnouncement_id() {
        return announcement_id;
    }

    public void setAnnouncement_id(Long announcement_id) {
        this.announcement_id = announcement_id;
    }

    public String getAnnouncement_str() {
        return announcement_str;
    }

    public void setAnnouncement_str(String announcement_str) {
        this.announcement_str = announcement_str;
    }

    public Integer getAnnouncement_status() {
        return announcement_status;
    }

    public void setAnnouncement_status(Integer announcement_status) {
        this.announcement_status = announcement_status;
    }

    public Integer getAnnouncement_priority() {
        return announcement_priority;
    }

    public void setAnnouncement_priority(Integer announcement_priority) {
        this.announcement_priority = announcement_priority;
    }

    public String getAnnouncement_name() {
        return announcement_name;
    }

    public void setAnnouncement_name(String announcement_name) {
        this.announcement_name = announcement_name;
    }
    
}
