/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.announcement;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ipvision.common.Filters;
import com.ipvision.common.FiltersSubFields;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import util.Constants;
import util.MyAppError;

/**
 *
 * @author Wasif Islam
 */
class AnnouncementDAO {

    private static Logger logger = Logger.getLogger(AnnouncementDAO.class.getName());
    private static ArrayList<Announcement> announcementList = null;
    private static HashMap<Long, Announcement> announcementMap = new HashMap<Long, Announcement>();
    private static HashMap<Long, Integer> priorityMap = new HashMap<Long, Integer>();

    public AnnouncementDAO() {
    }

    private String condition(String filters) {
        String condition = "where ";

        Filters filters_obj;
        Gson json = new GsonBuilder().serializeNulls().create();
        filters_obj = json.fromJson(filters, Filters.class);
        FiltersSubFields filtersSubFields = null;
        for (int i = 0; i < filters_obj.getRules().size(); i++) {
            String op = "";
            filtersSubFields = json.fromJson(filters_obj.getRules().get(i).toString(), FiltersSubFields.class);

            if ("eq".equals(filtersSubFields.getOp())) {
                if ("announcement_status".equals(filtersSubFields.getField())) {
                    if (filtersSubFields.getData().startsWith("a")) {
                        op = " = 0";
                    } else {
                        op = " = 1";
                    }
                } else {
                    op = " = '" + filtersSubFields.getData() + "'";
                }
            } else if ("ne".equals(filtersSubFields.getOp())) {
                if ("announcement_status".equals(filtersSubFields.getField())) {
                    if (filtersSubFields.getData().startsWith("a")) {
                        op = " != 0";
                    } else {
                        op = " != 1";
                    }
                } else {
                    op = " != '" + filtersSubFields.getData() + "'";
                }
            } else if ("bw".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'" + filtersSubFields.getData() + "%'";
            } else if ("ew".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "'";
            } else if ("cn".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "%'";
            } else if ("gt".equals(filtersSubFields.getOp())) {
                op = " > " + filtersSubFields.getData();
            } else if ("lt".equals(filtersSubFields.getOp())) {
                op = " < " + filtersSubFields.getData();
            } else if ("ge".equals(filtersSubFields.getOp())) {
                op = " >= " + filtersSubFields.getData();
            } else if ("le".equals(filtersSubFields.getOp())) {
                op = " <= " + filtersSubFields.getData();
            }

            if (i > 0) {
                condition += " " + filters_obj.getGroupOp() + " ";
            }

            condition += filtersSubFields.getField() + op;
        }
        return condition;
    }

    public int count(String filters) {
        DBConnection dbConnection = null;
        Statement statement = null;
        int count = 0;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "";
            if (filters != null && filters.length() > 0) {
                sql = "select count(*) from announcements " + condition(filters);
            } else {
                sql = "select count(*) from announcements";
            }
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                count = resultSet.getInt("count(*)");
            }
        } catch (Exception e) {
            logger.fatal("Exception in getCount announcement:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return count;
    }

    public List<Announcement> search(String filters, String sort, String sort_field, int from, int to) {
        DBConnection dbConnection = null;
        Statement statement = null;
        announcementList = new ArrayList<Announcement>();
        announcementMap = new HashMap<Long, Announcement>();
        priorityMap = new HashMap<Long, Integer>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();

            String limit_sql = "";
            String sql = "";
            if (!(to < 0)) {
                limit_sql = "limit " + from + "," + (to - from);
            }

            String order_sql = "";
            if (sort != null && sort.length() > 0 && sort_field != null && sort_field.length() > 0) {

                order_sql = "order by " + sort_field + " " + sort;
            } else {
                order_sql = "order by announcement_priority asc";
            }

            if (filters != null && filters.length() > 0) {
                sql = "select * from announcements " + condition(filters) + " " + order_sql + " " + limit_sql;
            } else {
                sql = "select * from announcements " + order_sql + " " + limit_sql;
            }
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Announcement dto = new Announcement();
                dto.setAnnouncement_id(resultSet.getLong("announcement_id"));
                dto.setAnnouncement_name(resultSet.getString("announcement_name"));
                dto.setAnnouncement_str(resultSet.getString("announcement_str"));
                dto.setAnnouncement_priority(resultSet.getInt("announcement_priority"));
                dto.setAnnouncement_status(resultSet.getInt("announcement_status"));
                dto.setAnnouncement_status_name(Constants.STATUS_STRING[dto.getAnnouncement_status()]);
                announcementMap.put(dto.getAnnouncement_id(), dto);
                announcementList.add(dto);
                priorityMap.put(dto.getAnnouncement_id(), dto.getAnnouncement_priority());
            }

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in searchAnnouncementDTOList:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return announcementList;
    }

    public MyAppError save(Announcement announce) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select announcement_id from announcements where announcement_priority=?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setInt(1, announce.getAnnouncement_priority());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Priority conflicts with other announcements");
                resultSet.close();
                return error;
            }
            sql = "insert into announcements(announcement_name, announcement_str, announcement_status, announcement_priority) "
                    + "values(?,?,?,?)";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, announce.getAnnouncement_name());
            ps.setString(2, announce.getAnnouncement_str());
            ps.setInt(3, announce.getAnnouncement_status());
            ps.setInt(4, announce.getAnnouncement_priority());
            ps.executeUpdate();

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Error while adding Announcements:" + ex);
            logger.fatal("Error while adding Announcements: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return error;
    }

    public MyAppError update(Announcement announce) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select announcement_name from announcements where announcement_priority=? AND announcement_id!=" + announce.getAnnouncement_id();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setInt(1, announce.getAnnouncement_priority());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Priority conflicts with other announcements");
                resultSet.close();
                return error;
            }

            sql = "update announcements set announcement_name=?,announcement_str=?,announcement_status=?,announcement_priority=? where announcement_id=" + announce.getAnnouncement_id();

            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, announce.getAnnouncement_name());
            ps.setString(2, announce.getAnnouncement_str());
            ps.setInt(3, announce.getAnnouncement_status());
            ps.setInt(4, announce.getAnnouncement_priority());
            ps.executeUpdate();
            priorityMap.put(announce.getAnnouncement_id(), announce.getAnnouncement_priority());

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing Announcements: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public String getDBIds(String id) {
        String announcementIds = "";
        String[] ids = id.split(",");
        for (int i = 0; i < ids.length; i++) {
            if (i > 0) {
                announcementIds += ",";
            }
            Announcement adto = new Announcement();
            adto = announcementList.get(Integer.parseInt(ids[i]) - 1);
            announcementIds += adto.getAnnouncement_id();
        }
        return announcementIds;
    }

    public MyAppError delete(String announcement_id) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "delete from announcements where announcement_id in (" + announcement_id + ")";
            ps = dbConnection.connection.prepareStatement(sql);

            ps.executeUpdate();

            String[] delete_id = announcement_id.split(",");
            for (int i = 0; i < delete_id.length; i++) {
                
                priorityMap.remove(Long.parseLong(delete_id[i]));
            }

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error while deleting Announcement");
            logger.fatal("Error while deleting Announcement: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public static Announcement getAnnouncementDTO(long id) {
        return announcementMap.get(id);
    }

    public static Integer getPriority() {
        int priority = 0;
        if (priorityMap != null) {

            for (int i = 1; i < 11; i++) {
                if (!priorityMap.containsValue(i)) {
                    priority = i;
                    break;
                } else {
                    priority = -1;
                }
            }
        }
        return priority;
    }
}
