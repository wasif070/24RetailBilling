/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.report;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ipvision.common.Filters;
import com.ipvision.common.FiltersSubFields;
import com.ipvision.gateway.GatewayDAO;
import com.ipvision.user.Users;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import util.Utils;

/**
 *
 * @author Wasif Islam
 */
public class ReportDAO {

    static Logger logger = Logger.getLogger(ReportDAO.class.getName());
    private static ArrayList<Report> cdrList = null;
    //private HashMap<Long, Report> cdrDTOByID = null;
    //static ReportDAO cdrLoader = null;
    String curdate = String.valueOf(Utils.getDateLong(Utils.ToDateDDMMYYYY0h0m0s(System.currentTimeMillis())));
    String now = String.valueOf(System.currentTimeMillis());
    NumberFormat pddformatter = new DecimalFormat("#00.00");
    GatewayDAO gatewayDAO = new GatewayDAO();

    public ReportDAO() {
        // forceReload();
    }

    private String condition(String filters, Users usr) {
        String condition = "where ( ";
        Filters filters_obj;
        Gson json = new GsonBuilder().serializeNulls().create();
        filters_obj = json.fromJson(filters, Filters.class);
        FiltersSubFields filtersSubFields = null;
        for (int i = 0; i < filters_obj.getRules().size(); i++) {
            String op = "";
            filtersSubFields = json.fromJson(filters_obj.getRules().get(i).toString(), FiltersSubFields.class);

            if ("eq".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "ringing_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField()) || "refused_time".equals(filtersSubFields.getField()) || "bye_time".equals(filtersSubFields.getField())) {
                    op = " >= " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".000")) + " AND " + filtersSubFields.getField() + " <= " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".999"));
                } else if ("ringing_duration".equals(filtersSubFields.getField()) || "call_duration".equals(filtersSubFields.getField())) {
                    op = " > " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()) - 1000) + " AND " + filtersSubFields.getField() + " <= " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()));
                } else {
                    op = " = " + filtersSubFields.getData();
                }
            } else if ("ne".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "ringing_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField()) || "refused_time".equals(filtersSubFields.getField()) || "bye_time".equals(filtersSubFields.getField())) {
                    op = " < " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".000")) + " AND " + filtersSubFields.getField() + " > 0" + " OR " + filtersSubFields.getField() + " > " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".999"));
                } else if ("ringing_duration".equals(filtersSubFields.getField()) || "call_duration".equals(filtersSubFields.getField())) {
                    op = " < " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()) - 1000) + " AND " + filtersSubFields.getField() + " > 0" + " OR " + filtersSubFields.getField() + " > " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()));
                } else {
                    op = " != " + filtersSubFields.getData();
                }

            } else if ("gt".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "ringing_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField()) || "refused_time".equals(filtersSubFields.getField()) || "bye_time".equals(filtersSubFields.getField())) {
                    op = " > " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".999"));
                } else if ("ringing_duration".equals(filtersSubFields.getField()) || "call_duration".equals(filtersSubFields.getField())) {
                    op = " > " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()));
                } else {
                    op = " > " + filtersSubFields.getData();
                }
            } else if ("lt".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "ringing_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField()) || "refused_time".equals(filtersSubFields.getField()) || "bye_time".equals(filtersSubFields.getField())) {
                    op = " < " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".000")) + " AND " + filtersSubFields.getField() + " > 0";
                } else if ("ringing_duration".equals(filtersSubFields.getField()) || "call_duration".equals(filtersSubFields.getField())) {
                    op = " <= " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()) - 1000) + " AND " + filtersSubFields.getField() + " > 0";
                } else {
                    op = " < " + filtersSubFields.getData();
                }
            } else if ("ge".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "ringing_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField()) || "refused_time".equals(filtersSubFields.getField()) || "bye_time".equals(filtersSubFields.getField())) {
                    op = " >= " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".000"));
                } else if ("ringing_duration".equals(filtersSubFields.getField()) || "call_duration".equals(filtersSubFields.getField())) {
                    op = " > " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()) - 1000);
                } else {
                    op = " >= " + filtersSubFields.getData();
                }
            } else if ("le".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "ringing_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField()) || "refused_time".equals(filtersSubFields.getField()) || "bye_time".equals(filtersSubFields.getField())) {
                    op = " <= " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".999")) + " AND " + filtersSubFields.getField() + " > 0";
                } else if ("ringing_duration".equals(filtersSubFields.getField()) || "call_duration".equals(filtersSubFields.getField())) {
                    op = " <= " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData())) + " AND " + filtersSubFields.getField() + " > 0";
                } else {
                    op = " <= " + filtersSubFields.getData();
                }
            } else if ("bw".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'" + filtersSubFields.getData() + "%'";
            } else if ("ew".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "'";
            } else if ("cn".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "%'";
            }
            if (i > 0) {
                condition += " " + filters_obj.getGroupOp() + " ";
            }
            condition += " ( " + filtersSubFields.getField() + op + " ) ";

        }
        if (usr.getRole_id() > 0) {
            condition += " ) AND (origin_caller='" + usr.getUser_id() + "' OR term_ip in (" + gatewayDAO.getGatewayIPbyClient(usr.getUser_id()) + "))";
        } else {
            condition += " )";
        }
        return condition;
    }

    public HashMap<String, Object> count(String filters, Users usr) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        long successCall = 0;
        long totalCall = 0;
        long callDuration = 0;
        DBConnection dbConnection = null;
        Statement statement = null;
        String sql = "";
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            if (filters != null && filters.length() > 0) {
                sql = "select disconnect_cause, count(*), SUM(origin_bill), SUM(term_bill), SUM(call_duration) from reports " + condition(filters, usr) + " group by disconnect_cause";
            } else {
                sql = "select disconnect_cause, count(*), SUM(origin_bill), SUM(term_bill), SUM(call_duration) from reports where ( (refused_time >= " + curdate + " OR bye_time >= " + curdate + ") AND (refused_time <= " + now + " OR bye_time <= " + now + ") ) " + (usr.getRole_id() > 0 ? " AND (origin_caller='" + usr.getUser_id() + "' OR term_ip in (" + gatewayDAO.getGatewayIPbyClient(usr.getUser_id()) + "))" : "") + " group by disconnect_cause";
            }

            logger.debug("report SUM sql-->" + sql);

            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                if ("200 OK".equals(resultSet.getString("disconnect_cause"))) {
                    successCall += resultSet.getLong("count(*)");
                    callDuration += resultSet.getLong("SUM(call_duration)");
                    map.put("success", successCall);
                    map.put("sumOrgBill", resultSet.getDouble("SUM(origin_bill)"));
                    map.put("sumTermBill", resultSet.getDouble("SUM(term_bill)"));
                    map.put("sumDuration", Utils.getTimeHHMMSS(callDuration));
                    //totalCall += successCall;
                }
                totalCall += resultSet.getLong("count(*)");
                map.put("count", totalCall);

            }
            if (successCall > 0) {
                map.put("acd", Utils.getTimeHHMMSS(callDuration / successCall));
            } else {
                map.put("acd", "00:00:00");
            }
            if (totalCall > 0) {
                //double asr = (successCall / totalCall) * 100;
                double success = successCall;
                double total = totalCall;
                map.put("asr", pddformatter.format((success / total) * 100) + "%");
            } else {
                map.put("asr", "0%");
            }

        } catch (Exception e) {
            logger.fatal("Exception in getCDRCount" + e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return map;
    }

    public List<Report> find(String filters, String sort, String sort_field, int from, int to, Users usr) {

        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            cdrList = new ArrayList<Report>();
            //cdrDTOByID = new HashMap<Long, Report>();
            String sql = "";

            String limit_sql = "limit " + from + "," + (to - from);

            String order_sql = "";
            if (sort != null && sort.length() > 0 && sort_field != null && sort_field.length() > 0) {
                order_sql = " order by " + sort_field + " " + sort;
            } else {
                order_sql = " order by cdr_id desc";
            }

            if (filters != null && filters.length() > 0) {
                sql = "select * from reports " + condition(filters, usr) + " " + order_sql + " " + limit_sql;
            } else {
                sql = "select * from reports where ( (refused_time >= " + curdate + " OR bye_time >= " + curdate + ") AND (refused_time <= " + now + " OR bye_time <= " + now + ") ) " + (usr.getRole_id() > 0 ? " AND (origin_caller='" + usr.getUser_id() + "' OR term_ip in (" + gatewayDAO.getGatewayIPbyClient(usr.getUser_id()) + ")) " : " ") + order_sql + " " + limit_sql;
            }
            logger.debug("report search sql-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Report dto = new Report();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                //dto.setCdr_date(resultSet.getLong("cdr_date"));
                dto.setCall_id(resultSet.getString("call_id"));
                dto.setOrigin_caller(resultSet.getString("origin_caller"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_port(resultSet.getString("origin_port"));
                dto.setOrigin_rate(resultSet.getString("origin_rate"));
                dto.setOrigin_bill(resultSet.getDouble("origin_bill"));
                if (usr.getRole_id() > 0) {
                    dto.setTerm_callee(resultSet.getString("previous_term_callee"));
                    dto.setTerm_ip(resultSet.getString("previous_term_ip"));
                    dto.setTerm_port(resultSet.getString("previous_term_port"));
                } else {
                    dto.setTerm_callee(resultSet.getString("term_callee"));
                    dto.setTerm_ip(resultSet.getString("term_ip"));
                    dto.setTerm_port(resultSet.getString("term_port"));
                }
                dto.setTerm_rate(resultSet.getString("term_rate"));
                dto.setTerm_bill(resultSet.getDouble("term_bill"));
                dto.setInvite_time(Utils.ToDateDDMMYYYYhhmmss(resultSet.getLong("invite_time")));
                dto.setRinging_time(Utils.ToDateDDMMYYYYhhmmss(resultSet.getLong("ringing_time")));
                dto.setAccepted_time(Utils.ToDateDDMMYYYYhhmmss(resultSet.getLong("accepted_time")));
                dto.setRefused_time(Utils.ToDateDDMMYYYYhhmmss(resultSet.getLong("refused_time")));
                dto.setBye_time(Utils.ToDateDDMMYYYYhhmmss(resultSet.getLong("bye_time")));
                dto.setRinging_duration(Utils.getTimeHHMMSS(resultSet.getLong("ringing_duration")));
                dto.setCall_duration(Utils.getTimeHHMMSS(resultSet.getLong("call_duration")));
                dto.setDisconnect_cause(resultSet.getString("disconnect_cause"));
                //cdrDTOByID.put(dto.getCdr_id(), dto);
                cdrList.add(dto);
            }

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getCDRDTOList: " + e.getMessage());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return cdrList;
    }
}
