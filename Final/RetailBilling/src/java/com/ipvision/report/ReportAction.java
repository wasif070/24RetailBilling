/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.report;

import com.ipvision.user.Users;
import com.opensymphony.xwork2.ActionSupport;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author Wasif Islam
 */
public class ReportAction extends ActionSupport {

    Map<String, Object> userdata = new HashMap<String, Object>();
    private List<Report> gridModel;
    private Integer rows = 10;
    private Integer page = 1;
    private String sord;
    private String sidx;
    private String filters;
    private Integer total = 0;
    private Integer records = 0;
    private HashMap<String, Object> results = new HashMap<String, Object>();

    @Override
    public String execute() throws Exception {

        HttpServletRequest request = ServletActionContext.getRequest();
        if (request.getSession().getAttribute("userName") != null) {
            Users usr = (Users) request.getSession().getAttribute("userName");

            int to = (rows * page);
            int from = to - rows;
            ReportDAO cdrDao = new ReportDAO();
            results = cdrDao.count(getFilters(), usr);
            records = Integer.parseInt(results.containsKey("count") ? results.get("count").toString() : "0");

            userdata.put("origin_rate", "Origination Bill:");
            userdata.put("origin_bill", Double.valueOf((results.containsKey("sumOrgBill") ? results.get("sumOrgBill").toString() : "0.0")));

            userdata.put("term_rate", "Termination Bill:");
            userdata.put("term_bill", Double.valueOf((results.containsKey("sumTermBill") ? results.get("sumTermBill").toString() : "0.0")));

            userdata.put("accepted_time", (results.containsKey("asr") ? "ASR : " + results.get("asr").toString() : "ASR : 0%"));

            userdata.put("bye_time", results.containsKey("acd") ? "ACD : " + results.get("acd").toString() : "ACD : 00:00:00");

            userdata.put("ringing_duration", "Duration:");
            userdata.put("call_duration", results.containsKey("sumDuration") ? results.get("sumDuration").toString() : "00:00:00");

            setGridModel(cdrDao.find(getFilters(), getSord(), getSidx(), from, to, usr));

            total = (int) Math.ceil((double) records / (double) rows);
        }
        return SUCCESS;
    }

    public Map<String, Object> getUserdata() {
        return userdata;
    }

    public void setUserdata(Map<String, Object> userdata) {
        this.userdata = userdata;
    }

    public List<Report> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<Report> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getRecords() {
        return records;
    }

    public void setRecords(Integer records) {
        this.records = records;
    }

    public HashMap<String, Object> getResults() {
        return results;
    }

    public void setResults(HashMap<String, Object> results) {
        this.results = results;
    }
}
