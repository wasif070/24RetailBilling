/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.report;

/**
 *
 * @author Wasif Islam
 */
public class Report {

    private Long cdr_id;
    //private Long cdr_date;
    private String call_id;
    private String origin_caller;
    private String origin_ip;
    private String origin_port;
    private String origin_rate;
    private Double origin_bill;
    private String term_callee;
    private String term_ip;
    private String term_port;
    private String term_rate;
    private Double term_bill;
    private String invite_time;
    private String ringing_time;
    private String accepted_time;
    private String refused_time;
    private String bye_time;
    private String ringing_duration;
    private String call_duration;
    private String disconnect_cause;
    private String total;

    public Long getCdr_id() {
        return cdr_id;
    }

    public void setCdr_id(Long cdr_id) {
        this.cdr_id = cdr_id;
    }

    public String getCall_id() {
        return call_id;
    }

    public void setCall_id(String call_id) {
        this.call_id = call_id;
    }

    public String getOrigin_caller() {
        return origin_caller;
    }

    public void setOrigin_caller(String origin_caller) {
        this.origin_caller = origin_caller;
    }

    public String getOrigin_ip() {
        return origin_ip;
    }

    public void setOrigin_ip(String origin_ip) {
        this.origin_ip = origin_ip;
    }

    public String getOrigin_port() {
        return origin_port;
    }

    public void setOrigin_port(String origin_port) {
        this.origin_port = origin_port;
    }

    public Double getOrigin_bill() {
        return origin_bill;
    }

    public void setOrigin_bill(Double origin_bill) {
        this.origin_bill = origin_bill;
    }

    public String getTerm_callee() {
        return term_callee;
    }

    public void setTerm_callee(String term_callee) {
        this.term_callee = term_callee;
    }

    public String getTerm_ip() {
        return term_ip;
    }

    public void setTerm_ip(String term_ip) {
        this.term_ip = term_ip;
    }

    public String getTerm_port() {
        return term_port;
    }

    public void setTerm_port(String term_port) {
        this.term_port = term_port;
    }

    public Double getTerm_bill() {
        return term_bill;
    }

    public void setTerm_bill(Double term_bill) {
        this.term_bill = term_bill;
    }

    public String getInvite_time() {
        return invite_time;
    }

    public void setInvite_time(String invite_time) {
        this.invite_time = invite_time;
    }

    public String getRinging_time() {
        return ringing_time;
    }

    public void setRinging_time(String ringing_time) {
        this.ringing_time = ringing_time;
    }

    public String getAccepted_time() {
        return accepted_time;
    }

    public void setAccepted_time(String accepted_time) {
        this.accepted_time = accepted_time;
    }

    public String getRefused_time() {
        return refused_time;
    }

    public void setRefused_time(String refused_time) {
        this.refused_time = refused_time;
    }

    public String getBye_time() {
        return bye_time;
    }

    public void setBye_time(String bye_time) {
        this.bye_time = bye_time;
    }

    public String getRinging_duration() {
        return ringing_duration;
    }

    public void setRinging_duration(String ringing_duration) {
        this.ringing_duration = ringing_duration;
    }

    public String getCall_duration() {
        return call_duration;
    }

    public void setCall_duration(String call_duration) {
        this.call_duration = call_duration;
    }

    public String getDisconnect_cause() {
        return disconnect_cause;
    }

    public void setDisconnect_cause(String disconnect_cause) {
        this.disconnect_cause = disconnect_cause;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getOrigin_rate() {
        return origin_rate;
    }

    public void setOrigin_rate(String origin_rate) {
        this.origin_rate = origin_rate;
    }

    public String getTerm_rate() {
        return term_rate;
    }

    public void setTerm_rate(String term_rate) {
        this.term_rate = term_rate;
    }    
}
