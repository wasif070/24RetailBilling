/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.activecalls;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ipvision.common.Filters;
import com.ipvision.common.FiltersSubFields;
import com.ipvision.gateway.GatewayDAO;
import com.ipvision.log.DBLoggerConstant;
import com.ipvision.log.IPVLogger;
import com.ipvision.user.Users;
import databaseconnector.DBConnection;
import databaseconnector.DBConnector;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import util.Utils;

/**
 *
 * @author reefat
 */
public class ActiveCallsDAO {

    private Users users;
    private static List<LiveUsers> liveUsers;// = buildList();
    private static List<LiveUsers> liveUsers_filtered_list;
    static Logger logger = Logger.getLogger(ActiveCallsDAO.class.getName());
    static IPVLogger iPVLogger = new IPVLogger();
    GatewayDAO gatewayDAO = new GatewayDAO();
    public ActiveCallsDAO() {
//        liveUsers = buildList();
    }

    public ActiveCallsDAO(Users param_users) {
        users = param_users;
        liveUsers = buildList(users);
    }

    public int count() {
        return (liveUsers != null ? liveUsers.size() : 0);
    }

//    public int countFilteredData() {
//        return (liveUsers_filtered_list != null ? liveUsers_filtered_list.size() : 0);
//    }
    public static List<LiveUsers> buildList(Users param_users) {
        DBConnection dBConnection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

//        iPVLogger.addLogEntry("testingEntry", DBLoggerConstant.SUCCESSFUL, DBLoggerConstant.FETCH, "Successfully fetched", null, ActiveCallsDAO.class.getName());
        GatewayDAO gatewayDAO = new GatewayDAO();
        ArrayList<LiveUsers> v_temp_liveUsers_list = new ArrayList<LiveUsers>();
//        MemcachedClient memcachedClient = null;
//        Future<Object> f;// = null;        
//        HashMap<String, String> temp_hashMap_for_memcache_keys;// = new HashMap<Integer, String>();
        boolean is_admin;// = false;
        String user_id = "";

        if (param_users != null) {
            if (param_users.getRole_id() < 0) {
                is_admin = true;
            } else {
                is_admin = false;
                if (param_users.getUser_id() != null) {
                    user_id = param_users.getUser_id();
                }
            }
//            iPVLogger.addLogEntry("buildList", DBLoggerConstant.INFO, DBLoggerConstant.OPERATION, "is_admin", "" + is_admin, ActiveCallsDAO.class.getName());
//            iPVLogger.addLogEntry("buildList", DBLoggerConstant.INFO, DBLoggerConstant.OPERATION, "user_id", user_id, ActiveCallsDAO.class.getName());
            try {
                dBConnection = DBConnector.getInstance().makeConnection();
                String sql = "select * from active_calls";
                ps = dBConnection.connection.prepareStatement(sql);
                rs = ps.executeQuery();
                while (rs.next()) {
                    LiveUsers o_liveUsers = new LiveUsers(rs.getString("call_id"),
                            rs.getString("origin_caller"),
                            rs.getString("origin_ip"),
                            (is_admin ? rs.getString("term_callee") : rs.getString("previous_term_callee")),
                            (is_admin ? rs.getString("term_ip") : ""),
                            (is_admin ? rs.getString("term_gw") : ""),
                            rs.getString("invite_time"),
                            rs.getString("accepted_time"),
                            rs.getString("ringing_time"),
                            rs.getString("progressing_time"),
                            rs.getString("duration"),
                            rs.getString("status"));
//                    iPVLogger.addLogEntry("buildList", DBLoggerConstant.INFO, DBLoggerConstant.OPERATION, "Term_callee", o_liveUsers.getTerm_callee(), ActiveCallsDAO.class.getName());
                    if (!is_admin) {
                        if (user_id.equals(o_liveUsers.getOrigin_caller()) || user_id.equals(o_liveUsers.getTerm_callee())) {
                            o_liveUsers.setTerm_gw("");
                            o_liveUsers.setTerm_ip("");
                            v_temp_liveUsers_list.add(timeFormatAndDuration(o_liveUsers));
                        }
                    } else {
                        o_liveUsers.setTerm_gw(gatewayDAO.getGatewayName(o_liveUsers.getTerm_ip()));
                        v_temp_liveUsers_list.add(timeFormatAndDuration(o_liveUsers));
                    }
                }

            } catch (Exception ex) {
                iPVLogger.addLogEntry("buildList", DBLoggerConstant.ERROR, DBLoggerConstant.FETCH, "Active Calls data fetching failed", ex.toString(), ActiveCallsDAO.class.getName());
                logger.debug("Exception occured in buildList() :: " + ex);
            } finally {
                try {
                    if (dBConnection != null) {
                        DBConnector.getInstance().freeConnection(dBConnection);
                    }
                } catch (Exception e) {
                    iPVLogger.addLogEntry("buildList", DBLoggerConstant.ERROR, DBLoggerConstant.CLOSING_DB_CONNECTION, "database connection closing failed", e.toString(), ActiveCallsDAO.class.getName());
                }
                try {
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    iPVLogger.addLogEntry("buildList", DBLoggerConstant.ERROR, DBLoggerConstant.DB, "preparedStatement closing failed", e.toString(), ActiveCallsDAO.class.getName());
                }
                try {
                    if (rs != null) {
                        rs.close();
                    }
                } catch (Exception e) {
                    iPVLogger.addLogEntry("buildList", DBLoggerConstant.ERROR, DBLoggerConstant.DB, "ResultSet closing failed", e.toString(), ActiveCallsDAO.class.getName());
                }
            }
        }
        return v_temp_liveUsers_list;
    }

    private static LiveUsers timeFormatAndDuration(LiveUsers param_liveUsers) {
        try {
            long str_accepted_time_in_long = 0;
            long str_invite_time_in_long = 0;

            if (!param_liveUsers.getAccepted_time().equals("0")) {
                str_accepted_time_in_long = Long.parseLong(param_liveUsers.getAccepted_time());
                param_liveUsers.setAccepted_time(Utils.ToDateDDMMYYYYhhmmss(str_accepted_time_in_long));

                long elapsed_time = System.currentTimeMillis() - str_accepted_time_in_long;
                param_liveUsers.setDuration(Utils.getTimeHHMMSS(elapsed_time));
            }
            if (param_liveUsers.getInvite_time() != null) {
                str_invite_time_in_long = Long.parseLong(param_liveUsers.getInvite_time());
                param_liveUsers.setInvite_time(Utils.ToDateDDMMYYYYhhmmss(str_invite_time_in_long));
            }
        } catch (Exception e) {
            iPVLogger.addLogEntry("timeFormatAndDuration", DBLoggerConstant.ERROR, DBLoggerConstant.OPERATION, "Exception in Time formation and duration calculation", e.toString(), ActiveCallsDAO.class.getName());
        }

        return param_liveUsers;
    }

//    public List<LiveUsers> find(int from, int to) {
//        forceReload();
//        if (LiveUsers.getSort_order() != null) {
//            Collections.sort(liveUsers, LiveUsers.liveUsersPropertiesComparator);
//        }
//        return liveUsers.subList(from, to);
//    }
//    private void forceReload() {   
//        liveUsers = buildList();
//    }
//    public List<LiveUsers> findFilteredList(String json_data, int from, int to) {
//        forceReload();
//        ConditionChecking conditionChecking = new ConditionChecking();
//        liveUsers_filtered_list = new ArrayList<LiveUsers>();
//        FiltersSubFields filtersSubFields;
//        Filters filters_obj;
//        Gson json = new GsonBuilder().serializeNulls().create();
//        filters_obj = json.fromJson(json_data, Filters.class);
//
//        String comparison_type;
//        boolean condition_matched = true;
//        boolean return_val = false;
//
//        for (LiveUsers filtered_liveusers : liveUsers) {
//            condition_matched = true;
//            for (int i = 0; i < filters_obj.getRules().size(); i++) {
//                filtersSubFields = json.fromJson(filters_obj.getRules().get(i).toString(), FiltersSubFields.class);
//                comparison_type = filtersSubFields.getOp();
//                switch (DataStorage.getInstance().getFieldMappingLiveUsers(filtersSubFields.getField())) {
//                    case 0:
//                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_liveusers.getCall_id(), filtersSubFields.getData());
//                        break;
//                    case 1:
//                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_liveusers.getOrigin_caller(), filtersSubFields.getData());
//                        break;
//                    case 2:
//                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_liveusers.getOrigin_ip(), filtersSubFields.getData());
//                        break;
//                    case 3:
//                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_liveusers.getTerm_callee(), filtersSubFields.getData());
//                        break;
//                    case 4:
//                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_liveusers.getTerm_ip(), filtersSubFields.getData());
//                        break;
//                    case 5:
//                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_liveusers.getTerm_gw(), filtersSubFields.getData());
//                        break;
//                    case 6:
//                        return_val = conditionChecking.checkConditionTime(comparison_type, filtered_liveusers.getInvite_time(), filtersSubFields.getData());
//                        break;
//                    case 7:
//                        return_val = conditionChecking.checkConditionTime(comparison_type, filtered_liveusers.getAccepted_time(), filtersSubFields.getData());
////                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_liveusers.getAccepted_time(), filtersSubFields.getData());
//                        break;
//                    case 8:
//                        return_val = conditionChecking.checkConditionNumeric(comparison_type, Utils.getTimeLong(filtered_liveusers.getDuration() + ":00"), Utils.getTimeLong(filtersSubFields.getData() + ":00"));
//                        break;
//                    case 9:
//                        return_val = conditionChecking.checkConditionString(comparison_type, filtered_liveusers.getStatus(), filtersSubFields.getData());
//                        break;
////                    case 8:
////                        checkConditionNumeric(comparison_type, filtered_liveusers.getClient_call_limit(), Integer.parseInt(filtersSubFields.getData()));
////                        break;
//                    default:
//                        break;
//                }
//
//                if (filters_obj.getGroupOp().equalsIgnoreCase("And")) {
//                    if (return_val == false) {
//                        condition_matched = false;
//                        break;
//                    }
//                } else {
//                    if (return_val == true) {
//                        condition_matched = true;
//                        break;
//                    } else if (i == filters_obj.getRules().size() - 1) {
//                        condition_matched = false;
//                    }
//                }
//            }
//            if (condition_matched) {
//                liveUsers_filtered_list.add(filtered_liveusers);
//            }
//        }
//
//        if (LiveUsers.getSort_order() != null) {
//            Collections.sort(liveUsers_filtered_list, LiveUsers.liveUsersPropertiesComparator);
//        }
//        if (to > liveUsers_filtered_list.size()) {
//            to = liveUsers_filtered_list.size() - from;
//        }
//
//        return liveUsers_filtered_list.subList(from, to);
//    }
    public List<LiveUsers> search(String filters, String fields, String sort, String sort_field, int from, int to) {
        GatewayDAO gatewayDAO = new GatewayDAO();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        liveUsers = new ArrayList<LiveUsers>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();

            String limit_sql = "";
            String sql = "";
            if (!(to < 0)) {
                limit_sql = "limit " + from + "," + (to - from);
            }


            String order_sql = "";
            if (sort != null && sort.length() > 0 && sort_field != null && sort_field.length() > 0) {
                order_sql = "order by " + sort_field + " " + sort;
            } else {
                order_sql = "order by id desc";
            }

            if (filters != null && filters.length() > 0) {
                sql = "select " + fields + " from active_calls " + condition(filters) + " " + order_sql + " " + limit_sql;
            } else {
                sql = "select " + fields + " from active_calls " + (users.getRole_id() > 0 ? " where (origin_caller='" + users.getUser_id() + "' OR term_ip in (" + gatewayDAO.getGatewayIPbyClient(users.getUser_id()) + ")) " : " ") + order_sql + " " + limit_sql;
            }
//            iPVLogger.addLogEntry("search", DBLoggerConstant.INFO, DBLoggerConstant.SQL, "Full sql for searching", sql, ActiveCallsDAO.class.getName());
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                LiveUsers o_liveUsers = new LiveUsers(rs.getString("call_id"),
                        rs.getString("origin_caller"),
                        rs.getString("origin_ip"),
                        (users.getRole_id() < 0 ? rs.getString("term_callee") : rs.getString("previous_term_callee")),
                        (users.getRole_id() < 0 ? rs.getString("term_ip") : ""),
                        (users.getRole_id() < 0 ? rs.getString("term_gw") : ""),
                        rs.getString("invite_time"),
                        rs.getString("accepted_time"),
                        rs.getString("ringing_time"),
                        rs.getString("progressing_time"),
                        rs.getString("duration"),
                        rs.getString("status"));
                o_liveUsers.setTerm_gw(gatewayDAO.getGatewayName(o_liveUsers.getTerm_ip()));
//                v_temp_liveUsers_list.add(timeFormatAndDuration(o_liveUsers));
                liveUsers.add(timeFormatAndDuration(o_liveUsers));
            }

        } catch (Exception e) {
            logger.fatal("Exception in searchRateplansDTOList:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }

            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
        }
        return liveUsers;
    }

    private String condition(String filters) {
        String condition = "where ( ";

        Filters filters_obj;
        Gson json = new GsonBuilder().serializeNulls().create();
        filters_obj = json.fromJson(filters, Filters.class);
        FiltersSubFields filtersSubFields = null;
        for (int i = 0; i < filters_obj.getRules().size(); i++) {
            String op = "";
            filtersSubFields = json.fromJson(filters_obj.getRules().get(i).toString(), FiltersSubFields.class);

            if ("eq".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField())) {
                    op = " > " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".999"));
                } else if ("duration".equals(filtersSubFields.getField())) {
                    op = " > " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()));
                } else {
                    op = " = '" + filtersSubFields.getData() + "'";
                }
            } else if ("ne".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField())) {
                    op = " < " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".000")) + " AND " + filtersSubFields.getField() + " > 0" + " OR " + filtersSubFields.getField() + " > " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".999"));
                } else if ("duration".equals(filtersSubFields.getField())) {
                    op = " < " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()) - 1000) + " AND " + filtersSubFields.getField() + " > 0" + " OR " + filtersSubFields.getField() + " > " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()));
                } else {
                    op = " != '" + filtersSubFields.getData() + "'";
                }
            } else if ("bw".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'" + filtersSubFields.getData() + "%'";
            } else if ("ew".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "'";
            } else if ("cn".equals(filtersSubFields.getOp())) {
                op = " LIKE " + "'%" + filtersSubFields.getData() + "%'";
            } else if ("gt".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField())) {
                    op = " > " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".999"));
                } else if ("duration".equals(filtersSubFields.getField())) {
//                    op = " > " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()));
                    op = " <= " + (System.currentTimeMillis() - Utils.getTimeLong(filtersSubFields.getData()) - 1000);
                }
            } else if ("lt".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField())) {
                    op = " < " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".000")) + " AND " + filtersSubFields.getField() + " > 0";
                } else if ("duration".equals(filtersSubFields.getField())) {
//                    op = " <= " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()) - 1000) + " AND " + filtersSubFields.getField() + " > 0";
                    op = " >= " + (System.currentTimeMillis() - Utils.getTimeLong(filtersSubFields.getData()) - 1000);
                }
            } else if ("ge".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField())) {
                    op = " >= " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".000"));
                } else if ("duration".equals(filtersSubFields.getField())) {
//                    op = " > " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData()) - 1000);
                    op = " < " + (System.currentTimeMillis() - Utils.getTimeLong(filtersSubFields.getData()));
                }
            } else if ("le".equals(filtersSubFields.getOp())) {
                if ("invite_time".equals(filtersSubFields.getField()) || "accepted_time".equals(filtersSubFields.getField())) {
                    op = " <= " + String.valueOf(Utils.getDateLong(filtersSubFields.getData() + ".999")) + " AND " + filtersSubFields.getField() + " > 0";
                } else if ("duration".equals(filtersSubFields.getField())) {
//                    op = " <= " + String.valueOf(Utils.getTimeLong(filtersSubFields.getData())) + " AND " + filtersSubFields.getField() + " > 0";
                    op = " >= " + (System.currentTimeMillis() - Utils.getTimeLong(filtersSubFields.getData()));
                }
            }

            if (i > 0) {
                condition += " " + filters_obj.getGroupOp() + " ";
            }

            condition += " ( " + (filtersSubFields.getField().equals("duration") ? "accepted_time" : filtersSubFields.getField()) + op + " ) ";
//            condition += filtersSubFields.getField() + op;
        }

        if (users.getRole_id() > 0) {
            condition += " ) AND (origin_caller='" + users.getUser_id() + "' OR term_ip in (" + gatewayDAO.getGatewayIPbyClient(users.getUser_id()) + "))";
        } else {
            condition += " )";
        }

//        iPVLogger.addLogEntry("condition", DBLoggerConstant.INFO, DBLoggerConstant.SQL, "condition part " + users.getRole_id(), condition, ActiveCallsDAO.class.getName());
        return condition;
    }

    public int count(String filters) {
        DBConnection dbConnection = null;
        Statement statement = null;
        int count = 0;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "";
            if (filters != null && filters.length() > 0) {
                sql = "select count(*) from active_calls " + condition(filters);
            } else {
                sql = "select count(*) from active_calls " + (users.getRole_id() > 0 ? "where (origin_caller='" + users.getUser_id() + "' OR term_ip in (" + gatewayDAO.getGatewayIPbyClient(users.getUser_id()) + "))" : "");
            }
//            iPVLogger.addLogEntry("count", DBLoggerConstant.INFO, DBLoggerConstant.SQL, "Full sql", sql, ActiveCallsDAO.class.getName());
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                count = resultSet.getInt("count(*)");
            }
        } catch (Exception e) {
            iPVLogger.addLogEntry("count", DBLoggerConstant.ERROR, DBLoggerConstant.FETCH, "Error while counting data", e.toString(), ActiveCallsDAO.class.getName());
            logger.fatal("Exception in getCount:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return count;
    }
}
