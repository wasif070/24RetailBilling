/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.activecalls;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reefat
 */
public class LiveUsers implements Serializable, Comparable<LiveUsers> {

    private String call_id;
    private String origin_caller;
    private String origin_ip;
    private String term_callee;
    private String term_ip;
    private String term_gw;
    private String invite_time;
    private String accepted_time;
    private String ringing_time;
    private String progressing_time;
    private String duration;
    private String status;
    private static Integer field;
    private static Integer sort_order;

    public LiveUsers() {
    }

    public LiveUsers(String call_id, String origin_caller, String origin_ip, String term_callee, String term_ip, String term_gw, String invite_time, String accepted_time, String ringing_time, String progressing_time, String duration, String status) {
        this.call_id = call_id;
        this.origin_caller = origin_caller;
        this.origin_ip = origin_ip;
        this.term_callee = term_callee;
        this.term_ip = term_ip;
        this.term_gw = term_gw;
        this.invite_time = invite_time;
        this.accepted_time = accepted_time;
        this.ringing_time = ringing_time;
        this.progressing_time = progressing_time;
        this.duration = duration;
        this.status = status;
    }

    public String getCall_id() {
        return call_id;
    }

    public void setCall_id(String call_id) {
        this.call_id = call_id;
    }

    public String getOrigin_caller() {
        return origin_caller;
    }

    public void setOrigin_caller(String origin_caller) {
        this.origin_caller = origin_caller;
    }

    public String getOrigin_ip() {
        return origin_ip;
    }

    public void setOrigin_ip(String origin_ip) {
        this.origin_ip = origin_ip;
    }

    public String getTerm_callee() {
        return term_callee;
    }

    public void setTerm_callee(String term_callee) {
        this.term_callee = term_callee;
    }

    public String getTerm_ip() {
        return term_ip;
    }

    public void setTerm_ip(String term_ip) {
        this.term_ip = term_ip;
    }

    public String getTerm_gw() {
        return term_gw;
    }

    public void setTerm_gw(String term_gw) {
        this.term_gw = term_gw;
    }

    public String getInvite_time() {
        return invite_time;
    }

    public void setInvite_time(String invite_time) {
        this.invite_time = invite_time;
    }

    public String getAccepted_time() {
        return accepted_time;
    }

    public void setAccepted_time(String accepted_time) {
        this.accepted_time = accepted_time;
    }

    public String getRinging_time() {
        return ringing_time;
    }

    public void setRinging_time(String ringing_time) {
        this.ringing_time = ringing_time;
    }

    public String getProgressing_time() {
        return progressing_time;
    }

    public void setProgressing_time(String progressing_time) {
        this.progressing_time = progressing_time;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public static Integer getField() {
        return field;
    }

    public static void setField(Integer field) {
        LiveUsers.field = field;
    }

    public static Integer getSort_order() {
        return sort_order;
    }

    public static void setSort_order(Integer sort_order) {
        LiveUsers.sort_order = sort_order;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int compareTo(LiveUsers t) {
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date var1 = new Date();
        Date var2 = new Date();

        switch (field) {
            case 6:
                try {
                    var1 = ft.parse(this.invite_time);
                } catch (ParseException ex) {
                    Logger.getLogger(LiveUsers.class.getName()).log(Level.SEVERE, null, ex);
                }

                try {
                    var2 = ft.parse(t.invite_time);
                } catch (ParseException ex) {
                    Logger.getLogger(LiveUsers.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case 7:
                try {
                    var1 = ft.parse(this.accepted_time);
                } catch (ParseException ex) {
                    Logger.getLogger(LiveUsers.class.getName()).log(Level.SEVERE, null, ex);
                }

                try {
                    var2 = ft.parse(t.accepted_time);
                } catch (ParseException ex) {
                    Logger.getLogger(LiveUsers.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            default:
                break;
        }


        switch (sort_order) {
            case 0:
                //ascending order
                return var1.compareTo(var2);
            case 1:
                //descending order
                return var2.compareTo(var1);
            default:
                return -1;
        }
    }
    public static Comparator<LiveUsers> liveUsersPropertiesComparator = new Comparator<LiveUsers>() {
        @Override
        public int compare(LiveUsers liveUsers1, LiveUsers liveUsers2) {

            String var_liveUsers_sorting_field_value_1 = null;// = client1.getFirstName().toUpperCase();
            String var_liveUsers_sorting_field_value_2 = null;// = liveUsers2.getFirstName().toUpperCase();

            switch (field) {

                case 0: //Call ID
                    var_liveUsers_sorting_field_value_1 = liveUsers1.getOrigin_caller().toUpperCase();
                    var_liveUsers_sorting_field_value_2 = liveUsers2.getOrigin_caller().toUpperCase();
                    break;

                case 1: //Origin Caller
                    var_liveUsers_sorting_field_value_1 = liveUsers1.getOrigin_caller().toUpperCase();
                    var_liveUsers_sorting_field_value_2 = liveUsers2.getOrigin_caller().toUpperCase();
                    break;
                case 2: //Oigin IP
                    var_liveUsers_sorting_field_value_1 = String.valueOf(liveUsers1.getOrigin_ip());
                    var_liveUsers_sorting_field_value_2 = String.valueOf(liveUsers2.getOrigin_ip());
                    break;
                case 3: //Term Callee
                    var_liveUsers_sorting_field_value_1 = liveUsers1.getTerm_callee().toUpperCase();
                    var_liveUsers_sorting_field_value_2 = liveUsers2.getTerm_callee().toUpperCase();
                    break;
                case 4: //Term IP
                    var_liveUsers_sorting_field_value_1 = liveUsers1.getTerm_ip().toUpperCase();
                    var_liveUsers_sorting_field_value_2 = liveUsers2.getTerm_ip().toUpperCase();
                    break;
                case 5: //Term GW
                    var_liveUsers_sorting_field_value_1 = liveUsers1.getTerm_gw().toUpperCase();
                    var_liveUsers_sorting_field_value_2 = liveUsers2.getTerm_gw().toUpperCase();
                    break;
                case 6: //Invite Time
                    return liveUsers1.compareTo(liveUsers2);
                case 7: //Accepted Time
                    return liveUsers1.compareTo(liveUsers2);
                case 8: //Duration
                    var_liveUsers_sorting_field_value_1 = (liveUsers1.getDuration() != null ? liveUsers1.getDuration().toUpperCase() : "");
                    var_liveUsers_sorting_field_value_2 = (liveUsers2.getDuration() != null ? liveUsers2.getDuration().toUpperCase() : "");
                    break;
                case 9: //Status
                    var_liveUsers_sorting_field_value_1 = liveUsers1.getStatus().toUpperCase();
                    var_liveUsers_sorting_field_value_2 = liveUsers2.getStatus().toUpperCase();
                    break;
                default:
                    break;
            }

            if (var_liveUsers_sorting_field_value_1 == null || var_liveUsers_sorting_field_value_2 == null) {
                return -1;
            }

            switch (sort_order) {
                case 0:
                    //ascending order
                    return var_liveUsers_sorting_field_value_1.compareTo(var_liveUsers_sorting_field_value_2);
                case 1:
                    //descending order
                    return var_liveUsers_sorting_field_value_2.compareTo(var_liveUsers_sorting_field_value_1);
                default:
                    return -1;
            }
        }
    };
}
