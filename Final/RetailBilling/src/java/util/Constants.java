/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Wasif Islam
 */
public class Constants {

    private static String auth_domains = "";
    private static Integer auth_port = null;
    public final static String ALL_DB_FIELDS = "*";
    public final static Integer SORT_ORDER_ASCENDING = 0;
    public final static Integer SORT_ORDER_DESCENDING = 1;
    public final static String STATUS_VALUE[] = {"0", "1"};
    public final static String STATUS_STRING[] = {"Active", "Block"};
    public final static String TYPE_STRING[] = {"Origination", "Termination", "Both"};
    private static final long update_time_difference = 0;
    public final static Integer MEMCACHE_PORT_NUMBER = 11211;
    public final static String DAY_VALUE[] = {"-1", "0", "1", "2", "3", "4", "5", "6"};
    public final static String DAY_STRING[] = {"All", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    public final static String PAYMENT_VALUE[] = {"0", "1", "2"};
    public final static String PAYMENT_STRING[] = {"Recharge", "Return", "Receive"};
    public final static int ADD = 0;
    public final static int EDIT = 1;
    public final static int DELETE = 2;

    public static long getUpdate_time_difference() {
        return update_time_difference;
    }

    public static String[] getSTATUS_STRING() {
        return STATUS_STRING;
    }

    public static String[] getTYPE_STRING() {
        return TYPE_STRING;
    }

    public static String getAuth_domains() {
        return auth_domains;
    }

    public static void setAuth_domains(String auth_domains) {
        Constants.auth_domains = auth_domains;
    }

    public static Integer getAuth_port() {
        return auth_port;
    }

    public static void setAuth_port(Integer auth_port) {
        Constants.auth_port = auth_port;
    }
}
