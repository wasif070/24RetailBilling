/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 *
 * @author reefat
 */
public class Utils {

    static Logger logger = Logger.getLogger(Utils.class.getName());

    public static String getSQLString(PreparedStatement ps) {
        String psString = ps.toString();
        try {
            return psString.substring(psString.indexOf(":") + 1).trim();
        } catch (Exception ex) {
            logger.debug("getSQLStringException--> " + ex);
        }
        return psString;
    }

    public static Integer findIndex(String[] param_array, String param_target_string) {
        Integer return_val = null;
        for (int i = 0; i < param_array.length; i++) {
            if (param_array[i].equals(param_target_string)) {
                return i;
            }
        }
        return return_val;
    }

    public static boolean checkEmailId(String address) {

        if (address == null) {
            return false;
        }
        if (address.length() == 0) {
            return false;
        }
//        Pattern p = Pattern.compile("^[\\w\\-\\_\\.]+\\@[[\\w\\-\\_]+\\.]+[a-zA-Z]{2,}$");
        Pattern p = Pattern.compile("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$");
        Matcher m = p.matcher(address);

        return m.matches();
    }

    public static boolean checkValidIP(String address) {

        if (address == null) {
            return false;
        }
        if (address.length() == 0) {
            return false;
        }
//        Pattern p = Pattern.compile("^[\\w\\-\\_\\.]+\\@[[\\w\\-\\_]+\\.]+[a-zA-Z]{2,}$");
        Pattern p = Pattern.compile("\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b");
        Matcher m = p.matcher(address);

        return m.matches();
    }

    public static boolean checkValidUserId(String userId) {

        if (userId == null) {
            return false;
        }
        if (userId.length() == 0) {
            return false;
        }

        Pattern p = Pattern.compile("^[a-zA-Z0-9_.]+$");
        Matcher m = p.matcher(userId);
        boolean b = m.matches();
        if (b == false) {
            return false;
        }
        return true;
    }

    public static boolean isDouble(String number) {
        try {
            Double.parseDouble(number);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isInteger(String number) {
        try {
            Integer.parseInt(number);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

   /* public static String convertDateTime(long time) {
        if (time > 0) {
            Date date = new Date(time);
            Format format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
            return format.format(date).toString();
        } else {
            return "0";
        }
    }*/

    /*public static String convertTime(long time) {
     String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(time),
     TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)),
     TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
     return hms;
     }*/
    public static String getTimeHHMMSS(long elapsed_time) {
        String time = "";
        float seconds = elapsed_time / 1000;
        float milis = elapsed_time % 1000;
        int min = (int) seconds / 60;
        int hour = (int) min / 60;
        int sec = (int) (seconds - min * 60);
        if (milis > 0) {
            sec += 1;
        }
        min = (int) (min - hour * 60);

        if (hour < 10) {
            time += "0" + hour;
        } else {
            time += hour;
        }

        if (min < 10) {
            time += ":0" + min;
        } else {
            time += ":" + min;
        }

        if (sec < 10) {
            time += ":0" + sec;
        } else {
            time += ":" + sec;
        }

        return time;
    }

    public static long getDateLong(String str_date) {
        long longDate = 0L;
        Date date = new Date();
        try {
            java.text.DateFormat formatter;
            formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
            date = (Date) formatter.parse(str_date);
            longDate = date.getTime();

        } catch (Exception e) {
            logger.debug("Exception :" + e);
        }
        return longDate;
    }

    public static long getTimeLong(String str_time) {
        String[] temp;
        temp = str_time.split(":");
        return (((Long.parseLong(temp[0]) * 60 * 60) + (Long.parseLong(temp[1]) * 60) + Long.parseLong(temp[2])) * 1000);
    }

    public static String ToDateDDMMYYYYhhmmss(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static String ToDateDDMMYYYY0h0m0s(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy 00:00:00.000");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static double FormatBalance(double balance) {
        //3 digit balance calculation
        balance = ((double) ((long) (balance * 1000)) / 1000);
        return balance;
    }

    public static String getDateDDMMYYhhmmss(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }
}
