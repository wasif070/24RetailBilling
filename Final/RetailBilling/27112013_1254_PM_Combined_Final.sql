-- MySQL dump 10.11
--
-- Host: localhost    Database: 24retailbilling
-- ------------------------------------------------------
-- Server version	5.0.37-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL auto_increment,
  `client_id` varchar(100) NOT NULL default '',
  `rateplan_id` int(11) NOT NULL,
  `client_password` varchar(50) NOT NULL,
  `client_name` varchar(100) NOT NULL default '',
  `client_email` varchar(100) NOT NULL,
  `client_type` tinyint(2) NOT NULL default '0',
  `client_status` tinyint(1) NOT NULL default '0',
  `client_credit_limit` decimal(18,4) NOT NULL default '0.0000',
  `client_balance` decimal(18,4) NOT NULL default '0.0000',
  `client_delete` tinyint(1) NOT NULL default '0',
  `prefix` varchar(32) NOT NULL default '',
  `client_call_limit` int(11) NOT NULL default '0',
  `client_created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


--
-- Table structure for table `dialpeers`
--

DROP TABLE IF EXISTS `dialpeers`;
CREATE TABLE `dialpeers` (
  `dp_id` bigint(20) unsigned NOT NULL auto_increment COMMENT 'Dial peer id [int]',
  `dp_name` varchar(100) NOT NULL COMMENT 'Dial peer name [string]',
  `dp_description` varchar(256) default '' COMMENT 'Description [string]',
  `dp_dnis_pattern` text NOT NULL COMMENT 'DNIS pattern [list]',
  `dp_capacity` int(6) unsigned default '100' COMMENT 'Capacity [int]',
  `dp_ani_translate` text COMMENT 'ANI translation [list]',
  `dp_dnis_translate` text COMMENT 'DNIS translation [list]',
  `dp_priority` int(3) default '0' COMMENT 'Priority [int]',
  `dp_gateway_list` varchar(1000) default '' COMMENT 'Gateway list [list]',
  `dp_enable` tinyint(1) unsigned default '1' COMMENT 'Enable [bool]',
  `dp_update_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT 'Timestamp [timestamp]',
  PRIMARY KEY  (`dp_id`),
  UNIQUE KEY `dialpeer_name` (`dp_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `gateway`
--

DROP TABLE IF EXISTS `gateway`;
CREATE TABLE `gateway` (
  `id` int(11) NOT NULL auto_increment,
  `gateway_name` varchar(100) NOT NULL,
  `gateway_ip` varchar(15) NOT NULL,
  `gateway_status` tinyint(1) NOT NULL default '0',
  `gateway_type` tinyint(1) NOT NULL default '0',
  `client_id` int(11) NOT NULL,
  `gateway_delete` tinyint(1) NOT NULL default '0',
  `inserted_time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `owner_id` int(11) NOT NULL default '-1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Table structure for table `rateplans`
--

DROP TABLE IF EXISTS `rateplans`;
CREATE TABLE `rateplans` (
  `rateplan_id` int(11) NOT NULL auto_increment,
  `rateplan_name` varchar(100) NOT NULL,
  `rateplan_des` varchar(100) NOT NULL,
  `rateplan_status` tinyint(1) NOT NULL default '1',
  `rateplan_delete` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`rateplan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `rates`
--

DROP TABLE IF EXISTS `rates`;
CREATE TABLE `rates` (
  `rate_id` int(11) NOT NULL auto_increment,
  `rateplan_id` int(11) NOT NULL default '0',
  `rate_destination_code` varchar(32) NOT NULL,
  `rate_destination_name` varchar(50) NOT NULL,
  `rate_per_min` decimal(10,4) NOT NULL default '0.0000',
  `rate_first_pulse` tinyint(2) NOT NULL default '0',
  `rate_next_pulse` tinyint(2) NOT NULL default '0',
  `rate_grace_period` tinyint(2) NOT NULL default '0',
  `rate_failed_period` tinyint(2) NOT NULL default '0',
  `rate_day` tinyint(2) NOT NULL default '0',
  `rate_from_hour` tinyint(2) NOT NULL default '0',
  `rate_from_min` tinyint(2) NOT NULL default '0',
  `rate_to_hour` tinyint(2) NOT NULL default '0',
  `rate_to_min` tinyint(2) NOT NULL default '0',
  `rate_created_date` decimal(13,0) NOT NULL default '0',
  `rate_status` tinyint(1) NOT NULL default '0',
  `rate_delete` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`rate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `cdr_id` bigint(18) NOT NULL auto_increment,
  `cdr_date` decimal(13,0) NOT NULL default '0',
  `call_id` varchar(50) NOT NULL default '',
  `origin_caller` varchar(20) NOT NULL default '',
  `origin_ip` varchar(15) default NULL,
  `origin_port` varchar(5) default NULL,
  `origin_bill` decimal(10,4) NOT NULL default '0.0000',
  `term_callee` varchar(20) NOT NULL default '',
  `term_ip` varchar(15) default NULL,
  `term_port` varchar(5) default NULL,
  `term_bill` decimal(10,4) NOT NULL default '0.0000',
  `invite_time` decimal(13,0) NOT NULL default '0',
  `ringing_time` decimal(13,0) NOT NULL default '0',
  `accepted_time` decimal(13,0) NOT NULL default '0',
  `refused_time` decimal(13,0) NOT NULL default '0',
  `bye_time` decimal(13,0) NOT NULL default '0',
  `ringing_duration` int(8) NOT NULL default '0',
  `call_duration` int(8) NOT NULL default '0',
  `disconnect_cause` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`cdr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL default '0',
  `user_password` varchar(32) NOT NULL,
  `user_full_name` varchar(100) default NULL,
  `user_status` tinyint(1) NOT NULL default '1',
  `user_delete` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (7,'admin',-1,'admin24','super admin',1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-27  6:55:15
