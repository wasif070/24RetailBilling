/*
 * $Id: HelloWorld.template,v 1.2 2008-03-27 05:47:21 ub3rsold4t Exp $
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import com.ipvision.activecalls.LiveUsers;
import com.opensymphony.xwork2.ActionSupport;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.internal.OperationFuture;

/**
 * <code>Set welcome message.</code>
 */
public class HelloWorld1 extends ActionSupport {

    public String execute() throws Exception {
        setMessage(getText(MESSAGE));
        return SUCCESS;
    }
    /**
     * Provide default valuie for Message property.
     */
    public static final String MESSAGE = "HelloWorld.message";
    /**
     * Field for Message property.
     */
    private String message;

    /**
     * Return Message property.
     *
     * @return Message property
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set Message property.
     *
     * @param message Text to display on HelloWorld page.
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public static void main(String[] args) {
        MemcachedClient memcachedClient = null;
        int portNum = 11211;

        HashMap<Integer, String> MemCacheKeys = new HashMap<Integer, String>();

        MemCacheKeys.put(1, "1");
        MemCacheKeys.put(2, "2");
        MemCacheKeys.put(3, "3");

        LiveUsers liveUsers = new LiveUsers();
        liveUsers.setCall_id("001");
        liveUsers.setOrigin_caller("reefat");
        liveUsers.setOrigin_ip("192.168.1.113");
        liveUsers.setTerm_callee("Wasif");
        liveUsers.setTerm_ip("192.168.1.95");
        liveUsers.setInvite_time("1:10:10 am");
        liveUsers.setAccepted_time("2:12:12 am");

        LiveUsers liveUsers_1 = new LiveUsers();
        liveUsers_1.setCall_id("002");
        liveUsers_1.setOrigin_caller("reefat");
        liveUsers_1.setOrigin_ip("192.168.1.113");
        liveUsers_1.setTerm_callee("Wasif");
        liveUsers_1.setTerm_ip("192.168.1.95");
        liveUsers_1.setInvite_time("3:10:10 am");
        liveUsers_1.setAccepted_time("4:12:12 am");

        LiveUsers liveUsers_2 = new LiveUsers();
        liveUsers_2.setCall_id("003");
        liveUsers_2.setOrigin_caller("reefat");
        liveUsers_2.setOrigin_ip("192.168.1.113");
        liveUsers_2.setTerm_callee("Wasif");
        liveUsers_2.setTerm_ip("192.168.1.95");
        liveUsers_2.setInvite_time("5:10:10 am");
        liveUsers_2.setAccepted_time("6:12:12 am");

        try {
            memcachedClient = new MemcachedClient(new InetSocketAddress("192.168.1.113", portNum));
        } catch (IOException ex) {
        }

//-------------------------------------------------------------------------------------------
        OperationFuture<Boolean> set = memcachedClient.set("1", 3600, liveUsers);
        set = memcachedClient.set("2", 3600, liveUsers_1);
        set = memcachedClient.set("3", 3600, liveUsers_2);
        set = memcachedClient.set("MemCacheKeys", 3600, MemCacheKeys);
        
        MemCacheKeys = null;
        Future<Object> f = memcachedClient.asyncGet("MemCacheKeys");
        try {
            MemCacheKeys = (HashMap<Integer, String>) f.get(500, TimeUnit.SECONDS);
        } catch (Exception ex) {
            Logger.getLogger(HelloWorld1.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (MemCacheKeys != null) {
            System.out.println("");
        }

//        liveUsers_2 = null;
//        Future<Object> f = memcachedClient.asyncGet("3");
//        try {
//            liveUsers_2 = (LiveUsers) f.get(5, TimeUnit.SECONDS);
//            if (liveUsers_2 == null) {
//                System.err.println("Null value returned :(");
//            } else {
//                System.out.println(liveUsers_2.getCall_id());
//                System.out.println(liveUsers_2.getTerm_callee());
//            }
//
//        } catch (Exception ex) {
//            f.cancel(false);
//
//        }
        
        memcachedClient.shutdown();


    }
}
