/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test.duration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reefat
 */
public class CalTest {

    public static void main(String[] args) {
        try {
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();


            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = ft.parse("2013-12-23 10:50:00");
            Date date2 = ft.parse("2013-12-23 10:51:30");
            
            cal1.setTime(date1);
            cal2.setTime(date2);
            if(cal1.getTime().after(cal2.getTime())){
                System.out.println("asd");
            }
        } catch (ParseException ex) {
            Logger.getLogger(CalTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
