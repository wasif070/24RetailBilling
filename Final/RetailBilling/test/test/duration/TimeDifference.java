/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test.duration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Interval;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import util.Utils;

/**
 *
 * @author reefat
 */
public class TimeDifference{

    public static void main(String[] args) {
        try {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = ft.parse("2013-12-23 10:50:00");
            Date date2 = ft.parse("2013-12-23 10:51:30");
            StringBuilder sb;
            long s = System.currentTimeMillis();
            
            DateTime start = new DateTime(date1.getYear(), date1.getMonth(), date1.getDate(), date1.getHours(), date1.getMinutes(), date1.getSeconds());
            DateTime end = new DateTime(date2.getYear(), date2.getMonth(), date2.getDate(), date2.getHours(), date2.getMinutes(), date2.getSeconds());
            
            sb = new StringBuilder();
            sb.append(Days.daysBetween(start, end).getDays()).append("d ");
            sb.append(Hours.hoursBetween(start, end).getHours() % 24).append("h ");
            sb.append(Minutes.minutesBetween(start, end).getMinutes() % 60).append("m ");
            sb.append(Seconds.secondsBetween(start, end).getSeconds() % 60).append("s");
            
            System.out.println("diff1 ==> " + (System.currentTimeMillis() - s));
            System.err.println("sb1 => " + sb.toString());


            long sss = System.currentTimeMillis();
            long diff = date2.getTime() - date1.getTime();
            Utils.getTimeHHMMSS(diff);
            
            
//            long diffSeconds = diff / 1000 % 60;
//            long diffMinutes = diff / (60 * 1000) % 60;
//            long diffHours = diff / (60 * 60 * 1000) % 24;
//            long diffDays = diff / (24 * 60 * 60 * 1000);
//
//            float milis = diff % 1000;
//            if (milis > 0) {
//                diffSeconds += 1;
//            }
//
////            sb.append(diff / (24 * 60 * 60 * 1000)).append("d ");
//            sb.append((diffHours < 10 ? "0" + diffHours : diffHours)).append(":");
//            sb.append((diffMinutes < 10 ? "0" + diffMinutes : diffMinutes)).append(":");
//            sb.append((diffSeconds < 10 ? "0" + diffSeconds : diffSeconds));//.append("s");
//            
//            sb.toString();
            
            System.out.println("dif 2 ==> " + (System.currentTimeMillis() - sss));
            System.err.println("sb2 => " + Utils.getTimeHHMMSS(diff));
//
//            sss = System.currentTimeMillis();
//            sb = new StringBuilder();
//            diff = date2.getTime() - date1.getTime();
//            sb.append(Utils.getTimeHHMMSS(diff));
//            System.out.println("diff3 ==> " + (System.currentTimeMillis() - sss));
//            System.err.println("sb3 => " + sb.toString());

//            long diff = date2.getTime() - date1.getTime();
//            long d = 0, hr = 0, min = 0, sec = 0;
//            
//            long in_sec = diff / 1000;
//            long in_min = in_sec / 60;
//            long in_hr = in_min / 60;
////            long in_d = in_min / 60;
//            
//            sec = in_sec % 60;
//            min = in_min % 60;
//            hr = in_min % 24;

//            System.out.print(Days.daysBetween(start, end).getDays() + " days, ");
//            System.out.print(Hours.hoursBetween(start, end).getHours() % 24 + " hours, ");
//            System.out.print(Minutes.minutesBetween(start, end).getMinutes() % 60 + " minutes, ");
//            System.out.print(Seconds.secondsBetween(start, end).getSeconds() % 60 + " seconds.");

        } catch (ParseException ex) {
            Logger.getLogger(TimeDifference.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
//    public static String getTimeHHMMSSCustom(long diff) {        
//            StringBuilder sb = new StringBuilder();
////            long diff = date2.getTime() - date1.getTime();
//
//            long diffSeconds = diff / 1000 % 60;
//            long diffMinutes = diff / (60 * 1000) % 60;
//            long diffHours = diff / (60 * 60 * 1000) % 24;
//            long diffDays = diff / (24 * 60 * 60 * 1000);
//
//            float milis = diff % 1000;
//            if (milis > 0) {
//                diffSeconds += 1;
//            }
//
////            sb.append(diff / (24 * 60 * 60 * 1000)).append("d ");
//            sb.append((diffHours < 10 ? "0" + diffHours : diffHours)).append(":");
//            sb.append((diffMinutes < 10 ? "0" + diffMinutes : diffMinutes)).append(":");
//            sb.append((diffSeconds < 10 ? "0" + diffSeconds : diffSeconds));//.append("s");
//
//            return sb.toString();
//
//    }
}
