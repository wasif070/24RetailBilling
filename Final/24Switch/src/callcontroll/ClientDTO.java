/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package callcontroll;

/**
 *
 * @author Ashraful
 */
public class ClientDTO {

    private long id;
    private double balance;
    private String clientId;
    private String clientPassword;
    private long rateplanId;
    private int clientType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientPassword() {
        return clientPassword;
    }

    public void setClientPassword(String clientPassword) {
        this.clientPassword = clientPassword;
    }

    public long getRateplanId() {
        return rateplanId;
    }

    public void setRateplanId(long rateplanId) {
        this.rateplanId = rateplanId;
    }

    public int getClientType() {
        return clientType;
    }

    public void setClientType(int clientType) {
        this.clientType = clientType;
    }
}
