/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package callcontroll;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;
import org.zoolu.sip.utils.Utilities;

public class BalanceFunctions {

    static Logger logger = Logger.getLogger(callcontroll.ClientRepository.class.getName());

    public BalanceFunctions() {
    }

    public static BalanceFunctions getInstance() {
        return (new BalanceFunctions());

    }

    public synchronized int updateBalance(InfoDTO info) {
        DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        int result = 0;

        try {
            CallRepository.getInstance().storeACKTime(info.getCallId(), System.currentTimeMillis());
            con = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();

            TimeoutLoader.getInstance().putAndGetTimeout(info.getStrClientId(), info.getCallId(), -info.getTimeout());

            int duration = info.getTimeout();
            int f_pulse = info.getRate_dto().getRate_first_pulse();
            int n_pulse = info.getRate_dto().getRate_next_pulse();
            n_pulse = n_pulse == 0 ? 1 : n_pulse;

//            logger.debug("Initial Duration-->" + duration);

            if (CallRepository.getInstance().getFirstPulseProcessTag(info.getCallId())) {
//                logger.debug("first time calling");
                CallRepository.getInstance().storeFirstPulseProcessTag(info.getCallId(), false);
                duration = duration - info.getRate_dto().getRate_grace_period();
                if (duration <= 0) {
                    duration = 0;
                } else if (duration <= f_pulse) {
                    duration = f_pulse;
                } else {
                    int total_pulse = (int) ((duration - f_pulse) / n_pulse);
                    if ((duration - f_pulse) % n_pulse > 0) {
                        total_pulse++;
                    }
                    duration = f_pulse + total_pulse * n_pulse;
                }
            } else if (duration % n_pulse > 0) {
                int total_pulse = (int) (duration / n_pulse) + 1;
                duration = total_pulse * n_pulse;
            }

            if (duration > 0) {
                double bill = ((info.getRate_dto().getRate_per_min() / 60) * duration);
                bill = Utilities.FormatBalance(bill);
                //logger.debug("Duration-->" + duration + ",Bill-->" + bill);
                result = stmt.executeUpdate("update clients set client_balance=client_balance-" + bill + " where id=" + info.getClientId());
                CallRepository.getInstance().storeBill(info.getCallId(), CallRepository.getInstance().getBill(info.getCallId()) + bill);
                
                //System.out.println("updateBalance rate per min-->"+info.getRate_dto().getRate_per_min());
                if (CallRepository.getInstance().containsRate(info.getCallId())) {
                    if (!CallRepository.getInstance().getRate(info.getCallId()).endsWith(String.valueOf(info.getRate_dto().getRate_per_min()))) {
                        String time_frame_rate = CallRepository.getInstance().getRate(info.getCallId());
                        CallRepository.getInstance().storeRate(info.getCallId(), time_frame_rate + ", " + String.valueOf(info.getRate_dto().getRate_per_min()));
                    }
                } else {
                    CallRepository.getInstance().storeRate(info.getCallId(), String.valueOf(info.getRate_dto().getRate_per_min()));
                }
            }
        } catch (Exception ex) {
            logger.debug("Exception in balance update-->" + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                }
            }
            if (con != null) {
                try {
                    databaseconnector.DBConnector.getInstance().freeConnection(con);
                } catch (Exception ex) {
                }
            }
        }

        return result;
    }

    public synchronized int updateTermBalance(InfoDTO info) {
        DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        int result = 0;

        try {
            con = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();
            if (info.getGracePeriod() > 0) {
                info.setTimeout(info.getTimeout() - info.getGracePeriod());
            }
            TimeoutLoader.getInstance().putAndGetTimeout(info.getStrClientId(), info.getCallId(), -info.getTimeout());
            double bill = ((info.getRatePerMinute() / 60) * info.getTimeout());
            bill = Utilities.FormatBalance(bill);
            //logger.debug("Duration-->" + info.getTimeout() + ",Bill-->" + bill);
            result = stmt.executeUpdate("update clients set client_balance=client_balance-" + bill + " where id=" + info.getClientId());
            CallRepository.getInstance().storeBill(info.getCallId(), CallRepository.getInstance().getBill(info.getCallId()) + bill);
            
            //System.out.println("updateTermBalance rate per min-->"+info.getRate_dto().getRate_per_min());
            if (CallRepository.getInstance().containsRate(info.getCallId())) {
                if (!CallRepository.getInstance().getRate(info.getCallId()).endsWith(String.valueOf(info.getRate_dto().getRate_per_min()))) {
                    String time_frame_rate = CallRepository.getInstance().getRate(info.getCallId());
                    CallRepository.getInstance().storeRate(info.getCallId(), time_frame_rate + ", " + String.valueOf(info.getRate_dto().getRate_per_min()));
                }
            } else {
                CallRepository.getInstance().storeRate(info.getCallId(), String.valueOf(info.getRate_dto().getRate_per_min()));
            }
        } catch (Exception ex) {
            logger.debug("Exception in balance update-->" + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                }
            }
            if (con != null) {
                try {
                    databaseconnector.DBConnector.getInstance().freeConnection(con);
                } catch (Exception ex) {
                }
            }
        }

        return result;
    }
}
