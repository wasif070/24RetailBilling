/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package callcontroll;

import local.server.CallLogger;
import local.server.LocationService;
import org.zoolu.sip.provider.SipProvider;

/**
 *
 * @author Ashraful
 */
public class InfoDTO {

    private String callId;
    private long clientId;
    private String strClientId;
    private int timeout;
    private double ratePerMinute;
    private int gracePeriod;
    private int nextTimeout;
    private SipProvider sipProvider;
    protected int destPort;
    protected String sourceIp;
    protected int sourcePort;
    protected String destIp;
    protected String dialedNo;
    protected CallLogger call_logger;
    protected LocationService location_service;
    protected boolean firstTime;
    protected RateDTO rate_dto;
    private long cSeqNumber;
    private String fromUserTag;
    private String toUserTag;

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public double getRatePerMinute() {
        return ratePerMinute;
    }

    public void setRatePerMinute(double ratePerMinute) {
        this.ratePerMinute = ratePerMinute;
    }

    public int getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(int gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public int getNextTimeout() {
        return nextTimeout;
    }

    public void setNextTimeout(int nextTimeout) {
        this.nextTimeout = nextTimeout;
    }

    public SipProvider getSipProvider() {
        return sipProvider;
    }

    public void setSipProvider(SipProvider sipProvider) {
        this.sipProvider = sipProvider;
    }

    public int getDestPort() {
        return destPort;
    }

    public void setDestPort(int destPort) {
        this.destPort = destPort;
    }

    public String getDestIp() {
        return destIp;
    }

    public void setDestIp(String destIp) {
        this.destIp = destIp;
    }

    public String getDialedNo() {
        return dialedNo;
    }

    public void setDialedNo(String dialedNo) {
        this.dialedNo = dialedNo;
    }

    public CallLogger getCall_logger() {
        return call_logger;
    }

    public void setCall_logger(CallLogger call_logger) {
        this.call_logger = call_logger;
    }

    public LocationService getLocation_service() {
        return location_service;
    }

    public void setLocation_service(LocationService location_service) {
        this.location_service = location_service;
    }

    public boolean isFirstTime() {
        return firstTime;
    }

    public void setFirstTime(boolean firstTime) {
        this.firstTime = firstTime;
    }

    public RateDTO getRate_dto() {
        return rate_dto;
    }

    public void setRate_dto(RateDTO rate_dto) {
        this.rate_dto = rate_dto;
    }

    public String getStrClientId() {
        return strClientId;
    }

    public void setStrClientId(String strClientId) {
        this.strClientId = strClientId;
    }

    public long getcSeqNumber() {
        return cSeqNumber;
    }

    public void setcSeqNumber(long cSeqNumber) {
        this.cSeqNumber = cSeqNumber;
    }

    public String getFromUserTag() {
        return fromUserTag;
    }

    public void setFromUserTag(String fromUserTag) {
        this.fromUserTag = fromUserTag;
    }

    public String getToUserTag() {
        return toUserTag;
    }

    public void setToUserTag(String toUserTag) {
        this.toUserTag = toUserTag;
    }

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public int getSourcePort() {
        return sourcePort;
    }

    public void setSourcePort(int sourcePort) {
        this.sourcePort = sourcePort;
    }

    @Override
    public String toString() {
        return "InfoDTO{" + "callId=" + callId + ", clientId=" + clientId + ", strClientId=" + strClientId + ", timeout=" + timeout + ", ratePerMinute=" + ratePerMinute + ", gracePeriod=" + gracePeriod + ", nextTimeout=" + nextTimeout + ", sipProvider=" + sipProvider + ", destPort=" + destPort + ", sourceIp=" + sourceIp + ", sourcePort=" + sourcePort + ", destIp=" + destIp + ", dialedNo=" + dialedNo + ", call_logger=" + call_logger + ", location_service=" + location_service + ", firstTime=" + firstTime + ", rate_dto=" + rate_dto + ", cSeqNumber=" + cSeqNumber + ", fromUserTag=" + fromUserTag + ", toUserTag=" + toUserTag + '}';
    }
}
