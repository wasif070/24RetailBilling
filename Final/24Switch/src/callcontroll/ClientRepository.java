/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package callcontroll;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.zoolu.sip.utils.PermissionDTO;
import org.zoolu.sip.utils.TimePart;
import org.zoolu.sip.utils.Utilities;

/**
 *
 * @author Rajib
 */
public class ClientRepository {

    static Logger logger = Logger.getLogger(ClientRepository.class.getName());
    static ClientRepository clientInfo = null;
    private static HashMap<String, ClientDTO> repository = new HashMap<String, ClientDTO>();
    private static long LOADING_INTERVAL = 0;
    private long loadingTime = 0;

    public ClientRepository() {
        checkForReload();
    }

    public static ClientRepository getInstance() {
        if (clientInfo == null) {
            createInfo();
        }
        return clientInfo;
    }

    private synchronized static void createInfo() {
        clientInfo = new ClientRepository();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public void reload() {
        repository = new HashMap<String, ClientDTO>();

        DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();
            rs = stmt.executeQuery("select id,rateplan_id,client_id,client_password,client_balance,client_type from clients where client_delete=0 and client_status=0");
            while (rs.next()) {
                ClientDTO dto = new ClientDTO();
                dto.setId(rs.getLong("id"));
                dto.setClientId(rs.getString("client_id"));
                dto.setBalance(rs.getDouble("client_balance"));
                dto.setClientPassword(rs.getString("client_password"));
                dto.setRateplanId(rs.getLong("rateplan_id"));
                dto.setClientType(rs.getInt("client_type"));
                repository.put(dto.getClientId(), dto);
            }
        } catch (Exception ex) {
            logger.debug("Exception in client repository-->" + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                }
            }
            if (con != null) {
                try {
                    databaseconnector.DBConnector.getInstance().freeConnection(con);
                } catch (Exception ex) {
                }
            }

        }
    }

    public synchronized PermissionDTO checkPermition(String client_id, String dialed_no) {
        PermissionDTO permDTO = new PermissionDTO();
        logger.debug("Inside check permition-->" + client_id + "-->" + dialed_no);
        checkForReload();

        DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        if (repository.containsKey(client_id)) {
//            logger.debug("repository.containsKey(client_id)");
            ClientDTO dto = repository.get(client_id);
            if (dto.getClientType() == 0 || dto.getClientType() == 2) {//only origin or both client is allowed to call
                TimePart time = Utilities.getTimePart(System.currentTimeMillis());

                try {
                    con = databaseconnector.DBConnector.getInstance().makeConnection();
                    stmt = con.connection.createStatement();
                    int minutes = Integer.parseInt(time.getHour()) * 60 + Integer.parseInt(time.getMinute());

                    String rate_sql = "select rate_destination_code,rate_per_min,rate_first_pulse,rate_next_pulse,rate_grace_period from rates,rateplans where rates.rate_delete=0 and rateplans.rateplan_delete=0 and rateplans.rateplan_status=0 and rates.rate_status=0 and rates.rateplan_id=rateplans.rateplan_id ";
                    rate_sql += " and INSTR(rate_destination_code,'" + dialed_no.charAt(0) + "')=1 and rates.rateplan_id=" + dto.getRateplanId();
                    rate_sql += " and (rate_from_hour*60+rate_from_min)<=" + minutes + " and " + minutes + "<=(rate_to_hour*60+rate_to_min) ";
                    rate_sql += " and (rate_day=-1 or rate_day=" + time.getWeek_day() + ") order by rate_id ASC ";

                    //logger.debug("Client Repo Rate SQL-->" + rate_sql);

                    rs = stmt.executeQuery(rate_sql);
                    int prefix_length = 0;
                    String dialing_prefix = "";
                    RateDTO r_dto = new RateDTO();
                    while (rs.next()) {
                        String dest_code = rs.getString("rate_destination_code");
                        if (dest_code.length() >= prefix_length) {
                            if (dialed_no.startsWith(dest_code)) {
                                prefix_length = dest_code.length();
                                r_dto.setRate_first_pulse(rs.getInt("rate_first_pulse"));
                                r_dto.setRate_next_pulse(rs.getInt("rate_next_pulse"));
                                r_dto.setRate_per_min(rs.getDouble("rate_per_min"));
                                dialing_prefix = dest_code;
                            }
                        }
                    }

                    double balance = dto.getBalance();

                    //logger.debug("Balance-->" + balance);
                    //System.out.println("r_dto.getRate_per_min()-->" + r_dto.getRate_per_min());
                    double rate_per_second = 0;
                    rate_per_second = r_dto.getRate_per_min() / 60;
                    int n_pulse = r_dto.getRate_next_pulse() == 0 ? 1 : r_dto.getRate_next_pulse();
                    balance -= rate_per_second * ((r_dto.getRate_first_pulse() == 0 ? n_pulse : r_dto.getRate_first_pulse()) + r_dto.getRate_grace_period());
                    balance = Utilities.FormatBalance(balance);

                   // System.out.println("Balance after process-->" + balance);
                    if (r_dto.getRate_per_min() > 0) {
                        //logger.debug("Balance after process-->" + balance);
                        double minute = balance / r_dto.getRate_per_min();
                       // System.out.println("minute-->" + (long) minute);
                        CallRepository.getInstance().storeMinute(client_id, (long) minute);
                    }

                    if (prefix_length == 0) {
                        permDTO.setErrorCode(403);
                    } else if (balance <= 0) {
                        permDTO.setErrorCode(402);
                    } else {
                        permDTO.setErrorCode(0);
                    }

                    permDTO.setBalance(balance);
                    permDTO.setRate_per_second(rate_per_second);
                    permDTO.setPrefix(dialing_prefix);

                    logger.debug("balance --> " +  balance);
                    logger.debug("rate_per_second --> " +  rate_per_second);
                    logger.debug("Dialing prefix-->" + dialing_prefix);


                } catch (Exception ex) {
                    logger.debug("Exception during permission check-->" + ex);
                } finally {
                    if (rs != null) {
                        try {
                            rs.close();
                        } catch (Exception ex) {
                        }
                    }
                    if (stmt != null) {
                        try {
                            stmt.close();
                        } catch (Exception ex) {
                        }
                    }
                    if (con != null) {
                        try {
                            databaseconnector.DBConnector.getInstance().freeConnection(con);
                        } catch (Exception ex) {
                        }
                    }

                }
            }
        }

        return permDTO;
    }

    public synchronized RateDTO getRateDTO(String client_id, String dialed_no) {
        checkForReload();

        DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        RateDTO r_dto = null;

        if (repository.containsKey(client_id)) {
            
            ClientDTO dto = repository.get(client_id);
            TimePart time = Utilities.getTimePart(System.currentTimeMillis());

            try {
                con = databaseconnector.DBConnector.getInstance().makeConnection();
                stmt = con.connection.createStatement();
                int minutes = Integer.parseInt(time.getHour()) * 60 + Integer.parseInt(time.getMinute());

                String rate_sql = "select rate_destination_code,rate_per_min,rate_first_pulse,rate_next_pulse,rate_grace_period from rates,rateplans where rateplans.rateplan_delete=0 and rates.rate_delete=0 and rateplans.rateplan_status=0 and rates.rate_status=0 and rates.rateplan_id=rateplans.rateplan_id ";
                rate_sql += " and INSTR(rate_destination_code,'" + dialed_no.charAt(0) + "')=1 and rates.rateplan_id=" + dto.getRateplanId();
                rate_sql += " and (rate_from_hour*60+rate_from_min)<=" + minutes + " and " + minutes + "<=(rate_to_hour*60+rate_to_min) ";
                rate_sql += " and (rate_day=-1 or rate_day=" + time.getWeek_day() + ") order by rate_id ASC ";

//                logger.debug("rate_sql --> \n" + rate_sql);
                
                rs = stmt.executeQuery(rate_sql);
                int prefix_length = 0;
                while (rs.next()) {
                    String dest_code = rs.getString("rate_destination_code");
                    if (dest_code.length() >= prefix_length) {
                        if (dialed_no.startsWith(dest_code)) {
                            r_dto = new RateDTO();
                            prefix_length = dest_code.length();
                            r_dto.setDialing_prefix(dest_code);
                            r_dto.setRate_first_pulse(rs.getInt("rate_first_pulse"));
                            r_dto.setRate_next_pulse(rs.getInt("rate_next_pulse"));
                            r_dto.setRate_per_min(rs.getDouble("rate_per_min"));
                            r_dto.setRate_grace_period(rs.getInt("rate_grace_period"));
                        }
                    }
                }

            } catch (Exception ex) {
                logger.debug("Exception during permission check-->" + ex);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (Exception ex) {
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (Exception ex) {
                    }
                }
                if (con != null) {
                    try {
                        databaseconnector.DBConnector.getInstance().freeConnection(con);
                    } catch (Exception ex) {
                    }
                }

            }
        }
        return r_dto;
    }

    public synchronized boolean contains(String key) {
        checkForReload();
        return repository.containsKey(key);
    }

    public synchronized ClientDTO get(String key) {
        checkForReload();
        return repository.get(key);
    }

    public synchronized void remove(String key) {
        checkForReload();
        repository.remove(key);
    }

    public synchronized int size() {
        checkForReload();
        return repository.size();
    }
}
