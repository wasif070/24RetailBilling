/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package callcontroll;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import local.server.AuthenticationService;
import org.apache.log4j.Logger;

/**
 *
 * @author Rajib
 */
public class AuthRepository {

    static Logger logger = Logger.getLogger(AuthRepository.class.getName());
    static AuthRepository authRepo = null;
    private static long LOADING_INTERVAL = 30 * 60 * 1000;
    private long loadingTime = 0;
    private static AuthenticationService auth_service = null;

    public AuthRepository() {
        checkForReload();
    }

    public static AuthRepository getInstance(AuthenticationService authentication_service) {
        if (authRepo == null) {
            auth_service = authentication_service;
            createInfo();
        }
        return authRepo;
    }

    public static AuthRepository getInstance() {
        return authRepo;
    }

    private synchronized static void createInfo() {
        authRepo = new AuthRepository();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public void reload() {
        auth_service.removeAllUsers();

        DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();
//            rs = stmt.executeQuery("select client_id,client_password from clients");
            rs = stmt.executeQuery("select client_id,client_password from clients where client_delete=0 and client_status=0");
            while (rs.next()) {
                auth_service.addUser(rs.getString("client_id"), rs.getString("client_password").getBytes());
            }
        } catch (Exception ex) {
            logger.debug("Exception in auth repository-->" + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                }
            }
            if (con != null) {
                try {
                    databaseconnector.DBConnector.getInstance().freeConnection(con);
                } catch (Exception ex) {
                }
            }

        }
    }

    public synchronized void add(String key, String value) {
        checkForReload();
        logger.debug("ADD");
        auth_service.addUser(key, value.getBytes());
    }

    public synchronized void edit(String prev_key, String new_key, String new_value) {
        checkForReload();
        logger.debug("EDIT");
        if (auth_service.hasUser(prev_key)) {
            auth_service.removeUser(prev_key);
            auth_service.addUser(new_key, new_value.getBytes());
        }
    }

    public synchronized boolean contains(String key) {
        checkForReload();
        return auth_service.hasUser(key);
    }

    public synchronized void remove(String key) {
        checkForReload();
        logger.debug("REMOVE");
        if (auth_service.hasUser(key)) {
            auth_service.removeUser(key);
        }
    }

    public synchronized int size() {
        checkForReload();
        return auth_service.size();
    }

    public synchronized void setAuthService(AuthenticationService authentication_service) {
        checkForReload();
        auth_service = authentication_service;
    }
}
