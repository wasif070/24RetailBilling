package callcontroll;

import java.util.Timer;
import java.util.TimerTask;

public class Reminder extends TimerTask {

    private Timer timer;
    private TimerListener listener;
    private InfoDTO info;

    public Reminder(TimerListener p_listener,InfoDTO info_dto) {
        timer = new Timer();
        listener = p_listener;
        info = info_dto;
        System.err.println("Timeout in seconds-->" + info_dto.getTimeout());
        timer.schedule(this, info_dto.getTimeout() * 1000);
    }

    public void run() {
        listener.onTimeout(info);
        timer.cancel(); //Terminate the timer thread
    }
}