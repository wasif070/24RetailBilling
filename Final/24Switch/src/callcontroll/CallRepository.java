/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package callcontroll;

import org.zoolu.net.UdpPacket;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.zoolu.sdp.SessionDescriptor;
import org.zoolu.sip.address.SipURL;

/**
 *
 * @author Rajib
 */
public class CallRepository {

    static Logger logger = Logger.getLogger(CallRepository.class.getName());
    static CallRepository callRepository = null;
    private static Map<String, ByeInfo> byeMap = new HashMap<String, ByeInfo>();
    private static Map<String, InfoDTO> infoMap = new HashMap<String, InfoDTO>();
    private static Map<String, UdpPacket> ackPacketMap = new HashMap<String, UdpPacket>();
    private static Map<String, Long> ackTimeMap = new HashMap<String, Long>();
    private static Map<String, Long> byeTimeMap = new HashMap<String, Long>();
    private static Map<String, Long> minuteMap = new HashMap<String, Long>();
    private static Map<String, Double> billMap = new HashMap<String, Double>();
    private static Map<String, SipURL> toHeaderMap = new HashMap<String, SipURL>();
    private static Map<String, Boolean> firstPulseProcessedMap = new HashMap<String, Boolean>();
    private static Map<String, Integer> callType = new HashMap<String, Integer>();
    private static Map<String, SessionDescriptor> sdpMap = new HashMap<String, SessionDescriptor>();
    private static Map<String, String> rateMap = new HashMap<String, String>();
    public static int TERMINATION_CALL = 1;
    public static int PIN2PIN_CALL = 2;

    public CallRepository() {
        byeMap = new HashMap<String, ByeInfo>();
        ackPacketMap = new HashMap<String, UdpPacket>();
        ackTimeMap = new HashMap<String, Long>();
        byeTimeMap = new HashMap<String, Long>();
        infoMap = new HashMap<String, InfoDTO>();
        minuteMap = new HashMap<String, Long>();
        billMap = new HashMap<String, Double>();
        toHeaderMap = new HashMap<String, SipURL>();
        firstPulseProcessedMap = new HashMap<String, Boolean>();
        callType = new HashMap<String, Integer>();
        sdpMap = new HashMap<String, SessionDescriptor>();
        rateMap = new HashMap<String, String>();
    }

    public static CallRepository getInstance() {
        if (callRepository == null) {
            createRepository();
        }
        return callRepository;
    }

    private synchronized static void createRepository() {
        if (callRepository == null) {
            callRepository = new CallRepository();
        }
    }

    public synchronized void storeByeInfo(String key, ByeInfo value) {
        byeMap.put(key, value);
    }

    public synchronized boolean containsByeInfo(String key) {
        return byeMap.containsKey(key);
    }

    public synchronized ByeInfo getByeInfo(String key) {
        return byeMap.get(key);
    }

    public synchronized void removeByeInfo(String key) {
        if (containsByeInfo(key)) {
            byeMap.remove(key);
        }
    }

    public synchronized int sizeOfByeInfo() {
        return byeMap.size();
    }

    public synchronized void storeACK(String key, UdpPacket value) {
        ackPacketMap.put(key, value);
    }

    public synchronized boolean containsACK(String key) {
        return ackPacketMap.containsKey(key);
    }

    public synchronized UdpPacket getACK(String key) {
        return ackPacketMap.get(key);
    }

    public synchronized void removeACK(String key) {
        if (containsACK(key)) {
            ackPacketMap.remove(key);
        }
    }

    public synchronized int sizeOfACK() {
        return ackPacketMap.size();
    }

    public synchronized void storeACKTime(String key, long value) {
        ackTimeMap.put(key, value);
    }

    public synchronized boolean containsACKTime(String key) {
        return ackTimeMap.containsKey(key);
    }

    public synchronized long getACKTime(String key) {
        return ackTimeMap.get(key);
    }

    public synchronized void removeACKTime(String key) {
        if (containsACKTime(key)) {
            ackTimeMap.remove(key);
        }

    }

    public synchronized int sizeOfACKTime() {
        return ackTimeMap.size();
    }

    public synchronized void storeByeTime(String key, long value) {
        byeTimeMap.put(key, value);
    }

    public synchronized boolean containsByeTime(String key) {
        return byeTimeMap.containsKey(key);
    }

    public synchronized long getByeTime(String key) {
        return byeTimeMap.get(key);
    }

    public synchronized void removeByeTime(String key) {
        if (containsByeTime(key)) {
            byeTimeMap.remove(key);
        }

    }

    public synchronized int sizeOfByeTime() {
        return byeTimeMap.size();
    }

    public synchronized void storeInfoDTO(String key, InfoDTO value) {
        infoMap.put(key, value);
    }

    public synchronized boolean containsInfoDTO(String key) {
        return infoMap.containsKey(key);
    }

    public synchronized InfoDTO getInfoDTO(String key) {
        return infoMap.get(key);
    }

    public synchronized void removeInfoDTO(String key) {
        if (containsInfoDTO(key)) {
            infoMap.remove(key);
        }
    }

    public synchronized int sizeOfInfoDTO() {
        return infoMap.size();
    }

    public synchronized void storeMinute(String key, long value) {
        minuteMap.put(key, value);
    }

    public synchronized boolean containsMinute(String key) {
        return minuteMap.containsKey(key);
    }

    public synchronized long getMinute(String key) {
        if (containsMinute(key)) {
            return minuteMap.get(key);
        }
        return 0;
    }

    public synchronized void removeMinute(String key) {
        if (containsMinute(key)) {
            minuteMap.remove(key);
        }
    }

    public synchronized int sizeOfMinute() {
        return minuteMap.size();
    }

    public synchronized void storeBill(String key, double value) {
        billMap.put(key, value);
    }

    public synchronized boolean containsBill(String key) {
        return billMap.containsKey(key);
    }

    public synchronized double getBill(String key) {
        if (containsBill(key)) {
            return billMap.get(key);
        }
        return 0;
    }

    public synchronized void removeBill(String key) {
        if (containsBill(key)) {
            billMap.remove(key);
        }
    }

    public synchronized int sizeOfBill() {
        return billMap.size();
    }

    public synchronized void storeToHeader(String key, SipURL value) {
        toHeaderMap.put(key, value);
    }

    public synchronized boolean containsToHeader(String key) {
        return toHeaderMap.containsKey(key);
    }

    public synchronized SipURL getToHeader(String key) {
        return toHeaderMap.get(key);

    }

    public synchronized void removeToHeader(String key) {
        if (containsToHeader(key)) {
            toHeaderMap.remove(key);
        }
    }

    public synchronized int sizeOfToHeader() {
        return toHeaderMap.size();
    }

    public synchronized void storeFirstPulseProcessTag(String key, boolean value) {
        firstPulseProcessedMap.put(key, value);
    }

    public synchronized boolean containsFirstPulseProcessTag(String key) {
        return firstPulseProcessedMap.containsKey(key);
    }

    public synchronized boolean getFirstPulseProcessTag(String key) {
        return firstPulseProcessedMap.get(key);

    }

    public synchronized void removeFirstPulseProcessTag(String key) {
        if (containsFirstPulseProcessTag(key)) {
            firstPulseProcessedMap.remove(key);
        }
    }

    public synchronized int sizeOfFirstPulseProcessTag() {
        return firstPulseProcessedMap.size();
    }

    public synchronized void storeCallType(String key, int value) {
        callType.put(key, value);
    }

    public synchronized boolean containsCallType(String key) {
        return callType.containsKey(key);
    }

    public synchronized int getCallType(String key) {
        if (callType.containsKey(key)) {
            return callType.get(key);
        }
        return 0;

    }

    public synchronized void removeCallType(String key) {
        if (containsCallType(key)) {
            callType.remove(key);
        }
    }

    public synchronized int sizeOfCallType() {
        return callType.size();
    }

    public synchronized void storeSDP(String key, SessionDescriptor value) {
        sdpMap.put(key, value);
    }

    public synchronized boolean containsSDP(String key) {
        return sdpMap.containsKey(key);
    }

    public synchronized SessionDescriptor getSDP(String key) {
        return sdpMap.get(key);

    }

    public synchronized void removeSDP(String key) {
        if (containsSDP(key)) {
            sdpMap.remove(key);
        }
    }

    public synchronized int sizeOfSDP() {
        return sdpMap.size();
    }

    public synchronized void storeRate(String key, String value) {
        rateMap.put(key, value);
    }

    public synchronized boolean containsRate(String key) {
        return rateMap.containsKey(key);
    }

    public synchronized String getRate(String key) {
        return rateMap.get(key);

    }

    public synchronized void removeRate(String key) {
        if (containsSDP(key)) {
            rateMap.remove(key);
        }
    }

    public synchronized int sizeOfRate() {
        return rateMap.size();
    }
}
