/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package callcontroll;

/**
 *
 * @author Ashraful
 */
public class RateDTO {

    private double rate_per_min;
    private int rate_first_pulse;
    private int rate_next_pulse;
    private int rate_grace_period;
    private String dialing_prefix;

    public double getRate_per_min() {
        return rate_per_min;
    }

    public void setRate_per_min(double rate_per_min) {
        this.rate_per_min = rate_per_min;
    }

    public int getRate_first_pulse() {
        return rate_first_pulse;
    }

    public void setRate_first_pulse(int rate_first_pulse) {
        this.rate_first_pulse = rate_first_pulse;
    }

    public int getRate_next_pulse() {
        return rate_next_pulse;
    }

    public void setRate_next_pulse(int rate_next_pulse) {
        this.rate_next_pulse = rate_next_pulse;
    }

    public int getRate_grace_period() {
        return rate_grace_period;
    }

    public void setRate_grace_period(int rate_grace_period) {
        this.rate_grace_period = rate_grace_period;
    }

    public String getDialing_prefix() {
        return dialing_prefix;
    }

    public void setDialing_prefix(String dialing_prefix) {
        this.dialing_prefix = dialing_prefix;
    }
}
