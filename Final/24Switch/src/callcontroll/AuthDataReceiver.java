/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package callcontroll;

import java.net.*;


import java.io.*;
import org.apache.log4j.Logger;

public class AuthDataReceiver extends Thread {

    static Logger logger = Logger.getLogger(AuthDataReceiver.class.getName());
    int s_port = 0;

    public AuthDataReceiver(int port) {
        this.s_port = port;
    }

    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(s_port);
            while (true) {
                try {
                    Socket socket = serverSocket.accept();
                    socket.setSoTimeout(500);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String line = reader.readLine();

//                    logger.debug("msg from auth --> " + line);
                    if (line.length() > 0) {
                        String[] auth_info = line.split(",");

                        if (line.startsWith("add")) {
                            AuthRepository.getInstance().add(auth_info[1].trim(), auth_info[2].trim());
//                            logger.debug("add (msg frm auth) --> " + auth_info[1].trim() + " -- " + auth_info[2].trim());
                        }
                        if (line.startsWith("edit")) {
                            AuthRepository.getInstance().edit(auth_info[3].trim(), auth_info[1].trim(), auth_info[2].trim());
//                            logger.debug("edit (msg frm auth) --> " + auth_info[3].trim() + " -- " + auth_info[1].trim() + " -- " + auth_info[2].trim());
                        }
                        if (line.startsWith("delete")) {
                            AuthRepository.getInstance().remove(auth_info[1].trim());
//                            logger.debug("delete (msg frm auth) --> " + auth_info[1].trim());
                        }
                    }
                } catch (IOException ex) {
                    try {
                        this.sleep(500);
                    } catch (InterruptedException iEx) {
                    }
                    logger.debug("Error in auth data receive: " + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            logger.debug("Error in receive: " + ex.getMessage());
        }
    }
}
