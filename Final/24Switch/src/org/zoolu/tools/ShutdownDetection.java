package org.zoolu.tools;

import callcontroll.CallRepository;
import callcontroll.InfoDTO;
import java.io.File;
import java.util.Date;
import local.server.CallLoggerImpl;
import org.apache.log4j.Logger;
import org.zoolu.sip.message.BaseMessageFactory;

public class ShutdownDetection extends Thread {

    private boolean running = false;
    private static long shutdownDetectionInterval = 1000;
    private static File file = new File("shutdown.dat");
    static Logger logger = Logger.getLogger(ShutdownDetection.class.getName());

    public ShutdownDetection() {
        running = true;
        if (file.exists()) {
            try {
                file.delete();
            } catch (Exception e) {
                try {
                    Thread.sleep(shutdownDetectionInterval);
                    file.delete();
                } catch (Exception ex) {
                }
            }
        }
    }

    @Override
    public void run() {
        while (running) {
            if (file.exists()) {
                try {
                    file.delete();
                } catch (Exception e) {
                }
                break;
            }
            try {
                Thread.sleep(shutdownDetectionInterval);
            } catch (Exception e) {
            }
        }

        for (Object callId : CallLoggerImpl.callers.keySet()) {
            String callId_str = callId.toString();
            if (CallRepository.getInstance().containsInfoDTO(callId_str)) {
                InfoDTO info = CallRepository.getInstance().getInfoDTO(callId_str);
                int bye = 1;
                BaseMessageFactory.sendForceBye(info,bye);
            } else {
                logger.debug("ShutDownDetection ::  infodto nai (for call_id) --> " + callId_str);
            }
        }


        CallLoggerImpl.forceStop = true;

        while (CallLoggerImpl.forceStop) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                logger.debug("Exception in 24Switch shutdown thread: " + ex.getMessage());
            }
        }
        logger.debug("24Switch has been stopped at: " + new Date());
        System.exit(0);
    }
}
