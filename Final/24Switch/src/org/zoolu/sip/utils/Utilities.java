/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zoolu.sip.utils;

import java.text.SimpleDateFormat;
import java.util.HashMap;

/**
 *
 * @author Ashraful
 */
public class Utilities {

    public static double FormatBalance(double balance) {
        //4 digit balance calculation
        balance = ((double) ((long) (balance * 10000)) / 10000);
        return balance;
    }

    public static TimePart getTimePart(long time) {
        TimePart parts = new TimePart();
        HashMap<String, Integer> weekMap = new HashMap<String, Integer>();
        weekMap.put("Sun", 0);
        weekMap.put("Mon", 1);
        weekMap.put("Tue", 2);
        weekMap.put("Wed", 3);
        weekMap.put("Thu", 4);
        weekMap.put("Fri", 5);
        weekMap.put("Sat", 6);
        
        SimpleDateFormat formatter = new SimpleDateFormat("EEE");       
        parts.setWeek_day(weekMap.get(formatter.format(new java.util.Date(time))));
        formatter = new SimpleDateFormat("HH");
        parts.setHour(formatter.format(new java.util.Date(time)));
        formatter = new SimpleDateFormat("mm");
        parts.setMinute(formatter.format(new java.util.Date(time)));

        return parts;
    }

    
}
