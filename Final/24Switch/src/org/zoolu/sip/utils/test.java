/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zoolu.sip.utils;

import java.io.File;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

/**
 *
 * @author Ashraful
 */
public class test {

    public static void main(String[] args) {
        System.out.println(getDurationOfWavInSeconds(new File("F:\\24RunningApps\\mjsip_1.7\\resources\\media\\local\\ua\\ring.wav")));
    }

    public static double getDurationOfWavInSeconds(File file) {
        AudioInputStream stream = null;

        try {
            stream = AudioSystem.getAudioInputStream(file);
            AudioFormat format = stream.getFormat();

            return file.length() / format.getSampleRate() / (format.getSampleSizeInBits() / 8.0) / format.getChannels();
        } catch (Exception e) {
            // log an error
            return -1;
        } finally {
            try {
                stream.close();
            } catch (Exception ex) {
            }
        }
    }
}
