/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package org.zoolu.sip.dialog;

import org.zoolu.sip.address.NameAddress;
import org.zoolu.sip.address.SipURL;
import static org.zoolu.sip.dialog.InviteDialog.D_INIT;
import org.zoolu.sip.header.RecordRouteHeader;
import org.zoolu.sip.transaction.*;
import org.zoolu.sip.message.*;
import org.zoolu.sip.provider.*;
import org.zoolu.tools.Log;

/**
 * Class InviteDialog can be used to manage invite dialogs. An InviteDialog can
 * be both client or server. (i.e. generating an INVITE request or responding to
 * an incoming INVITE request).
 * <p>
 * An InviteDialog can be in state inviting/waiting/invited, accepted/refused,
 * call, byed/byeing, and close.
 * <p>
 * InviteDialog supports the offer/answer model for the sip body, with the
 * following rules:
 * <br> - both INVITE-offer/2xx-answer and 2xx-offer/ACK-answer modes for
 * incoming calls
 * <br> - INVITE-offer/2xx-answer mode for outgoing calls.
 */
public class ExtendedDialog extends Dialog {

    public ExtendedDialog(SipProvider provider) {
        super(provider);
    }

    @Override
    public void onReceivedMessage(SipProvider sip_provider, Message msg) {
        printLog("inside onReceivedMessage(sip_provider,message)", Log.LEVEL_MEDIUM);
        // if request
        if (msg.isRequest()) {  // check CSeq
            if (!(msg.isAck() || msg.isCancel()) && msg.getCSeqHeader().getSequenceNumber() <= getRemoteCSeq()) {
                printLog("Request message is too late (CSeq too small): Message discarded", Log.LEVEL_HIGH);
                return;
            }
            // else
            // if invite
            if (msg.isInvite()) {
                verifyStatus(statusIs(D_INIT) || statusIs(D_CALL));
                // NOTE: if the invite_ts.listen() is used, you should not arrive here with the D_INIT state..
                //   however state D_INIT has been included for robustness against further changes.
                if (statusIs(D_INIT)) {
                    changeStatus(D_INVITED);
                } else {
                    changeStatus(D_ReINVITED);
                }
                // FORCE THIS NODE IN THE DIALOG ROUTE
                if (SipStack.on_dialog_route) {
                    SipURL url = new SipURL(sip_provider.getViaAddress(), sip_provider.getPort());
                    url.addLr();
                    msg.addRecordRouteHeader(new RecordRouteHeader(new NameAddress(url)));
                }
                invite_req = msg;
            } else // if ack (for 2xx)
            if (msg.isAck()) {
                if (!verifyStatus(statusIs(D_ACCEPTED) || statusIs(D_ReACCEPTED))) {
                    return;
                }
                changeStatus(D_CALL);
            } else // if bye 
            if (msg.isBye()) {
                if (!verifyStatus(statusIs(D_CALL) || statusIs(D_BYEING))) {
                    return;
                }
                changeStatus(D_BYED);
            } else // if cancel
            if (msg.isCancel()) {
                if (!verifyStatus(statusIs(D_INVITED) || statusIs(D_ReINVITED))) {
                    return;
                }
            } else // if any other request
            if (msg.isRequest()) {
                TransactionServer ts = new TransactionServer(sip_provider, msg, null);
                ts.respondWith(MessageFactory.createResponse(msg, 405, null, null));
            }
        } else // if response
        if (msg.isResponse()) {
            if (!verifyStatus(statusIs(D_CALL))) {
                return;
            }
            int code = msg.getStatusLine().getCode();
            verifyThat(code >= 200 && code < 300, "code 2xx was expected");
            // keep sending ACK (if already sent) for any "200 OK" received
            if (ack_req != null) {
                AckTransactionClient ack_tc = new AckTransactionClient(sip_provider, ack_req, null);
                ack_tc.request();
            }
        }
    }
    /**
     * The last invite message
     */
    Message invite_req;
    /**
     * The last ack message
     */
    Message ack_req;
    /**
     * The InviteTransactionClient.
     */
    InviteTransactionClient invite_tc;
    /**
     * The InviteTransactionServer.
     */
    InviteTransactionServer invite_ts;
    /**
     * The AckTransactionServer.
     */
    AckTransactionServer ack_ts;
    /**
     * The BYE TransactionServer.
     */
    TransactionServer bye_ts;
    /**
     * The InviteDialog listener
     */
    InviteDialogListener listener;
    /**
     * Whether offer/answer are in INVITE/200_OK
     */
    boolean invite_offer;
    protected static final int D_INIT = 0;
    protected static final int D_WAITING = 1;
    protected static final int D_INVITING = 2;
    protected static final int D_INVITED = 3;
    protected static final int D_REFUSED = 4;
    protected static final int D_ACCEPTED = 5;
    protected static final int D_CALL = 6;
    protected static final int D_ReWAITING = 11;
    protected static final int D_ReINVITING = 12;
    protected static final int D_ReINVITED = 13;
    protected static final int D_ReREFUSED = 14;
    protected static final int D_ReACCEPTED = 15;
    protected static final int D_BYEING = 7;
    protected static final int D_BYED = 8;
    protected static final int D_CLOSE = 9;

    /**
     * Gets the dialog state
     */
    protected String getStatus() {
        switch (status) {
            case D_INIT:
                return "D_INIT";
            case D_WAITING:
                return "D_WAITING";
            case D_INVITING:
                return "D_INVITING";
            case D_INVITED:
                return "D_INVITED";
            case D_REFUSED:
                return "D_REFUSED";
            case D_ACCEPTED:
                return "D_ACCEPTED";
            case D_CALL:
                return "D_CALL";
            case D_ReWAITING:
                return "D_ReWAITING";
            case D_ReINVITING:
                return "D_ReINVITING";
            case D_ReINVITED:
                return "D_ReINVITED";
            case D_ReREFUSED:
                return "D_ReREFUSED";
            case D_ReACCEPTED:
                return "D_ReACCEPTED";
            case D_BYEING:
                return "D_BYEING";
            case D_BYED:
                return "D_BYED";
            case D_CLOSE:
                return "D_CLOSE";
            default:
                return null;
        }
    }

    // ************************** Public methods **************************
    /**
     * Whether the dialog is in "early" state.
     */
    public boolean isEarly() {
        return status < D_ACCEPTED;
    }

    /**
     * Whether the dialog is in "confirmed" state.
     */
    public boolean isConfirmed() {
        return status >= D_ACCEPTED && status < D_CLOSE;
    }

    /**
     * Whether the dialog is in "terminated" state.
     */
    public boolean isTerminated() {
        return status == D_CLOSE;
    }

    /**
     * Whether the session is "active".
     */
    public boolean isSessionActive() {
        return (status == D_CALL);
    }

    /**
     * Gets the invite message
     */
    public Message getInviteMessage() {
        return invite_req;
    }

    /**
     * Termiante the call. This method should be called when the InviteDialog is
     * in D_CALL state
     * <p>
     * Increments the Cseq, moves to state D_BYEING, and creates new BYE
     * TransactionClient
     */
    public Message bye() {
        printLog("inside bye()", Log.LEVEL_MEDIUM);
        if (statusIs(D_CALL)) {
            return MessageFactory.createByeRequest(this);
        }
        return new Message();
    }
}
