/*
 * Copyright (C) 2006 Luca Veltri - University of Parma - Italy
 * 
 * This source code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package local.ua;

import java.util.ArrayList;

/**
 * Simple SIP UAS (User Agent) that can be executed with a GUI (it runs a GraphicalUA) or with a command-line interface (it runs a CommandLineUA).
 * <p/>
 * Class UAS allows the user to set several configuration parameters directly from comman-line before starting the proper UAS (GraphicalUA or CommandLineUA).
 */
public class UAS {

    /**
     * UserAgentProfile
     */
    public static UserAgentProfile ua_profile = null;
    /*-----------AUTO START PROGRAM------------------*/

    public UAS() {
        init();
    }

    /**
     * Parses command-line options and inits the SIP stack, a SIP provider and an UAS profile.
     */
    public void init() {
        // init SipStack
        ArrayList<String> configurations = new ArrayList<String>();
        configurations.add("transport_protocols=udp");
        configurations.add("audio=yes");
        configurations.add("video=no");
        configurations.add("bin_rat=rat");
        configurations.add("bin_vic=vic");
        configurations.add("media=audio 3000 RTP/AVP");
        configurations.add("media_spec=audio 18 G729 8000 160");
        configurations.add("media=video 3002 RTP/AVP");
        configurations.add("media_spec=video 101");
        configurations.add("debug_level=0");
        configurations.add("log_path=.");
        configurations.add("max_logsize=2000");
        configurations.add("media_path=resources" + System.getProperty("file.separator"));
        configurations.add("regc_min_attempt_timeout=30000");
        configurations.add("regc_max_attempt_timeout=60000");
        configurations.add("send_file=F:\\24RunningApps\\24Switch\\resources\\media\\local\\tones\\alfred_good_evening.wav");
        
        // init ua_profile
        ua_profile = new UserAgentProfile(configurations);
    }
}
