/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package local.sbc;

import java.io.*;
import java.net.*;

public class MessageMangler {

    public static void sendMsgToMediaGateway(String msg, String ip, int port) {
        try {
            DatagramSocket socket = new DatagramSocket();
            byte sendData[] = new byte[msg.length()];
            sendData = msg.getBytes();
            socket.send(new DatagramPacket(sendData, sendData.length, InetAddress.getByName(ip), port));
            socket.close();
        } catch (IOException ex) {
            System.out.println("Error in sending msg to MG-->" + ex.getMessage());
        }
    }
}
