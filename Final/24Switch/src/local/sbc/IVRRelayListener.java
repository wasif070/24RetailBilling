package local.sbc;


import org.zoolu.net.*;


/** Listener for SymmetricUdpRelay events.
  */
public interface IVRRelayListener
{
   /** When left peer address changes. */
   public void onIVRSymmetricUdpRelayLeftPeerChanged(IVRRelay symm_relay, SocketAddress soaddr);

   /** When it stops relaying UDP datagrams (both directions). */
   public void onIVRSymmetricUdpRelayTerminated(IVRRelay symm_relay);   
}