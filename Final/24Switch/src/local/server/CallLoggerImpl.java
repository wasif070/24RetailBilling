package local.server;

import callcontroll.CallRepository;
import com.ipvision.activecalls.ActiveCall;
import com.ipvision.activecalls.ActiveCallDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.zoolu.sip.header.StatusLine;
import org.zoolu.sip.message.Message;
import org.zoolu.sip.message.SipMethods;
import org.zoolu.tools.Log;
import org.zoolu.tools.DateFormat;

import java.util.Hashtable;
import java.util.Vector;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import local.sbc.AddressResolver;
import org.apache.log4j.Logger;
import org.zoolu.sip.address.NameAddress;
import org.zoolu.sip.address.SipURL;
import static org.zoolu.sip.message.BaseMessageFactory.createRequest;
import org.zoolu.sip.message.BaseSipResponses;
import org.zoolu.sip.provider.SipStack;

/**
 * CallLoggerImpl implements a simple CallLogger.
 * <p> A CallLogger keeps trace of all processed calls.
 */
public class CallLoggerImpl extends Thread implements CallLogger {

    static Logger logger = Logger.getLogger(CallLoggerImpl.class.getName());
//    static ActiveCall iPVLogger = new ActiveCall();
    /**
     * Maximum number of concurrent calls.
     */
    static final int MAX_SIZE = 10000;
    /**
     * Table : (String)call_id --> (Long)invite date.
     */
    Hashtable invite_dates;
    Hashtable call_progress_dates;
    /**
     * Table : (String)call_id --> (Long)2xx date.
     */
    Hashtable ringing_dates;
    Hashtable accepted_dates;
    /**
     * Table : (String)call_id --> (Long)4xx date.
     */
    Hashtable refused_dates;
    /**
     * Table : (String)call_id --> (Long)bye date.
     */
    static public Hashtable bye_dates;
    /**
     * Table : (String)call_id --> (String)caller.
     */
    public static Hashtable callers;
    /**
     * Table : (String)call_id --> (String)callee.
     */
    Hashtable callees;
    public static Hashtable term_callees;
    /**
     * Set : (String)call_id.
     */
    Vector calls;
    /**
     * Log.
     */
    Log call_log;
    static public Hashtable reasons;
    HashMap<String, Boolean> isAddedToDB;
    public static boolean forceStop = false;

    /**
     * Costructs a new CallLoggerImpl.
     */
    public CallLoggerImpl(String filename) {
        isAddedToDB = new HashMap<String, Boolean>();
        invite_dates = new Hashtable();
        call_progress_dates = new Hashtable();
        ringing_dates = new Hashtable();
        accepted_dates = new Hashtable();
        refused_dates = new Hashtable();
        bye_dates = new Hashtable();
        calls = new Vector();
        callers = new Hashtable();
        callees = new Hashtable();
        term_callees = new Hashtable();
        reasons = new Hashtable();

        call_log = new Log(filename, 1, -1, true);
        //call_log.println("Date \tCall-Id \tStatus \tCaller \tCallee \tSetup Time \tCall Time");
        call_log.println("CALL ID, CALLER, CALLEE, REASON, INVITE TIME,RINGING TIME, ACCEPTED TIME, REFUSED TIME, BYE TIME, RINGING DURATION, CALL DURATION");
        start();
    }

    @Override
    public void run() {
        boolean noMoreCallIds = true;
        while (!forceStop || noMoreCallIds) {
            try {
                if (forceStop) {
                    Thread.sleep(1000);
                    if (cdr.Repository.getInstance().getCdrQueue().isEmpty()) {
                        noMoreCallIds = false;
                    }
                }
                while (!cdr.Repository.getInstance().getCdrQueue().isEmpty()) {
                    String callId = cdr.Repository.getInstance().getCdrQueue().poll();

                    if (callId != null && callers.containsKey(callId) && callees.containsKey(callId) && reasons.containsKey(callId) && (refused_dates.containsKey(callId) || bye_dates.containsKey(callId))) {
                        updateCallLog(callId);
                        isAddedToDB.remove(callId);
                        ActiveCall.getInstance().getqRemovalList().add(callId);
                    }
                    if (callId != null && invite_dates.containsKey(callId) && (ringing_dates.containsKey(callId) || call_progress_dates.containsKey(callId) || accepted_dates.containsKey(callId))) {
                        if (!isAddedToDB.containsKey(callId)) {
                            addEntryToDB(callId);
                            isAddedToDB.put(callId, true);
                        } else {
                            if (accepted_dates.containsKey(callId) && isAddedToDB.get(callId)) {
                                addEntryToDB(callId);
                                isAddedToDB.put(callId, false);
                            }
                        }
                    }
                }

                Long current_time = System.currentTimeMillis();
                for (Object o : invite_dates.keySet()) {
                    String v_key = String.valueOf(o);
//                    if (isAddedToDB.containsKey(v_key) && isAddedToDB.get(v_key) && !accepted_dates.containsKey(v_key)) {
                    if (!accepted_dates.containsKey(v_key)) {
                        Long invite_time = (Long) invite_dates.get(o);
                        if ((current_time - invite_time) > 60000) {
                            try {
                                String call_id = String.valueOf(o);
                                reasons.put(call_id, "494  " + BaseSipResponses.reasonOf(494));
                                insert(refused_dates, call_id, System.currentTimeMillis());
                                cdr.Repository.getInstance().getCdrQueue().add(call_id);
//                                try {
////                                    SipURL sipUrl = new SipURL(callers.get(call_id).toString().split("@")[0].split(":")[1], "192.168.1.113", 5060);
////                                    SipURL sipUrlFrom = new SipURL(callees.get(call_id).toString().split("@")[0].split(":")[1], "192.168.1.113", 5060);
////                                    Message forceCancel = createRequest("CANCEL", sipUrl, new NameAddress(sipUrl), new NameAddress(sipUrlFrom), null, "UDP", "192.168.1.113", 5060, true, call_id, 123, null, null, null, null);
//
////                                    logger.debug("Force Cancel Message --> \n" + forceCancel.toString());
//                                } catch (Exception e) {
//                                    logger.debug("Error while making forceCancel message --> " + e);
//                                }
                            } catch (Exception e) {
                                logger.debug("Exception while force removing DEAD entries --> " + e);
                            }
                        }
                    }
                }
                Thread.sleep(500);
            } catch (Exception ex) {
                logger.debug("Exepction in Call Logger Thread-->" + ex.getMessage());
            }
        }
        forceStop = false;
    }

    /**
     * Updates log with the present message.
     */
    public void update(Message msg, LocationService location_service) {

        String method = msg.getCSeqHeader().getMethod();
        String call_id = msg.getCallIdHeader().getCallId();
        cdr.Repository.getInstance().getCdrQueue().add(call_id);
        //System.out.println("msg--->" + msg.toString());
        if (method.equalsIgnoreCase(SipMethods.INVITE)) {

            if (!callers.containsKey(call_id)) {
                String caller_from_header = msg.getFromHeader().getNameAddress().getAddress().toString();
                try {
                    String[] caller = caller_from_header.split(":");
                    for (Enumeration e = location_service.getUserContactURLs(caller[1]); e.hasMoreElements();) {
                        caller[0] = (String) e.nextElement();
                    }
                    ///callers.put(call_id, caller[0]); /-----Calleer Local IP
                    String[] org_local = caller[0].split("@");
                    String org_public = AddressResolver.binding_table.get(org_local[1].toString()).toString();
                    callers.put(call_id, org_local[0] + "@" + org_public); //-----Caller Public IP
                } catch (Exception e) {
                    callers.put(call_id, caller_from_header);
                }
            }
            String callee_to_header = msg.getToHeader().getNameAddress().getAddress().toString();
            try {
                String[] term_local = callee_to_header.split("@");
                String term_public = AddressResolver.binding_table.get(term_local[1].toString()).toString();

                if (!term_callees.containsKey(call_id)) {
                    term_callees.put(call_id, term_local[0] + "@" + term_public); //---Callee Public IP
                }
                callees.put(call_id, term_local[0] + "@" + term_public); //---Callee Public IP
            } catch (Exception e) {
                if (!term_callees.containsKey(call_id)) {
                    term_callees.put(call_id, callee_to_header); //-----Callee Local IP
                }
                callees.put(call_id, callee_to_header); //-----Callee Local IP
            }


            if (msg.isRequest()) {
                if (!invite_dates.containsKey(call_id)) {
                    long time = System.currentTimeMillis();
                    insert(invite_dates, call_id, time);
                }
            } else {

                StatusLine status_line = msg.getStatusLine();
                int code = status_line.getCode();
                if (code == 180) {
                    long time = System.currentTimeMillis();
                    insert(ringing_dates, call_id, time);

                } else if (code == 183) {
                    long time = System.currentTimeMillis();
                    insert(call_progress_dates, call_id, time);
                } else if (code >= 200 && code < 300 && !accepted_dates.containsKey(call_id)) {
                    long time = System.currentTimeMillis();
                    insert(accepted_dates, call_id, time);
                    String reason = status_line.getReason();
                    reasons.put(call_id, String.valueOf(code) + " " + reason);
                } else if (code >= 300 && !refused_dates.containsKey(call_id)) {
                    long time = System.currentTimeMillis();
                    insert(refused_dates, call_id, time);
                    String reason = status_line.getReason();
                    reasons.put(call_id, String.valueOf(code) + " " + reason);
                }
            }
        } else if (method.equalsIgnoreCase(SipMethods.BYE)) {
            if (msg.isRequest()) {
                if (!bye_dates.containsKey(call_id)) {
                    long time = System.currentTimeMillis();
                    insert(bye_dates, call_id, time);
                }
            }
        }
    }

    /**
     * Insters/updates a call-state table.
     */
    public void insert(Hashtable table, String call_id, long time) {
        if (!invite_dates.containsKey(call_id) && !accepted_dates.containsKey(call_id) && !refused_dates.containsKey(call_id) && !bye_dates.containsKey(call_id));
        {
            if (calls.size() >= MAX_SIZE) {
                String call_0 = (String) calls.elementAt(0);
                invite_dates.remove(call_0);
                ringing_dates.remove(call_0);
                call_progress_dates.remove(call_0);
                accepted_dates.remove(call_0);
                refused_dates.remove(call_0);
                bye_dates.remove(call_0);
                callers.remove(call_0);
                callees.remove(call_0);
                term_callees.remove(call_0);
                calls.removeElementAt(0);
                reasons.remove(call_0);
            }
            calls.addElement(call_id);
        }
        table.put(call_id, time);
    }

    /**
     * Prints a generic event log.
     */
    private void eventlog(Date time, String call_id, String event, String caller, String callee) {  //call_log.println(DateFormat.formatHHMMSS(time)+"\t"+call_id+"\t"+event+"\t"+caller+"\t"+callee);
        call_log.println(DateFormat.formatYYYYMMDD(time) + "\t" + call_id + "\t" + event + "\t" + caller + "\t" + callee);
    }

    /**
     * Prints a call report.
     */
    private void calllog(String call_id) {
        Date invite_time = (Date) invite_dates.get(call_id);
        Date accepted_time = (Date) accepted_dates.get(call_id);
        Date bye_time = (Date) bye_dates.get(call_id);
        if (invite_time != null && accepted_time != null && bye_time != null) //call_log.println(DateFormat.formatHHMMSS(invite_time)+"\t"+call_id+"\tCALL \t"+callers.get(call_id)+"\t"+callees.get(call_id)+"\t"+(accepted_time.getTime()-invite_time.getTime())+"\t"+(bye_time.getTime()-accepted_time.getTime()));
        {
            call_log.println(DateFormat.formatYYYYMMDD(invite_time) + "\t" + call_id + "\tCALL \t" + callers.get(call_id) + "\t" + callees.get(call_id) + "\t" + (accepted_time.getTime() - invite_time.getTime()) + "\t" + (bye_time.getTime() - accepted_time.getTime()));
        }
    }

    private void addEntryToDB(String call_id) {

        ActiveCallDTO activeCallDTO = new ActiveCallDTO();
        try {
            activeCallDTO.setInvite_time(Long.parseLong(invite_dates.get(call_id).toString()));

            if (callers.containsKey(call_id)) {
                activeCallDTO.setCall_id(call_id);
                String caller = callers.get(call_id).toString();
                if (caller.contains(":") && caller.contains("@")) {
                    String[] org_temp = caller.split(":");
                    String[] org_temp_ip = org_temp[1].split("@");
                    activeCallDTO.setOrigin_caller(org_temp_ip[0]);
                    activeCallDTO.setOrigin_ip(org_temp_ip[1]);
                } else {
                    logger.debug("Exception in addEntryToDB callers");
                    activeCallDTO.setOrigin_caller("'Caller' NOT FOUND");
                    activeCallDTO.setOrigin_ip("'Caller' IP NOT FOUND");
                }

            }
            if (callees.containsKey(call_id)) {
                String callee = callees.get(call_id).toString();
                if (callee.contains(":") && callee.contains("@")) {
                    String[] term_temp = callee.split(":");
                    String[] term_temp_ip = term_temp[1].split("@");
                    activeCallDTO.setTerm_callee(term_temp_ip[0]);
                    activeCallDTO.setTerm_ip(term_temp_ip[1]);
                } else {
                    logger.debug("Exception in addEntryToDB callees");
                    activeCallDTO.setTerm_callee("'Callee' NOT FOUND");
                    activeCallDTO.setTerm_ip("'Callee' IP NOT FOUND");
                }
            }
            if (term_callees.containsKey(call_id)) {
                String callee = term_callees.get(call_id).toString();
                if (callee.contains(":") && callee.contains("@")) {
                    String[] term_temp = callee.split(":");
                    String[] term_temp_ip = term_temp[1].split("@");
                    activeCallDTO.setPrevious_term_callee(term_temp_ip[0]);
                    activeCallDTO.setPrevious_term_ip(term_temp_ip[1]);
                } else {
                    logger.debug("Exception in addEntryToDB term_callees");
                    activeCallDTO.setPrevious_term_callee("'Callee' NOT FOUND");
                    activeCallDTO.setPrevious_term_ip("'Callee' IP NOT FOUND");
                }
            }
            if (ringing_dates.containsKey(call_id)) {
                activeCallDTO.setRinging_time(Long.parseLong(ringing_dates.get(call_id).toString()));
                activeCallDTO.setStatus("RINGING");
            }
            if (call_progress_dates.containsKey(call_id)) {
                activeCallDTO.setProgressing_time(Long.parseLong(call_progress_dates.get(call_id).toString()));
                activeCallDTO.setStatus("CALL IN PROGRESS");
            }
            if (accepted_dates.containsKey(call_id)) {
                activeCallDTO.setAccepted_time(Long.parseLong(accepted_dates.get(call_id).toString()));
                activeCallDTO.setStatus("CONNECTED");
            }

            ActiveCall.getInstance().getqActiveCall().add(activeCallDTO);

        } catch (Exception ex) {
            String s = "";
            try {
                s = "call-id : " + activeCallDTO.getCall_id() + ", origin_caller : " + activeCallDTO.getOrigin_caller() + ", origin_ip : " + activeCallDTO.getOrigin_ip() + ", term_callee : " + activeCallDTO.getTerm_callee() + ", term_ip : " + activeCallDTO.getTerm_ip() + ", invite-time : " + activeCallDTO.getInvite_time() + ", progression_time : " + activeCallDTO.getProgressing_time() + ", ringing-time : " + activeCallDTO.getRinging_time() + ", accepted-time : " + activeCallDTO.getAccepted_time();
                logger.debug("Exception during addEntryToDB s-->" + s);
            } catch (Exception e) {
            }
            logger.debug("Exception during addEntryToDB-->" + ex.getMessage());
        }
    }

    private void updateCallLog(String call_id) {
        long ring_duration = 0;
        long call_duration = 0;
        long invite_time = 0;
        long ringing_time = 0;
        long accepted_time = 0;
        long refused_time = 0;
        long bye_time = 0;

        if (invite_dates.containsKey(call_id)) {
            invite_time = (Long) invite_dates.get(call_id);
        }
        if (ringing_dates.containsKey(call_id)) {
            ringing_time = (Long) ringing_dates.get(call_id);
        }

        if (accepted_dates.containsKey(call_id)) {
            accepted_time = (Long) accepted_dates.get(call_id);
        }

        if (refused_dates.containsKey(call_id)) {
            refused_time = (Long) refused_dates.get(call_id);
        }

        if (bye_dates.containsKey(call_id)) {
            bye_time = (Long) bye_dates.get(call_id);
        }

        /*if (invite_time != 0 && ringing_time != 0 && accepted_time != 0 && bye_time != 0) {
         ring_duration = accepted_time - ringing_time;
         call_duration = bye_time - accepted_time;
         }*/
        if (ringing_time > 0 && (accepted_time > 0 || refused_time > 0)) {
            if (accepted_time > 0) {
                ring_duration = accepted_time - ringing_time;
            } else {
                ring_duration = refused_time - ringing_time;
            }
        }
        if (accepted_time > 0 && bye_time > 0) {
            call_duration = bye_time - accepted_time;
        }
        //call
        if (SipStack.debug_level == 1 || SipStack.debug_level == 3) {
            // call_log.println(call_id + "," + callers.get(call_id) + "," + callees.get(call_id) + "," + reasons.get(call_id) + "," + ((invite_time == null) ? "" : DateFormat.formatYYYYMMDD(invite_time)) + "," + ((ringing_time == null) ? "" : DateFormat.formatYYYYMMDD(ringing_time)) + "," + ((accepted_time == null) ? "" : DateFormat.formatYYYYMMDD(accepted_time)) + "," + ((refused_time == null) ? "" : DateFormat.formatYYYYMMDD(refused_time)) + "," + ((bye_time == null) ? "" : DateFormat.formatYYYYMMDD(bye_time)) + "," + ring_duration + "," + call_duration);
            call_log.println(call_id + "," + callers.get(call_id) + "," + callees.get(call_id) + "," + reasons.get(call_id) + "," + invite_time + "," + ringing_time + "," + accepted_time + "," + refused_time + "," + bye_time + "," + ring_duration + "," + call_duration);
        }
        if (SipStack.debug_level == 2 || SipStack.debug_level == 3) {

            String origin_caller = "";
            String origin_ip = "";
            String origin_port = "";
            if (callers.containsKey(call_id)) {
                String caller = callers.get(call_id).toString();
                String[] org_temp = caller.split(":");
                String[] org_temp_ip = org_temp[1].split("@");
                origin_caller = org_temp_ip[0];
                origin_ip = org_temp_ip[1];
                origin_port = org_temp[2];
            }
            String term_callee = "";
            String term_ip = "";
            String term_port = "";
            if (callees.containsKey(call_id)) {
                String callee = callees.get(call_id).toString();
                String[] term_temp = callee.split(":");
                String[] term_temp_ip = term_temp[1].split("@");
                term_callee = term_temp_ip[0];
                term_ip = term_temp_ip[1];
                term_port = term_temp[2];
            }
            String previous_term_callee = "";
            String previous_term_ip = "";
            String previous_term_port = "";
            if (term_callees.containsKey(call_id)) {
                String callee = term_callees.get(call_id).toString();
                String[] term_temp = callee.split(":");
                String[] term_temp_ip = term_temp[1].split("@");
                previous_term_callee = term_temp_ip[0];
                previous_term_ip = term_temp_ip[1];
                previous_term_port = term_temp[2];
            }



            try {
                if ((ServerEngine.dbConnection.connection != null && ServerEngine.dbConnection.connection.isClosed()) || ServerEngine.dbConnection.connection == null) {
                    ServerEngine.dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                }
            } catch (Exception ex) {
                logger.debug("Exception during opening DBconnection CallLoggerImpl-->" + ex.getMessage());
            }

            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                String sql = "select cdr_id from reports where call_id = ?";
                ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
                ps.setString(1, call_id);
                rs = ps.executeQuery();

                if (rs.next()) {
                    sql = "update reports set call_id=?, origin_caller=?, origin_ip=?, origin_port=?, "
                            + "term_callee=?, term_ip=?, term_port=?, invite_time=?, ringing_time=?, "
                            + "accepted_time=?, refused_time=?, bye_time=?, ringing_duration=?, "
                            + "call_duration=?, disconnect_cause=?, origin_bill=?, term_bill=?, origin_rate=?, term_rate=?, "
                            + "previous_term_callee=?, previous_term_ip=?, previous_term_port=? "
                            + "where cdr_id=" + rs.getLong("cdr_id");
                    ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
                } else {
                    sql = "insert into reports(call_id, origin_caller, origin_ip, origin_port, "
                            + "term_callee, term_ip, term_port, invite_time, ringing_time, accepted_time, "
                            + "refused_time, bye_time, ringing_duration, call_duration, disconnect_cause, origin_bill, term_bill, origin_rate, term_rate, "
                            + "previous_term_callee, previous_term_ip, previous_term_port)";
                    sql += " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
                }
                ps.setString(1, call_id);
                ps.setString(2, origin_caller);
                ps.setString(3, origin_ip);
                ps.setString(4, origin_port);
                ps.setString(5, term_callee);
                ps.setString(6, term_ip);
                ps.setString(7, term_port);
                ps.setLong(8, invite_time);
                ps.setLong(9, ringing_time);
                ps.setLong(10, accepted_time);
                ps.setLong(11, refused_time);
                ps.setLong(12, bye_time);
                ps.setLong(13, ring_duration);
                ps.setLong(14, call_duration);
                ps.setString(15, reasons.get(call_id).toString());
                ps.setDouble(16, CallRepository.getInstance().getBill(call_id));
                CallRepository.getInstance().removeBill(call_id);
                ps.setDouble(17, 0.0);
                ps.setString(18, CallRepository.getInstance().getRate(call_id));
                CallRepository.getInstance().removeRate(call_id);
                ps.setString(19, "0.0");
                ps.setString(20, previous_term_callee);
                ps.setString(21, previous_term_ip);
                ps.setString(22, previous_term_port);
                ps.executeUpdate();


            } catch (Exception ex) {
                logger.debug("Exepction in CDR insertion in database SQL-->" + ps.toString());
                logger.debug("Exepction in CDR insertion in database-->" + ex.getMessage());
            } finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }
                    if (rs != null) {
                        rs.close();
                    }
                } catch (Exception e) {
                    logger.debug("Exepction in CDR insertion in finally block database-->" + e.getMessage());
                }
            }

        }

        invite_dates.remove(call_id);
        ringing_dates.remove(call_id);
        call_progress_dates.remove(call_id);
        accepted_dates.remove(call_id);
        refused_dates.remove(call_id);
        bye_dates.remove(call_id);
        callers.remove(call_id);
        callees.remove(call_id);
        term_callees.remove(call_id);
        reasons.remove(call_id);
        calls.removeElement(call_id);

    }
}