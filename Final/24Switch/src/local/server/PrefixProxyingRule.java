/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This source code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package local.server;

import org.zoolu.sip.address.SipURL;
import org.zoolu.net.SocketAddress;

/**
 * PrefixProxyingRule is a ProxyingRule based on the prefix of URLs.
 */
public class PrefixProxyingRule implements ProxyingRule {

    /**
     * Prefix for the default rule.
     */
    public static final String DEFAULT_PREFIX = "default";
    /**
     * Matching prefix.
     */
    String prefix;
    String new_prefix;
    String old_prefix;
    int priority;
    /**
     * Next-hop server.
     */
    SocketAddress nexthop;

    /**
     * Creates a new PrefixProxyingRule.
     */
    public PrefixProxyingRule(String prefix, SocketAddress nexthop) {
        this.prefix = prefix;
        this.nexthop = nexthop;
    }

    public PrefixProxyingRule(String prefix, String old_prefix, String new_prefix, SocketAddress nexthop, int priority) {
        this.prefix = prefix;
        this.new_prefix = new_prefix;
        this.old_prefix = old_prefix;
        this.nexthop = nexthop;
        this.priority = priority;

    }

    /**
     * Gets the proper next-hop SipURL for the selected URL. It returns the
     * SipURL used to reach the selected URL.
     *
     * @param sip_url the selected destination URL
     * @return returns the proper next-hop SipURL for the selected URL if the
     * proxying rule matches the URL, otherwise it returns null.
     */
    public SipURL getNexthop(SipURL sip_url) {
        String username = sip_url.getUserName();
        if (username != null && username.startsWith(prefix)) {
            if (old_prefix.length() > 0 && new_prefix.length() > 0) {
                for (int i = 0; i < old_prefix.length(); i++) {
                    if (old_prefix.charAt(i) == username.charAt(0)) {
                        username = username.substring(1);
                        //System.out.println("user name1-->" + username);
                    }
                }
                username = new_prefix + username;
                //System.out.println("user name0-->" + username);
            } else if (old_prefix.length() == 0) {
                username = new_prefix + username;
                //System.out.println("user name2-->" + username);
            } else if (new_prefix.length() == 0) {
                for (int i = 0; i < old_prefix.length(); i++) {
                    username = username.substring(1);
                    //System.out.println("user name3-->" + username);
                }
            }
        }
        if ((username != null && username.startsWith(new_prefix)) || prefix.equalsIgnoreCase(DEFAULT_PREFIX)) {
            return new SipURL(username, nexthop.getAddress().toString(), nexthop.getPort());
        } else {
            return null;
        }
    }

    @Override
    public String getPrefix() {
        return prefix;
    }

    /**
     * Gets the String value.
     */
    public String toString() {
        return "{prefix=" + prefix + "," + "nexthop=" + nexthop + "}";
    }
}
