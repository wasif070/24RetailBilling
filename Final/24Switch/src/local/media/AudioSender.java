package local.media;

import ivr.SoundPlayerEngine;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;
import local.server.Proxy;
import org.apache.log4j.Logger;

/**
 * AudioSender is a pure-java audio stream sender. It uses the javax.sound
 * library (package).
 */
public class AudioSender {

    private Logger logger = Logger.getLogger(AudioSender.class.getName());
    private Integer codec_port = 0;
    private String ext = "";
    private String DIR = "";
    private int payload_type = 18; //0 ulaw
    private int frame_size = 20; //160 ulaw    

    public AudioSender(Integer codec) {
        System.out.println("Audio Sender codec-->" + codec);
        if (codec == 18) {
            this.codec_port = 18;
            this.ext = "g729";
            this.payload_type = codec;
            this.frame_size = 20;
        } else {
            this.codec_port = 0;
            this.ext = "ulaw";
            this.payload_type = codec;
            this.frame_size = 160;
        }
        this.DIR = System.getProperty("user.dir") + File.separator + ext + File.separator;
    }

    public void sendAudio(String[] args, Proxy proxy) {

        String daddr = args[0];
        int dport = Integer.parseInt(args[1]);
        //int socket = Integer.parseInt(args[2]);
        int number = Integer.parseInt(args[2]);

        //int payload_type = 18; //0 ulaw
        int sample_rate = 8000;
        int sample_size = 1;
        //int frame_size = 20; //160 ulaw
        int frame_rate = 0;
        int hour = number / 60;
        int min = number - (hour * 60);
        SoundPlayerEngine engine = new SoundPlayerEngine(this.codec_port);
        ArrayList<String> filenames = new ArrayList<String>();
//        System.out.println("File-->" + this.DIR + "start_g729.wav");
//        filenames.add(this.DIR + "start_g729.wav");
//        filenames.add(this.DIR + "start_g729.wav");
//        filenames.add(this.DIR + "start_g729.wav");
//        filenames.add(this.DIR + "start_g729.wav");

        filenames.add(this.DIR + "vm-youhave." + this.ext);
        filenames.addAll(engine.file_selector(hour));
        filenames.add(this.DIR + "hours." + this.ext);
        filenames.addAll(engine.file_selector(min));
        filenames.add(this.DIR + "minutes." + this.ext);

        frame_rate = sample_rate / (frame_size / sample_size);

        try {
            Vector<InputStream> inputStreams = new Vector<InputStream>();

            for (String filename : filenames) {
                File file = new File(filename);
                InputStream ins = new FileInputStream(file);
                inputStreams.add(ins);
            }

            Enumeration<InputStream> enu = inputStreams.elements();
            SequenceInputStream sis = new SequenceInputStream(enu);
            RtpStreamSender sender = new RtpStreamSender(sis, true, this.payload_type, frame_rate, this.frame_size, daddr, dport);
            sender.setListener(proxy);
            sender.run();
            sender.halt();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}