/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.activecalls;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.Queue;
import local.server.ServerEngine;
import org.apache.log4j.Logger;

/**
 *
 * @author reefat
 */
public class ActiveCall extends Thread {

    static Logger logger = Logger.getLogger(ActiveCall.class.getName());
    private static Queue<ActiveCallDTO> qActiveCall = null;
    private static Queue<String> qRemovalList = null;
    private static ActiveCall activeCall = null;

    public ActiveCall() {
        if (qActiveCall == null) {
            qActiveCall = new LinkedList<ActiveCallDTO>();
        }
        if (qRemovalList == null) {
            qRemovalList = new LinkedList<String>();
        }
    }

    public static ActiveCall getInstance() {
        if (activeCall == null) {
            activeCall = new ActiveCall();
        }
        return activeCall;
    }

    public Queue<ActiveCallDTO> getqActiveCall() {
        return qActiveCall;
    }

    public Queue<String> getqRemovalList() {
        return qRemovalList;
    }

    public static void setqRemovalList(Queue<String> qRemovalList) {
        ActiveCall.qRemovalList = qRemovalList;
    }

    @Override
    public void run() {
        while (true) {
            if (!qActiveCall.isEmpty()) {
                addActiveCallEntry(qActiveCall.poll());
            }

            if (!qRemovalList.isEmpty()) {
                removeActiveCallEntry(qRemovalList.poll());
            }

            if (qActiveCall.isEmpty() && qRemovalList.isEmpty()) {
                try {
                   // System.out.println("Thread in sleep for 2 secs");
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    logger.debug("Exception in Active Call Thread-->"+ex.getMessage());
                }
            }
        }
    }

    private void addActiveCallEntry(ActiveCallDTO param_liveUsers) {
        PreparedStatement ps = null;
        String sql = "";
        ResultSet rs = null;
        try {
            sql = "select accepted_time from active_calls where call_id = ?";
            ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
            ps.setString(1, param_liveUsers.getCall_id());
            rs = ps.executeQuery();

            String insert_or_update_str = "INSERT INTO active_calls SET call_id = ?, ";
            String condition = "";
            while (rs.next()) {
                insert_or_update_str = "UPDATE active_calls SET ";
                condition = "WHERE call_id = ?";
            }
            sql = insert_or_update_str
                    + "origin_caller = ?, "
                    + "origin_ip = ?, "
                    + "term_callee = ?, "
                    + "term_ip = ?, "
                    + "term_gw = ?, "
                    + "invite_time = ?, "
                    + "accepted_time = ?, "
                    + "ringing_time = ?, "
                    + "progressing_time = ?, "
                    + "duration = ?, "
                    + "status = ?, "
                    + "previous_term_callee = ?, "
                    + "previous_term_ip = ? "
                    + condition;
            int i = 1;
            ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
            if (condition.length() == 0) {
                ps.setString(i++, param_liveUsers.getCall_id());
            }
            ps.setString(i++, param_liveUsers.getOrigin_caller());
            ps.setString(i++, param_liveUsers.getOrigin_ip());
            ps.setString(i++, param_liveUsers.getTerm_callee());
            ps.setString(i++, param_liveUsers.getTerm_ip());
            ps.setString(i++, param_liveUsers.getTerm_gw());
            ps.setLong(i++, param_liveUsers.getInvite_time());
            ps.setLong(i++, param_liveUsers.getAccepted_time());
            ps.setLong(i++, param_liveUsers.getRinging_time());
            ps.setLong(i++, param_liveUsers.getProgressing_time());
            ps.setLong(i++, param_liveUsers.getDuration());
            ps.setString(i++, param_liveUsers.getStatus());
            ps.setString(i++, param_liveUsers.getPrevious_term_callee());
            ps.setString(i++, param_liveUsers.getPrevious_term_ip());
            if (condition.length() > 0) {
                ps.setString(i++, param_liveUsers.getCall_id());
            }
            ps.executeUpdate();
        } catch (Exception e) {
            logger.debug("Exception in active call entry-->" + e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                logger.debug("Exception in active call entry finally block-->" + ex.getMessage());
            }
        }
    }

    private void removeActiveCallEntry(String param_callId) {
        PreparedStatement ps = null;
        try {
            String sql = "delete from active_calls where call_id = ?";
            ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
            ps.setString(1, param_callId);

            ps.executeUpdate();
        } catch (Exception e) {
            logger.debug("Exception in remove active call entry-->" + e.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                logger.debug("Exception in remove active call entry finally block-->" + ex.getMessage());
            }
        }
    }
}
