/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipvision.activecalls;

/**
 *
 * @author reefat
 */
public class ActiveCallDTO {

    private String call_id;
    private String origin_caller;
    private String origin_ip;
    private String term_callee;
    private String term_ip;
    private String previous_term_callee;
    private String previous_term_ip;
    private String term_gw;
    private long invite_time;
    private long accepted_time;
    private long ringing_time;
    private long progressing_time;
    private long duration;
    private String status;

    public String getCall_id() {
        return call_id;
    }

    public void setCall_id(String call_id) {
        this.call_id = call_id;
    }

    public String getOrigin_caller() {
        return origin_caller;
    }

    public void setOrigin_caller(String origin_caller) {
        this.origin_caller = origin_caller;
    }

    public String getOrigin_ip() {
        return origin_ip;
    }

    public void setOrigin_ip(String origin_ip) {
        this.origin_ip = origin_ip;
    }

    public String getTerm_callee() {
        return term_callee;
    }

    public void setTerm_callee(String term_callee) {
        this.term_callee = term_callee;
    }

    public String getTerm_ip() {
        return term_ip;
    }

    public void setTerm_ip(String term_ip) {
        this.term_ip = term_ip;
    }

    public String getTerm_gw() {
        return term_gw;
    }

    public void setTerm_gw(String term_gw) {
        this.term_gw = term_gw;
    }

    public long getInvite_time() {
        return invite_time;
    }

    public void setInvite_time(long invite_time) {
        this.invite_time = invite_time;
    }

    public long getAccepted_time() {
        return accepted_time;
    }

    public void setAccepted_time(long accepted_time) {
        this.accepted_time = accepted_time;
    }

    public long getRinging_time() {
        return ringing_time;
    }

    public void setRinging_time(long ringing_time) {
        this.ringing_time = ringing_time;
    }

    public long getProgressing_time() {
        return progressing_time;
    }

    public void setProgressing_time(long progressing_time) {
        this.progressing_time = progressing_time;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrevious_term_callee() {
        return previous_term_callee;
    }

    public void setPrevious_term_callee(String previous_term_callee) {
        this.previous_term_callee = previous_term_callee;
    }

    public String getPrevious_term_ip() {
        return previous_term_ip;
    }

    public void setPrevious_term_ip(String previous_term_ip) {
        this.previous_term_ip = previous_term_ip;
    }
    
}
